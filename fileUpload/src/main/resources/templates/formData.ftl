<html>
<head>
    <title>Welcome!</title>
    <script src="/js/jquery-3.1.1.js"></script>
    <script src="/bootstrap/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/bootstrap/bootstrap-fileinput/fileinput.css">
    <script src="/bootstrap/bootstrap-fileinput/fileinput.js"></script>
</head>
<body>

<input id="file-upload" type="file" name="file" multiple>

<input type="button" value="提交" onclick="save()">

<script type="text/javascript">
    function save() {
        let formData = new FormData();
        formData.append("name", "李白");
        console.log(formData);
        $.ajax({
            url: "/formData/upload",
            data: formData,
            type: "post",
            processData: false,
            contentType: false,
            success: function (result) {
                console.log(result);
            }
        })
    }

    $(document).ready(function () {

        $("#file-upload").fileinput({
            'theme': 'explorer-fas',
            'uploadUrl': '/formData/upload',
            showUpload: false,          //是否显示上传按钮
            showCaption: true,          //是否显示标题
            uploadAsync:true,           //异步上传
            dropZoneEnabled:false,      //是否显示拖拽区域
            showCaption: true,          //是否显示标题
            maxFileCount: 10, //表示允许同时上传的最大文件个数
            showRemove:false, //是否显示移除按钮
            showPreview: false          //文件预览
        }).on('filebatchselected', function (event, data) {
            console.log("选择文件:");
            console.log(event);
            console.log(data);
            $("#file-upload").fileinput("upload");
        }).on("fileuploaded", function (event, data) {
            console.log("文件上传成功!");
        });

        //异步上传返回结果处理
        $('#file-upload').on('fileerror', function (event, data, msg) {
            console.log("fileerror");
            console.log(data);
        });
        //异步上传返回结果处理
        $("#file-upload").on("fileuploaded", function (event, data, previewId, index) {
            console.log("异步上传返回结果处理");
        });

        //上传前
        $('#file-upload').on('filepreupload', function (event, data, previewId, index) {
            console.log("filepreupload");
        });

    });


</script>

</body>
</html>

drop table  if exists user;
CREATE TABLE user (
  id int(11),
  name varchar(45),
  department_id int(11),
  create_time varchar(50),
  PRIMARY KEY (id)
);

drop table if exists department;
CREATE TABLE department (
  id int(11) ,
  name varchar(45),
  PRIMARY KEY (id)
);
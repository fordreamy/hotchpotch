package com.demo.controller.mvc;

import com.alibaba.fastjson.JSON;
import com.demo.config.Page;
import com.demo.entity.User;
import com.demo.util.ToolUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class CrossDomainController {
    protected final Log log = LogFactory.getLog(this.getClass());

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/getBaiDu")
    @ResponseBody
    public String home(HttpServletRequest request) {

        ToolUtil.getParameterMap(request);

        ResponseEntity<String> responseEntity = restTemplate.getForEntity("http://www.baidu.com", String.class);
        log.info("获取百度返回页面内容:");
        String callbackName = "bd";
        String s = responseEntity.getBody();
        Map map = new HashMap<String,String>();
        map.put("k1",responseEntity.getBody());

        String body = JSON.toJSONString(map);
        s = callbackName + "(" + body + ")";
        log.info("jsonp-->" + s);
        return s;
    }

}

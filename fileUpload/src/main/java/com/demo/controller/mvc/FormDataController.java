package com.demo.controller.mvc;

import com.demo.entity.User;
import com.demo.util.RequestUtil;
import com.demo.util.ResultVo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Random;

@Controller
@RequestMapping("/formData")
public class FormDataController {

    @Value("${upload.imgDir}")
    private String uploadImgDir;

    Log log = LogFactory.getLog(getClass());

    @GetMapping("/toFormDataPage")
    public String index() {
        log.info("跳转页面至formData.ftl");
        return "formData";
    }

    /**
     * 表单数据和文件一起保存
     */
    @PostMapping("uploadForm")
    @ResponseBody
    public String uploadForm(String name, MultipartFile files, HttpServletRequest request) {

        log.info("图片保存路径:" + uploadImgDir);

        return "success";
    }

    @PostMapping("upload")
    @ResponseBody
    public ResultVo<String> handFormUpload(MultipartFile file, HttpServletRequest request) {

        log.info("上传文件路径:" + uploadImgDir);
        if (file != null) {
            log.info("文件:" + file.getOriginalFilename());
        }
        ResultVo<String> resultVo = new ResultVo<>();

        resultVo.setData("success");
        return resultVo;
    }
}

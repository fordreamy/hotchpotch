package com.demo.controller.mvc;

import com.demo.config.ApplicationListenerConf;
import com.demo.config.Page;
import com.demo.entity.User;
import com.demo.service.MyService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    Page page;

    @Autowired
    MyService myService;

    @GetMapping("/")
    public String index() {
        myService.getName();
        logger.info("Greeting");
        logger.info("pageSize:" + page.getPageSize());
        return "index";
    }

    @GetMapping("/login")
    public String home(User user, HttpServletRequest request) {
        if ("jack".equals(user.getName())) {
            logger.info("登陆成功!");
            request.getSession().setAttribute("user", user);
            return "home";
        }
        return "login";
    }

}

package com.demo.controller.mvc;

import com.demo.entity.User;
import com.demo.util.RequestUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

@Controller
public class FileUploadController {

    @Value("${upload.imgDir}")
    private String uploadImgDir;

    Log log = LogFactory.getLog(getClass());

    @PostMapping("uploadController")
    @ResponseBody
    public String handFormUpload(MultipartFile file, MultipartFile file2, HttpServletRequest request) {
        if (!file.isEmpty()) {
            log.info("文件名:" + file.getOriginalFilename());
            log.info("文件大小:" + file.getSize());
        }

        log.info("图片保存路径:" + uploadImgDir);

        // 上传文件路径
        String filePath = uploadImgDir + file.getOriginalFilename();
        if (file.getContentType().contains("octet-stream")) {
            log.error("空文件");
            //没有上传图片
        } else {
            log.error("有文件");
        }
        String filePath2 = uploadImgDir + file2.getOriginalFilename();
        System.out.println(filePath);
        System.out.println(file2.getOriginalFilename());

        System.out.println(request.getParameter("name"));

        User user = new User();
        RequestUtil.getParameters(request, user);
        System.out.println(user.toString());

        if (!file.isEmpty()) {
            try {
                // 文件保存路径
                // 转存文件
                file.transferTo(new File(filePath));
                file2.transferTo(new File(filePath2));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return "success";
    }
}

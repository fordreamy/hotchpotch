package com.demo.config;

import lombok.Data;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "page")
@Data
public class Page {

    Log log = LogFactory.getLog(Page.class);

    private int pageNum;
    private int pageSize;

    public Page() {
        log.info("初始化配置类");
    }
}

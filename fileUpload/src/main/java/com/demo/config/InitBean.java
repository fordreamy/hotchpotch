package com.demo.config;

import com.demo.service.MyService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InitBean {

    Log log = LogFactory.getLog(getClass());

    @Bean
    public MyService getMyServiceYa() {
        System.out.println("getMyService---------------------");
        return new MyService();
    }

    public void sayHello() {
        log.info("自定义bean的方法");
    }

}

package com.demo.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.File;

@Component
public class AutoExec implements CommandLineRunner {

    Log log = LogFactory.getLog(getClass());

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private ApplicationListenerConf applicationListenerConf;

    @Value("${upload.imgDir}")
    private String imgDir;

    @Override
    public void run(String... args) {
        log.info("系统启动后执行开始************************************************");
        log.info("获取IP和端口:" + applicationListenerConf.getUrl());
        log.info("创建上传目录:" + imgDir);
        for (String arg : args) {
            log.info("arg:" + arg);
        }

        File imgDirFile = new File(imgDir);
        if (!imgDirFile.exists()) {
            boolean mkdirsBoolean = imgDirFile.mkdirs();
            if (mkdirsBoolean) {
                log.info("创建图片存储目录:" + imgDir);
            } else {
                log.info("图片存储目录创建失败:" + imgDir);
            }
        } else {
            log.info("图片存储目录已存在:" + imgDir);
        }

        ResponseEntity<String> responseEntity = restTemplate.getForEntity("http://www.baidu.com", String.class);
        log.info("获取百度返回页面内容:");
        System.out.println(responseEntity.getBody());

        log.info("系统启动后执行结束************************************************");
    }
}

package com.demo.config;

import com.demo.service.MyService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
//@ConditionalOnBean(name = "dataSource")
@ConditionalOnBean(MyService.class)
public class ConditionalBean {

    Log log = LogFactory.getLog(getClass());

    @Bean
    public void cb(){
        log.info("初始化DataSource后才初始化此Bean");
    }

}

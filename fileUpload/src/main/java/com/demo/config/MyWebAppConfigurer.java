package com.demo.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 资源映射路径
 */
@Configuration
public class MyWebAppConfigurer implements WebMvcConfigurer {

    Log log = LogFactory.getLog(getClass());

    @Value("${upload.imgDir}")
    private String imgDir;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.info("获取图片目录:" + imgDir);
        registry.addResourceHandler("/image/**").addResourceLocations("file:" + imgDir);
    }
}


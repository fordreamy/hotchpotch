package com.demo.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class EnvConfig {

    Log log = LogFactory.getLog(getClass());

    @Autowired
    private Environment env;

    @Bean
    public void getServerPort() {
        log.info("获取系统属性和自定义属性");
        log.info("user.dir:" + env.getProperty("user.dir"));
        log.info("user.dir:" + env.getProperty("JAVA_HOME"));
        log.info("user.dir:" + env.getProperty("server.port"));
        log.info("user.dir:" + env.getProperty("upload.imgDir"));

    }


}

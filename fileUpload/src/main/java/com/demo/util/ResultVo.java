package com.demo.util;

import lombok.Data;

@Data
public class ResultVo<T> {
    String message;
    T data;
}

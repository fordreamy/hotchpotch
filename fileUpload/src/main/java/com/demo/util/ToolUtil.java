package com.demo.util;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

public class ToolUtil {
    public static Map<String, String> getParameterMap(HttpServletRequest request) {
        Map<String, String[]> map = request.getParameterMap();
        Set<String> keySet = map.keySet();
        Iterator<String> iterator = keySet.iterator();
        Map<String, String> paraMap = new HashMap<>();
        //保存参数到Map
        while (iterator.hasNext()) {
            String keyTemp = iterator.next();
            if (map.get(keyTemp).length == 1) {
                paraMap.put(keyTemp, map.get(keyTemp)[0]);
            }
            System.out.println("key->" + keyTemp);
            System.out.println("value->" + paraMap.get(keyTemp));
        }
        return paraMap;
    }

    public static Map<String, String> getHeaderNames(HttpServletRequest request) {
        Enumeration enumeration = request.getHeaderNames();

        if (enumeration.hasMoreElements()) {
            Object object = enumeration.nextElement();
            System.out.println(object);
            System.out.println(object.getClass());
            System.out.println(object.toString());
        }
        Map<String, String> paraMap = new HashMap<>();
        return paraMap;
    }

    public static void main(String[] args) {
        System.out.println("");
    }
}

package com.demo;

import java.io.*;

public class CopyFile {
    public static void main(String[] args) throws IOException {

        int readCount = 1;
        String src = System.getProperty("user.dir") + "/netURL/src/main/resources/files/s1.txt";
        String dec = System.getProperty("user.dir") + "/netURL/target/s1.txt";
        InputStream ins = new FileInputStream(new File(src));
        byte[] b = new byte[1024];
        int len = ins.read(b);
        OutputStream os = new FileOutputStream(dec);
        while (len != -1) {
            System.out.println("第" + readCount++ + "次读取len:" + len);
            os.write(b, 0, len);
            len = ins.read(b);
        }
        ins.close();
        os.close();

    }
}

package com.demo.socket;

import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {

        int readCount = 1;
        String sendFilePath = System.getProperty("user.dir") + "/netURL/src/main/resources/files/s1.txt";
        Socket socket = new Socket("127.0.0.1", 9000);
        OutputStream os = socket.getOutputStream();
        File file = new File(sendFilePath);
        String fileName = file.getName();
        int len;

        //发送文件名,读取服务器返回信息，确认收到文件名
        /*os.write(fileName.getBytes());
        InputStream in = socket.getInputStream();
        byte[] nameByte = new byte[50];
        int len = in.read(nameByte);
        String returnName = new String(nameByte, 0, len);
        if (fileName.equals(returnName)) {
            System.out.println("文件名没有收到，结束");
            socket.close();
        }*/

        System.out.println("开始发送文件:" + file.getAbsolutePath());
        FileInputStream fis = new FileInputStream(file);
        byte b[] = new byte[1024];
        while ((len = fis.read(b)) != -1) {
            os.write(b, 0, len);
            System.out.println("第" + readCount++ + "次读取len:" + len);
        }
        System.out.println(len);
        System.out.println("发送完成");
        //关闭流
        os.close();
        fis.close();
        socket.close();

    }
}

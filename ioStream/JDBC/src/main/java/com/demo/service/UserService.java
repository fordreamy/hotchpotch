package com.demo.service;

import com.demo.entity.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    public User findUserById(Long id);

    public Map findUserById2(Long id);

    public List<User> getUserByDepartmentId(Long departmenetId);

}

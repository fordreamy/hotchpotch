package com.demo.service.impl;

import com.demo.dao.UserDao;
import com.demo.entity.User;
import com.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserDao userDao;
	
	public User findUserById(Long id) {
		User user = userDao.findUserById(id);
		return user;
	}

	public Map findUserById2(Long id) {
		Map user = userDao.findUserById2(id);
		return user;
	}

	public List<User> getUserByDepartmentId(Long departmenetId) {
		return userDao.getUserByDepartmentId(departmenetId);
	}

}

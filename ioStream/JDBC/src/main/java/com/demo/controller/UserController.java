package com.demo.controller;

import com.demo.entity.User;
import com.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;


@Controller
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping("/user")
    @ResponseBody
    public User say(HttpServletRequest request) {
        long id =  Long.valueOf(request.getParameter("id").toString());
        User user = userService.findUserById(id);
        return user;
    }
    @RequestMapping("/user2/{id}")
    @ResponseBody
    public Map findUserById2(@PathVariable Long id){
        Map user = userService.findUserById2(id);
        return user;
    }
    @RequestMapping("/dept/{id}")
    @ResponseBody
    public List<User> getUserByDepartmentId(@PathVariable Long id){
        List<User> userList = userService.getUserByDepartmentId(id);
        return userList;
    }
}

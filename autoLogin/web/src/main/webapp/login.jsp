<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>登录</title>
</head>
<body>
<form action="login" method="post">
    用户名：<input name="username" type="text" value="admin"><br>
    密码：<input name="password" type="password" value="123456">

    <c:if test="${cookie['autoLogin'].value eq 'autoLogin'}">
        <input type="checkbox" checked="checked" name="autoLogin" value="autoLogin">自动登录
    </c:if>

    <c:if test="${cookie['autoLogin'].value != 'autoLogin'}">
        <input type="checkbox" name="autoLogin" value="autoLogin">自动登录
    </c:if>

    <input type="submit" value="提交">
</form>
</body>
</html>
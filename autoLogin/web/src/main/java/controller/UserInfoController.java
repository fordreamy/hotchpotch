package controller;

import com.scorpio.utils.StringUtils;
import dao.D1;

import java.io.*;
import java.util.Date;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(urlPatterns = "/userInfo")
public class UserInfoController extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException
    {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        StringBuffer sb = new StringBuffer();
        sb.append("<html><head><title>Hello World!</title></head><body>");
        D1 d1 = new D1();
        d1.show();
        sb.append(new Date());
        sb.append("<h1>Hello "+request.getSession().getAttribute("username")+"</h1></body>");
        out.println(sb.toString());
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }

}
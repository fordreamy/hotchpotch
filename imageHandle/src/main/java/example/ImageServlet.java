package example;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ImageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public ImageHandleUitl thumbnailService;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String srcImagePath = request.getServletContext().getRealPath("") + "\\images\\1.png";
		String destImagePath = request.getServletContext().getRealPath("") + "\\imagesOut\\1cut.png";

		boolean result = ImageHandleUitl.defaultSize(srcImagePath, destImagePath);
		System.out.println(result ? "ok" : "error");
		System.out.println("生成缩略图路径:"+destImagePath);
		response.sendRedirect("index.html");
		return;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}

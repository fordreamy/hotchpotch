package example;

import net.coobird.thumbnailator.geometry.Positions;
import org.junit.Test;

public class TestDemo {

    String root = "E:\\commonProgram\\hotchpotch\\imageHandle";

    @Test()
    public void defaultSize() {
        String srcImagePath = root + "\\WebContent\\images\\1.png";
        String destImagePath = root + "\\WebContent\\imagesOut\\result.png";
        boolean result = false;
        result = ImageHandleUitl.defaultSize(srcImagePath, destImagePath);
    }

    @Test()
    public void customSize() {
        String srcImagePath = root + "\\WebContent\\images\\1.png";
        String destImagePath = root + "\\WebContent\\imagesOut\\customSize.png";
        boolean result = false;
        result = ImageHandleUitl.customSize(srcImagePath, destImagePath, 1920, 1080,true);
    }

    public static void main(String[] args) {
        String root = "E:\\commonProgram\\hotchpotch\\imageHandle";
        String srcImagePath = root + "\\WebContent\\images\\1.png";
        String destImagePath = root + "\\WebContent\\imagesOut\\result.png";
        String watermarkImagePath = root + "\\WebContent\\images\\watermark.jpg";
        String srctImageDir = root + "\\WebContent\\images";
        String destImageDir = root + "\\WebContent\\imagesOut";

        boolean result = false;
		/*result = ImageHandleUitl.customSizeBatch(srctImageDir, destImageDir, 200, 200, "jpg","", false);
		result = ImageHandleUitl.customSizeBatch(srctImageDir, destImageDir, 200, 200, "jpg","bach-", false);
		result = ImageHandleUitl.scale(srcImagePath, destImagePath, 2d);
		result = ImageHandleUitl.rotate(srcImagePath, destImagePath, 90);
		result = ImageHandleUitl.watermark(srcImagePath, destImagePath, watermarkImagePath, Positions.CENTER,1.0f);*/

    }

}

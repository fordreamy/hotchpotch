package example;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import net.coobird.thumbnailator.ThumbnailParameter;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import net.coobird.thumbnailator.name.Rename;

public class ImageHandleUitl {

	public static final int WIDTH = 200;
	public static final int HEIGHT = 200;

	/**
	 * 按照默认宽高(200,200)缩放图片
	 * 
	 * @param srcImagePath
	 *            原图片路径
	 * @param destImagePath
	 *            修改后图片路径
	 * @return true表示成功,false表示失败
	 */
	public static boolean defaultSize(String srcImagePath, String destImagePath) {

		try {
			Thumbnails.of(srcImagePath).size(WIDTH, HEIGHT).toFile(destImagePath);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 自定义宽高缩放图片
	 * 
	 * @param srcImagePath
	 *            原图片路径
	 * @param destImagePath
	 *            修改后图片路径
	 * @param width
	 *            图片宽度
	 * @param height
	 *            图片高度
	 * @param keepAspectRatio
	 *            保持图片纵横比,true则是,false则否
	 * @return true表示成功,false表示失败
	 */
	public static boolean customSize(String srcImagePath, String destImagePath, int width, int height,
			boolean keepAspectRatio) {

		try {
			Thumbnails.of(srcImagePath).size(width, height).keepAspectRatio(keepAspectRatio).toFile(destImagePath);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 自定义宽高批量缩放图片
	 * 
	 * @param srcImageDir
	 *            原图片目录
	 * @param destImageDir
	 *            修改后图片目录
	 * @param width
	 *            图片宽度
	 * @param height
	 *            图片高度
	 * @param format
	 *            图片格式,"JPEG","jpg","png"
	 * @param prefix
	 *            生成图片的文件名前缀,为空“”则无前缀名
	 * @param keepAspectRatio
	 *            保持图片纵横比,true则是,false则否
	 * @return true表示成功,false表示失败
	 */
	public static boolean customSizeBatch(String srcImageDir, String destImageDir, int width, int height, String format,final String prefix,
			boolean keepAspectRatio) {

		try {

			File destinationDir = new File(destImageDir);

			Rename rename = new Rename() {
				@Override
				public String apply(String fileName, ThumbnailParameter param) {
					return appendPrefix(fileName, prefix);
				}
			};

			Thumbnails.of(new File(srcImageDir).listFiles()).size(width, height).outputFormat(format)
					.keepAspectRatio(keepAspectRatio).toFiles(destinationDir, rename);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 按比例缩小和放大图片
	 * 
	 * @param srcImagePath
	 *            原图片路径
	 * @param destImagePath
	 *            修改后图片路径
	 * @param scaleValue
	 *            缩放比例,值必须大于0.0,小于1是缩小,大于1是放大
	 * @return true表示成功,false表示失败
	 */
	public static boolean scale(String srcImagePath, String destImagePath, double scaleValue) {

		try {
			Thumbnails.of(srcImagePath).scale(scaleValue).toFile(destImagePath);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 旋转图片
	 * 
	 * @param srcImagePath
	 *            原图片路径
	 * @param destImagePath
	 *            修改后图片路径
	 * @param rotateDegree
	 *            旋转度数,顺时针
	 * @return true表示成功,false表示失败
	 */
	public static boolean rotate(String srcImagePath, String destImagePath, double rotateDegree) {

		try {
			Thumbnails.of(srcImagePath).scale(1.0).rotate(rotateDegree).toFile(destImagePath);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 给图片添加水印
	 * 
	 * @param srcImagePath
	 *            原图片路径
	 * @param destImagePath
	 *            修改后图片路径
	 * @param watermarkImagePath
	 *            要添加的水印图片路径
	 * @param positions
	 *            水印位置,值为Positions.CENTER等
	 * @param opacity
	 *            水印透明度,值在0.0f和1.0f之间
	 * @return
	 */
	public static boolean watermark(String srcImagePath, String destImagePath, String watermarkImagePath,
			Positions positions, float opacity) {

		try {
			BufferedImage watermarkImage = ImageIO.read(new File(watermarkImagePath));
			Thumbnails.of(srcImagePath).scale(1.0).watermark(positions, watermarkImage, opacity)
					.toFile(destImagePath);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class CollectionTest {

	public static void main(String[] args) {

		List<Object> list = new ArrayList<>();
		People people = new People();
		list.add(people);
		list.add(people);
		People people2 = new People();
		System.out.println(list.size());
		people2 = people;
		
		if(list.contains(people2)){
			System.out.println("yes");
		}else {
			System.out.println("no");
		}
		list.add("hello");
		
		if(list.contains(new String("hello")))System.out.println("yyy");
		System.out.println(people.equals(people2));
		System.out.println("---------------------------------------");
		//HashSet
		HashSet<String> set = new HashSet<String>();
		set.add("zzz");
		set.add("ppp");
		set.add("aaa");
		Iterator<String> iterator = set.iterator();
		while(true){
			if(iterator.hasNext()){
				System.out.println(iterator.next());
			}else {
				break;
			}
		}
		//HashMap
		HashMap<Object, Object> map = new HashMap<>();
		map.put("one",1);
		map.put(12, 123);
		map.put(12, 12);
		System.out.println(map.get(12));
		
	}

}

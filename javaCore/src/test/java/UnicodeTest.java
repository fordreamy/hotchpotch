import java.io.UnsupportedEncodingException;


public class UnicodeTest {

	public static void main(String[] args) throws UnsupportedEncodingException {
		System.out.println("\u4e0f");
		System.out.println("\u0041");
		String s1 = "號";
		System.out.println(s1);
		
		char[] strChar=s1.toCharArray();
		for (char c : strChar) {
			System.out.println(c);
			System.out.println(s1+"二进制:"+Integer.toBinaryString(c));
		}
		
		byte[] bt = s1.getBytes();
		for (byte c : bt) {
			System.out.println(c);
			System.out.println(Integer.toBinaryString(c));
		}
		System.out.println("--------------------------------------");
		
		byte[] utf8 = s1.getBytes("UTF-8");
		for (byte c : utf8) {
			System.out.println(c);
		}
		System.out.println("--------------------------------------");
		byte[] iso = s1.getBytes("ISO-8859-1");
		for (byte c : iso) {
			System.out.println(c);
			System.out.println(Integer.toBinaryString(c));
		}
		System.out.println("--------------------------------------");
		byte[] utf16be = s1.getBytes("UTF-16BE");
		for (byte c : utf16be) {
			System.out.println(c);
		}
		System.out.println("--------------------------------------");
		byte[] utf16 = s1.getBytes("UTF-16");
		for (byte c : utf16) {
			System.out.println(c);
			System.out.println(Integer.toBinaryString(c));
		}
		System.out.println("--------------------------------------");

	}

}

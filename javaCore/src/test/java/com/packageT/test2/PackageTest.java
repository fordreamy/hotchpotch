package com.packageT.test2;

import com.packageT.test1.P1;

public class PackageTest {

	public static void main(String[] args) {
		
		//不同包，不能访问修饰符为protected和默认
		P1 p1 = new P1();
		
		//不同包，继承父类后的子类创建的对象不能访问修饰符为protected和默认
		//但是子类中可以调用父类的修饰符为protected的方法和属性
		P1Sub p1Sub = new P1Sub();
		
		System.out.println("--------------------------------------------");
		
		//相同包，可以访问修饰符为protected和默认值的方法和变量
		P3 p3 = new P3();
		p3.defaultdOut();
		p3.protectedOut();

	}
}

package com.packageT.test1;

public class P1 {

	protected String protectedName = "修饰符是protected";
	String defaultName = "修饰符是default";
	
	protected void protectedOut() {
		System.out.println("我是protected方法");
	}
	
	void defaultdOut() {
		System.out.println("我是default方法");
	}
	
}

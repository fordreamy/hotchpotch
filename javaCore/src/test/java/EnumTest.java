import java.util.ArrayList;
import java.util.LinkedList;

public enum EnumTest {
	FRANK("The given name of me"), LIU("The family name of me");
	private String context;

	private String getContext() {
		return this.context;
	}

	 EnumTest(String context) {
		this.context = context;
	}

	public static void main(String[] args) {
		for (EnumTest name : EnumTest.values()) {
			System.out.println(name + " : " + name.getContext());
			System.out.println(name.getClass());
		}
		System.out.println(EnumTest.FRANK.getDeclaringClass());
		System.out.println(EnumTest.LIU.getContext());
		LinkedList<String> list = new LinkedList<>();
		list.add("h");
		String s = "s";
		String s1 = s;
		String fString = s1;
		System.out.println(fString);
		s1 = "s2";
		System.out.println(fString);

		ArrayList<String> list2 = new ArrayList<>();
		list2.add("hi");
		final ArrayList<String> list22 = list2;
		
		System.out.println(list22.size());
		list2.add("hi");
		System.out.println(list22.size());
		
		ArrayList<String> list3 = list22;
		System.out.println(list3.size());
		list3.add("hi");
		System.out.println(list2.size());

	}
}
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.StyleContext.SmallAttributeSet;

public class Test2 {
	private static int a = 0;

	public class t2 {
		class t3 {
			class t4 {
				public t4() {
					System.out.println("youuuuuuu");
				}
			}
		}
	};

	public static void main(String[] args) {

		/*
		 * double rate = 0.234125; rate = (int)(rate*100000+0.5)/100000.0;
		 * 
		 * System.out.println(rate);
		 */

		// 2015-02-26 19:32:00
		String s1 = "2014-12-16  14:00: ";

		System.err.println(s1.matches("\\d{4}(-\\d{2}){2}\\s+(\\d{2}:)+[0-9]*"));

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("1", 1);

		System.err.println(paramMap.get("1").getClass());
		paramMap.put("1", "1");

		System.err.println(paramMap.get("1").getClass());

		ArrayList<String> list2 = new ArrayList<>();
		ArrayList<String> list3 = new ArrayList<>();
		list2.add("12");
		System.err.println(list2.hashCode());
		System.err.println(list3.hashCode());
		System.err.println("__________________________________");
		People p1 = new People();
		People p2 = new People();
		People p3 = p2;
		System.err.println(p1.hashCode());
		System.err.println(p2.hashCode());
		System.err.println(p2.toString());
		System.err.println(p3.hashCode());
		System.err.println("__________________________________");
		String str1 = "hello";
		String str2 = new String("hello");
		System.err.println(str1.hashCode());
		System.err.println(str2.hashCode());

		StringBuilder sBuilder = new StringBuilder("hello");
		System.err.println(sBuilder.hashCode() + "" + p1);
		ArrayList<String> list4 = new ArrayList<>(100);
		System.out.println(list4.size());
		System.out.println(new Test2().new t2().new t3().new t4());
		
		 T t = new T() ; 
		 t.d();
		 

	}
}

class T {
	 enum Size {
		 //SMALL("s"), LARGE("l");
		 SMALL(1), LARGE(2);
		Size(int s) {
			
		};
		String string = "hello";
		void out(){
			System.out.println(string);
		}
	};

	void d() {
		System.out.println("-----------------");
		System.out.println(Size.LARGE.name());
		System.out.println(Size.LARGE.toString());
		System.out.println(Size.LARGE.getDeclaringClass());
		Size.LARGE.out();
		Size tSize = Size.values()[0];
		tSize.out();
	}
}

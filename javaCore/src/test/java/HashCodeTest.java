import java.util.*;

import javax.swing.JSpinner.ListEditor;


public class HashCodeTest {

	public static void main(String[] args) {
		String s1 = "abc";
		String s2 = new String("abc");
		String s3 = "abc";
		System.out.println(s1.hashCode() == s2.hashCode());
		System.out.println(s1 == s2); 
		
		System.out.println(s1.hashCode() == s3.hashCode());
		System.out.println(s1 == s3); 
		
		HashSet<String> set = new HashSet<>();
		
		set.add("a");
		set.add("a");
		set.add("b");
		set.add(new String("a"));
		
		for (String string : set) {
			
			System.out.println(string);
		}
		
		HashSet<People> hPeoples = new HashSet<People>();
		hPeoples.add(new People());
		hPeoples.add(new People());
		hPeoples.add(new People());
		
		for (People people : hPeoples) {
			System.out.println(people.getName());
			System.out.println(people.hashCode());
		}
		

		ArrayList list = new ArrayList<>();
		list.add(new String("H2222222222222222222222"));
		System.out.println(list.hashCode());
		ArrayList list2 = new ArrayList<>();
		list2.add(new String("H55555555555555555555555555555uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"));
		System.out.println(list2.hashCode());
		System.out.println(list.equals(list2));
		
		System.out.println(list == list2);
		
		List l = list2;
		System.out.println(l.hashCode() +"-"+list2.hashCode());
		
		Collection<Object> collection = new ArrayList<Object>();
		collection.add("hello");
		collection.add(123);
		collection.add(123.3);
		collection.add(new People());
		for(Object s:collection){
			System.out.println(s.toString());
			System.out.println(s.getClass());
		}
		Collection<List> c = new ArrayList<List>();
		c.add(new ArrayList());
		
		Iterator iterator =  c.iterator();
		if(iterator.hasNext()){
			List list3 = (List) iterator.next();
			
			list3.add("hi");
		}
		System.out.println(c.toString());
	}

}

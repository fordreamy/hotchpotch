package p1;

public class F {
    String name="F";

    public F(){
        System.out.println("F构造函数");
    }
    static {
        System.out.println("F静态代码块");

    }

    public static F f = new F();
    static {
        System.out.println("F静态代码块2");

    }
    {
        System.out.println("F代码块");
    }

    public String getName() {
        System.out.println("F");
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
    }
}

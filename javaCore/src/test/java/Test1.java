import AllClass.innerClass.InnerClass;

import java.util.Date;

public class Test1
{

    public static void main(String[] args) {

        Fruit f = new Apple();
        Apple a = (Apple) f;
        System.out.println(a.getName());
        String s = "Abc";
//        System.out.println(s.replaceFirst());

        char[] chars = s.toCharArray();
        chars[0] += 32;
        System.out.println(String.valueOf(chars));
    }
}
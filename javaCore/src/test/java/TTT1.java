import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

public class TTT1 {
    @Test
    public void decomposeDoubleStrand() {
        assertEquals(
                "Frame 1: AGG TGA CAC CGC AAG CCT TAT ATT AGC\n" +
                        "Frame 2: A GGT GAC ACC GCA AGC CTT ATA TTA GC\n" +
                        "Frame 3: AG GTG ACA CCG CAA GCC TTA TAT TAG C\n\n" +
                        "Reverse Frame 1: GCT AAT ATA AGG CTT GCG GTG TCA CCT\n" +
                        "Reverse Frame 2: G CTA ATA TAA GGC TTG CGG TGT CAC CT\n" +
                        "Reverse Frame 3: GC TAA TAT AAG GCT TGC GGT GTC ACC T",
                DNADecomposer.decomposeDoubleStrand("AGGTGACACCGCAAGCCTTATATTAGC"));
    }
}

class DNADecomposer {
    public static String decomposeDoubleStrand(final String doubleStrand) {
        String[] s = new String[7];
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < doubleStrand.length(); i++) {
            sb.append(doubleStrand.charAt(i));
            if ((i + 1) % 3 == 0 && i != 0) {
                sb.append(" ");
            }
        }
        s[1] = sb.toString().trim();
        System.out.println(s[1]);

        sb = new StringBuffer();
        String temp = s[1].replaceAll(" ", "");
        for (int i = 0; i < temp.length(); i++) {
            sb.append(doubleStrand.charAt(i));
            if (i % 3 == 0) {
                sb.append(" ");
            }
        }
        System.out.println(sb.toString());
        s[2] = sb.toString();

        sb = new StringBuffer();
        temp = s[1].replaceAll(" ", "");
        for (int i = 0; i < temp.length(); i++) {
            sb.append(doubleStrand.charAt(i));
            if ((i - 1) % 3 == 0) {
                sb.append(" ");
            }
        }
        System.out.println(sb.toString());
        s[3] = sb.toString();
        sb = new StringBuffer();

        sb = new StringBuffer();
        temp = s[1];
        for (int i = temp.length() - 1; i >= 0; i--) {
            char c = temp.charAt(i);
            if (c == 'C') c = 'G';
            else if (c == 'G') c = 'C';
            else if (c == 'A') c = 'T';
            else if (c == 'T') c = 'A';
            sb.append(c);

        }
        System.out.println(sb.toString());
        s[4] = sb.toString().trim();

        sb = new StringBuffer();
        temp = s[3];
        for (int i = temp.length() - 1; i >= 0; i--) {
            char c = temp.charAt(i);
            if (c == 'C') c = 'G';
            else if (c == 'G') c = 'C';
            else if (c == 'A') c = 'T';
            else if (c == 'T') c = 'A';
            sb.append(c);

        }
        System.out.println(sb.toString());
        s[5] = sb.toString();

        sb = new StringBuffer();
        temp = s[2];
        for (int i = temp.length() - 1; i >= 0; i--) {
            char c = temp.charAt(i);
            if (c == 'C') c = 'G';
            else if (c == 'G') c = 'C';
            else if (c == 'A') c = 'T';
            else if (c == 'T') c = 'A';
            sb.append(c);

        }
        System.out.println(sb.toString());
        s[6] = sb.toString();
        System.out.println("--");
        String r = "Frame 1: "+s[1]+"\n"+"Frame 2: "+s[2]+"\n"+"Frame 3: "+s[3]+"\n\n"+"Reverse Frame 1: "+s[4]+"\n"+"Reverse Frame 2: "+s[5]+"\n"+"Reverse Frame 3: "+s[6];
        System.out.println(r);
        return r;
    }
}
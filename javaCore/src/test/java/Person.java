import java.util.ArrayList;



public class Person<E> {

	private String name = "Jim";
	private int age = 0;
	
	
	public Person(E e){
		System.out.println("I'm construct");
		System.out.println(e.getClass());
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	@Override
	public String toString() {
		return "name:"+name+","+"age:"+age;
	}
	
	void OutputInfo(){
		
	}
}

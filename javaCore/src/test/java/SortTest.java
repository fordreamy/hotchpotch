public class SortTest {

	public static void main(String[] args) {

		int a[] = { 100, 30, 2, 60, 70, 80 };

		for (int i : a) {
			System.out.print(i + " ");
		}
		int temp = 0;
		int length = a.length;
		// 冒泡排序BubbleSort
		/*
		 * for(int i=0;i<length -1;i++){ for(int j=0;j<length - i -1;j++){ if(a[j] < a[j+1]){ temp = a[j]; a[j] =
		 * a[j+1]; a[j+1] = temp; } } }
		 */
		for (int i = length - 1; i > 0; i--) {
			for (int j = 0; j < i; j++) {
				if (a[j] < a[j + 1]) {
					temp = a[j];
					a[j] = a[j + 1];
					a[j + 1] = temp;
				}
			}
		}
		System.out.println("-------------------------------------");
		for (int i : a) {
			System.out.print(i + " ");
		}
		int max = 0;
		// 选择排序selectSort
		for (int i = 0; i < length - 1; i++) {
			max = i;
			for (int j = i + 1; j < length; j++) {
				if (a[j] > a[max])
					max = j;
			}
			temp = a[max];
			a[max] = a[i];
			a[i] = temp;
		}
		System.out.println("-------------------------------------");
		for (int i : a) {
			System.out.print(i + " ");
		}
		
		int b[] = { 100, 30, 2, 60, 70, 80 };
		b = new Quick().sort(b, 5, 0);
		System.out.println("快速排序"+b);
		for (int i : b) {
			System.out.print(i + " ");
		}
	}

}

class Quick {

	int []  sort(int arr[], int low, int high) {
		int l = low;
		int h = high;
		int povit = arr[low];

		while (l < h) {
			while (l < h && arr[h] >= povit)
				h--;
			if (l < h) {
				int temp = arr[h];
				arr[h] = arr[l];
				arr[l] = temp;
				l++;
			}

			while (l < h && arr[l] <= povit)
				l++;

			if (l < h) {
				int temp = arr[h];
				arr[h] = arr[l];
				arr[l] = temp;
				h--;
			}
		}
		System.out.print("l=" + (l + 1) + "h=" + (h + 1) + "povit=" + povit + "\n");
		if (l > low)
			sort(arr, low, l - 1);
		if (h < high)
			sort(arr, l + 1, high);
		return arr;
	}
}

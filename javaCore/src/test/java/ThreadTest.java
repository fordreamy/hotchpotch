import java.io.UnsupportedEncodingException;

public class ThreadTest {

	public static void main(String[] args) throws UnsupportedEncodingException, InterruptedException {

		Thread1 thread1 = new Thread1("11");

		Thread t1 = new Thread(thread1,"1");

		Thread1 thread12 = new Thread1("22");
		Thread t2 = new Thread(thread12,"2");
		
		t1.start();
		t2.start();
		MyThread mt=new MyThread();  
		new Thread(mt).start();//同一个mt，但是在Thread中就不可以，如果用同一  
		new Thread(mt).start();//个实例化对象mt，就会出现异常  
		new Thread(mt).start();
		Thread.sleep(1000);
		for (int i = 0; i < 100; i++) {
			
			System.out.println(Thread.currentThread().getName());
			System.out.println(Thread.currentThread().getClass());
			if(Thread.currentThread().interrupted()){
				System.out.println("stop");
			}
			/*if(Thread.currentThread().isAlive()){
				System.out.println("stop2");
			}*/
			if(Thread.currentThread().isDaemon()){
				System.out.println("stop3");
			}
		}
	}

}

class Thread1 implements Runnable {

	String info = "";

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println(info);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	Thread1(String info) {
		this.info = info;
	}

}

class MyThread implements Runnable {
	private int ticket = 10;

	public void run() {
		for (int i = 0; i < 20; i++) {
			if (this.ticket > 0) {
				System.out.println("卖票：ticket" + this.ticket--);
			}
		}
	}
}

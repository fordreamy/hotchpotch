
public class StringTest {

	public static void main(String[] args) {

		int times = 100;
		String string = "";
		StringBuilder sBuilder = new StringBuilder();
		StringBuffer sBuffer = new StringBuffer();
		long s = 0;
		s = System.nanoTime();
		for(int i=0;i<times;i++){
			string += "hello";
		}
		System.out.println(System.nanoTime() - s);
		
		s = System.nanoTime();
		for(int i=0;i<times;i++){
			sBuilder.append("hello");
		}
		System.out.println((System.nanoTime() - s));
		
		s = System.nanoTime();
		for(int i=0;i<times;i++){
			sBuffer.append("hello");
		}
		System.out.println(System.nanoTime() - s);
		
	}

}

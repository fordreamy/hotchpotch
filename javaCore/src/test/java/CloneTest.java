import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class CloneTest implements Cloneable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	
	
	private ArrayList<Object> List = new ArrayList<>();

	public CloneTest deepClone() {
		ByteArrayOutputStream byteOut = null;
		ObjectOutputStream objOut = null;
		ByteArrayInputStream byteIn = null;
		ObjectInputStream objIn = null;

		try {
			byteOut = new ByteArrayOutputStream();
			objOut = new ObjectOutputStream(byteOut);
			objOut.writeObject(this);

			byteIn = new ByteArrayInputStream(byteOut.toByteArray());
			objIn = new ObjectInputStream(byteIn);

			return (CloneTest) objIn.readObject();
		} catch (IOException e) {
			throw new RuntimeException("Clone Object failed in IO.", e);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Class not found.", e);
		} finally {
			try {
				byteIn = null;
				byteOut = null;
				if (objOut != null)
					objOut.close();
				if (objIn != null)
					objIn.close();
			} catch (IOException e) {
			}
		}

	}

	CloneTest deepClone2(){
		CloneTest cloneTest = null;
		try {
			cloneTest = (CloneTest) super.clone();
			cloneTest.List = (ArrayList<Object>) this.List.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cloneTest;
	}
	
	CloneTest shallowClone() {
		try {
			return (CloneTest) super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		CloneTest cloneTest = new CloneTest();

		cloneTest.List.add("apple");
		System.out.println(cloneTest.List.toString());

		CloneTest deep = null;
		CloneTest deep2 = null;
		CloneTest shallow = null;
		deep = cloneTest.deepClone();
		shallow = cloneTest.shallowClone();
		deep2 = cloneTest.deepClone2();
		cloneTest.List.add("orange");
		
		System.out.println("深"+deep.List.toString());
		System.out.println("深2"+deep2.List.toString());
		System.out.println("浅"+shallow.List.toString());

		//日期的clone克隆方法已被重写
		Date date = new Date(123);
		System.out.println(date.toString());
		Date dateClone = (Date) date.clone();
		Date date2 = date;
		date.setTime(123456);
		System.out.println("未克隆"+date2.toString());
		System.out.println("克隆后"+dateClone.toString());
		
		
		People people = new People(){
			
			public int getAg(){
				System.out.println("hhh");
				return 0;
			}
		};
		people.getAge();
		
	}
}
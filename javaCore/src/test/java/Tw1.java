public  class Tw1{
    public static void main(String[] args) {
        InputStream in = new FileInputSteam();
        new DataInputStream(new BufferedInputStream(in)).read();
        Aa a = new Aa();
        a.a = new Aa();
        a.a.a = new Aa();
        a.a();
    }
}
abstract class InputStream{
    public abstract int read();
}

class FileInputSteam extends InputStream{
    public int read(){
        System.out.println("原生读取字节");
        return 0;
    }
}
class BufferedInputStream extends InputStream{
    InputStream in = null;
    public BufferedInputStream(InputStream in){
        this.in = in;
    }
    public int read(){
        System.out.println("装饰模式BufferedInputStream：读取字节");
        in.read();
        return 0;
    }
    public int read1(InputStream in){
        System.out.println("装饰模式BufferedInputStream：读取字节");
        in.read();
        return 0;
    }
}
class DataInputStream extends InputStream{
    InputStream in = null;
    public DataInputStream(InputStream in){
        this.in = in;
    }
    public int read(){
        System.out.println("装饰模式DataInputStream：读取字节");
        in.read();
        return 0;
    }
}
class Aa{
    Aa a = null;
    void a(){
        if(a == null)
            return;
        System.out.println("a");
        a.a();
    }
}
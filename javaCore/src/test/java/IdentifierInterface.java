import java.util.ArrayList;
import java.util.List;


public class IdentifierInterface {

	public static void main(String[] args) {
		
		
		List<String> list = new ArrayList<>();
		
		System.out.println(list instanceof Object);
		System.out.println(list instanceof ArrayList);
		System.out.println(list instanceof List);
		
		
		Man man = new IdentifierInterface().new Man();
		
		System.out.println(man instanceof Man);
		System.out.println(isInstance(man));
		System.out.println(man instanceof Man);

	}

	public static String isInstance(Object object){
		String string = "";
		if(object instanceof List){
			string += "是List";
		}
		if(object instanceof ArrayList){
			string += "是ArrayList";
		}
		if(object instanceof String){
			string += "是String";
		}
		if(object instanceof People){
			string += "继承父类People";
		}
		if(object instanceof PeopleI){
			string += "实现接口PeopleI";
		}
		if(object instanceof IdentifierInterface){
			string += "继承IdentifierInterface";
		}
		string += "继承Object";
		return string;
	}
	
	interface PeopleI{
		
	}
	
	class People{
		
	}
	
	class Man extends People implements PeopleI{
		
	}
	
}

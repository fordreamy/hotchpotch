package database.QueryDB.DBConn;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

public class InsertData {


	public static void createAndInsert(Connection conn) throws Exception {
		Statement s = null;
		ResultSet rs = null;
		DatabaseMetaData meta = conn.getMetaData();
		ResultSet res = meta.getTables(null, null, null, new String[] { "TABLE" });
		HashSet<String> set = new HashSet<String>();
		while (res.next()) {
			set.add(res.getString("TABLE_NAME"));
		}
		for (String string : set) {
			System.out.println(string);
		}
		try {
			s = conn.createStatement();
			
			if (set.contains("FIRSTTABLE")) {
				System.out.println("表FIRSTTABLE已经存在!");
				s.executeUpdate("DROP TABLE firsttable");
				s.executeUpdate("create table firsttable(id varchar(20), name varchar(20))");
			} else {
				System.out.println("表FIRSTTABLE不存在,创建新表!");
				s.executeUpdate("create table firsttable(id varchar(20), name varchar(20))");
			}
			s.executeUpdate("insert into firsttable values('1', 'Jim')");
			rs = s.executeQuery("select * from firsttable");
			while (rs.next()) {
				System.out.println("id:"+rs.getString(1));
				System.out.println("name:"+rs.getString(2));
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			conn.close();
			conn = null;
			s.close();
			s = null;
			rs.close();
			rs = null;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

## 未检查(unchecked)异常
所有派生于Error类或RuntimeException类的所有异常称为未检查异常
## 已检查(checked)异常
除了未检查异常，所有其它异常都是已检查异常
## 异常经验
如果出现RuntimeException异常，那么一定是你的问题是一条相当有道理的规则  
应当在使用变量之前判断空指针而防止出现NullPointerException异常等  
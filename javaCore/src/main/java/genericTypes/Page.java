package genericTypes;

import entity.Car;
import entity.Shoes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Page<T> {

    private int pageNum = 0;
    private List<T> lists = new ArrayList<T>();

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public List<T> getList() {
        return lists;
    }

    public void setList(List<T> lists) {
        this.lists = lists;
    }

    public List<T> getRecordsByPageNum(int pageNum) {
        return lists;
    }

    public Page<T> add(T t) {
        lists.add(t);
        return this;
    }

    public Object aa(T t) {
        return t;
    }

    public <T extends Comparable & Serializable> T min(T[] t) {
        if (t == null) return null;
        T smallest = t[0];
        for (int i = 0; i < t.length; i++) {
            if (smallest.compareTo(t[i]) > 0) {
                smallest = t[i];
            }
        }
        return smallest;
    }

    public int getSize() {
        return lists.size();
    }

    public static void main(String[] args) {
        Page<Car> p1 = new Page<Car>();

        p1.add(new Car()).add(new Car());

        System.out.println(p1.getSize());
        System.out.println(p1.aa(new Car()).getClass());

        System.out.println(new Page<Shoes>().add(new Shoes()).getSize());

        ArrayListDiy<Car> ald = new ArrayListDiy<Car>();
        for (int i = 0; i < 50; i++) {
//            ald.add(new Car());
        }

        System.out.println(ald.size());

    }
}

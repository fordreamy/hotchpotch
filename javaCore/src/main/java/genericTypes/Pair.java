package genericTypes;

import entity.Car;
import entity.Shoes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Pair<T> {

    private T first;
    private T second;

    public Pair() {
    }

    public Pair(T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst(T[] t) {
        return t[0];
    }

    public <T extends Comparable> Pair<T> minmax(T[] t) {
        T min = t[0];
        T max = t[0];
        for (int i = 0; i < t.length; i++) {
            if (min.compareTo(t[i]) > 0) min = t[i];
            if (max.compareTo(t[i]) < 0) max = t[i];
        }
        Pair<T> p = new Pair<>(min, max);
        return p;
    }

    public void output() {
        System.out.println("min:" + first + ", max:" + second);
    }
    public void listMethod(List<String> stringList){
    }

    public static void main(String[] args) {
        String s[] = {"1", "2", "3"};
        new Pair<String>().minmax(s).output();


    }
}

package arithmetic;

import java.util.Random;

public class PyramidOfNumber {

	public static void main(String[] args) {
		int n=4;
		int[][] a = new int[n][n];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j <= i; j++) {
				a[i][j] = new Random().nextInt(9)+1;
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
		int upper[] = new int[n];
		upper[0]=a[0][0];
		int isum[] = new int[n];
		isum[0]=a[0][0];
		
		for (int i = 1; i < a.length; i++) {
			isum[0] = upper[0] + a[i][0];
			for (int k = 1; k < a.length; k++) {
				if(upper[k-1]>upper[k]){
					isum[k]=upper[k-1]+a[i][k];
				}else{
					isum[k]=upper[k]+a[i][k];
				}
			}
			for(int k=0;k<a.length;k++){
				System.out.print("--"+isum[k]+" ");
				upper[k] = isum[k];
			}
			System.out.println();
		}
		int max=0;
		for(int k=0;k<upper.length;k++){
			if(max < upper[k]){
				max =upper[k];
			}
		}
		System.out.println("---"+max);
	}

}

package arithmetic;

public class Sort {
    //        static int a[] = {5, 1, 2, 2, 9, 3, 0, 4};
    static int a[] = {2, 3, 1};

    public static void main(String[] args) {
//        bubbledSort(a);
        selectionSort(a);
        output(a);
    }

    static void output(int[] a) {
        for (int i : a) {
            System.out.print(i + " ");
        }
    }

    //冒泡排序(bubble sort):每次比较相邻的两个元素,顺序错误则交换顺序
    static void bubbledSort(int[] a) {
        int tmp = 0;
        for (int i = a.length - 1; i > 0; i--) { //遍历的次序共length-1次
            for (int j = 0; j < i; j++) {        //每次排序从第一个元素开始
                if (a[j] > a[j + 1]) {
                    tmp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = tmp;
                }
            }
        }
    }

    //选择排序(Selection sort):每次选择最大或者最小值
    static void selectionSort(int[] a) {
        int tmp = 0;
        int min = 0;
        for (int i = 0; i < a.length - 1; i++) {
            min = i;
            for (int j = i + 1; j < a.length; j++) {
                if (a[j] < a[min]) {
                    min = j;
                }
            }
            if (min != i) {
                tmp = a[i];
                a[i] = a[min];
                a[min] = tmp;
            }
        }
    }
}

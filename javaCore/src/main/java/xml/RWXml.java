package xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class RWXml {
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		//根据dtd文件校验xml,可不写
		factory.setValidating(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		if (builder.isValidating()) {
			System.out.println("ok");
		}
		File file = new File("Dynamic:\\work\\MVC2\\mvc2WS\\javaIO\\src\\xml\\doc.xml");
		Document doc = builder.parse(file);
		Element root = doc.getDocumentElement();
		System.out.println(root.getTagName());
		System.out.println(root.getNextSibling());
System.out.println(doc.getElementsByTagName("bookList").item(0).getChildNodes().getLength());
System.out.println(doc.getElementsByTagName("bookList").item(0).getChildNodes().item(2).getNodeName());
		NodeList books;
		Node node;

		books = doc.getElementsByTagName("book");
		NodeList bookInfo;
		Node tmp;
		for (int i = 0; i < books.getLength(); i++) {
			node = books.item(i);
			bookInfo = node.getChildNodes();
			for (int j = 0; j < bookInfo.getLength(); j++) {
				tmp = bookInfo.item(j);
				if (tmp instanceof Element) {
					System.out.println(tmp.getNodeName());
					System.out.println(tmp.getNodeValue());
					System.out.println(tmp.getTextContent());
				}
			}
		}

	}

}

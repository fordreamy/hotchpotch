package network.EchoServer;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * This program implements a simple server that listens to port 8189 and echoes
 * back all client input.
 * 
 * @version 1.20 2004-08-03
 * @author Cay Horstmann
 */
public class EchoServer {
	public static void main(String[] args) throws IOException {
		ServerSocket serverSocket = new ServerSocket(9000);
		Socket socket = serverSocket.accept();
		InputStream ins = socket.getInputStream();
		OutputStream os = socket.getOutputStream();
		PrintWriter pw = new PrintWriter(os, true);
		pw.println("Hello! Enter exit to exit.");
		Scanner scanner = new Scanner(ins);
		boolean done = false;
		while (!done && scanner.hasNextLine()) {
			String line = scanner.nextLine();
			pw.println("hello:" + line);
			if (line.equalsIgnoreCase("exit")) {
				done = true;
			}
		}
		scanner.close();
		socket.close();
		serverSocket.close();
	}
}

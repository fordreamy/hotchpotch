package network.SocketTest;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * This program makes a socket connection to the atomic clock in Boulder, Colorado, and prints the
 * time that the server sends.
 *
 * @author Cay Horstmann
 * @version 1.20 2004-08-03
 */
public class SocketTest {
    public static void main(String[] args) throws IOException {
        try  {
            Socket s = new Socket("baidu.com", 80);
            s.getInputStream();
            String line;
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(s.getInputStream()));
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }

        }catch (Exception e){

        }

    }
}

package io.read;

import io.FileDemo;

import java.io.*;


public class BufferedReaderDemo {
    public static void main(String[] args) throws IOException {
        String p = FileDemo.getJavaFilePath(BufferedReaderDemo.class, "") + "/../1.txt";
        System.out.println(readLine(p));
    }


    public static String readLine(String filePath) {
        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();
        int n = 0;
        String line = "";
        try {
//            br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
            br = new BufferedReader(new FileReader(filePath));
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("字符总数:" + n);
        return sb.toString();
    }

}
package io;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;

public class FilterInputStreamDemo {

	public static void main(String[] args) throws IOException {
		String path = "Dynamic:\\work\\MVC2\\mvc2WS\\javaIO\\data" + File.separator + "1.txt";
		DataInputStream dataInputStream = new DataInputStream(new FileInputStream(path));
		byte b;
		byte[] tmp = new byte[50];
		int i = 0;
		while ((b = (byte) dataInputStream.read()) != -1) {
			
			System.out.println("------------------"+b);
			byte[] ba = { b };
			System.out.println("single byte->"+new String(ba));
			System.out.println("String->"+new String(ba, "gb2312"));
			tmp[i++] = b;
			for(int j=0;j<20;j++){
				System.out.print(tmp[j]+"_");
			}
			System.out.println();
			System.out.println("tmp->"+new String(tmp));
			System.out.println("tmp??22222222222->"+new String(tmp,"gb2312"));
			System.out.println("tmp33333333333->"+new String(tmp,"utf-8"));
		}
		
	}

}
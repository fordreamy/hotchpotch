#### Java的IO分类:
+ 基于字节操作的I/O接口: InputStream和OutputStream
+ 基于字符操作的I/O接口: Writer和Reader
+ 基于磁盘操作的I/O接口: File
+ 基于网络操作的I/O接口: Socket  

#### 编码:
GB2312是双字节编码,范围A1~F7,包含6763个汉字  
GBK扩展了GB2312包含了更多汉字也是双字节,编码范围8140~FEFE(去掉XX7F),总共23940个码位,表示21003个汉字  
GB18030可能是单字节、双字节或四字节编码,使用不广泛  
UTF-16  
是Unicode的一种实现方式.Java以UTF-16作为内存的字符存储格式,统一采用两个字节表示一个字符。
UTF-16BE: 16 位 UCS 转换格式，Big Endian（最低地址存放高位字节，符合人们的阅读习惯）字节顺序   
UTF-16LE: 16 位 UCS 转换格式，Little-endian（最高地址存放高位字节）字节顺序   
UTF-16: 16 位 UCS 转换格式，字节顺序（是高字节在前还是低字节在前）由流中的前两字节中字节顺序标记来确定  
UTF-16BE: FE FF 字节顺序标记  
UTF-16LE: FF FE 字节顺序标记  
  
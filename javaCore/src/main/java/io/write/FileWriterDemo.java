package io.write;

import io.FileDemo;

import java.io.FileWriter;
import java.io.IOException;


public class FileWriterDemo {
    public static void main(String[] args) throws IOException {

        String p = FileDemo.getJavaFilePath(FileWriterDemo.class, "") + "/../1.txt";
        FileWriter fw = new FileWriter(p);
        fw.write(new char[]{'a','b'});
        fw.write(65);
        fw.write("hello 世界!");
        fw.close();
    }

    /**
     * 读取多个字符到缓冲数组里
     *
     * @param cbuf
     * @param filePath
     * @return
     */
    public static void write(char cbuf[], String filePath) {
        FileWriter fr = null;
        StringBuffer sb = new StringBuffer();
        int ch = 0;
        try {
            fr = new FileWriter(filePath);
            fr.write(cbuf);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("字符总数:");
    }
}

package io;

import java.io.UnsupportedEncodingException;

public class CodeConvert {

	public static String getCode(String content, String format) throws UnsupportedEncodingException {
		byte[] bytes = content.getBytes(format);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			sb.append(Integer.toHexString(bytes[i] & 0xff).toUpperCase() + " ");
		}

		return sb.toString();
	}

	public static void main(String[] args) throws UnsupportedEncodingException {
		System.out.println("gbk : " + getCode("你", "gbk"));
		System.out.println("gb2312 : " + getCode("你", "gb2312"));
		System.out.println("iso-8859-1 : " + getCode("你", "iso-8859-1"));
		System.out.println("unicode : " + getCode("你", "unicode"));
		System.out.println("utf-16 : " + getCode("你", "utf-16"));
		System.out.println("utf-8 : " + getCode("你", "utf-8"));
		System.out.println("\u4F60");
		System.out.println(java.nio.charset.Charset.defaultCharset());

		String str = "你";
		System.out.println(str.codePointAt(0));
        System.out.println(Integer.toHexString(str.codePointAt(0)).toUpperCase());
        str = "a";
        System.out.println(Integer.toHexString(str.codePointAt(0)).toUpperCase());
	}
}

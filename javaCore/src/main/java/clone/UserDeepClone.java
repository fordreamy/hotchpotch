package clone;

import java.util.ArrayList;
import java.util.List;

public class UserDeepClone implements Cloneable{
    String name;
    int age;
    public List<String> listString = new ArrayList();
    public List<UserDeepClone> listUser = new ArrayList();

    public UserDeepClone(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    protected UserDeepClone clone(){
        UserDeepClone d = null;
        try {
             d = (UserDeepClone) super.clone();
             d.listUser = new ArrayList<>();
             for(UserDeepClone u:listUser){
                 d.listUser.add(u.clone());
             }

        }catch (Exception e){
        }
        return d;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

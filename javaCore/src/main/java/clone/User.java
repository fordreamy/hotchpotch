package clone;

import java.util.ArrayList;
import java.util.List;

public class User{
    String name;
    int age;
    public List<String> listString = new ArrayList();
    public List<User> listUser = new ArrayList();

    public void endlessLoop(){
        System.out.println("开启无尽模式");
        listUser.get(0).endlessLoop();
    }

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

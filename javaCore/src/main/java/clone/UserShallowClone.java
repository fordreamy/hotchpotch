package clone;

import java.util.ArrayList;
import java.util.List;

public class UserShallowClone implements Cloneable{
    String name;
    int age;
    public List<String> listString = new ArrayList();
    public List<UserShallowClone> listUser = new ArrayList();

    public UserShallowClone(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    protected UserShallowClone clone(){
        try {
            return (UserShallowClone)super.clone();
        }catch (Exception e){
            return null;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

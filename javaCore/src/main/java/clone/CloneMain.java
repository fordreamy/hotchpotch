package clone;

public class CloneMain {

    public static void main(String[] args) {

        System.out.println("浅克隆-----------------------------------");
        System.out.println("克隆前初始化");
        UserShallowClone u = new UserShallowClone("原始人", 21);
        u.listString.add("aaa");
        u.listUser.add(new UserShallowClone("listUser1", 21));

        System.out.println(u.listString);
        System.out.println(u.listUser.size());
        for (UserShallowClone d : u.listUser) {
            System.out.println(d.getName() + d.getAge());
        }

        System.out.println("克隆后-----------------------------------");
        UserShallowClone usc = u.clone();

        usc.listString.add("bbb");
        usc.listUser.add(new UserShallowClone("listUser2", 21));
        usc.listUser.add(new UserShallowClone("listUser3", 21));
        System.out.println(usc.listString);
        System.out.println(usc.listUser.size());
        for (UserShallowClone d : usc.listUser) {
            System.out.println(d.getName() + d.getAge());
        }

        System.out.println("验证-----------------------------------");
        System.out.println(u.listString);
        System.out.println(u.listUser.size());
        for (UserShallowClone d : u.listUser) {
            System.out.println(d.getName() + d.getAge());
        }

        System.out.println("深克隆-----------------------------------");
        UserDeepClone ud = new UserDeepClone("原始人", 21);
        System.out.println("克隆前初始化");
        ud.listString.add("aaa");
        ud.listUser.add(new UserDeepClone("listUser1", 21));
        System.out.println(ud.listString);
        System.out.println(ud.listUser.size());
        for (UserDeepClone d : ud.listUser) {
            System.out.println(d.getName() + d.getAge());
        }

        System.out.println("克隆后");
        UserDeepClone udc = ud.clone();
        udc.listString.add("bbb");
        udc.listUser.add(new UserDeepClone("listUser2", 21));
        udc.listUser.add(new UserDeepClone("listUser3", 21));
        System.out.println(udc.listString);
        System.out.println(udc.listUser.size());
        for (UserDeepClone d : udc.listUser) {
            System.out.println(d.getName() + d.getAge());
        }

        System.out.println("验证-----------------------------------");
        System.out.println(ud.listString);
        System.out.println(ud.listUser.size());
        for (UserDeepClone d : ud.listUser) {
            System.out.println(d.getName() + d.getAge());
        }

        User user = new User("j",2);
        System.out.println(user);
        user.listUser.add(user);
        System.out.println(user.listUser.get(0).listUser.get(0).listUser.get(0).getName());
       // user.endlessLoop();

    }
}

package annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

@UserAnnotation(name = "jack")
public class TestAnnotation {

    @Check(v = "jim")
    @MaxValue(max = 20)
    public String v;

    public static void main(String[] args) throws Exception {
        boolean hasAnnotation = TestAnnotation.class.isAnnotationPresent(UserAnnotation.class);
        System.out.println(hasAnnotation);
        if (hasAnnotation) {
            UserAnnotation u = TestAnnotation.class.getAnnotation(UserAnnotation.class);
            System.out.println("name" + u.name());
            System.out.println("id" + u.id());
        }
        Field v = TestAnnotation.class.getDeclaredField("v");
        Annotation[] a = v.getAnnotations();
        System.out.println("属性v的注解:");
        for (Annotation t : a) {
            System.out.println(t);
            System.out.println(t.annotationType());
            System.out.println("是否是Check注解:" + (t instanceof Check));
        }

        MaxValue maxValue = v.getAnnotation(MaxValue.class);
        System.out.println("注解最大值:" + maxValue.max());
        System.out.println("-------执行所有有注解@Check的方法---------");
        TestDemo testDemo = new TestDemo();
        Method[] methods = TestDemo.class.getMethods();
        for(Method m:methods){
            if(m.isAnnotationPresent(Check.class)){
                try{
                    m.invoke(testDemo);
                    System.out.println("方法执行成功:"+m.getDeclaringClass()+"--"+m.getName());
                }catch (Exception e){
                    System.out.println("方法执行出错:"+m.getName());
                }
            }
        }

    }

}
package AllClass;

/**
 * 匿名类格式：new ClassType(){ method and data}
 *           new InterfaceType(){ method and data}
 */
public class AnonymousClass {
    public static void main(String[] args) {
        new AnonymousOne(){}.a();
        new AnonymousOne(){
            @Override
            public void a(){
                System.out.println("覆盖了a");
            }
        }.a();
    }
}

class AnonymousOne{
    public void a(){
        System.out.println("a");
    }
}

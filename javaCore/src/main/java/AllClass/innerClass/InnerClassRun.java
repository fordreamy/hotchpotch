package AllClass.innerClass;

public class InnerClassRun {
    public static void main(String[] args) {
        InnerClass.InnerA innerA = new InnerClass("hi").new InnerA();
        System.out.println(innerA.getName());
        innerA.setName("jim");
        System.out.println(innerA.getName());
        /*innerA.a();
        innerA.b("hi");
        new InnerClass("h"){}.new InnerA().a();*/
    }
}

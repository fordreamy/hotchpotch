package AllClass.innerClass;


/**
 * 有时候只是为了将一个类隐藏在另一个类的内部，不需要内部类引用外部类对象，
 * 此时可以将内部类声明为static,以便取消产生的引用。
 * 静态内部类除了没有生成它的外部类对象引用特权外，与其它内部类一模一样。
 * 静态内部类可以有静态方法，可直接调用。
 */
public class StaticInnerClass {

    public static void main(String[] args) {
        new StaticInnerClass().outerToA().a();
        System.out.println(new StaticInnerClass().outerToA().innerAName);
        StaticInnerClass.InnerA ina = new StaticInnerClass().outerToA();
        StaticInnerClass.InnerA inb = new StaticInnerClass().outerToA();
        ina.innerAName = "tom";
        System.out.println(ina.innerAName);
        System.out.println(inb.innerAName);
        //可调用静态内部类的静态方法
        StaticInnerClass.InnerA.b();
        //已不能调用内部类方法
        //new StaticInnerClass().new InnerA().a();
    }

    public StaticInnerClass(){
        System.out.println("调用外部类构造器");
    }
    public void outerA(){
        System.out.println("调用外部方法outerA");
    }
    public InnerA outerToA(){
        System.out.println("创建并返回内部类对象");
        return new InnerA();
    }

    public static class InnerA{
        public String innerAName = "Jim";
        public void a(){
            System.out.println("调用内部类方法a");
            //已不能调用外部类方法
            // outerA();
        }
        public static void b(){
            System.out.println("调用静态内部类的静态方法b");
            //已不能调用外部类方法
            // outerA();
        }
    }

}

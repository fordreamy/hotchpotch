package AllClass.innerClass;


/**
 * 内部类：是定义在另一个类中的类，需要内部类的主要原因如下：
 * 1. 内部类可以访问该类定义所在的作用域中的数据，包括私有数据
 * 2. 内部类可以对同一个包内的其他类隐藏起来
 * 3. 当想要定义一个回调函数但是不想编写大量代码时，使用匿名内部类比较便捷
 * 创建内部类：OuterClass.InnerClass in = new OuterClass().new InnerClass();
 */
public class InnerClass {
    private String name = "Jack";

    public InnerClass(String o){
        System.out.println("调用外部类构造器");
    }
    public void outerA(){
        System.out.println("调用外部方法outerA");
    }

    public class InnerA{
        public  void setName(String name){
            InnerClass.this.name = name;
        }
        public String getName(){
            return InnerClass.this.name;
        }
        public  void a(){
            System.out.println("调用内部类方法a");
            //内部类调用外部方法一：
            InnerClass.this.outerA();
            //内部类调用外部方法二：
            outerA();
        }

        public void b(String b){
            class BB {
                public BB(){
                    System.out.println("b变量能访问，但是不能修改，编译器会默认加上final");
                    System.out.println("局部内部类，方法内部的类，只有此方法能调用");
                }
            }
            System.out.println("调用内部类方法a");
            //内部类调用外部方法一：
            InnerClass.this.outerA();
            //内部类调用外部方法二：
            outerA();
            new BB();
        }
    }

}

package i18n;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Test {
	public static void main(String[] args) {
		Locale locale = new Locale("zh", "CN");
		System.out.println(locale.getDisplayName(Locale.FRENCH));

		Calendar c = Calendar.getInstance();
		TimeZone timeZone = c.getTimeZone();
		System.out.println("timeZone " + timeZone + "  ");
		// 将Calendar对象转换为Date对象
		Date date = c.getTime();

		// 创建SimpleDateFormat对象，指定目标格式
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		// 将日期转换为指定格式的字符串
		String now = sdf.format(date);
		System.out.println("当前时间：" + now);
		
		System.out.println(new Date().getTime());
		System.out.println(new Date(1428595200000l));
		System.out.println("\uFB03"+"Aa");
		System.out.println("\u00c5"+"ss");

	}

}

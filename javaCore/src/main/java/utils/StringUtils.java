package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    /**
     * @param strDate
     * @param pattern 格式如:yyyy-MM-dd hh:mm:ss:SSS 年月日时分秒毫秒
     * @return
     */
    public static Date stringToDate(String strDate, String pattern) {
        Date date = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        try {
            date = simpleDateFormat.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static boolean isFindMatched(String content, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        return matcher.find();
    }

    /**
     * 返回正则匹配到的字符串
     *
     * @param content
     * @param regex
     * @return
     */
    public static String firstMatchedString(String content, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return "";
    }

    /**
     * 正则匹配到的字符串的开始下标
     *
     * @param content
     * @param regex
     * @return
     */
    public static int matchedStringStartIndex(String content, String regex) {
        String m = firstMatchedString(content, regex);
        return m != "" ? content.indexOf(m) : -1;
    }

    /**
     * 正则匹配到的字符串的结束下标
     *
     * @param content
     * @param regex
     * @return
     */
    public static int matchedStringEndIndex(String content, String regex) {
        String m = firstMatchedString(content, regex);
        return m != "" ? matchedStringStartIndex(content, regex) + m.length() : -1;
    }

    public static void main(String[] args) {
        String s = "selectSUM(    total_money) as F9922    from (select T7626.COUNTRY_NAME as F7620,        sum(T778.MONEY) / sum(total_money) as F7622,        SUBSTRING(T815.TIME_CODE, 1, 4) as F7494,        SUBSTRING(T815.TIME_CODE, 5, 2) as F7496   from HGQKJ.MAIN_USAHG1 T778  inner join HGQKJ.DIM_TIME T815 on T778.TIME_CODE = T815.TIME_CODE  inner join HGQKJ.DIM_COUNTRY_USA_VIEW T7626 on T778.COUNTRY_CODE =     T7626.COUNTRY_CODE  where (SUBSTRING(T815.TIME_CODE, 1, 4) = '2017' AND        SUBSTRING(T815.TIME_CODE, 5, 2) in ('04', '06', '05') AND        T7626.COUNTRY_NAME in ('巴西', '泰国', '加纳'))  group by T7626.COUNTRY_NAME,   SUBSTRING(T815.TIME_CODE, 1, 4),   SUBSTRING(T815.TIME_CODE, 5, 2)) T9905   group by T9905.F7494, T9905.F7496, T9905.F7620) T9904    order by F9906 ASC, F9907 ASC, F9910 ASC limit 10000";
        String regex = "SUM\\s*\\(\\s+(total_money)\\)\\s+(as)";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(s);
        System.out.println(m.matches());

        System.out.println(firstMatchedString(s, regex));
        System.out.println(isFindMatched(s,regex));

    }
}

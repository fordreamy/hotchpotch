## Summary of regular-expression constructs 
### Construct Matches
### Characters
```
\t          The tab character ('\u0009')
\n          The newline (line feed) character ('\u000A')
\r          The carriage-return character ('\u000D')
```
### Character classes
```
[abc]            a, b, or c (simple class)
[^abc]           Any character except a, b, or c (negation逻辑非)
[a-zA-Z]         a through z or A through Z, inclusive (range)
[a-d[m-p]]       a through d, or m through p: [a-dm-p] (union)
[a-z&&[def]]     d, e, or f (intersection交集)
[a-z&&[^bc]]     a through z, except for b and c: [ad-z] (subtraction差集)
[a-z&&[^m-p]]    a through z, and not m through p: [a-lq-z](subtraction)
```
### Predefined character classes
```
. 	Any character (may or may not match line terminators)
\d 	A digit: [0-9]
\D 	A non-digit: [^0-9]
\s 	A whitespace character: [ \t\n\x0B\f\r]
\S 	A non-whitespace character: [^\s]
\w 	A word character: [a-zA-Z_0-9]
\W 	A non-word character: [^\w]
```
### Greedy quantifiers
```
X? 	X, once or not at all
X* 	X, zero or more times
X+ 	X, one or more times
X{n} 	X, exactly n times
X{n,} 	X, at least n times
X{n,m} 	X, at least n but not more than m times
```
### Logical operators
```
XY 	X followed by Y
X|Y 	Either X or Y
(X) 	X, as a capturing group
```

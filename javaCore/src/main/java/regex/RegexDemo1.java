package regex;

import utils.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDemo1 {
    public static void o(String content, String regex) {
        String r = StringUtils.firstMatchedString(content, regex);
        if (r == "") {
            System.err.println("根据" + regex + "在" + content + "中无匹配到值");
        } else {
            System.out.println("根据" + regex + "在" + content + "中匹配到值:" + r);
        }
    }

    public static void main(String[] args) {
        // 按指定模式在字符串查找
        String line = "This order was placed for QT3000! OK?";
        String pattern = "(\\D*)(\\d+)(.*)";

        // 创建 Pattern 对象
        Pattern r = Pattern.compile(pattern);

        // 现在创建 matcher 对象
        Matcher m = r.matcher(line);
        if (m.find( )) {
            System.out.println("Found value: " + m.group(0) );
            System.out.println("Found value: " + m.group(1) );
            System.out.println("Found value: " + m.group(2) );
            System.out.println("Found value: " + m.group(3) );
        } else {
            System.out.println("NO MATCH");
        }

        String s = "a\nb cd kk\u0009kkk t\t Ef\ng  hi";
        System.out.println(s);
        String regex = "[A-Z]";
        o("aaaaab", "a*b");
        o("abcdEEFj", "[A-Z]");
        o("efjoo", "[a-d[m-p]]");
        o("Ad", "[a-z&&[def]]");
        o("ab123bc", ".\\d");
        o("123abc", "\\Dynamic");
        o("123\tabc", "\\s");
        o("123换行\nabc", "\\s");
        o("字母数字下划线_123abc", "\\w");
        o("123abc", "\\p{Lower}");
        o("123abc", "abcd");
    }
}

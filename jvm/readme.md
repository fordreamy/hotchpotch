## 执行jar

```
用法: java [-options] class [args...]
           (执行类)
   或  java [-options] -jar jarfile [args...]
           (执行 jar 文件)
例: java -Xms400m -jar jvm-0.1.0.jar p1
```

**常用参数**

| 参数  | 说明  |
| :------------ |:---------------|
| -cp | 目录和 zip/jar 文件的类搜索路径 |
|  -classpath | 目录和 zip/jar 文件的类搜索路径,用 ; 分隔的目录, JAR 档案和 ZIP 档案列表, 用于搜索类文件 |
| -D<名称>=<值> | 设置系统属性 |
| -XshowSettings | 显示所有设置并继续 |
| -Xloggc       | 将 GC 状态记录在文件中(带时间戳)|
| -Xms<size>    | 设置初始 Java 堆大小|
| -Xmx<size>    | 设置最大 Java 堆大小|
| -Xss<size>    | 设置 Java 线程堆栈大小|


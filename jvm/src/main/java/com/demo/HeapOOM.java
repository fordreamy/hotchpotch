package com.demo;

import java.util.ArrayList;
import java.util.List;

/**
 * VM Args:-Xms20M -Xmx20M -XX:+HeapDumpOnOutOfMemoryError
 */
public class HeapOOM {
    static class OOMObject {
    }

    public static void main(String[] args) {
        List<OOMObject> list = new ArrayList<OOMObject>();
        try {
            JVMArgsUtils.getMemoryUsage();
            JVMArgsUtils.getCommandLineArgs();
            while (true) {
                list.add(new OOMObject());
            }
        } catch (Exception e) {
            System.out.println("--");
            e.printStackTrace();
        }
    }
}

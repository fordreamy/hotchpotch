## HotSpot虚拟机主要参数

执行命令将输出结果保存到文件中java -XX:+PrintFlagsFinal Hello >>1.txt

虚拟机参数-XX:+PrintFlagsFinal可以输出所有参数的名称和默认值
#### 参数的使用方式如下:

`-XX:+<option> 开启option参数,值是布尔值`  
`-XX:-<option> 关闭option参数`  
`-XX:<option>=<value> 将option值设置为value`  

#### 下表是主要参数
|jvm参数|说明|
|-------|---|
|-Xms20M|设置初始堆大小
|-Xmx20M|设置最大堆大小
|-Xmn10M|新生代值
|-Xss100M|java线程堆栈大小
|<h4>内存管理参数</h4>|
|InitialHeapSize|初始化堆大小,和-Xms相等,约为物理内存1/64
|MaxHeapSize|最大堆大小,和-Xmx相等,约为物理内存1/4
|NewRatio|老年代新生代比例，默认2
|SurvivorRatio|新生代的Eden区和Survivor区容量比值,默认8
|MaxTenuringThreshold|晋升到老年代的对象年龄
|MaxPermsize/MaxMetaspaceSize|jkd7永久代/jkd8元空间
|<h4>调试参数</h4>|
|-XX:+HeapDumpOnOutOfMemoryError|堆转储快照,默认关闭
|-XX:+HeapDumpPath|堆转储快照路径
|-XX:+PrintCommandLineFlags|打印虚拟机启动时的参数
|-XX:+PrintGC|打印GC信息
|-XX:+PrintGCDetails|打印GC详细信息


大多数情况下，对象在新生代Eden区中分配。当Eden区没有足够空间进行分配时，虚拟机将发起一次Minor GC  
-Xms20M、-Xmx20M、-Xmn10M这3个参数限制了Java堆大小为20MB，不可扩展，其中10MB分配给新生代，求得老年代10MB(20-10)  

堆内存=新生代+年老代  

新生代(Eden区+1个Survivor区(总共有两个)，比例由-XX：SurvivorRatio=8设置,新生代内存总比例为8:1:1)
新生代总可用空间为（Eden区+1个Survivor区的总容量）。

新生代和年老代默认值  
新生代=1/3堆内存=1/2老年代
Eden=8/10新生代=8倍Survivor

其中，新生代 ( Young )被细分为Eden和两个Survivor区域，这两个 Survivor 区域分别被命名为 from 和 to，以示区分。
默认的，Edem : from : to = 8 :1 : 1 ( 可以通过参数–XX:SurvivorRatio 来设定 )，即： Eden = 8/10 的
新生代空间大小，from = to = 1/10 的新生代空间大小。

-XX：PretenureSizeThreshold参数，令大于这个设置值的对象直接在老
年代分配。PretenureSizeThreshold参数只对Serial和ParNew两款收集器有效，Parallel Scavenge收集器不
认识这个参数
如果对象在Eden出生并经过第一次Minor GC后仍然存活，并且能被
Survivor容纳的话，将被移动到Survivor空间中，并且对象年龄设为1。不能的话放入年老代


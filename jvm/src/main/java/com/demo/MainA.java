package com.demo;

public class MainA {
    public static void main(String[] args) {

        for (int i = 0; i < args.length; i++) {
            if (i == 0) {
                System.out.println("main方法参数开始----------------------------------------");
            }
            System.out.println("参数" + i + ":" + args[i]);
            if (i == args.length - 1) {
                System.out.println("main方法参数结束----------------------------------------");
            }
        }
        JVMArgsUtils.getProperties();
        JVMArgsUtils.getCommandLineArgs();
        JVMArgsUtils.getMemoryUsage();
        JVMArgsUtils.getRuntimeMemory();
        try {
            Thread.sleep(200000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

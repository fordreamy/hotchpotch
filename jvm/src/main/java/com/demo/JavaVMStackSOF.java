package com.demo;

/**
 * VM Args:-Xss200k
 */
public class JavaVMStackSOF {
    private int stackLength = 1;

    public void stackLeak() {
        stackLength++;
        System.out.println(stackLength);
        stackLeak();
    }

    public static void main(String[] args) throws Throwable {
        new JavaVMStackSOF().stackLeak();
    }
}

package com.demo;

public class TestAllocation {
    /**
     * VM参数：-verbose:gc-Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8
     */
    private static final int _1MB = 1024 * 1024;

    public static void testAllocation() {
        byte[] allocation1, allocation2, allocation3, allocation4,t1,t2,t3;
        allocation1 = new byte[2 * _1MB];
        allocation2 = new byte[2 * _1MB];
        allocation3 = new byte[2 * _1MB];
        t1 = new byte[100 * 1024];
        allocation4 = new byte[4 * _1MB];//出现一次Minor GC
    }

    public static void main(String[] args) {
        System.out.println("可用年轻代: " + (int)(10 * 1024 * 0.9));
        System.out.println("Eden: " + (int)(10 * 1024 * 0.8));
        System.out.println("Survivor: " + (int)(10 * 1024 * 0.1));
        System.out.println("年老代: " + (int)(10 * 1024));

        /*JVMArgsUtils.getMemoryUsage();
        JVMArgsUtils.getRuntimeMemory();*/
        testAllocation();
    }
}

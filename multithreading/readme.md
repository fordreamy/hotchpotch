### 代码同步<<Java多线程编程核心技术-高洪颜>>
### 名词解释
1. 非线程安全  
主要指多个线程对同一个对象中的同一个实例变量进行操作时出现值被修改、值不同步的情况
2. 脏读  
当多个线程访问同一个方法时，虽然用synchronized进行同步避免数据交叉，但在取值时可能会发生值已经被其它线程修改过了，这种情况就是脏读。

### 1.实现多线程
主要有两种，一种是继承Thread类，一种是实现Runnable接口

### 知识点
1. `非线程安全`存在于实例变量中，方法内部的私有变量则不存在`非线程安全`问题,方法内私有变量是每个线程独有的，不共享
2. 两个线程访问同一个对象的同步方法时一定是线程安全的,方法加上synchronized后就会排队执行。
3. 前提:线程A、B持有相同的对象o,o有方法methodA和methodB
+ A线程先持有o对象的Lock锁(调用synchronized方法methodA)，B线程可以异步调用o对象的非synchronized方法methodB
+ A线程先持有o对象的Lock锁(调用synchronized方法methodA)，B线程在调用synchronized方法methodB时需要等待，要等methodA先执行完
4. 多个线程访问同一对象的不同名称的synchronized同步方法或synchronized(this)同步代码块时，调用的效果就是顺序执行，是同步的，阻塞的。
5. 锁非this对象优点：
  如果在一个类中有很多个synchronized方法，这时虽然能实现同步但是会受到阻塞，所以会影响运行效率。
  但如果使用同步代码块非this对象，则sychronized(非this)代码块中的程序与同步方法是异步的，不与其它锁this同步方法争抢this锁，可大大提高运行效率。
6. synchronized加到static静态方法是给Class类上锁，而加到非static静态方法是给对象上锁
7. volatile关键字: 
   + 只能修饰变量
   + 强制从公共堆栈中取得变量的值，而不是从线程私有数据栈中取得

package com.thread.chapter2;

import cn.hutool.core.thread.ThreadUtil;
import lombok.Data;

@Data
public class Demo221 extends Thread {

    private String execMethod;
    private Demo221 task;

    public Demo221(Demo221 task, String execMethod) {
        this.task = task;
        this.execMethod = execMethod;
    }

    public synchronized void syncMethodA() {
        System.out.println("syncMethodA 开始:" + currentThread().getName());
        ThreadUtil.sleep(10000);
        System.out.println("syncMethodA 结束:" + currentThread().getName());
    }

    public synchronized void syncMethodB() {
        System.out.println("syncMethodB 开始:" + currentThread().getName());
        System.out.println("syncMethodB 结束:" + currentThread().getName());
    }

    public void methodC() {
        System.out.println("methodC 开始:" + currentThread().getName());
        System.out.println("methodC 结束:" + currentThread().getName());
    }

    @Override
    public void run() {
        super.run();
        if ("A".equals(execMethod)) {
            System.out.println("A" + execMethod);
            task.syncMethodA();
        } else if ("B".equals(execMethod)) {
            System.out.println("B" + execMethod);
            task.syncMethodB();
        } else {
            System.out.println("C" + execMethod);
            task.methodC();
        }
    }

    /*不同的线程调用同一实例task的不同synchronized方法,只有当拿到锁的同步方法执行完成后才能执行其它的同步方法。
    不同线程调用同一实例的相同同步方法也和上述一样结果*/

    public static void main(String[] args) {
        Demo221 task = new Demo221();
        Demo221 a = new Demo221(task, "A");
        a.start();
        Demo221 b = new Demo221(task, "B");
        b.start();
        Demo221 c = new Demo221(task, "C");
        c.start();
    }
}

package com.thread.chapter2;

public class HasSelfPrivateNum {
    private int num;

    public  void addI(String threadName) {
        try {
            num = 100;
            if (threadName.equals("a")) {
                System.out.println("线程:" + Thread.currentThread().getName() + ": a set over");
                Thread.sleep(2000);
            } else {
                num = 200;
                System.out.println("线程:" + Thread.currentThread().getName() + ": b set over");
            }
            System.out.println(threadName + " num=" + num);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addI2(String userName) {
        try {
            num = 100;
            if (userName.equals("a")) {
                System.out.println("a set over");
                Thread.sleep(2000);
            } else {
                num = 200;
                System.out.println("b set over");
            }
            System.out.println(userName + " num=" + num);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    synchronized public void safeAddI2(String userName) {
        try {
            num = 100;
            if (userName.equals("a")) {
                System.out.println("a set over");
                Thread.sleep(2000);
            } else {
                num = 200;
                System.out.println("b set over");
            }
            System.out.println(userName + " num=" + num);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

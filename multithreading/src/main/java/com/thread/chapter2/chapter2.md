## 第2章 对象及变量的并发访问
知识点:
```
synchronized对象监视器是Object时的使用
synchronized对象监视器是Class时的使用
非线程安全是如何出现的
关键字volatile的作用
关键字synchronized和volatile的区别
```

### 2.1 synchronized同步方法
非安全: 多线程 同一对象实例变量 值不一致
#### 2.1.1 方法内的变量为线程安全
#### 2.1.2 实例变量非线程安全
#### 2.1.3 多个对象多个锁
关键字synchronized锁 是对象锁
多线程访问同一个对象 先取得对象锁先执行synchronized方法 其余等待
多线程访问多对象 多个锁
#### 2.1.4 synchronized方法与锁对象
A线程先持有object对象的Lock锁,B线程可以异步调用object对象的非synchronized方法
A线程先持有object对象的Lock锁,B线程若想调用object对象中的其它synchronized方法则需要等待,虽然调用的不是同一个synchronized方法
#### 2.1.5 脏读
脏读就是读取实例变量时,值已经被其它线程更改过了
#### 2.1.6 synchronized锁重入
synchronized拥有锁重入功能,当一个线程获取对象锁后,再次请求该对象的锁时可以再次得到该对象的锁。即一个synchronized方法内可以调用另一个synchronized方法
当存在父子关系时,子类可以通过“可重入锁”调用父类的synchronized同步方法
#### 2.1.7 出现异常,锁自动释放
#### 2.1.8 同步不具继承性
子类覆盖父类的synchronized方法,但是没有synchronized修饰,那么子类的方法就不是个同步方法

### 2.2 synchronized同步语句块
#### 2.2.1 synchronized方法的弊端
synchronized同步方法是有弊端的,如果线程A执行一个耗时长的synchronized任务,线程B调用synchronized方法就必须等待
#### 2.2.2 synchronized同步代码块的使用
当并发线程访问同一个对象的synchronized(this)同步代码块时,一段时间内只能有一个线程执行,其它线程必须等待
#### 2.2.3 用同步代码块解决同步方法的弊端
不用将整个方法同步,仅将方法内的部分代码设置成同步即可
#### 2.2.4 一半异步一半同步
不在synchronized块中就是异步执行,在synchronized块中就是同步执行
#### 2.2.5 synchronized代码块间的同步性
当一个线程访问object对象的synchronized(this)同步代码块时,其它线程访问object的同步代码块将会被阻塞,说明synchronized使用的对象监视器是一个
#### 2.2.6 验证synchronized(this)代码块是锁定当前对象的
#### 2.2.7 将任意对象作为对象监视器
多个线程调用同一对象的synchronized同步方法或synchronized(this)同步代码块时,调用效果就是顺序执行,也就是同步的,阻塞的
锁非this对象有一定的优点：如果一个类中有很多synchronized方法,这时虽能同步,但会受到阻塞,影响运行效率。
但使用锁非this对象,则synchronized(非this)代码块程序与同步方法是异步的,不与其它锁this同步方法争抢this锁。
#### 2.2.8 验证3结论
synchronized(非this对象x)是将x对象作为“对象监视器”,这样就可以得到以下3个结论
当多个线程执行synchronized(x){}同步代码块时呈同步效果
当其它线程执行x对象的synchronized同步方法时呈同步效果
当其它线程执行x对象的synchronized(this)代码块时呈同步效果
#### 2.2.9 静态同步synchronized方法与synchronized(class)代码块
synchronized可以应用在static静态方法上,即对Class类加锁。
Class锁可以对所有的对象实例起作用,对象锁的情况下如果是new不同的对象,则对调用synchronized方法是异步执行的,如果是Class锁那么即使不同对象也是同步执行synchronized方法
同步synchronized(class)代码块作用和synchronized static方法作用是一样的
#### 2.2.10 String常量池特性
当两个线程用同样的String常量池锁时,线程只能同步执行。因此大多情况不用String作为锁对象。
#### 2.2.11 同步synchronized方法无限等待与解决
同一个对象的两个同步方法用的是对象锁,那么就有可能会出现等待
#### 2.2.12 多线程的死锁
死锁即多个线程在等待一个不可能被释放的锁,从而导致所有的任务无法继续完成。
### 2.3 volatile关键字
#### 2.3.2 解决同步死循环
关键字volatile作用是强制从公共堆栈中取得变量的值,而不是从线程私有数据栈中取得变量的值
#### 2.3.3 解决异步死循环
synchronized和volatile比较
1. volatile是线程同步的轻量级实现,volatile只能修饰变量
2. 多线程访问volatile不会阻塞,synchronized会阻塞
3. volatile能保证数据可见性,不能保证原子性。而synchronized可以
4. volatile解决的是变量在多个线程之间的可见性,而synchronized解决的是多个线程访问资源的同步性

线程安全包括原子性和可见性两个方面,Java的同步机制都是围绕这两方面来确保线程安全的
#### 2.3.4 volatile非原子性
关键字volatile虽然增加了实例变量在多线程之间的可见性，但是不具备同步性，也不具备原子性
#### 2.3.5 使用原子类进行i++操作
i++操作使用synchronized实现同步外，还可以使用原子类进行实现
#### 2.3.6 原子类也并不完全安全
#### 2.3.7 synchronized代码块有volatile同步的功能






















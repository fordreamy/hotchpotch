package com.thread.chapter2;

import cn.hutool.core.thread.ThreadUtil;
import lombok.Data;

@Data
public class Demo222 extends Thread {

    private String execMethod;
    private Demo222 task;

    public Demo222(Demo222 task, String execMethod) {
        this.task = task;
        this.execMethod = execMethod;
    }

    public void syncMethodA() {
        synchronized (this) {
            System.out.println("syncMethodA 开始:" + currentThread().getName());
            ThreadUtil.sleep(10000);
            System.out.println("syncMethodA 结束:" + currentThread().getName());
        }
    }

    public synchronized void syncMethodB() {
        System.out.println("syncMethodB 开始:" + currentThread().getName());
        System.out.println("syncMethodB 结束:" + currentThread().getName());
    }

    public void methodC() {
        System.out.println("methodC 开始:" + currentThread().getName());
        System.out.println("methodC 结束:" + currentThread().getName());
    }

    @Override
    public void run() {
        super.run();
        if ("A".equals(execMethod)) {
            System.out.println("即将执行方法: syncMethodA");
            task.syncMethodA();
        } else if ("B".equals(execMethod)) {
            System.out.println("即将执行方法: syncMethodB");
            task.syncMethodB();
        } else {
            System.out.println("即将执行方法: syncMethodC");
            task.methodC();
        }
    }

    /*
     * 当并发线程访问同一个对象的synchronized(this)同步代码块时,一段时间内只能有一个线程执行,其它线程必须等待
     * 不管是同步代码块还是同步方法,同一时间只能有一个执行,其它调用等待
     * */

    public static void main(String[] args) {
        Demo222 task = new Demo222();
        Demo222 a = new Demo222(task, "A");
        a.start();
        Demo222 b = new Demo222(task, "B");
        b.start();
        Demo222 c = new Demo222(task, "C");
        c.start();
    }
}

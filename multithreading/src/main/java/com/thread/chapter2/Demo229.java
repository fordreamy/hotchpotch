package com.thread.chapter2;

import cn.hutool.core.thread.ThreadUtil;
import lombok.Data;

@Data
public class Demo229 extends Thread {

    private String execMethod = new String();
    private Demo229 task;

    public Demo229(Demo229 task, String execMethod) {
        this.task = task;
        this.execMethod = execMethod;
    }

    public static synchronized void syncMethodA() {
        System.out.println("syncMethodA 开始:" + currentThread().getName());
        ThreadUtil.sleep(2000);
        System.out.println("syncMethodA 结束:" + currentThread().getName());
    }

    public synchronized void syncMethodB() {
        synchronized (Demo229.class) {
            System.out.println("syncMethodB 开始:" + currentThread().getName());
            System.out.println("syncMethodB 结束:" + currentThread().getName());
        }
    }

    public synchronized void syncMethodC() {
        synchronized (this) {
            System.out.println("syncMethodC 开始:" + currentThread().getName());
            System.out.println("syncMethodC 结束:" + currentThread().getName());
        }
    }

    @Override
    public void run() {
        super.run();
        if ("A".equals(execMethod)) {
            task.syncMethodA();
        } else if ("B".equals(execMethod)) {
            task.syncMethodB();
        } else if ("C".equals(execMethod)) {
            task.syncMethodC();
        }
    }

    /*
     */

    public static void main(String[] args) {
        //同步synchronized(class)代码块作用和synchronized static方法作用是一样的
        //该里】
        /*Demo229 task = new Demo229();
        Demo229 a = new Demo229(task, "A");
        a.start();
        Demo229 b = new Demo229(task, "B");
        b.start();*/

        //Class锁可以对所有的对象实例起作用,对象锁的情况下如果是new不同的对象,则对调用synchronized方法是异步执行的,
        // 如果是Class锁那么即使不同对象也是同步执行synchronized方法
        //该例A、B是Class锁,是同步执行。C是对象锁不同的对象是不同的锁因此异步执行
        new Demo229(new Demo229(), "A").start();
        new Demo229(new Demo229(), "B").start();
        new Demo229(new Demo229(), "C").start();
    }
}

package com.thread.chapter3;

import cn.hutool.core.thread.ThreadUtil;
import lombok.Data;


@Data
public class Demo3_2_2 extends Thread {

    @Override
    public void run() {
        super.run();
        System.out.println("执行子线程中" + currentThread().getName());
        ThreadUtil.sleep(5000);
    }

    public static void main(String[] args) {
        Demo3_2_2 t = new Demo3_2_2();
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("主线程结束" + currentThread().getName());
    }
}

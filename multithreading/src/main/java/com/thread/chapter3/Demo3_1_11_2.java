package com.thread.chapter3;

import cn.hutool.core.thread.ThreadUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Demo3_1_11_2 {

    private List<String> list = new ArrayList<>();

    synchronized public void push() {
        try {
            while (list.size() == 1) {
                System.out.println("生产等待:" + Thread.currentThread().getName());
                this.wait();
            }
            list.add("元素:" + Math.random());
            this.notifyAll();
            System.out.println(Thread.currentThread().getName() + "生产push=" + list.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    synchronized public String pop() {
        String returnValue = "";
        try {
            while (list.size() == 0) {
                System.out.println("消费等待:" + Thread.currentThread().getName());
                this.wait();
            }
            returnValue = list.get(0);
            list.remove(0);
            this.notify();
            System.out.println("消费pop=" + list.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    static class P {
        private Demo3_1_11_2 lock;

        public P(Demo3_1_11_2 lock) {
            this.lock = lock;
        }

        public void pushService() {
            lock.push();
        }
    }

    static class C {
        private Demo3_1_11_2 lock;

        public C(Demo3_1_11_2 lock) {
            this.lock = lock;
        }

        public void popService() {
            lock.pop();
        }
    }

    public static void main(String[] args) {
        //多生产者一个消费者,List最大容量是1
        Demo3_1_11_2 lock = new Demo3_1_11_2();
        P p1 = new P(lock);
        P p2 = new P(lock);
        P p3 = new P(lock);
        P p4 = new P(lock);
        new Thread(() -> {
            while (true) {
                p1.pushService();
            }
        }).start();
        new Thread(() -> {
            while (true) {
                p2.pushService();
            }
        }).start();
        new Thread(() -> {
            while (true) {
                p3.pushService();
            }
        }).start();
        new Thread(() -> {
            while (true) {
                p4.pushService();
            }
        }).start();

        ThreadUtil.sleep(5000);
        C c = new C(lock);
        new Thread(() -> {
            while (true) {
                c.popService();
            }
        }).start();

    }
}

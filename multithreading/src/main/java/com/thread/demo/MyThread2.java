package com.thread.demo;

/**
 * 每个线程不共享数据count,创建的是不同的实例
 */
public class MyThread2 extends Thread {
    private int count = 5;

    public MyThread2(String name) {
        super();
        this.setName(name); //设置线程名称
    }

    @Override
    public void run() {
        super.run();
        while (count > 0) {
            System.out.println(this.currentThread().getName() + "计数,count=" + count);
            count--;
        }
    }

    public static void main(String[] args) {
        MyThread2 a = new MyThread2("A");
        MyThread2 b = new MyThread2("B");
        MyThread2 c = new MyThread2("C");
        a.start();
        b.start();
        c.start();
    }
}

package com.thread.demo.usage.share;

public class ShareDataThread extends Thread {
    private int count = 5;

    @Override
    public void run() {
        super.run();
        count--;
        System.out.println("线程" + currentThread().getName() + " 开始计算,count=" + count);
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //此例不用while或者for,因为使用同步后其它线程就得不到运行机会了,一直由一个线程进行减法运算
        System.out.println("线程" + currentThread().getName() + " 结束计算,count=" + count);
    }

    public static void main(String[] args) {
        //此例子每个线程共享count变量，因为公用了一个实例对象
        ShareDataThread shareDataThread = new ShareDataThread();
        Thread a = new Thread(shareDataThread, "A");
        Thread b = new Thread(shareDataThread, "B");
        Thread c = new Thread(shareDataThread, "C");
        a.start();
        b.start();


        c.start();
        new Thread(shareDataThread, "D").start();

        new Thread(shareDataThread, "E").start();
    }
}

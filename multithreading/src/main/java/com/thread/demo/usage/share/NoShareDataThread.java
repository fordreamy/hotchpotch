package com.thread.demo.usage.share;

public class NoShareDataThread extends Thread {
    private int count = 5;

    public NoShareDataThread(String name) {
        super();
        this.setName(name); //设置线程名称
    }

    @Override
    public void run() {
        super.run();
        while (count > 0) {
            count--;
            System.out.println("线程" + currentThread().getName() + " 计算,count=" + count);
        }
    }

    public static void main(String[] args) {
        //此例子每个线程都有自己的count变量，线程间不共享，因为每个线程都是一个新的实例对象
        NoShareDataThread a = new NoShareDataThread("A");
        NoShareDataThread b = new NoShareDataThread("B");
        NoShareDataThread c = new NoShareDataThread("C");
        a.start();
        b.start();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        c.start();
    }
}

package com.thread.demo.usage.wait;

public class WaitDemo1 {
    public static void main(String[] args) throws InterruptedException {
        WaitTemp waitTemp = new WaitTemp();
        WaitThreadA a = new WaitThreadA(waitTemp);
        WaitThreadB b = new WaitThreadB(waitTemp);
        b.wait();
        b.wait(222);
        a.start();
//        a.wait(20000);
//        a.join();
        b.start();
    }
}

class WaitTemp {
    void methodA() {
        for (int i = 1; i < 5; i++) {
            System.out.println("method A");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    void methodB() {
        for (int i = 1; i < 5; i++) {
            System.out.println("method B");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class WaitThreadA extends Thread {
    private WaitTemp waitTemp;

    public WaitThreadA(WaitTemp waitTemp) {
        this.waitTemp = waitTemp;
    }

    @Override
    public void run() {
        super.run();
//        waitTemp.methodA();
        synchronized (waitTemp) {
            System.out.println("AAAAAAAAAAAA");
            try {
                waitTemp.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            waitTemp.methodA();
        }
    }
}

class WaitThreadB extends Thread {
    private WaitTemp waitTemp;

    public WaitThreadB(WaitTemp waitTemp) {
        this.waitTemp = waitTemp;
    }

    @Override
    public void run() {
        super.run();
//        waitTemp.methodB();
        synchronized (waitTemp) {
            waitTemp.methodB();
            waitTemp.notify();
        }
    }
}



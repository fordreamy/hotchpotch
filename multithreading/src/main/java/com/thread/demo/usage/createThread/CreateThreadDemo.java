package com.thread.demo.usage.createThread;

public class CreateThreadDemo extends Thread {
    @Override
    public void run() {
        super.run();
        System.out.println("CreateThreadDemo");
        System.out.println(Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        System.out.println("main 方法线程名:" + Thread.currentThread().getName());
        CreateThreadDemo threadDemo = new CreateThreadDemo();
        threadDemo.start();
        //结果输出可以看到run方法调用相对晚一点，说明线程执行时是异步的。
        System.out.println("竟然比run方法先结束");

        //加上此语句后run方法就没机会执行了，说明所有线程执行完后main方法才算结束
//        System.exit(0);
    }
}

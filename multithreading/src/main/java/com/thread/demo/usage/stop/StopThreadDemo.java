package com.thread.demo.usage.stop;

/*停止线程方法：
        1. 使用退出标志，使线程正常退出
        2. stop方法强行停止，不建议用
        3. interrupted方法中断线程*/
public class StopThreadDemo extends Thread {

    @Override
    public void run() {
        super.run();

    }

    public static void main(String[] args) {
        Thread.interrupted();
    }
}

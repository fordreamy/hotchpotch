package com.thread.demo.usage.join;

public class JoinDemo1 extends Thread {
    @Override
    public void run() {
        super.run();
        try {
            for (int i = 0; i < 5; i++) {
                System.out.println("第" + i + "次执行" + Thread.currentThread().getName());
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        JoinDemo1 joinDemo1 = new JoinDemo1();
        System.out.println("线程joinDemo1活动状态：" + joinDemo1.isAlive());
        joinDemo1.start();
        System.out.println("线程joinDemo1活动状态：" + joinDemo1.isAlive());
        joinDemo1.join();
        System.out.println("线程joinDemo1活动状态：" + joinDemo1.isAlive());
        System.out.println("最后执行,线程名字" + Thread.currentThread().getName());
    }
}

package com.thread.demo.usage.priority;

/**
 * JAVA中线程的优先级为1-10共10个等级，设置优先级小于1或者大于10都会报错
 * 线程优先级有继承性，A线程调用B线程，那么B线程的优先级就和A一样
 */
public class PriorityDemo extends Thread {
    @Override
    public void run() {
        super.run();
        System.out.println(Thread.currentThread().getName() + "优先级:" + Thread.currentThread().getPriority());
        long start = System.currentTimeMillis();
        int count = 0;
        for (int i = 0; i < 10000; i++) {
//            Thread.yield();
            count++;
        }
        System.out.println(Thread.currentThread().getName() + "用时:" + (System.currentTimeMillis() - start));
    }

    public static void main(String[] args) {
        Thread.currentThread().setPriority(6);
        System.out.println(Thread.currentThread().getName() + "优先级:" + Thread.currentThread().getPriority());
        new PriorityDemo().start();


        //优先级高的先执行，虽然创建的比低的晚
        PriorityDemo lowLevel = new PriorityDemo();
        lowLevel.setName("lowLevel");
        lowLevel.setPriority(1);
        lowLevel.start();
        PriorityDemo highLevel = new PriorityDemo();
        highLevel.setName("highLevel");
        highLevel.setPriority(10);
        highLevel.start();
    }
}

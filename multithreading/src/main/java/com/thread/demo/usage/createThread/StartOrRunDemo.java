package com.thread.demo.usage.createThread;

import java.util.Random;

public class StartOrRunDemo extends Thread {
    @Override
    public void run() {
        super.run();
        try {
            for (int i = 0; i < 10; i++) {
                int time = new Random().nextInt(1000);
                Thread.sleep(time);
                System.out.println("run=" + Thread.currentThread().getName());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName());
    }

    public static void main(String[] args) throws InterruptedException {
        StartOrRunDemo startOrRunDemo = new StartOrRunDemo();
        startOrRunDemo.setName("myThread");
//        startOrRunDemo.start();

        //如果执行startOrRunDemo.run()，就不是异步执行而是同步了。由main主线程来调用，等执行完毕后才执行后面的代码。
        startOrRunDemo.run();

        for (int i = 0; i < 10; i++) {
            int time = new Random().nextInt(1000);
            Thread.sleep(time);
            System.out.println("main=" + Thread.currentThread().getName());
        }
        //可以看到线程具有随机性，被执行先后顺序不确定。start方法通知“线程规划器”此线程已准备就绪，等待调用run方法。


    }
}

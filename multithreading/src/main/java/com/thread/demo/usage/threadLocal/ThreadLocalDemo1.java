package com.thread.demo.usage.threadLocal;

public class ThreadLocalDemo1 {
    private static ThreadLocal threadLocal = new ThreadLocal();

    public static void main(String[] args) {
        if(threadLocal.get()==null){
            threadLocal.set("值1");
            threadLocal.set("值2");
        }
        System.out.println(threadLocal.get());
        System.out.println(threadLocal.get());
        System.out.println(threadLocal.get());
        threadLocal.remove();
        System.out.println(threadLocal.get());
    }
}

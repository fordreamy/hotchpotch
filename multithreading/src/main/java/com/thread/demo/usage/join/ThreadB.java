package com.thread.demo.usage.join;

public class ThreadB extends Thread {
    @Override
    synchronized public void run() {
        try {
            System.out.println("begin 线程B " + Thread.currentThread().getName());
            Thread.sleep(5000);
            System.out.println("end 线程B " + Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

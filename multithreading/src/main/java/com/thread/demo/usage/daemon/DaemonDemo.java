package com.thread.demo.usage.daemon;

/**
 * Java线程中有两种，一种是用户线程，一种是守护线程。
 * 守护线程的典型应用是垃圾回收器GC
 */
public class DaemonDemo extends Thread {
    @Override
    public void run() {
        super.run();
        System.out.println(Thread.currentThread().getName() + "优先级:" + Thread.currentThread().getPriority());
        long start = System.currentTimeMillis();
        int count = 0;
        while (true) {
            count++;
            System.out.println("count:" + count);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        DaemonDemo daemonDemo = new DaemonDemo();
        //如果为false，则线程永远不会结束，为true则没有非守护线程了就要结束
        daemonDemo.setDaemon(true);
        daemonDemo.start();
        Thread.sleep(2000);
        System.out.println("执行到这已经没有非守护线程了，那么守护线程就要结束了");
    }
}

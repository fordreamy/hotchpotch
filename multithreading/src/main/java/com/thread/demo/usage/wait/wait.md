### wait()
当前线程等待，直到另一个线程调用notify()方法或者notifyAll()方法
### wait(long timeout)
当前线程等待，直到另一个线程调用notify()方法或者notifyAll()方法，或者指定的时间timeout已经过去。  
waite()和notify()必须在synchronized方法中或synchronized block中使用
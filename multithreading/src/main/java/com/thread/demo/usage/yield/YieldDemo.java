package com.thread.demo.usage.yield;

/**
 * yield方法是放弃当前的CPU资源，让其它任务去占用CPU的执行时间，但放弃的时间不确定，有可能刚刚放弃马上又获得CPU时间片。
 */
public class YieldDemo extends Thread {
    @Override
    public void run() {
        super.run();
        long start = System.currentTimeMillis();
        int count = 0;
        for (int i = 0; i < 10000; i++) {
//            Thread.yield();
            count++;
        }
        System.out.println(Thread.currentThread().getName() + "用时:" + (System.currentTimeMillis() - start));
    }

    public static void main(String[] args) {
        //加上Thread.yield()后用时显著增加
        new YieldDemo().start();
    }
}

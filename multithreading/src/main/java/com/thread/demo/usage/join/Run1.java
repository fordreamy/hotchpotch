package com.thread.demo.usage.join;

public class Run1 {
    public static void main(String[] args) throws InterruptedException {
        ThreadB b = new ThreadB();
        ThreadA a = new ThreadA(b);
        a.start();
        b.start();
        b.join(2000);
        System.out.println("main 方法结束"+Thread.currentThread().getName());
    }
}

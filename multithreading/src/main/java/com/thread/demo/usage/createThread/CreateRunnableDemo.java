package com.thread.demo.usage.createThread;

public class CreateRunnableDemo implements Runnable {
    @Override
    public void run() {
        System.out.println("实现Runnable接口");
        System.out.println(Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        Runnable runnable = new CreateRunnableDemo();
        Thread thread = new Thread(runnable);
        thread.start();
        System.out.println("竟然比run方法先结束");
    }
}

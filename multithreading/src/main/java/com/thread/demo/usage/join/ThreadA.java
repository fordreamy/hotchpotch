package com.thread.demo.usage.join;

public class ThreadA extends Thread {
    private ThreadB b;

    public ThreadA(ThreadB b) {
        this.b = b;
    }

    public void run() {
        try {
            synchronized (b) {
                System.out.println("begin 线程A " + Thread.currentThread().getName());
                Thread.sleep(5000);
                System.out.println("begin 线程A " + Thread.currentThread().getName());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

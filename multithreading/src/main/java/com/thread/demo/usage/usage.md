# 知识点
+ 线程的启动
+ 暂停线程
+ 停止线程
+ 线程的优先级
+ 线程安全

# 进程和线程理解
进程直观上讲就是一个应用程序，打开任务管理器看到的就是进程，进程是操作系统的基本运行单元。
线程可以理解为进程中独立运行的子任务。如QQ.exe运行时就有发送消息，下载文件，视频聊天等同时运行。

# 多线程优点
最大限度利用cpu空闲时间处理更多的任务，多线程是异步的。

# 创建多线程
实现多线程的方式主要有两种，一种是继承Thread类，另一种是实现Runnable接口。
Thread类实现了Runnable接口，Thread方式创建线程的局限是不支持多继承，为了支持多继承可以用实现Runnable接口的方式，这两种方式工作性质是一样的没有本质的区别

package com.thread.demo;

public class MyThread extends Thread {
    @Override
    public void run() {
        super.run();
        System.out.println("通过继承Thread创建线程");
    }

    public static void main(String[] args) {
        MyThread t = new MyThread();
        t.start();
        System.out.println("线程执行的慢，先执行1");
        new MyThread().start();
        System.out.println("线程执行的慢，先执行2");
    }
}

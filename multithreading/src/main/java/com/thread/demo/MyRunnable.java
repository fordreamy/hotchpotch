package com.thread.demo;

public class MyRunnable implements Runnable {
    @Override
    public void run() {
        try {
            Thread.sleep(2000);
            System.out.println("通过实现Runnable接口创建线程");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(new MyRunnable());
        t1.start();
        new Thread(new MyRunnable()).start();
        System.out.println("1");
        System.out.println("2");
        new Thread(new MyRunnable()).start();
    }
}

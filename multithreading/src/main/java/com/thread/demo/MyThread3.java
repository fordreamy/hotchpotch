package com.thread.demo;

/**
 * 每个线程共享数据count,只创建了一个自定义线程实例,同时处理count会产生非线程安全问题,打印结果不是递减的
 * 解释：某些JVM中，i--会有三个步骤，1:取得原有i值，2:计算i-1，3:赋值给i
 * 方法：加上synchronized关键字，一个线程执行完run方法后另一个线程才能执行run方法，排队执行。
 */
public class MyThread3 extends Thread {
    private int count = 5;

    @Override
    synchronized public void run() {
        super.run();
        count--;
        System.out.println(this.currentThread().getName() + "计数,count=" + count);
    }

    public static void main(String[] args) {
        MyThread3 thread3 = new MyThread3();
        Thread a = new Thread(thread3, "A");
        Thread b = new Thread(thread3, "B");
        a.start();
        b.start();
        new Thread(thread3,"C").start();
        new Thread(thread3,"D").start();
        new Thread(thread3,"E").start();
    }
}

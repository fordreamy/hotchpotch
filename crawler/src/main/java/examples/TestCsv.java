package examples;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import org.h2.tools.Csv;
import org.h2.tools.SimpleResultSet;

public class TestCsv {

	public void writeToCsv() throws Exception {
		SimpleResultSet rs = new SimpleResultSet();
		rs.addColumn("NAME", Types.VARCHAR, 255, 0);
		rs.addColumn("EMAIL", Types.VARCHAR, 255, 0);
		rs.addRow("Li Lei", "bob.meier@abcde.abc");
		rs.addRow("Han Meimei", "john.jones@abcde.abc");
		new Csv().write("data/test.csv", rs, null);
		rs.close();
	}

	public void readFromCsv() throws Exception {
		ResultSet rs = new Csv().read("data/test.csv", null, null);
		ResultSetMetaData meta = rs.getMetaData();
		while (rs.next()) {
			for (int i = 0; i < meta.getColumnCount(); i++) {
				System.out.println(meta.getColumnLabel(i + 1) + ": " + rs.getString(i + 1));
			}
			System.out.println();
		}
		rs.close();
	}

	public static void main(String[] args) throws Exception {
		new TestCsv().writeToCsv();
		new TestCsv().readFromCsv();
	}
}
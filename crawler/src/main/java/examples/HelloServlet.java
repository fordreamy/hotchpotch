package examples;

import java.io.*;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import initDB.initH2.entity.User;
import utils.SessionFactoryUtils;

public class HelloServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private SessionFactory sessionFactory = SessionFactoryUtils.getSessionFactory();

	@SuppressWarnings("unchecked")
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		response.setContentType("text/html");

		Session session = this.sessionFactory.openSession();

		String name = "";
		String query = "FROM User";
		List<User> list = session.createQuery(query).list();
		for (User user : list) {
			name += user.getUserName() + ",";
		}

		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Hello World!</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>Hello " + name + "</h1>");
		out.println("</body>");
		out.println("</html>");
	}
}
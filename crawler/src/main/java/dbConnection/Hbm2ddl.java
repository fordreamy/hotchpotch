package dbConnection;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import initDB.initH2.entity.User;

public class Hbm2ddl {

	private static SessionFactory sessionFactory = null;
	private static Configuration cfg = null;
	private static ServiceRegistry serviceRegistry = null;

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		cfg = new Configuration().configure();
		serviceRegistry = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties()).build();
		sessionFactory = cfg.buildSessionFactory(serviceRegistry);

		Session session = sessionFactory.openSession();

		session.beginTransaction();
		session.save(new User());
		session.getTransaction().commit();

		session = sessionFactory.openSession();
		String hql = "from User";
		Query query = session.createQuery(hql);
		List<User> result = query.list();

		for (User event : (List<User>) result) {
			System.out.println(event.getUserName());
		}
	}

}

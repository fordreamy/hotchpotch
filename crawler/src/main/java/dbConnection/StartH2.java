package dbConnection;

import java.io.IOException;

public class StartH2 {

	public static void main(String[] args) throws IOException {

		String cmd = "cmd /c cd D:/maven-repository/repository/com/h2database/h2/1.4.193 && start java -jar h2-1.4.193.jar -tcpAllowOthers";
		Runtime.getRuntime().exec(cmd);
		
	}

}

package getFundInfo;

import java.io.*;
import java.util.Date;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import utils.DateUtils;

@WebServlet(urlPatterns = "/fundAnalysisController")
public class FundAnalysisController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private FundAnalysisService fas = new FundAnalysisService();

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("application/json; charset=utf-8");
		PrintWriter pWriter = response.getWriter();
		int action = Integer.valueOf(request.getParameter("action"));
		String returnJson = null;
		String fundCode = null;
		switch(action){
		case 0:
			fundCode = request.getParameter("fundCode");
			returnJson = GetFundInfo.danWeiJingZhi("http://fund.eastmoney.com/pingzhongdata/" + fundCode + ".js");
			break;
		case 1:
			fundCode = request.getParameter("fundCode");
			returnJson = fas.getFundByCode(fundCode);
			break;
		case 2:
			returnJson = fas.getAllFundName();
			break;
		case 3:
			returnJson = fas.getAllFundDif("","");
			break;
		case 4:
			//returnJson = fas.getMinFund(DateUtils.dateToString(new Date(), "yyyy-MM-dd"), 10);
			returnJson = fas.getMinFund("2017-11-10", 10);
			break;
		}
		pWriter.write(returnJson);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

	public static void main(String[] args) {
	}
}
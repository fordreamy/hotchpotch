package getFundInfo;

import java.io.IOException;
import java.util.Date;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import utils.DateUtils;

public class GetFundInfo {
	
	/**
	 * 
	 * @param url 例:http://fund.eastmoney.com/pingzhongdata/164402.js
	 * @return 基金净值信息,格式[["2017-01-03",0.897],["2017-01-04",0.904]]
	 */
	public static String danWeiJingZhi(String url){
		String rsponse = GetJson.getResponse(url);
		if(rsponse == "" || !rsponse.contains("单位净值走势")) return "";
		String qianHaiJunGong = null;
		qianHaiJunGong = rsponse.substring(rsponse.indexOf("Data_netWorthTrend"), rsponse.indexOf("/*累计净值走势*/"));

		qianHaiJunGong = qianHaiJunGong.substring(qianHaiJunGong.indexOf("["), qianHaiJunGong.lastIndexOf(";"));
		JSONArray jsonArray = JSONArray.fromObject(qianHaiJunGong);
		JSONObject jsonObject = null;
		JSONArray chartArray = new JSONArray();
		JSONArray dataArray = new JSONArray();
		for (int i = 0; i < jsonArray.size(); i++) {
			jsonObject = jsonArray.getJSONObject(i);
			dataArray = new JSONArray();
			dataArray.add(DateUtils.dateToString(new Date((long) jsonObject.get("x")), "yyyy-MM-dd"));
			dataArray.add(jsonObject.get("y"));
			chartArray.add(dataArray);
		}
		qianHaiJunGong = chartArray.toString();
		return qianHaiJunGong;
	}

	public static void main(String[] args) throws IOException {
		System.out.println(danWeiJingZhi("http://fund.eastmoney.com/pingzhongdata/750007.js"));;
		
	}
}

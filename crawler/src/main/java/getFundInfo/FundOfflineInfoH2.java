package getFundInfo;

import java.io.*;
import java.util.List;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import org.hibernate.Session;

import entities.NetAssertValue;
import initDB.initH2.SessionFactoryUtils;
import net.sf.json.JSONArray;

@WebServlet(urlPatterns = "/fundOfflineInfoH2")
public class FundOfflineInfoH2 extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setContentType("application/json; charset=utf-8");
		PrintWriter pWriter = response.getWriter();
		String fundCode = request.getParameter("fundCode");
		Session session = SessionFactoryUtils.getSessionFactory().openSession();
		String query = "FROM NetAssertValue a where a.fundCode = '" + fundCode + "'";
		@SuppressWarnings("unchecked")
		List<NetAssertValue> list = session.createQuery(query).list();
		JSONArray jsonArray = new JSONArray();
		JSONArray tmp = new JSONArray();
		for (NetAssertValue value : list) {
			tmp = new JSONArray();
			tmp.add(value.getFundDate());
			tmp.add(value.getNetAssertValue());
			jsonArray.add(tmp);
		}

		pWriter.write(jsonArray.toString());
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}

	public static void main(String[] args) {
		System.out.println("hello world");
	}
}
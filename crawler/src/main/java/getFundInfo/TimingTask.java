package getFundInfo;

import java.util.Timer;
import java.util.TimerTask;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class TimingTask {
	public static void main(String[] args) {
		TimingTask mt2 = new TimingTask();
		mt2.task();
	}

	public static boolean isSend = false;

	public void task() {
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				System.out.println("执行任务ing");
				String json = GetJson.getResponse("http://211.88.38.127:7180/api/v12/clusters");
				JSONObject jsonObject = new JSONObject();
				JSONArray jsonArray = (JSONArray) JSONObject.fromObject(json).get("items");
				jsonObject = (JSONObject) jsonArray.get(0);
				if ("BAD_HEALTH".equals(jsonObject.get("entityStatus"))) {
					if (!isSend) {
						System.out.println(" BAD_HEALTH!");
						SendMail.send(json);
					}
					isSend = true;
				}else{
					isSend = false;
				}
			}
		};

		// task - 所要安排的任务。
		// delay - 执行任务前的延迟时间，单位是毫秒。
		// period - 执行各后续任务之间的时间间隔，单位是毫秒。
		Timer timer = new Timer();
		long delay = 0;
		long intevalPeriod = 1 * 1000 * 10;
		timer.scheduleAtFixedRate(task, delay, intevalPeriod);
	}
}

package getFundInfo;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import utils.MysqlSessionFactoryUtils;
import entities.FundNetAssertValue;

public class FundUtils{

	@SuppressWarnings("unchecked")
	public static List<FundNetAssertValue> getFundByCode(String fundCode){
		Session session = MysqlSessionFactoryUtils.getSessionFactory().openSession();
		String sql = "SELECT b.fund_date AS fundDate, b.fund_code AS fundCode,b.NET_ASSERT_VALUE AS netAssertValue,a.fund_name AS fundName,a.fund_short_name AS fundShortName "
				   + "FROM fund_info a,net_assert_value b "
				   + "WHERE a.FUND_CODE = b.FUND_CODE AND a.FUND_CODE='"+fundCode+"'";
		Query query = session.createSQLQuery(sql).setResultTransformer(Transformers.aliasToBean(FundNetAssertValue.class));
		List<FundNetAssertValue> list = query.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		Session session = MysqlSessionFactoryUtils.getSessionFactory().openSession();
		String sql = "SELECT a.fund_code as fundCode,a.fund_name as fundName,a.fund_short_name as fundShortName FROM fund_info a";
		Query query = session.createSQLQuery(sql).setResultTransformer(Transformers.aliasToBean(FundNetAssertValue.class));
		System.out.println(query.list().get(0).getClass());
		List<FundNetAssertValue> list = query.list();
		for(int i=0;i<list.size();i++){
			System.out.println(list.get(i).getFundName());
		}
		getFundByCode("590003");
		
	}
}
package initDB.initOracle;

import java.sql.Connection;

import initDB.DBConnection;
import initDB.DBScripRunner;

/**
 * 初始化数据库
 * 
 * Oracle驱动版本
 * JDBC Driver & UCP Downloads - 12c Release 1  
 *　　Oracle Database 12c Release 1 (12.1.0.2) drivers
 *　　Oracle Database 12c Release 1 (12.1.0.1) drivers  
 * JDBC Driver Downloads - 11g  
 *　　Oracle Database 11g Release 2 (11.2.0.4), (11.2.0.3), (11.2.0.2.0), (11.2.0.1.0) drivers  
 *　　Oracle Database 11g Release 1 (11.1.0.7), (11.1.0.6) drivers  
 * Universal Connection Pool (UCP) - 11g  
 *　　Oracle Database 11g Release 2 (11.2.0.4), (11.2.0.3), (11.2.0.2), (11.2.0.1.0)  
 * Older JDBC Driver Downloads - 10g (Unsupported)  
 *　　Oracle Database 10g Release 2 (10.2.0.5), (10.2.0.4), (10.2.0.3), (10.2.0.2), (10.2.0.1.0) drivers  
 *
 * ojdbc14.jar (1,569,316 bytes) - classes for use with JDK 1.4 and 1.5
 * ojdbc6.jar (1,988,051 bytes) - Classes for use with JDK 1.6. It contains the JDBC driver classes except classes for NLS support in Oracle Object and Collection types.
 * ojdbc7.jar (3,397,734 bytes) - Certified with JDK 7 and JDK 8; It contains the JDBC driver classes except classes for NLS support in Oracle Object and Collection types.
 */

public class InitOracle {

	public static void main(String[] args) throws Exception {
		System.out.println("类根目录:" + ClassLoader.getSystemResource(""));
		System.out.println("当前类路径:" + InitOracle.class.getResource("").getPath()); // Class文件所在路径

		String filePath = InitOracle.class.getResource("").getPath() + "initOracle.sql";

		Connection conn = null;
		try {
			conn = DBConnection.getConnection("oracle.jdbc.driver.OracleDriver","jdbc:oracle:thin:@210.25.24.55:1521:myorcl", "ywxt2", "ywxt2");
		} catch (Exception e) {
			System.out.println("oracle链接失败!");
			e.printStackTrace();
		}
		DBScripRunner.sqlBatch(filePath, conn);
	}

}

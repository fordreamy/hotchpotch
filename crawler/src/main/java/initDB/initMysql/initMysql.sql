/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.6.26 : Database - test
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mysqlDB1` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `mysqlDB1`;

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `role_id` varchar(32) NOT NULL COMMENT '角色id',
  `role_code` varchar(32) DEFAULT NULL COMMENT '角色编码',
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `role` */

insert  into `role`(`role_id`,`role_code`,`role_name`) values ('1','rolecode1','管理员'),('4c46336a6d9741db913d7522bd61c521','rolecode2','总经理'),('5142ac505bd8456fb668af6dc4dfb8e2','rolecode3','部门经理');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` varchar(32) NOT NULL COMMENT '用户id',
  `user_code` varchar(32) DEFAULT NULL COMMENT '用户编码',
  `user_name` varchar(100) DEFAULT NULL COMMENT '用户名称',
  `user_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '用户标识符',
  `user_password` varchar(32) DEFAULT NULL COMMENT '密码',
  `user_age` int(11) DEFAULT NULL COMMENT '年龄',
  `user_sex` varchar(1) DEFAULT NULL COMMENT '性别',
  `user_email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `salt` varchar(32) DEFAULT NULL COMMENT '密码盐',
  `createTime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`user_id`,`user_code`,`user_name`,`user_status`,`user_password`,`user_age`,`user_sex`,`user_email`,`salt`,`createTime`) values ('0d20c11624ce4154ac156a47415e82ba','admin','admin',1,'64ba9ee1df6c3111f2ab0e675d648778',21,'1','admin@163.com','0d6cb6b016992b911c604716ed9c81d5','2017-01-19 18:34:00'),('c0a2d7b1363a4faebe8c82cf4537b99a','11111','11111',1,'d97ec5385f5412385298cd2fdb480518',11,'1','1','9295ba818014872e66465d3dcb6d2a7c',NULL);

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `user_role_id` varchar(32) NOT NULL COMMENT '用户角色id',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `role_id` varchar(32) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`user_role_id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE,
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_role` */

insert  into `user_role`(`user_role_id`,`user_id`,`role_id`) values ('1b359fa2e1164cb8875ac0c35504d70e','0d20c11624ce4154ac156a47415e82ba','1'),('74e2b79e25c5404a8b8339b494b396f5','c0a2d7b1363a4faebe8c82cf4537b99a','4c46336a6d9741db913d7522bd61c521'),('c3ba6a188f0d490694b9c986ff3833d4','c0a2d7b1363a4faebe8c82cf4537b99a','1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

package initDB.initH2.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

	@Id
	@Column(name = "user_id")
	private String userId;

	@Column(name = "user_code")
	private String userCode;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "user_status")
	private int userStatus;

	@Column(name = "user_password")
	private String userPassword;

	@Column(name = "user_age")
	private int userAge;

	@Column(name = "user_sex")
	private int userSex;

	@Column(name = "user_email")
	private String userEmail;

	@Column(name = "salt")
	private String salt;

	
	public User() {
	}

	public User(String userId, String userCode, String userName, int userStatus, String userPassword, int userAge,
			int userSex, String userEmail, String salt) {
		super();
		this.userId = userId;
		this.userCode = userCode;
		this.userName = userName;
		this.userStatus = userStatus;
		this.userPassword = userPassword;
		this.userAge = userAge;
		this.userSex = userSex;
		this.userEmail = userEmail;
		this.salt = salt;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public int getUserAge() {
		return userAge;
	}

	public void setUserAge(int userAge) {
		this.userAge = userAge;
	}

	public int getUserSex() {
		return userSex;
	}

	public void setUserSex(int userSex) {
		this.userSex = userSex;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userCode=" + userCode + ", userName=" + userName + ", userStatus="
				+ userStatus + ", userPassword=" + userPassword + ", userAge=" + userAge + ", userSex=" + userSex
				+ ", userEmail=" + userEmail + ", salt=" + salt + "]";
	}

}

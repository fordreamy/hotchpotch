package initDB.initH2.dbConnection;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class Hbm2ddl {

	private static SessionFactory sessionFactory = null;
	private static Configuration cfg = null;
	private static ServiceRegistry serviceRegistry = null;

	public static void main(String[] args) {

		cfg = new Configuration().configure();
		serviceRegistry = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties()).build();
		sessionFactory = cfg.buildSessionFactory(serviceRegistry);

		org.hibernate.Session session = sessionFactory.openSession();

		System.out.println(session.toString());
	}

}

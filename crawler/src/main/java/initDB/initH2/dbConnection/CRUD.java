package initDB.initH2.dbConnection;

import java.sql.*;

/**
 *通过JDBC连接数据库，执行增删改查操作 
 * @author psp
 *
 */
public class CRUD {

	public void create(String sql) throws Exception {
		Connection conn = DBConnection.getConnection();
		conn.createStatement().execute(sql);
		conn.close();
	}

	public void retrieve(String sql) throws Exception {
		Connection conn = DBConnection.getConnection();
		Statement stmt = conn.createStatement();
		ResultSet rset = stmt.executeQuery(sql);
		while (rset.next()) {
			System.out.println(rset.getString(1));
			System.out.println(rset.getString(2));
		}
	}

	public void delete(String sql) throws Exception {
		Connection conn = DBConnection.getConnection();
		Statement stmt = conn.createStatement();
		stmt.execute(sql);
	}

	public void update(String sql) {

	}

	public void insert(String sql) throws Exception {
		Connection conn = DBConnection.getConnection();
		Statement stmt = conn.createStatement();
		stmt.execute(sql);
	}

	public static void main(String[] args) throws Exception {
		/*new CRUD().create("DROP TABLE IF EXISTS TEST;CREATE TABLE TEST(ID INT,NAME VARCHAR(255));INSERT INTO TEST VALUES(1, 'Hello');");
		new CRUD().insert("INSERT INTO TEST VALUES(2, 'world');");
		new CRUD().retrieve("select * from test");*/
		//new CRUD().delete("delete from test");

	}

}

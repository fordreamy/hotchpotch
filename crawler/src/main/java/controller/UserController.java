package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

import initDB.initH2.SessionFactoryUtils;
import initDB.initH2.entity.User;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@javax.servlet.annotation.WebServlet(urlPatterns = "/user")
public class UserController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json; charset=utf-8");
		PrintWriter pWriter = response.getWriter();

		JSONObject jsonObject = new JSONObject();
		String query = "FROM User";
		Session session =SessionFactoryUtils.getSessionFactory().openSession();
		@SuppressWarnings("unchecked")
		List<User> list = session.createQuery(query).list();
		System.out.println("用户数据---------------------------");
		for (User user : list) {
			System.out.println(user.toString());
		}
		
		jsonObject.put("data", JSONArray.fromObject(list));
		String jsonstr = jsonObject.toString();

		pWriter.write(jsonstr);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}

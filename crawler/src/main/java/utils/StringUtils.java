package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringUtils {
	
	/**
	 * 
	 * @param strDate
	 * @param pattern  格式如:yyyy-MM-dd hh:mm:ss:SSS 年月日时分秒毫秒
	 * @return
	 */
	public static Date stringToDate(String strDate,String pattern){
		Date date = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		try {
			date = simpleDateFormat.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	public static void main(String[] args) {
		System.out.println(stringToDate("2017-12-15 13:11:11","yyyy-MM-dd HH:mm:ss"));
	}
}

package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fund_info")
public class FundInfo {

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "fund_code")
	private String fundCode;

	@Column(name = "fund_name")
	private String fundName;
	
	@Column(name = "fund_short_name")
	private String fundShortName;

	@Column(name = "fund_type")
	private String fund_type;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getFundShortName() {
		return fundShortName;
	}

	public void setFundShortName(String fundShortName) {
		this.fundShortName = fundShortName;
	}

	public String getFund_type() {
		return fund_type;
	}

	public void setFund_type(String fund_type) {
		this.fund_type = fund_type;
	}

	
	
}

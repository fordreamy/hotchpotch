package entities;

public class FundNetAssertValue {

	private String id;
	private String fundCode;
	private String fundDate;
	private double netAssertValue;
	private double dif;
	private String fundName;
	private String fundShortName;
	private String fundType;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public String getFundDate() {
		return fundDate;
	}
	public void setFundDate(String fundDate) {
		this.fundDate = fundDate;
	}
	public double getNetAssertValue() {
		return netAssertValue;
	}
	public void setNetAssertValue(double netAssertValue) {
		this.netAssertValue = netAssertValue;
	}
	public String getFundName() {
		return fundName;
	}
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	public String getFundShortName() {
		return fundShortName;
	}
	public void setFundShortName(String fundShortName) {
		this.fundShortName = fundShortName;
	}
	public String getFundType() {
		return fundType;
	}
	public void setFundType(String fundType) {
		this.fundType = fundType;
	}
	public double getDif() {
		return dif;
	}
	public void setDif(double dif) {
		this.dif = dif;
	}
	
}

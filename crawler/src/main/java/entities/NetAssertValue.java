package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "NET_ASSERT_VALUE")
public class NetAssertValue {

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "fund_code")
	private int fundCode;

	@Column(name = "fund_date")
	private String fundDate;

	@Column(name = "net_assert_value")
	private double netAssertValue;

	
	public NetAssertValue() {
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public int getFundCode() {
		return fundCode;
	}


	public void setFundCode(int fundCode) {
		this.fundCode = fundCode;
	}


	public String getFundDate() {
		return fundDate;
	}


	public void setFundDate(String fundDate) {
		this.fundDate = fundDate;
	}


	public double getNetAssertValue() {
		return netAssertValue;
	}


	public void setNetAssertValue(double netAssertValue) {
		this.netAssertValue = netAssertValue;
	}

}

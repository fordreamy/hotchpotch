import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class T1 {
    @Test
    public void Tests() {
        assertEquals("makeReadable(0)", "00:00:00", HumanReadableTime.makeReadable(0));
        assertEquals("makeReadable(5)", "00:00:05", HumanReadableTime.makeReadable(5));
        assertEquals("makeReadable(60)", "00:01:00", HumanReadableTime.makeReadable(60));
        assertEquals("makeReadable(86399)", "23:59:59", HumanReadableTime.makeReadable(86399));
        assertEquals("makeReadable(359999)", "99:59:59", HumanReadableTime.makeReadable(359999));
    }

    public static void main(String[] args) {
    }
}

class HumanReadableTime {
    public static String makeReadable(int seconds) {
        int h;
        int m;
        int s;
        String r = "";
        h = seconds / 60 / 60;
        m = (seconds - h * 60 * 60) / 60;
        s = seconds - h * 60 * 60 - m * 60;
        if (h > 9) r += h + ":";
        if (h <= 9 && h > 0) r += "0" + h + ":";
        if (h == 0) r += "00:";

        if (m > 9) r += m + ":";
        if (m <= 9 && m > 0) r += "0" + m + ":";
        if (m == 0) r += "00:";

        if (s > 9) r += s;
        if (s <= 9 && s > 0) r += "0" + s;
        if (s == 0) r += "00";
        return r;
    }
}

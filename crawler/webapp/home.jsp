<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
</head>
<body>

	<a href="<c:url value="/helloWorld"/>">hello World</a>
	<a href="<c:url value="/helloJson"/>">hello Json</a>
	<a href="<c:url value="/pages/fundChartOnline.html"/>">在线基金图</a>
	<a href="<c:url value="/pages/queryFund.html"/>">查询基金</a>
	<a href="<c:url value="/pages/fundChartOfflineH2.html"/>">基金图离线h2</a>
	<a href="<c:url value="/pages/fundChartOfflineMysql.html"/>">基金图离线mysql</a>
	<a href="console">H2控制台</a>

</body>
</html>

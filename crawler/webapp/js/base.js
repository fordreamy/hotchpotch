
function loadXMLDoc(url, cfunc) {
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			cfunc();
		}
	};
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

function renderChart(divId, title, data) {
	var Odiv = document.createElement("div");
	Odiv.id = divId;
	Odiv.style.width = "100%";
	Odiv.style.height = "250px";
	document.body.appendChild(Odiv);
	var myChart = echarts.init(document.getElementById(Odiv.id));
	myChart.setOption(option = {
		title : {
			text : title
		},
		tooltip : {
			trigger : 'axis'
		},
		xAxis : {
			data : data.map(function(item) {
				return item[0];
			})
		},
		yAxis : {
			splitLine : {
				show : false
			}
		},
		toolbox : {
			left : 'center',
			feature : {
				dataZoom : {
					yAxisIndex : 'none'
				},
				restore : {},
				saveAsImage : {}
			}
		},
		dataZoom : [ {
			startValue : '2014-06-01'
		}, {
			type : 'inside'
		} ],
		visualMap : {
			top : 10,
			right : 10,
			pieces : [ {
				gt : 0,
				lte : 0.5,
				color : '#ff9933'
			}, {
				gt : 0.5,
				lte : 1,
				color : '#ffde33'
			}, {
				gt : 1,
				lte : 1.5,
				color : '#096'
			}, {
				gt : 1.5,
				lte : 2,
				color : '#cc0033'
			}, {
				gt : 2.5,
				lte : 3,
				color : '#660099'
			}, {
				gt : 3,
				color : '#7e0023'
			} ],
			outOfRange : {
				color : '#999'
			}
		},
		series : {
			name : title,
			type : 'line',
			data : data.map(function(item) {
				return item[1];
			}),
			markLine : {
				silent : true,
				data : [ {
					yAxis : 0.5
				}, {
					yAxis : 1
				}, {
					yAxis : 1.5
				}, {
					yAxis : 2
				}, {
					yAxis : 2.5
				} ]
			}
		}
	});
}

function renderCharts(result){
	for (var k = 0; k < result.length; k++) {
		var Odiv = document.createElement("div");
		Odiv.id = result[k].shortName;
		Odiv.style.width = "100%";
		Odiv.style.height = "250px";
		document.body.appendChild(Odiv);
		var myChart = echarts.init(document.getElementById(Odiv.id));
		var data = result[k].data;
		var title = result[k].name;
		myChart.setOption(option = {
			title : {
				text : title
			},
			tooltip : {
				trigger : 'axis'
			},
			xAxis : {
				data : data.map(function(item) {
					return item[0];
				})
			},
			yAxis : {
				splitLine : {
					show : false
				}
			},
			toolbox : {
				left : 'center',
				feature : {
					dataZoom : {
						yAxisIndex : 'none'
					},
					restore : {},
					saveAsImage : {}
				}
			},
			dataZoom : [ {
				startValue : '2014-06-01'
			}, {
				type : 'inside'
			} ],
			visualMap : {
				top : 10,
				right : 10,
				pieces : [ {
					gt : 0,
					lte : 0.5,
					color : '#ff9933'
				}, {
					gt : 0.5,
					lte : 1,
					color : '#ffde33'
				}, {
					gt : 1,
					lte : 1.5,
					color : '#096'
				}, {
					gt : 1.5,
					lte : 2,
					color : '#cc0033'
				}, {
					gt : 2.5,
					lte : 3,
					color : '#660099'
				}, {
					gt : 3,
					color : '#7e0023'
				} ],
				outOfRange : {
					color : '#999'
				}
			},
			series : {
				name : title,
				type : 'line',
				data : data.map(function(item) {
					return item[1];
				}),
				markLine : {
					silent : true,
					data : [ {
						yAxis : 0.5
					}, {
						yAxis : 1
					}, {
						yAxis : 1.5
					}, {
						yAxis : 2
					}, {
						yAxis : 2.5
					} ]
				}
			}
		});
	}
}
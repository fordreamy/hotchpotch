var pathname = document.location.pathname;
var projectName = pathname.substr(0, pathname.substr(1).indexOf("/") + 1);
document.write("<script src='" + projectName + "/js/base.js'></script>");

function getFundByCode(url, fundCode) {
	url = projectName + '/' + url + "&&fundCode=" + fundCode;
	$.getJSON(url, function(result) {
		renderChart(result.fundShortName, result.fundName, result.data);
	});
}

function fundChartOnline(url, code,name) {
		var u = projectName + '/' + url + "&&fundCode=" + code;
		$.getJSON(u, function(result) {
			renderChart("id_" + Math.random() * 10, name, result);
		});
}

function fundMin(url) {
	var uploadedDataURL = projectName + '/' + url;
	$.getJSON(uploadedDataURL, function(result) {
		renderCharts(result);
	});

}

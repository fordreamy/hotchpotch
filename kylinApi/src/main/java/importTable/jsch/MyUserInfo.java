package importTable.jsch;

import com.jcraft.jsch.UserInfo;

public class MyUserInfo implements UserInfo {
    public String getPassphrase() {
        System.out.println("getPassphrase");
        return null;
    }

    public String getPassword() {
        System.out.println("getPassword");
        return null;
    }

    public boolean promptPassword(String s) {
        System.out.println("promptPassword:" + s);
        return false;
    }

    public boolean promptPassphrase(String s) {
        System.out.println("promptPassphrase:" + s);
        return false;
    }

    public boolean promptYesNo(String s) {
        System.out.println("promptYesNo:" + s);
        return true;//notice here!
    }

    public void showMessage(String s) {
        System.out.println("showMessage:" + s);
    }
}
package importTable.jsch;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

import java.io.IOException;

public class JschConsole {
    static String hostname = "211.88.20.70";
    static String userName = "root";
    static String password = "Root#$2017";
    static int DEFAULT_SSH_PORT = 22;

    public static void main(String[] args) throws IOException, InterruptedException {
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(userName, hostname, DEFAULT_SSH_PORT);
            session.setPassword(password);
            session.setUserInfo(new MyUserInfoInner());
            session.setConfig("StrictHostKeyChecking", "no");

            session.connect(30000);   // making a connection with timeout.
            Channel channel = session.openChannel("shell");
            channel.setInputStream(System.in);
            channel.setOutputStream(System.out);
            channel.connect(3 * 1000);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

class MyUserInfoInner implements UserInfo {
    public String getPassphrase() {
        System.out.println("getPassphrase");
        return null;
    }

    public String getPassword() {
        System.out.println("getPassword");
        return null;
    }

    public boolean promptPassword(String s) {
        System.out.println("promptPassword:" + s);
        return false;
    }

    public boolean promptPassphrase(String s) {
        System.out.println("promptPassphrase:" + s);
        return false;
    }

    public boolean promptYesNo(String s) {
        System.out.println("promptYesNo:" + s);
        return true;//notice here!
    }

    public void showMessage(String s) {
        System.out.println("showMessage:" + s);
    }
};
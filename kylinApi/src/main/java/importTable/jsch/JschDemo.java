package importTable.jsch;

import com.jcraft.jsch.*;
import importTable.jsch.JschThread;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

public class JschDemo {
    static String hostname = "211.88.20.70";
    static String userName = "root";
    static String password = "Root#$2017";
    static int DEFAULT_SSH_PORT = 22;

    public static void main(String[] args) throws IOException, InterruptedException {

        String importCmd1 = "export JAVA_HOME=/opt/jdk1.8.0_111;sqoop import --hive-import " +
                "--connect jdbc:oracle:thin:@210.25.24.55:1521:myorcl --username temp --password temp " +
                "--hive-overwrite --table HIVEINSERT " +
                "--hive-table test1  " +
                "--hive-database test -m 1 --fields-terminated-by \"\\001\"; ";
        String importCmd2 = "export JAVA_HOME=/opt/jdk1.8.0_111;" +
                "sqoop import --hive-import " +
                "--connect jdbc:oracle:thin:@210.25.24.55:1521:myorcl --username temp --password temp " +
                "--hive-overwrite --table HIVEINSERT2 " +
                "--hive-table test2  " +
                "--hive-database test -m 1 --fields-terminated-by \"\\001\"; ";
        new JschThread(importCmd1, hostname, userName, password).start();
        new JschThread(importCmd2, hostname, userName, password).start();

    }
}





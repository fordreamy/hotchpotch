package importTable;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;

public class CommandUtils {
    // 字符编码默认是utf-8
    private static String DEFAULTCHART = "UTF-8";

    public static Connection conn = null;
    public static Session session = null;

    public static void initSession(String hostname, String username, String password) {
        boolean isAuthenticated = false;

        try {
            if (conn == null) {
                conn = new Connection(hostname);
                conn.connect();
            }
            isAuthenticated = conn.authenticateWithPassword(username, password);
            if (session == null) {
                session = conn.openSession();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isAuthenticated == false) {
            System.out.println("登录失败!");
        }
        System.out.println("登录" + hostname + "成功!用户:" + username + " 密码" + password);

    }

    public static void close() {
        if (conn != null) {
            System.out.println("关闭连接!");
            conn.close();
            conn = null;
        }
        if (session != null) {
            System.out.println("关闭session!");
            session.close();
            session = null;
        }
    }

    /**
     * 解析脚本执行返回的结果集
     *
     * @param in      输入流对象
     * @param charset 编码
     * @return 以纯文本的格式返回
     * @author Ickes
     * @since V0.1
     */
    private static String processStdout(InputStream in, String charset) {
        InputStream stdout = new StreamGobbler(in);
        StringBuffer buffer = new StringBuffer();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(stdout, charset));
            String line = null;
            while ((line = br.readLine()) != null) {
                buffer.append(line + "\n");
            }
            br.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }

    /**
     * ss
     * @param cmd
     */
    public static void execCommand(String cmd) {
        String result = "";
        String stderr = "";
        String standout = "";
        try {
            session.execCommand(cmd);// 执行命令
            standout = processStdout(session.getStdout(), DEFAULTCHART);
            System.out.println("state:" + session.getState());
            System.out.println("ExitStatus:" + session.getExitStatus());
            System.out.println("Stdout:" + standout);
            if (standout.equals("")) {
                stderr = processStdout(session.getStderr(), DEFAULTCHART);
                if (!stderr.equals("")) {
                    System.out.println("出错了! 异常信息-> " + stderr);
                } else {
                    System.out.println("执行成功!");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
		/*CommandUtils.initSession("211.88.20.111", "root", "Ciecc@2016");
		System.out.println(CommandUtils.execute("uname -a && date && uptime && mkdir /tmp/testtest/t1"));
		CommandUtils.close();*/
        String importCmd1 = "export JAVA_HOME=/opt/jdk1.8.0_111;sqoop import --hive-import " +
                "--connect jdbc:oracle:thin:@210.25.24.55:1521:myorcl --username temp --password temp " +
                "--hive-overwrite --table HIVEINSERT " +
                "--hive-table test1  " +
                "--hive-database test -m 1 --fields-terminated-by \"\\001\" ";
        CommandUtils.initSession("211.88.20.70", "root", "Root#$2017");
        CommandUtils.execCommand(importCmd1);
        CommandUtils.close();


    }

}

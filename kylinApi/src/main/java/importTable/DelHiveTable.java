package importTable;

import ch.ethz.ssh2.ChannelCondition;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DelHiveTable {
    public static void main(String[] args) {

        String cmd;
        String hostname = "211.88.20.70";
        String userName = "root";
        String password = "Root#$2017";
        String delHiveTable1 = "hive -e 'use test;drop table test1';";
        String delHiveTable2 = "hive -e 'use test;drop table test2';";

        ExecutorService exe = Executors.newFixedThreadPool(2);
        exe.execute(new DumbPTYThead(delHiveTable1, hostname, userName, password));
        exe.execute(new DumbPTYThead(delHiveTable2, hostname, userName, password));
        exe.shutdown();
        while (true) {
            if (exe.isTerminated()) {
                System.out.println("删除hive表的所有线程结束了！");
                break;
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

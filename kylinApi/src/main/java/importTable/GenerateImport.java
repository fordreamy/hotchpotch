package importTable;

import java.io.FileWriter;
import java.io.IOException;

public class GenerateImport {

    //static String filePath = "C:\\\\Users\\\\ps\\\\Desktop\\\\hgqkj\\\\hqqkj_1-10\\\\generateImport.txt";
    static String filePath = "C:\\\\Users\\\\ps\\\\Desktop\\\\hgqkj\\\\hqqkj_1-10\\\\tempt.txt";

    public static void main(String[] args) throws IOException {
        System.out.println(imp);
        System.out.println(createView);
        String ft[][]={{"FACT_AT","VW_DIM_PRODUCT_EU_VH_AT"},{"FACT_BG","VW_DIM_PRODUCT_EU_VH_BG"},{"FACT_BU","VW_DIM_PRODUCT_EU_VH_BU"},
                {"FACT_CY","VW_DIM_PRODUCT_EU_VH_CY"},{"FACT_CZ","VW_DIM_PRODUCT_EU_VH_CZ"}, {"FACT_DK","VW_DIM_PRODUCT_EU_VH_DK"},
                {"FACT_EE","VW_DIM_PRODUCT_EU_VH_EE"},{"FACT_ES","VW_DIM_PRODUCT_EU_VH_ES"},{"FACT_EU15","DIM_COUNTRY_EU_ORG_VIEW"},
                {"FACT_EU25","DIM_COUNTRY_EU_ORG_VIEW"}, {"FACT_EU27","DIM_COUNTRY_EU_ORG_VIEW"},{"FACT_FI","VW_DIM_PRODUCT_EU_VH_FI"},
                {"FACT_GR","VW_DIM_PRODUCT_EU_VH_GR"}};
        /*String ft[] = {"FACT_AT","FACT_BG", "FACT_BU","FACT_CY", "FACT_CZ",
                "FACT_DK", "FACT_EE","FACT_ES","FACT_EU15","FACT_EU25",
                "FACT_EU27", "FACT_FI", "FACT_GR", "FACT_HU", "FACT_IR",
                "FACT_LV", "FACT_LX", "FACT_MT"};*/

        FileWriter fw = new FileWriter(filePath, true);
        for (int i = 0; i < ft.length; i++) {
            String tmp = ft[i][0];
            fw.write("drop table " + tmp + ";");
            fw.write("\n");
        }
        for (int i = 0; i < ft.length; i++) {
            String tmp = ft[i][0];
            fw.write(imp.replace("oracleTable", tmp + "_SL").replace("hiveTable", tmp));
            fw.write("\n\n");
            fw.write(createView.replace("FACT_TT", tmp));
            fw.write("\n\n");
        }
        fw.write("------------------------------------------------------------------\n\n");
        /*String productTable[] = {"VW_DIM_PRODUCT_EU_VH_BG", "VW_DIM_PRODUCT_EU_VH_CY", "VW_DIM_PRODUCT_EU_VH_CZ", "VW_DIM_PRODUCT_EU_VH_DK", "VW_DIM_PRODUCT_EU_VH_EE",
                "VW_DIM_PRODUCT_EU_VH_FI", "VW_DIM_PRODUCT_EU_VH_GR", "VW_DIM_PRODUCT_EU_VH_HU", "VW_DIM_PRODUCT_EU_VH_IR", "VW_DIM_PRODUCT_EU_VH_LV",
                "VW_DIM_PRODUCT_EU_VH_LX", "VW_DIM_PRODUCT_EU_VH_MT"};*/
        for (int i = 0; i < ft.length; i++) {
            String tmp = ft[i][1];
            fw.write(imp.replace("oracleTable", tmp).replace("hiveTable", tmp));
            fw.write("\n\n");
        }
        fw.flush();
        fw.close();

    }

    static String imp = "sqoop import --hive-import --connect jdbc:oracle:thin:@172.24.0.75:1521:lcdbb1 --username qkj --password qkj2384ciecc --hive-overwrite --table oracleTable --hive-table hiveTable  --hive-database hgqkj -m 1 --fields-terminated-by \"\\001\";";
    static String createView = "create or replace view FACT_TT_ORG_VIEW AS\n" +
            "SELECT A.TIME_CODE,\n" +
            "       A.IE_CODE,\n" +
            "       B.PROXY_COUNTRY_CODE AS COUNTRY_CODE,\n" +
            "       A.PRODUCT_CODE,\n" +
            "       A.PARTITION_TIME,\n" +
            "       A.QUANTITY,\n" +
            "       A.MONEY,\n" +
            "       A.SUMQ,\n" +
            "       A.SUMM,\n" +
            "       A.QUANTITY_S,\n" +
            "       A.MONEY_S,\n" +
            "       A.SUMQ_S,\n" +
            "       A.SUMM_S,\n" +
            "       A.QUANTITY_L,\n" +
            "       A.MONEY_L,\n" +
            "       A.SUMQ_L,\n" +
            "       A.SUMM_L\n" +
            "  FROM FACT_TT A\n" +
            " INNER JOIN DIM_COUNTRY_EUHG_CE_VIEW B\n" +
            "    ON A.COUNTRY_CODE = B.COUNTRY_CODE\n" +
            "UNION ALL\n" +
            "SELECT A.TIME_CODE,\n" +
            "       A.IE_CODE,\n" +
            "       A.COUNTRY_CODE,\n" +
            "       A.PRODUCT_CODE,\n" +
            "       A.PARTITION_TIME,\n" +
            "       A.QUANTITY,\n" +
            "       A.MONEY,\n" +
            "       A.SUMQ,\n" +
            "       A.SUMM,\n" +
            "       A.QUANTITY_S,\n" +
            "       A.MONEY_S,\n" +
            "       A.SUMQ_S,\n" +
            "       A.SUMM_S,\n" +
            "       A.QUANTITY_L,\n" +
            "       A.MONEY_L,\n" +
            "       A.SUMQ_L,\n" +
            "       A.SUMM_L\n" +
            "  FROM FACT_TT A;";
}

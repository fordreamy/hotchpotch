package importTable;

import importTable.jsch.JschThread;

import java.io.*;

public class SqoopImportJschWork {

    static String hostname = "211.88.20.111";
    static String userName = "root";
    static String password = "qazWSX123!@#";
    static String javaHome = "export JAVA_HOME=/opt/jdk1.8.0_111;";
    static String ott[][] = {{"fact_mt_sl", "fact_mt"}};
    static String fs[][];
    static String filePath = "src/main/java/importTable/OracleToHive.txt";

    public static void main(String[] args) throws IOException {

        init("jdbc:oracle:thin:@172.24.0.75:1521:lcdbb1", "qkj", "qkj2384ciecc", "hgqkj");
        //delHiveTable("vw_dim_product_eu_vh_cy,vw_dim_product_eu_vh_mt");
        //importHiveTable(fs);
    }

    public static void init(String driverUrl, String uname, String upassword, String hiveDB) {
        sqoopImportShell = javaHome + sqoopImportShell.replace("driverUrl", driverUrl).replace("uname", uname).replace("upassword", upassword).replace("hiveDB", hiveDB);
        delHiveTableShell = javaHome + delHiveTableShell.replace("hiveDB", hiveDB);
        createViewShell = javaHome + createViewShell.replace("hiveDB", hiveDB);
        try {
            String str;
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(filePath))));
            int i = 0;
            String[][] tmp = new String[100][2];
            while ((str = br.readLine()) != null) {
                if (!"".equals(str)) {
                    tmp[i++] = str.replace(" ", "").split(",");
                }
            }
            fs = new String[i][2];
            System.out.println("行数:" + i);
            for (int j = 0; j < i; j++) {
                fs[j][0] = tmp[j][0];
                fs[j][1] = tmp[j][1];
                System.out.println(getSqoopImport(fs[j][1],fs[j][0]));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void delHiveTable(String tableNames) {
        String[] tn = tableNames.split(",");
        for (String tmp : tn) {
            System.out.println(delHiveTableShell.replace("hiveTable", tmp));
            new JschThread(delHiveTableShell.replace("hiveTable", tmp), hostname, userName, password).start();
        }
    }

    public static void importHiveTable(String[][] sa) {
        for (int i = 0; i < sa.length; i++) {
            String tmp = sa[i][0];
            System.out.println(getSqoopImport(sa[i][0], sa[i][1]));
            new JschThread(getSqoopImport(sa[i][0], sa[i][1]), hostname, userName, password).start();
        }
    }

    public static String getSqoopImport(String oracleTable, String hiveTable) {
        String s = sqoopImportShell.replace("oracleTable", oracleTable).replace("hiveTable", hiveTable);
        return s;
    }

    public static String delHiveTableShell = "hive -e 'use hiveDB;drop table hiveTable';";
    public static String sqoopImportShell = "sqoop import --hive-import " +
            "--connect driverUrl --username uname --password upassword " +
            "--hive-overwrite --table oracleTable " +
            "--hive-table hiveTable  " +
            "--hive-database hiveDB -m 1 --fields-terminated-by \"\\001\";";
    static String createViewShell = "hive -e 'use hiveDB;" +
            "create or replace view FACT_TABLE_ORG_VIEW AS\n" +
            "SELECT A.TIME_CODE,\n" +
            "       A.IE_CODE,\n" +
            "       B.PROXY_COUNTRY_CODE AS COUNTRY_CODE,\n" +
            "       A.PRODUCT_CODE,\n" +
            "       A.PARTITION_TIME,\n" +
            "       A.QUANTITY,\n" +
            "       A.MONEY,\n" +
            "       A.SUMQ,\n" +
            "       A.SUMM,\n" +
            "       A.QUANTITY_S,\n" +
            "       A.MONEY_S,\n" +
            "       A.SUMQ_S,\n" +
            "       A.SUMM_S,\n" +
            "       A.QUANTITY_L,\n" +
            "       A.MONEY_L,\n" +
            "       A.SUMQ_L,\n" +
            "       A.SUMM_L\n" +
            "  FROM FACT_TABLE A\n" +
            " INNER JOIN DIM_COUNTRY_EUHG_CE_VIEW B\n" +
            "    ON A.COUNTRY_CODE = B.COUNTRY_CODE\n" +
            "UNION ALL\n" +
            "SELECT A.TIME_CODE,\n" +
            "       A.IE_CODE,\n" +
            "       A.COUNTRY_CODE,\n" +
            "       A.PRODUCT_CODE,\n" +
            "       A.PARTITION_TIME,\n" +
            "       A.QUANTITY,\n" +
            "       A.MONEY,\n" +
            "       A.SUMQ,\n" +
            "       A.SUMM,\n" +
            "       A.QUANTITY_S,\n" +
            "       A.MONEY_S,\n" +
            "       A.SUMQ_S,\n" +
            "       A.SUMM_S,\n" +
            "       A.QUANTITY_L,\n" +
            "       A.MONEY_L,\n" +
            "       A.SUMQ_L,\n" +
            "       A.SUMM_L\n" +
            "  FROM FACT_TABLE A;" +
            "'";

}
package importTable;

import ch.ethz.ssh2.ChannelCondition;
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;
import importTable.jsch.JschThread;

import java.io.*;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SqoopImport {
    static String hostname = "211.88.20.70";
    static String userName = "root";
    static String password = "Root#$2017";
    static String javaHome = "export JAVA_HOME=/opt/jdk1.8.0_111;";
    static String ft[][] = {{"FACT_MT", "VW_DIM_PRODUCT_EU_VH_MT"}, {"FACT_CY", "VW_DIM_PRODUCT_EU_VH_CY"}};

    public static void main(String[] args) throws IOException {

        /*initShell("jdbc:oracle:thin:@210.25.24.55:1521:myorcl", "temp", "temp","test");
        new DumbPTYThead(getSqoopImport("HIVEINSERT", "test1"), hostname, userName, password).start();
        new DumbPTYThead(delHiveTableShell.replace("hiveTable", "test1"), hostname, userName, password).start();*/

        initShell("jdbc:oracle:thin:@172.24.0.75:1521:lcdbb1", "qkj", "qkj2384ciecc", "test");
        new DumbPTYThead(javaHome + "hive -e 'use test;drop view fact_mt_org_view;'", hostname, userName, password).start();
        new DumbPTYThead(javaHome + "hive -e 'use test;drop view fact_cy_org_view;'", hostname, userName, password).start();

        /*----------well done----------*/
        delHiveTable("fact_cy,fact_mt,vw_dim_product_eu_vh_cy,vw_dim_product_eu_vh_mt");
        //delHiveTable(ft);
        //importHiveTable("VW_DIM_PRODUCT_EU_VH_MT,VW_DIM_PRODUCT_EU_VH_CY");
        //importHiveTable(ft);
        //createView(ft);

    }

    public static void initShell(String driverUrl, String uname, String upassword, String hiveDB) {
        sqoopImportShell = javaHome + sqoopImportShell.replace("driverUrl", driverUrl).replace("uname", uname).replace("upassword", upassword).replace("hiveDB", hiveDB);
        delHiveTableShell = javaHome + delHiveTableShell.replace("hiveDB", hiveDB);
        createViewShell = javaHome + createViewShell.replace("hiveDB", hiveDB);
    }

    public static void delHiveTable(String[][] sa) {
        for (int i = 0; i < sa.length; i++) {
            String tmp = sa[i][0];
            System.out.println(delHiveTableShell.replace("hiveTable", tmp));
            new DumbPTYThead(delHiveTableShell.replace("hiveTable", tmp), hostname, userName, password).start();
        }
    }

    public static void delHiveTable(String tableNames) {
        String[] tn = tableNames.split(",");
        for (String tmp : tn) {
            System.out.println(delHiveTableShell.replace("hiveTable", tmp));
            new DumbPTYThead(delHiveTableShell.replace("hiveTable", tmp), hostname, userName, password).start();
        }
    }

    public static void importHiveTable(String[][] sa) {
        for (int i = 0; i < sa.length; i++) {
            String tmp = sa[i][0];
            System.out.println(getSqoopImport(tmp + "_SL", tmp));
            new DumbPTYThead(getSqoopImport(tmp + "_SL", tmp), hostname, userName, password).start();
        }
    }

    /**
     * 一次导入一张表
     * @param oracleTable
     * @param hiveTable
     */
    public static void importHiveTable(String oracleTable, String hiveTable) {
        System.out.println(getSqoopImport(oracleTable, hiveTable));
        new DumbPTYThead(getSqoopImport(oracleTable, hiveTable), hostname, userName, password).start();
    }

    /**
     * 导入多张表
     * @param tableNames 表名,以逗号分隔
     */
    public static void importHiveTable(String tableNames) {
        String[] tn = tableNames.split(",");
        for (String tmp : tn) {
            System.out.println(getSqoopImport(tmp, tmp));
            new DumbPTYThead(getSqoopImport(tmp, tmp), hostname, userName, password).start();
        }
    }

    public static String getSqoopImport(String oracleTable, String hiveTable) {
        String s = sqoopImportShell.replace("oracleTable", oracleTable).replace("hiveTable", hiveTable);
        return s;
    }

    public static void createView(String [][]sa) {
        for (int i = 0; i < sa.length; i++) {
            String tmp = sa[i][0];
            System.out.println(createViewShell.replace("FACT_TABLE", tmp));
            new DumbPTYThead(createViewShell.replace("FACT_TABLE", tmp), hostname, userName, password).start();
        }
    }


    public static String delHiveTableShell = "hive -e 'use hiveDB;drop table hiveTable';";
    public static String sqoopImportShell = "sqoop import --hive-import " +
            "--connect driverUrl --username uname --password upassword " +
            "--hive-overwrite --table oracleTable " +
            "--hive-table hiveTable  " +
            "--hive-database hiveDB -m 1 --fields-terminated-by \"\\001\";";
    static String createViewShell = "hive -e 'use hiveDB;" +
            "create or replace view FACT_TABLE_ORG_VIEW AS\n" +
            "SELECT A.TIME_CODE,\n" +
            "       A.IE_CODE,\n" +
            "       B.PROXY_COUNTRY_CODE AS COUNTRY_CODE,\n" +
            "       A.PRODUCT_CODE,\n" +
            "       A.PARTITION_TIME,\n" +
            "       A.QUANTITY,\n" +
            "       A.MONEY,\n" +
            "       A.SUMQ,\n" +
            "       A.SUMM,\n" +
            "       A.QUANTITY_S,\n" +
            "       A.MONEY_S,\n" +
            "       A.SUMQ_S,\n" +
            "       A.SUMM_S,\n" +
            "       A.QUANTITY_L,\n" +
            "       A.MONEY_L,\n" +
            "       A.SUMQ_L,\n" +
            "       A.SUMM_L\n" +
            "  FROM FACT_TABLE A\n" +
            " INNER JOIN DIM_COUNTRY_EUHG_CE_VIEW B\n" +
            "    ON A.COUNTRY_CODE = B.COUNTRY_CODE\n" +
            "UNION ALL\n" +
            "SELECT A.TIME_CODE,\n" +
            "       A.IE_CODE,\n" +
            "       A.COUNTRY_CODE,\n" +
            "       A.PRODUCT_CODE,\n" +
            "       A.PARTITION_TIME,\n" +
            "       A.QUANTITY,\n" +
            "       A.MONEY,\n" +
            "       A.SUMQ,\n" +
            "       A.SUMM,\n" +
            "       A.QUANTITY_S,\n" +
            "       A.MONEY_S,\n" +
            "       A.SUMQ_S,\n" +
            "       A.SUMM_S,\n" +
            "       A.QUANTITY_L,\n" +
            "       A.MONEY_L,\n" +
            "       A.SUMQ_L,\n" +
            "       A.SUMM_L\n" +
            "  FROM FACT_TABLE A;" +
            "'";

}

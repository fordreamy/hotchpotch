package kylinRest;

import sun.misc.BASE64Encoder;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpTest {

    private static String encoding;
    private static String user;
    private static String password;
    private static final String baseURL = "https://www.csdn.net/";

    public static void main(String[] args) {
        String cubeDesc = "username:shuaipengmail@163.com\npassword:csdn11235";
        createCube("https://passport.csdn.net/account/login","POST",cubeDesc);
    }

    private  static String createCube(String reqUrl,String method,String reqestBody){

        StringBuilder out = new StringBuilder();
        try {
            URL url = new URL(reqUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            connection.setRequestProperty  ("Authorization", "Basic " + encoding);

           /* connection.setRequestProperty("Referer"," http://211.88.20.70:7070/kylin/cubes/add/");
            connection.setRequestProperty("Host","211.88.20.70:7070");
            connection.setRequestProperty("Cookie","project=%22learn_kylin%22");
            connection.setRequestProperty("Accept-Language","zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2");
            connection.setRequestProperty("Cache-Control","no-cache");
            connection.setRequestProperty("Pragma","no-cache");*/

            connection.setRequestProperty("Content-Type","application/json;charset=utf-8");
            if(reqestBody !=null){
                byte[] outputInBytes = reqestBody.getBytes("UTF-8");
                OutputStream os = connection.getOutputStream();
                os.write(outputInBytes);
                os.close();
            }
            InputStream content = (InputStream)connection.getInputStream();
            BufferedReader in  = new BufferedReader (new InputStreamReader (content));
            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
                out.append(line);
            }
            in.close();
            connection.disconnect();

        } catch(Exception e) {
            e.printStackTrace();
        }
        return out.toString();
    }

    private  static String excute(String para,String method,String body){

        StringBuilder out = new StringBuilder();
        try {
            URL url = new URL(baseURL+para);
            //String para = "/user/authentication";
            byte[] key = (user+":"+password).getBytes();
            encoding = new BASE64Encoder().encode(key);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            connection.setRequestProperty  ("Authorization", "Basic " + encoding);
            connection.setRequestProperty("Content-Type","application/json");
            if(body !=null){
                byte[] outputInBytes = body.getBytes("UTF-8");
                OutputStream os = connection.getOutputStream();
                os.write(outputInBytes);
                os.close();
            }
            InputStream content = (InputStream)connection.getInputStream();
            BufferedReader in  = new BufferedReader (new InputStreamReader (content));
            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
                out.append(line);
            }
            in.close();
            connection.disconnect();

        } catch(Exception e) {
            e.printStackTrace();
        }
        return out.toString();
    }
}

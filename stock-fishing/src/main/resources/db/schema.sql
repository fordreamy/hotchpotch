CREATE TABLE stock
(
    id            int,
    stock_code    varchar(8),
    stock_name    varchar(50),
    stock_show_name    varchar(50),
    position    int,
    remark varchar(20),
    last_price    double
);


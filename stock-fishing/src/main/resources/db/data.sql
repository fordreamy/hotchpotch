INSERT into stock VALUES (1, 'sh000001', '上证指数', 'zhengshang',1,'', null);
INSERT into stock VALUES (1, 'sz159949', '创业50', 'chuangye',2,'', null);
INSERT into stock VALUES (1, 'sh588000', '科创50', 'kechuang',3,'', null);
INSERT into stock VALUES (1, '--------', '--------', '--------',1,'', null);

INSERT into stock VALUES (1, 'sh512880', '证券ETF', 'zhengquanet',1,'et', null);
INSERT into stock VALUES (1, 'sz159995', '芯片ETF', 'xinpianet',1,'et', null);
INSERT into stock VALUES (1, 'sh512660', '军工ETF', 'jungonget',1,'et', null);
INSERT into stock VALUES (1, 'sh512690', '酒ETF', 'jiuet',1,'et', null);
INSERT into stock VALUES (1, 'sh515000', '科技ETF', 'kejiet',2,'et', null);
INSERT into stock VALUES (1, 'sh512010', '医药ETF', 'yiyaoet',2,'et', null);
INSERT into stock VALUES (1, 'sh516670', '医疗设备ETF', 'yishebeiet',2,'et', null);
INSERT into stock VALUES (1, 'sh516910', '物流ETF', 'wuliuet',2,'et', null);
INSERT into stock VALUES (1, 'sz159611', '电力ETF', 'dianliet',3,'et', null);
INSERT into stock VALUES (1, 'sz159996', '家电ETF', 'jiadianet',3,'et', null);
INSERT into stock VALUES (1, 'sh512800', '银行ETF', 'yinhanget',3,'et', null);
INSERT into stock VALUES (1, 'sh562530', '旅游ETF', 'lvyouet',3,'et', null);
INSERT into stock VALUES (1, '--------', '--------', '--------',1,'', null);

/*我的*/
INSERT into stock VALUES (1, 'sh601868', '中国能建', 'nengjian',1,'', null);
INSERT into stock VALUES (1, 'sh600157', '永泰能源', 'yongtainengyuan',1,'', null);
INSERT into stock VALUES (1, 'sz300922', '天秦装备', 'tianqinzhuangb',1,'', null);
INSERT into stock VALUES (1, 'sz300749', '顶固集创', 'dinggujichuang',1,'', null);
INSERT into stock VALUES (1, 'sz002638', '勤上股份', 'qinshanggufen',1,'', null);

/*证券保险*/
INSERT into stock VALUES (1, 'sz002797', '第一创业', 'diyichuangye',2,'', null);
INSERT into stock VALUES (1, 'sh601108', '财通证券', 'caitongzhengq',2,'', null);
INSERT into stock VALUES (1, 'sh601236', '红塔证券', 'hongtazhengq',2,'', null);
INSERT into stock VALUES (1, 'sh601318', '中国平安', 'zhongguopingan',2,'', null);
INSERT into stock VALUES (1, 'sh601319', '中国人保', 'zhongguorenbao',2,'', null);

/*半导体*/
INSERT into stock VALUES (1, 'sz002409', '雅克科技', 'yakekeji',3,'', null);
INSERT into stock VALUES (1, 'sh600584', '长电科技', 'changdiankeji',3,'', null);
INSERT into stock VALUES (1, 'sz003026', '中晶科技', 'zhongjingkeji',3,'', null);
INSERT into stock VALUES (1, 'sh605358', '立昂微', 'liangwei',3,'', null);
INSERT into stock VALUES (1, 'sh600460', '士兰微', 'shilanwei',3,'', null);
INSERT into stock VALUES (1, '--------', '--------', '--------',1,'', null);

/*电力*/
INSERT into stock VALUES (1, 'sh601778', '晶科科技', 'jingkekeji',1,'', null);
INSERT into stock VALUES (1, 'sz001286', '陕西能源', 'shanxinengyuan',1,'', null);
INSERT into stock VALUES (1, 'sh601985', '中国核电', 'zhongguohedian',1,'', null);
INSERT into stock VALUES (1, 'sh600905', '三峡能源', 'sanxianengyuan',1,'', null);

/*酒*/
INSERT into stock VALUES (1, 'sh600559', '老白干酒', 'laobaigan',2,'', null);
INSERT into stock VALUES (1, 'sh603919', '金徽酒', 'jinhuijiu',2,'', null);
INSERT into stock VALUES (1, 'sh603198', '迎驾贡酒', 'yingjiagongjiu',2,'', null);
INSERT into stock VALUES (1, 'sh600132', '重庆啤酒', 'chongqingpiujiu',2,'', null);

/*医药*/
INSERT into stock VALUES (1, 'sz000538', '云南白药', 'yunnanbaiyao',3,'', null);
INSERT into stock VALUES (1, 'sz002603', '以岭药业', 'yilingyaoye',3,'', null);
INSERT into stock VALUES (1, 'sz002007', '华兰生物', 'hualanshengwu',3,'', null);
INSERT into stock VALUES (1, 'sz300142', '沃森生物', 'wosenshengwu',3,'', null);
INSERT into stock VALUES (1, '--------', '--------', '--------',1,'', null);

/*军工*/
INSERT into stock VALUES (1, 'sh600118', '中国卫星', 'zhongguoweixing',1,'', null);
INSERT into stock VALUES (1, 'sh600685', '中船防务', 'zhongchuanfangw',1,'', null);
INSERT into stock VALUES (1, 'sh601606', '长城军工', 'changchengjung',1,'', null);

/*游戏*/
INSERT into stock VALUES (1, 'sz002555', '三七互娱', 'sanqihuyu',2,'', null);
INSERT into stock VALUES (1, 'sz002602', '世纪华通', 'shijihuatong',2,'', null);
INSERT into stock VALUES (1, 'sz002624', '完美世界', 'wanmeishijie',2,'', null);

/*旅游*/
INSERT into stock VALUES (1, 'sh600749', '西藏旅游', 'xizanglvyou',3,'', null);
INSERT into stock VALUES (1, 'sz000978', '桂林旅游', 'guilinlvyou',3,'', null);
INSERT into stock VALUES (1, 'sh600138', '中青旅', 'zhongqinglv',3,'', null);
INSERT into stock VALUES (1, '--------', '--------', '--------',1,'', null);

/*建筑装饰*/
INSERT into stock VALUES (1, 'sh601800', '中国交建', 'jiaojian',1,'', null);
INSERT into stock VALUES (1, 'sh600528', '中铁工业', 'zhongtie',1,'', null);

/*家电*/
INSERT into stock VALUES (1, 'sz000333', '美的集团', 'meidijituan',2,'', null);
INSERT into stock VALUES (1, 'sz000651', '格力电器', 'geli',2,'', null);

/*通讯设备*/
INSERT into stock VALUES (1, 'sz000063', '中兴通讯', 'zhongxingtong',3,'', null);
INSERT into stock VALUES (1, 'sz300394', '天孚通信', 'tianfutongxin',3,'', null);
INSERT into stock VALUES (1, '--------', '--------', '--------',1,'', null);

/*电力设备*/
INSERT into stock VALUES (1, 'sh600438', '通威股份', 'tongweigufen',1,'', null);
INSERT into stock VALUES (1, 'sh601012', '隆基绿能', 'longjilvneng',1,'', null);
INSERT into stock VALUES (1, 'sz002709', '天赐材料', 'tiancicailiao',1,'', null);

/*计算机应用*/
INSERT into stock VALUES (1, 'sz300079', '数码视讯', 'shumashixun',2,'', null);
INSERT into stock VALUES (1, 'sz300682', '朗新科技', 'langxinkeji',2,'', null);
INSERT into stock VALUES (1, 'sz002230', '科大讯飞', 'kedaxunfei',2,'', null);

/*食品*/
INSERT into stock VALUES (1, 'sh600305', '恒顺醋业', 'hengshuncuye',3,'', null);
INSERT into stock VALUES (1, 'sh600597', '光明乳业', 'guangmingruye',3,'', null);
INSERT into stock VALUES (1, 'sz000895', '双汇发展', 'shuanghuifazhan',3,'', null);
INSERT into stock VALUES (1, '--------', '--------', '--------',1,'', null);

/*如影随形*/
INSERT into stock VALUES (1, 'sh601028', '玉龙股份', 'yulonggufen',1,'', null);
INSERT into stock VALUES (1, 'sh600893', '航发动力', 'hangfadongli',1,'', null);




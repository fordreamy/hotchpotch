package com.fishing.init;

import com.fishing.stock.domain.Stock;

import java.util.List;

/**
 * 静态数据类
 */
public class StaticData {
    public static List<Stock> stockList;
}

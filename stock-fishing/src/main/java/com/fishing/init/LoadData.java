package com.fishing.init;

import com.fishing.stock.domain.Stock;
import com.fishing.stock.service.IStockService;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 加载数据库数据
 */
@Component
public class LoadData {
    @Resource
    private IStockService stockService;

    @PostConstruct
    public void init() {
        System.out.println("初始化方法一:");
        List<Stock> stockList = stockService.selectStockList(new Stock());

        List<Stock> firstColumn = stockList.stream().filter(n -> n.getPosition() == 1).toList();
        List<Stock> secondColumn = stockList.stream().filter(n -> n.getPosition() == 2).toList();
        List<Stock> thirdColumn = stockList.stream().filter(n -> n.getPosition() == 3).toList();
        int s = 0, t = 0;
        stockList = new ArrayList<>();

        for (Stock stock : firstColumn) {
            stockList.add(stock);
            if (stock.getStockCode().contains("---")) {
                continue;
            }
            if (s < secondColumn.size()) {
                stockList.add(secondColumn.get(s++));
            }
            if (t < thirdColumn.size()) {
                stockList.add(thirdColumn.get(t++));
            }

        }

        StaticData.stockList = stockList;
        System.out.println("获取数据库数据:" + StaticData.stockList);
    }
}

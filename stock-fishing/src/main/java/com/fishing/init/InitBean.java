package com.fishing.init;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class InitBean implements InitializingBean {

    @Override
    public void afterPropertiesSet() {
        System.out.println("初始化方法二:");
    }
}

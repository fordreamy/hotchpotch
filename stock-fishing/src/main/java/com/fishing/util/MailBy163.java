package com.fishing.util;


import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;

import java.util.Date;
import java.util.Properties;

public class MailBy163 {

    public static void main(String[] args) {
        MailBy163.sendMail();
    }

    // 发件人的邮箱账号
    public static String fromMailAccount = "shuaipengmail@163.com";
    // 发件人的邮箱授权码,非邮箱登录密码,可以登录邮箱后设置
    public static String fromMailPassword = "sqm123";

    // 发件人邮箱的 SMTP 服务器地址, 163邮箱的 SMTP 服务器地址为: smtp.163.com
    public static String myEmailSMTPHost = "smtp.163.com";

    // 收件人邮箱
    public static String receiveMailAccount = "shuaipeng20@qq.com";

    public static void sendMail() {

        try {
            // 创建参数配置, 用于连接邮件服务器的参数配置
            Properties props = new Properties(); // 参数配置
            props.setProperty("mail.transport.protocol", "smtp"); // 使用的协议（JavaMail规范要求）
            props.setProperty("mail.smtp.host", myEmailSMTPHost); // 发件人的邮箱的 SMTP
            props.setProperty("mail.smtp.auth", "true");
            // 根据配置创建会话对象, 用于和邮件服务器交互
            Session session = Session.getDefaultInstance(props);
            // 设置为debug模式, 可以查看详细的发送 log
//            session.setDebug(true);
            // 创建一封邮件
            MimeMessage message = createMimeMessage(session, fromMailAccount, receiveMailAccount);
            // 获取邮件传输对象
            Transport transport = session.getTransport();
            // 连接邮箱
            transport.connect(fromMailAccount, fromMailPassword);
            // 发送邮件, 发到所有的收件地址, message.getAllRecipients() 获取到的是在创建邮件对象时添加的所有收件人,
            transport.sendMessage(message, message.getAllRecipients());
            // 关闭连接
            transport.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 创建一封只包含文本的简单邮件
     *
     * @param session     和服务器交互的会话
     * @param sendMail    发件人邮箱
     * @param receiveMail 收件人邮箱
     */
    public static MimeMessage createMimeMessage(Session session, String sendMail, String receiveMail) throws Exception {
        // 创建一封邮件
        MimeMessage message = new MimeMessage(session);
        // 发件人昵称
        message.setFrom(new InternetAddress(sendMail, "夜晚有点黑", "UTF-8"));
        // To: 收件人（可以增加多个收件人、抄送、密送）
        message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiveMail, "psp", "UTF-8"));
        // 邮件主题
        message.setSubject("今天风和日丽", "UTF-8");
        // 邮件正文
        message.setContent("您收到一封提示邮件,请注意查收!", "text/html;charset=UTF-8");
        // 设置发件时间
        message.setSentDate(new Date());
        // 保存设置
        message.saveChanges();
        return message;
    }
}

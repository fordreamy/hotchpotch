package com.fishing.util;

import com.fishing.stock.domain.Stock;

import java.util.ArrayList;
import java.util.List;

public class ConvertUtils {
    public static List<Stock> propertiesToStock() {
        List<Stock> list = new ArrayList<>();
        PropertiesUtils.properties.keySet().forEach(n -> {
            //key格式sh600905_三峡能源
            Stock s = new Stock();
            s.setStockCode(n.toString().substring(0, n.toString().indexOf("_")));
            s.setStockName(n.toString().substring(n.toString().indexOf("_") + 1));
            list.add(s);
        });
        return list;
    }
}

package com.fishing.util;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.fishing.stock.domain.Stock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tiger
 */
public class StockUtils {

    protected static final Logger logger = LoggerFactory.getLogger(StockUtils.class);

    /**
     * 从新浪获取每日股票价格,注意开盘之前获取的价格是0
     *
     * @param code 例:<a href="http://hq.sinajs.cn/list=sz000002">...</a>,返回格式var hq_str_sz000002="万 科Ａ,28.050,28.020,28.030,28.290,27.740,28.030,28.040,68434585,1913388242.740,186381,28.030,77500,28.020,14800,28.010,114400,28.000,26200,27.990,16200,28.040,22300,28.050,14700,28.060,6200,28.070,11700,28.080,2020-10-09,15:00:03,00";
     *             数值依次是今日开盘价、昨日收盘价、当前价、今日最高价、今日最低价
     * @return 股票价格, 依次是开盘、收盘、最高、最低、日期
     */
    public static String[] getStockPriceBySina(String code) {
        String url = "http://hq.sinajs.cn/list=" + code;
        String[] price = new String[6];
        try {
            String response = GetResponse.getResponse(url, "GBK");
//            System.out.println(response);
            response = response.substring(response.indexOf(",") + 1, response.length() - 2);
            String[] tmp = response.split(",");
            price[0] = tmp[29];  //日期
            price[1] = tmp[0]; //开盘价
            price[2] = tmp[2]; //收盘价
            price[3] = tmp[3];//最高价
            price[4] = tmp[4];//最低价
            price[5] = tmp[1];//昨日收盘价
        } catch (Exception e) {
            logger.error("爬取股票价格失败!");
            e.printStackTrace();
            return null;
        }
        logger.debug("截取股票价格成功!");
        return price;
    }

    /**
     * 更新属性文件的最新价格,属性文件内已经存在编码
     */
    public static void updatePropertiesFilePrice(String propertiesFileName) {
        PropertiesUtils.loadProperties(propertiesFileName);

        List<Stock> list = ConvertUtils.propertiesToStock();
        List<String[]> s = new ArrayList<>();
        for (Stock stock : list) {
            String[] price = StockUtils.getStockPriceBySina(stock.getStockCode());
            assert price != null;
            String[] kv = new String[2];
            kv[0] = stock.getStockCode() + "_" + stock.getStockName();
            kv[1] = price[5] + "_" + price[2];
            s.add(kv);
        }
        PropertiesUtils.setPropertyArray(s);
    }

    public static void insertPropertiesFileAllStockCodeFromAPI(String propertiesFileName) {
        String response = GetResponse.getResponse("http://api.mairui.club/hslt/list/8e7af2083383b649b", "UTF-8");
        JSONArray jsonArray = JSON.parseArray(response);

        PropertiesUtils.loadProperties(propertiesFileName);
        PropertiesUtils.clear();
        for (Object o : jsonArray) {
            JSONObject jsonObject = JSONObject.parseObject(o.toString());
            String code = jsonObject.get("dm").toString();
            if (code.startsWith("688")) {
                continue;
            }
            String name = jsonObject.get("mc").toString();
            String exchange = jsonObject.get("jys").toString();
            PropertiesUtils.setProperty(exchange + code + "_" + name, "0");
        }
    }

    public static void main(String[] args) {

        String response = GetResponse.getResponse("http://api.mairui.club/hslt/list/40655585c9f9efab89", "UTF-8");
        System.out.println(response);
//        updatePropertiesFilePrice("last_and_now_price.properties");

    }
}

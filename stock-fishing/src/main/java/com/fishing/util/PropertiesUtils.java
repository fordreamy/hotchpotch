package com.fishing.util;

import org.springframework.util.FileCopyUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

public class PropertiesUtils {

    public static Properties properties;

    public static String propertiesFileName;

    /**
     * 加载Properties属性文件
     *
     * @return Properties属性文件
     */
    public static Properties loadProperties(String name) {
        propertiesFileName = name;
        properties = new Properties();
        InputStream in;
        String sourceFile = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + name;
        if (new File(sourceFile).exists()) {
            try {
                System.out.println("获取源文件:" + sourceFile);
                in = new FileInputStream(sourceFile);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            in = Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
        }
        try {
            assert in != null;
            properties.load(new InputStreamReader(in, StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return properties;
    }

    public static void setProperty(String key, String value) {

        String filePath = getPropertiesFilePath(propertiesFileName);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(filePath);
            properties.setProperty(key, value);
            // 将Properties集合保存到流中
            properties.store(fos, null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void setPropertyArray(List<String[]> keyValue) {

        String filePath = getPropertiesFilePath(propertiesFileName);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(filePath);
            for (String[] s : keyValue) {
                properties.setProperty(s[0], s[1]);
            }
            // 将Properties集合保存到流中
            properties.store(fos, null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 清空所有属性
     */
    public static void clear() {
        try {
            properties.clear();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void copyPropertyFile(String sourceFileName, String toFileName) {
        String sourceFilePath = getPropertiesFilePath(sourceFileName);
        String toFilePath = getPropertiesFilePath(toFileName);
        try {
            InputStream in = new FileInputStream(sourceFilePath);
            OutputStream fos = new FileOutputStream(toFilePath);
            FileCopyUtils.copy(in, fos);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String getPropertiesFilePath(String fileName) {
        String filePath = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + fileName;

        if (!new File(filePath).exists()) {
            // 获取jar包的所在路径
            String jarPath = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("")).toString();
            jarPath = jarPath.replace("jar:file:/", "").replaceAll("!.*", "");
            filePath = jarPath.substring(0, jarPath.lastIndexOf("/")) + File.separator + fileName;
        }
        return filePath;
    }
}

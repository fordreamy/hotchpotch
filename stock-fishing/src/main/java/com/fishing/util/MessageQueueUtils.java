package com.fishing.util;

import java.util.HashMap;

public class MessageQueueUtils {
    public static int capacity = 10000;
    public static HashMap<String, String> map = new HashMap<>();

    public MessageQueueUtils(int capacity) {
        MessageQueueUtils.capacity = capacity;
    }

    public static HashMap<String, String> take() {
        return map;
    }

    public static void put(String key, String value) {
        map.put(key, value);
    }

    public static void clear() {
        map = new HashMap<>();
    }

    public static void main(String[] args) {
        MessageQueueUtils.put("2","2");
        MessageQueueUtils.put("1","1");
        MessageQueueUtils.put("1","1");
        System.out.println(MessageQueueUtils.take());
        System.out.println(MessageQueueUtils.take());
        System.out.println(MessageQueueUtils.take());
    }
}

package com.fishing.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.StompWebSocketEndpointRegistration;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * websocket 配置
 *
 * @author ruoyi
 */
@Configuration
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        String defaultWebsocketEndpointPath = "/server/*";
        String defaultSockJsEndpointPath = "/server/*";
        StompWebSocketEndpointRegistration webSockedEndpoint = registry.addEndpoint(defaultWebsocketEndpointPath);
        StompWebSocketEndpointRegistration sockJsEndpoint = registry.addEndpoint(defaultSockJsEndpointPath);
//        withSockJS()指定端点使用SockJS协议
        sockJsEndpoint.withSockJS();
        //允许跨域访问
        webSockedEndpoint.setAllowedOrigins("*");
        sockJsEndpoint.setAllowedOrigins("*");
    }
}

package com.fishing.schedule;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.fishing.config.SystemParameter;
import com.fishing.init.StaticData;
import com.fishing.stock.domain.Stock;
import com.fishing.util.*;
import com.fishing.vo.ShowDataVO;
import com.fishing.websocket.WebSocketServer;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Arrays;

@Component
public class BlackTechnology {

    /**
     * 定时show time
     */
    @Scheduled(cron = "*/16 * * * * ?")
    public void show() throws IOException, InterruptedException {
        // 行数,超过则换行
        int defaultLineCount = 3;
        // 动态行数
        int dynamicLineCount = defaultLineCount;
        // 列宽度
        int firstWidth = 16, secondWidth = 7, thirdWidth = 7;

        if (SystemParameter.SCHEDULED_SHOW_ENABLE) {

            //展示字符
            StringBuilder showStr = new StringBuilder();
            int lineCountPoint = 0;
            for (Stock stock : StaticData.stockList) {
                // 分割线
                if (stock.getStockCode().contains("---")) {
                    if (NumberUtil.isInteger(stock.getStockName())) {
                        //动态改变行个数
                        dynamicLineCount = Integer.parseInt(stock.getStockName());
                    } else {
                        dynamicLineCount = defaultLineCount;
                    }
                    showStr.append("\n");
                    showStr.append("-".repeat((firstWidth + secondWidth + thirdWidth + 2) * dynamicLineCount));
                    showStr.append("\n");
                    lineCountPoint = 0;
                    continue;
                }

                // 换行
                if (lineCountPoint == dynamicLineCount) {
                    showStr.append("\n");
                    lineCountPoint = 0;
                }

                lineCountPoint++;

                String[] p = StockUtils.getStockPriceBySina(stock.getStockCode());
                assert p != null;
                p = Arrays.stream(p).skip(1).toArray(String[]::new);
                Double[] price = Arrays.stream(p).map(Double::valueOf).toArray(Double[]::new);

                // 上证指数去除小数点
                if (stock.getStockCode().equals("sh000001")) {
                    price = Arrays.stream(price).map(s -> NumberUtil.round(s, 0).doubleValue()).toArray(Double[]::new);
                }

                // 展示项,依次是名称、当前价格、涨幅
                ShowDataVO item = new ShowDataVO();
                item.setName(StaticData.stockList.stream().filter(n -> n.getStockCode().equals(stock.getStockCode())).findFirst().orElse(new Stock()).getStockShowName());
                item.setPrice(price[1].toString());
                // 涨幅
                double range = (price[1] - price[4]) / price[4] * 100;
                // 股票涨幅大于3加*
                item.setRange(range > 3 ? NumberUtil.roundStr(range, 3) + "**" : NumberUtil.roundStr(range, 3));
                // ETF涨幅大于1.5加*
                if (item.getName().contains("et")) {
                    item.setRange(range > 1.5 ? NumberUtil.roundStr(range, 3) + "**" : NumberUtil.roundStr(range, 3));
                }

                if (range > 3.5 || range < -5) {
                    if (!CacheUtils.alreadyTip.containsKey(stock.getStockCode())) {
                        System.out.println("**********************报警提示UP**********************");
                        CacheUtils.alreadyTip.put(stock.getStockCode(), true);
                        WebSocketServer.broadcastMessage(LocalDateTimeUtil.format(LocalDateTimeUtil.now(), "HH:mm") + " " + stock.getStockShowName() + " " +
                                NumberUtil.roundStr(range, 6));
                        MessageQueueUtils.put(stock.getStockCode(), stock.getStockShowName() + " " + NumberUtil.roundStr(range, 6));
                    }
                }
                if (range > 0 && range < 2 || range > -3 && range < 0) {
                    if (CacheUtils.alreadyTip.containsKey(stock.getStockCode())) {
                        System.out.println("**********************报警提示DOWN**********************");
                        CacheUtils.alreadyTip.remove(stock.getStockCode());
                        WebSocketServer.broadcastMessage(LocalDateTimeUtil.format(LocalDateTimeUtil.now(), "HH:mm") + " " + stock.getStockShowName() + " " +
                                NumberUtil.roundStr(range, 6));
                        MessageQueueUtils.put(stock.getStockCode(), stock.getStockShowName() + " " + NumberUtil.roundStr(range, 6));
                    }
                }

                showStr.append(StrUtil.fillAfter(item.getName(), ' ', firstWidth));
                showStr.append(StrUtil.fillAfter(item.getPrice(), ' ', secondWidth));
                showStr.append(StrUtil.fillAfter(item.getRange(), ' ', thirdWidth));
                showStr.append("| ");
            }
            //清屏
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            //展示
            System.out.println(showStr);
        }
    }

}

package com.fishing.config;

import lombok.Data;

/**
 * 系统参数
 */
@Data
public class SystemParameter {
    public static boolean SCHEDULED_SHOW_ENABLE = true;

    public static boolean SCHEDULED_UPDATE_PRICE_ENABLE = false;

    public static boolean SEND_MAIL_ENABLE = false;

}

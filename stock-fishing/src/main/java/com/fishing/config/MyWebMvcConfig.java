package com.fishing.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * @author tiger
 * 定义过滤规则和静态资源位置
 * 配置跨域
 * 配置拦截器
 */
@Configuration
public class MyWebMvcConfig implements WebMvcConfigurer {

    /**
     * 全局跨域配置
     *
     * @param registry 跨域参数
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedHeaders("*")
                .allowedMethods("*")
                .maxAge(1800);
    }

}

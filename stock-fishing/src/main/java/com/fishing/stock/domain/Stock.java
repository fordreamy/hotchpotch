package com.fishing.stock.domain;

import lombok.Data;

/**
 * 基础信息对象
 *
 * @author psp
 */
@Data
public class Stock {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Integer id;

    /**
     * code
     */
    private String stockCode;

    /**
     * 名称
     */
    private String stockName;

    /**
     * 展示名称
     */
    private String stockShowName;

    /**
     * 位置
     */
    private Integer position;

    /**
     * 当前价
     */
    private Double lastPrice;

}

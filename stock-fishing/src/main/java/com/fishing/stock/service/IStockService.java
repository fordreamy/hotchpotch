package com.fishing.stock.service;

import com.fishing.stock.domain.Stock;

import java.util.List;

/**
 * 基础信息Service接口
 *
 * @author psp
 */
public interface IStockService {

    /**
     * 查询基础信息列表
     *
     * @param stock 基础信息
     * @return 基础信息集合
     */
    List<Stock> selectStockList(Stock stock);

}

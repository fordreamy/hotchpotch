package com.fishing.stock.service.impl;

import com.fishing.stock.domain.Stock;
import com.fishing.stock.mapper.StockMapper;
import com.fishing.stock.service.IStockService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 基础信息Service业务层处理
 *
 * @author psp
 */
@Service
public class StockServiceImpl implements IStockService {
    @Resource
    private StockMapper stockMapper;

    /**
     * 查询基础信息列表
     *
     * @param stock 基础信息
     * @return 基础信息
     */
    @Override
    public List<Stock> selectStockList(Stock stock) {
        return stockMapper.selectStockList(stock);
    }

}

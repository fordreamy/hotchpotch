package com.fishing.stock.mapper;


import com.fishing.stock.domain.Stock;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 基础信息Mapper接口
 *
 * @author psp
 */
@Mapper
public interface StockMapper {

    /**
     * 查询基础信息列表
     *
     * @param stock 基础信息
     * @return 基础信息集合
     */
    List<Stock> selectStockList(Stock stock);

}

package com.fishing.stock.controller;

import com.alibaba.fastjson2.JSON;
import com.fishing.config.SystemParameter;
import com.fishing.util.CacheUtils;
import com.fishing.util.MessageQueueUtils;
import com.fishing.util.StockUtils;
import com.fishing.websocket.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 基础信息Controller
 *
 * @author psp
 */
@Slf4j
@RestController
@RequestMapping("/show")
public class StockController {

    @GetMapping("/sendMsg")
    public Map<String, Object> sendMsg() {
        log.debug("开始更新");
        Map<String, Object> map = new HashMap<>();
        map.put("data", MessageQueueUtils.take());
        map.put("length", MessageQueueUtils.take().size());
        MessageQueueUtils.clear();
        return map;
    }

    @GetMapping("/updateAllStockLastAndNowPrice")
    public String updateAllStockLastAndNowPrice() {
        log.debug("开始更新");
        LocalDateTime start = LocalDateTime.now();
        StockUtils.updatePropertiesFilePrice("last_and_now_price.properties");
        log.info("结束,用时:{}分钟", Duration.between(start, LocalDateTime.now()).toMinutes());
        return "OK";
    }

    @GetMapping("/getAllStockCodeFromAPI")
    public String getAllStockCodeFromAPI() {
        StockUtils.insertPropertiesFileAllStockCodeFromAPI("last_and_now_price.properties");
        return "OK";
    }

    @GetMapping("/showEnable")
    public String showEnable() {
        SystemParameter.SCHEDULED_SHOW_ENABLE = true;
        return "OK";
    }

    @GetMapping("/showDisable")
    public String showDisable() {
        SystemParameter.SCHEDULED_SHOW_ENABLE = false;
        return "OK";
    }

    @GetMapping("/updateEnable")
    public String updateEnable() {
        SystemParameter.SCHEDULED_UPDATE_PRICE_ENABLE = true;
        return "OK";
    }

    @GetMapping("/updateDisable")
    public String updateDisable() {
        SystemParameter.SCHEDULED_UPDATE_PRICE_ENABLE = false;
        return "OK";
    }

    @GetMapping("/againBeginRemind")
    public String againBeginRemind() {
        CacheUtils.alreadyTip = new HashMap<>();
        return "OK";
    }

    @GetMapping("/broadcastMessage")
    public String broadcastMessage() {
        Map<String, Object> map = new HashMap<>();
        map.put("type", 1);
        map.put("message", "come on");
        String msg = JSON.toJSONString(map);
        WebSocketServer.broadcastMessage(msg);
        return "OK";
    }
}

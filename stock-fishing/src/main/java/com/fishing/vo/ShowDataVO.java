package com.fishing.vo;

import lombok.Data;

/**
 * 展示VO
 */
@Data
public class ShowDataVO {
    /**
     * 名称
     */
    String name;
    /**
     * 当前价格
     */
    String price;
    /**
     * 涨幅
     */
    String range;
}

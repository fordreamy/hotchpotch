1.服务器请求过程

``` java
//创建服务端socket
ServerSocket serverSocket =  new ServerSocket(8080, 1, InetAddress.getByName("127.0.0.1"));
socket = serverSocket.accept();         //开始监听连接，此时方法阻塞直到接收到一个请求

//创建输入流,接收来自浏览器等URL请求
input = socket.getInputStream();
Request request = new Request(input);  //根据输入流创建请求对象,封装的体现
request.parse();                       //可以对请求做一些解析拦截等操作

//创建输出流,返回请求的页面等
output = socket.getOutputStream();
Response response = new Response(output); //根据输出流创建相应对象
response.sendStaticResource();            //依据请求内容返回相应结果
相应结果包括Response Headers和Response内容
Response Headers部分内容如下
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: text/html;charset=UTF-8
Date: Fri, 09 Mar 2018 08:35:27 GMT
Content-Length: 8992
Expires: Mon, 26 Jul 1997 05:00:00 GMT
Server: nginx
Response内容是html页面内容等
//
```

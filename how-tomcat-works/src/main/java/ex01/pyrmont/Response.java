package ex01.pyrmont;

import java.io.OutputStream;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.File;

/*
  HTTP Response = Status-Line
    *(( general-header | response-header | entity-header ) CRLF)
    CRLF
    [ message-body ]
    Status-Line = HTTP-Version SP Status-Code SP Reason-Phrase CRLF
*/

public class Response {

    private static final int BUFFER_SIZE = 1024;
    Request request;
    OutputStream output;

    public Response(OutputStream output) {
        this.output = output;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public void sendStaticResource() throws IOException {
        byte[] bytes = new byte[BUFFER_SIZE];
        FileInputStream fis = null;
        try {
            File file = new File(HttpServer.WEB_ROOT, request.getUri());
            System.out.println("开始返回页面了哈...");
            System.out.println(request.getUri());
            byte[] b = new byte[2000];
            //System.out.println(request.toString());
            if (file.exists()) {
                fis = new FileInputStream(file);
                //加上后浏览器才能识别这是HTML,否则输出文本
                String head = "HTTP/1.1 200 OK\r\n" +
                        "Content-Type: text/html\r\n" +
                        "\r\n";
                output.write(head.getBytes());

                int ch = fis.read(bytes, 0, BUFFER_SIZE);
                int bsum = 0;
                int is = 0, ie = 0;
                while (ch != -1) {
                    output.write(bytes, 0, ch);
                    for (int i = 0; i < ch; i++) {
                        b[bsum++] = bytes[i];
                    }
                    ch = fis.read(bytes, 0, BUFFER_SIZE);
                }
                System.out.println("返回总字节数" + bsum);
                if (request.getUri().contains("index.html")) {
                    System.out.println(head + new String(b));
                }
                if (request.getUri().contains("logo.gif")) {
                    System.out.println("接下来的内容是img内容,乱码");
                    System.out.println(new String(b));
                }
            } else {
                // file not found
                String errorMessage = "HTTP/1.1 404 File Not Found\r\n" +
                        "Content-Type: text/html\r\n" +
                        "Content-Length: 23\r\n" +
                        "\r\n" +
                        "<h1>File Not Found</h1>";
                output.write(errorMessage.getBytes());
            }
        } catch (Exception e) {
            // thrown if cannot instantiate a File object
            System.out.println(e.toString());
        } finally {
            if (fis != null)
                fis.close();
        }
    }
}
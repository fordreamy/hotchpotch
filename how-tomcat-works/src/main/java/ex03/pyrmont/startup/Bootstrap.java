package ex03.pyrmont.startup;

import ex03.pyrmont.connector.http.HttpConnector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Bootstrap {
    private static final Logger logger = LogManager.getLogger(Bootstrap.class);

    public static void main(String[] args) {
        HttpConnector connector = new HttpConnector();
        connector.start();
        logger.info("服务启动。。。");
        System.out.println("http://localhost:8080/index.html");
        System.out.println("http://localhost:8080/servlet/PrimitiveServlet");
        System.out.println("http://localhost:8080/servlet/ModernServlet?userName=tarzan&password=pwd");
    }
}
package servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;

public class PrimitiveServlet implements Servlet {
    private static final Logger logger = LogManager.getLogger(PrimitiveServlet.class);

    public void init(ServletConfig config) throws ServletException {
        System.out.println("init");
    }

    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        logger.info("开始返回servlet请求结果,要在头部表明文档类型");
        PrintWriter out = response.getWriter();
        String contentType = "HTTP/1.1 200 OK\r\n" +
                "Content-Type: text/plain\r\n" +
                "\r\n";
        out.println(contentType);
        out.println("Hello. Roses are red.");
        // 如果没有回车换行，下面这行不会输出到客户端
        out.print("Violets are blue.");
    }

    public void destroy() {
        System.out.println("destroy");
    }

    public String getServletInfo() {
        return null;
    }

    public ServletConfig getServletConfig() {
        return null;
    }

}

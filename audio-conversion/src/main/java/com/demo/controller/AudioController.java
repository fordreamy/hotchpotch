package com.demo.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpUtil;
import com.demo.util.AudioUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;

@Controller
public class AudioController {
    private Log log = LogFactory.getLog(AudioController.class);

    @Value("${app_id}")
    private String appId;

    @Value("${secret_key}")
    private String secretKey;

    @RequestMapping("/audio")
    @ResponseBody
    public String audio() {
        log.info("开始进行语音转换");
        String url = "https://labs.wntime.com:8089/u/8207/8207/202011/85d3a69dcb4143fea55b83b5c9ddf8ec.mp3";

        //设置缓存文件路径
        String tempFilePath = System.getProperty("java.io.tmpdir") + url.substring(url.lastIndexOf('/') + 1);
        log.info("缓存文件路径:" + tempFilePath);
        //下载语音文件
        HttpUtil.downloadFile(url, FileUtil.file(tempFilePath));
        //语音转文字
        String result = AudioUtil.standard(tempFilePath, appId, secretKey);
        //删除语音文件
        FileUtil.del(tempFilePath);
        return result;
    }

}

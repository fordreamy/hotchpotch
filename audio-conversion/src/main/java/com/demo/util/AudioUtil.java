package com.demo.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.iflytek.msp.lfasr.LfasrClient;
import com.iflytek.msp.lfasr.model.Message;

public class AudioUtil {

    private static LfasrClient lfasrClient;

    /**
     * 注意：同时只能执行一个 示例
     *
     * @param args a
     * @throws InterruptedException e
     */
    public static void main(String[] args) throws InterruptedException {
        String AUDIO_FILE_PATH = System.getProperty("user.dir") + "\\src\\main\\resources\\audio/lfasr.wav";
        String result = standard(AUDIO_FILE_PATH, "5fbf4efb", "cc736d409fc3ba0ec1dfba2c97ee32e7");
        System.out.println(result);
    }

    /**
     * 简单 demo 样例
     */
    public static String standard(String audioFilePath, String appId, String secretKey) {
        AudioUtil a = new AudioUtil();
        System.out.println(a);
        if (lfasrClient == null) {
            //1、创建客户端实例
            lfasrClient = LfasrClient.getInstance(appId, secretKey);
        }

        //2、上传
        Message task = lfasrClient.upload(audioFilePath);
        String taskId = task.getData();
        System.out.println("转写任务 taskId：" + taskId);

        //3、查看转写进度
        int status = 0;
        while (status != 9) {
            Message message = lfasrClient.getProgress(taskId);
            JSONObject object = JSON.parseObject(message.getData());
            status = object.getInteger("status");
        }

        //4、获取结果
        Message result = lfasrClient.getResult(taskId);

        return result.getData();
    }

}

package builder;

public class BuilderTest {
    public static void main(String[] args) {
        Director director = new Director();
        ConcreteBuilder1 c1 = new ConcreteBuilder1();
        director.construct(c1);
        director.construct(c1);
        director.construct(c1);
        Product p = c1.getResult();
        p.show();

        ConcreteBuilder2 c2 = new ConcreteBuilder2();
        new Director().construct(c2);
        c2.getResult().show();
    }
}

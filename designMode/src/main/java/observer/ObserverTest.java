package observer;

public class ObserverTest {
    public static void main(String[] args) {
        ConcreteSubject concreteSubject = new ConcreteSubject();

        Observer o1 = new ConcreteObserver("观察者A", concreteSubject);
        Observer o2 = new ConcreteObserver("观察者B", concreteSubject);

        concreteSubject.attach(o1);
        concreteSubject.attach(o2);

        concreteSubject.setSubjectState("red");
        concreteSubject.notification();

        concreteSubject.setSubjectState("blue");
        System.out.println("观察者请注意,观察者请注意,观察者请注意,主题变成了"+concreteSubject.getSubjectState());
        concreteSubject.notification();

    }
}

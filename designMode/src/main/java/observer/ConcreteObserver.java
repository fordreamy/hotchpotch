package observer;

public class ConcreteObserver extends Observer {
    private String name;
    private ConcreteSubject concreteSubject;

    public ConcreteObserver(String name, ConcreteSubject concreteSubject) {
        this.name = name;
        this.concreteSubject = concreteSubject;
    }

    @Override
    public void update() {
        System.out.println("观察者" + name + "新状态是" + concreteSubject.getSubjectState());
    }
}

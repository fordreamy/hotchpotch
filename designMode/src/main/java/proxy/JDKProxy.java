package proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class JDKProxy {
    public static void main(String[] args) {
        Subject1 subject = new SubjectImpl1();
        Subject1 proxy = (Subject1) Proxy.newProxyInstance(subject.getClass().getClassLoader(), subject.getClass().getInterfaces(), new ProxyInvocationHandler(subject));
        proxy.sayHello("Tom", "hello");
    }
}

interface Subject1 {
    void sayHello(String name, String greeting);
}

class SubjectImpl1 implements Subject1 {

    @Override
    public void sayHello(String name, String greeting) {
        System.out.println(name + ": " + greeting);
    }
}

class ProxyInvocationHandler implements InvocationHandler {

    Subject1 target;

    public ProxyInvocationHandler(Subject1 target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("进入代理-----------------------------");
        System.out.println("调用的方法名:" + method.getName());
        System.out.println("参数列表:");
        for (int i = 0; i < args.length; i++) {
            System.out.println("参数" + i + ":" + args[i]);
        }
        Object o = method.invoke(target, args);
        System.out.println("结束代理-----------------------------");
        return o;
    }
}
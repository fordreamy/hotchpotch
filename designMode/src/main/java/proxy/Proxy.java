package proxy;

public class Proxy implements Subject {

    Subject realSubject = null;

    public Proxy(){
        this.realSubject = new RealSubject();
    }

    @Override
    public void request() {
        System.out.println("进入代理!");
        realSubject.request();
    }
}

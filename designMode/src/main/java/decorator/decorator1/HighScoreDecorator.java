package decorator.decorator1;

public class HighScoreDecorator extends Decorator{

	public HighScoreDecorator(SchoolReport sr) {
		super(sr);
	}
	
	private void reportHighScore(){
		System.out.println("装饰分数");
	}
	
	@Override
	public void report() {
		super.report();
		this.reportHighScore();
	}

}

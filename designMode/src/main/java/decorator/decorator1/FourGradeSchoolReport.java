package decorator.decorator1;

public class FourGradeSchoolReport extends SchoolReport {

	@Override
	public void report() {
		System.out.println("未经装饰的成绩单");
	}

	@Override
	public void sign() {
		System.out.println("家长签字");
		
	}

}

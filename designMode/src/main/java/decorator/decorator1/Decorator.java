package decorator.decorator1;

public class Decorator extends SchoolReport {

	private SchoolReport sr = null;
	
	public Decorator(SchoolReport sr){
		this.sr = sr;
	}
	
	@Override
	public void report() {
		this.sr.report();
		
	}

	@Override
	public void sign() {
		this.sr.sign();
	}


}

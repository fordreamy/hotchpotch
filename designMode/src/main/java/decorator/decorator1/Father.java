package decorator.decorator1;

/*
 * 装饰模式
 * 用途:
 * 1.在不修改原始类的情况下，动态地扩展一个对象的功能，只是为了避免修改原始类而引起其它bug,适用于扩展。
 * 2.需要动态给一个对象增加功能，这些功能可以动态撤销。即需要增加时则调用装饰方法，撤销则调用原始的。
 * 
 * 
 */

public class Father {

	public static void main(String[] args) {

		SchoolReport sr = null;
		String m;
		
		System.out.println("****************这是原始的功能****************");
		sr = new FourGradeSchoolReport();
		sr.report();

		System.out.println("****************高分装饰****************");
		sr = new FourGradeSchoolReport();
		sr = new HighScoreDecorator(sr);
		sr.report();
		System.out.println("****************排名装饰****************");
		sr = new FourGradeSchoolReport();
		sr = new SortDecorator(sr);
		sr.report();
		System.out.println("****************添加了装饰后，原有的类功能变了，但是没有改变原来的类****************");
		sr = new FourGradeSchoolReport();
		sr = new SortDecorator(new HighScoreDecorator(sr));
		sr.report();
		
		
	}

}

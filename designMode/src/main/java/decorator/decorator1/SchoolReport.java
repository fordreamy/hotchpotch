package decorator.decorator1;

public abstract class SchoolReport {
	
	public abstract void report();
	
	public abstract void sign();

}

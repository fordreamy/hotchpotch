package decorator.decorator1;

public class SortDecorator extends Decorator{

	public SortDecorator(SchoolReport sr) {
		super(sr);
	}
	
	private void reportSort(){
		System.out.println("装饰排名");
	}
	
	@Override
	public void report() {
		super.report();
		this.reportSort();
	}

}

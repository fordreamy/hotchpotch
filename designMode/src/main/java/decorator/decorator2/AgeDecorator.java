package decorator.decorator2;

public class AgeDecorator extends Decorator {
	public AgeDecorator(People people) {
		super(people);
		System.out.println("AgeDecorator(People people)");
	}

	@Override
	public void info() {
		super.info();
		age();
	}

	public void age() {
		System.out.println("年龄:已活了1000年");
	}
}

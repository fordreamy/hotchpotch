package decorator.decorator2;

public class TestDecorator {

	public static void main(String[] args) {

		// 无任何装饰，原始功能
		People people = new Student();
		people.info();
		// 单独装饰
		System.out.println("*********单独装饰*********");
		Decorator decoratorAddress = new AddressDecorator(new Student());
		decoratorAddress.info();
		Decorator decoratorSex = new SexDecorator(new Student());
		decoratorSex.info();
		Decorator decoratorAge = new AgeDecorator(new Student());
		decoratorAge.info();
		// 组合装饰
		System.out.println("*********组合装饰*********");
		Decorator decoratorAddressAge = new SexDecorator(new AgeDecorator(new AddressDecorator(new Student())));
		decoratorAddressAge.info();
		//不用装饰模式
		System.out.println("*********不用装饰模式*********");
		NonDecoratorAdress nonDecoratorAdress = new NonDecoratorAdress();
		nonDecoratorAdress.info();
		NonDecoratorAdress nonDecoratorAge = new NonDecoratorAge();
		nonDecoratorAge.info();
		NonDecoratorAdress nonDecoratorSex = new NonDecoratorSex();
		nonDecoratorSex.info();

	}

}

package decorator.decorator2;

public class NonDecoratorAge extends NonDecoratorAdress {  
  
    @Override  
    public void info() {
        super.info();
        age();
    }  
  
    public void age(){
    	System.out.println("年龄:已活了1000年");
    }  
  
}  



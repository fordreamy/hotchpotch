package decorator.decorator2;

public class NonDecoratorSex extends NonDecoratorAge {

	@Override
	public void info() {
		super.info();
		sex();
	}

	public void sex() {
		System.out.println("性别:雄性");
	}

}

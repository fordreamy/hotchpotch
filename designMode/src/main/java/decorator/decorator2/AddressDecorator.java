package decorator.decorator2;

public class AddressDecorator extends Decorator {
	
	public AddressDecorator(People people) {
		super(people);
		System.out.println("AddressDecorator(People people)");
	}

	@Override
	public void info() {
		super.info();
		address();
	}

	public void address() {
		System.out.println("地址：在火星居住");
	}

}

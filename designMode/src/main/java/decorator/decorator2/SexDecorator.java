package decorator.decorator2;

public class SexDecorator extends Decorator {
	public SexDecorator(People people) {
		super(people);
		System.out.println("AgeDecorator(People people)");
	}

	@Override
	public void info() {
		super.info();
		sex();
	}

	public void sex() {
		System.out.println("性别:雄性");
	}
}

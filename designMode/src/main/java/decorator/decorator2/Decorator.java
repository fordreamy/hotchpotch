package decorator.decorator2;

public abstract class Decorator extends People {
	private People people;

	public Decorator(People people) {
		System.out.println("Decorator(People people) "+people.getClass());
		this.people = people;
	}

	@Override
	public void info() {
		people.info();
	}
}

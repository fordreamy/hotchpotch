package strategy.strategy1;

public interface IStrategy {
	void operate();
}

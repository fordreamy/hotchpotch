package strategy.strategy1;

public class Context {

	private IStrategy istrategy;
	
	public Context(IStrategy istrategy){
		this.istrategy = istrategy;
	}
	
	public void operator(){
		this.istrategy.operate();
	}
	
}

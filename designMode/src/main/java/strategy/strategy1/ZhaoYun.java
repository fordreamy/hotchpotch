package strategy.strategy1;


/*策略模式
 * 
 * 
 * 
 * 
 * */
public class ZhaoYun {

	public static void main(String[] args) {

		new Context(new BackDoor()).operator();
		new Context(new GivenGreenDoor()).operator();
		
	}

}

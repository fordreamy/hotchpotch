package strategy.strategy2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JavaSortStrategy {


	public static void main(String[]args){
		List<Book> list = new ArrayList<Book>();
		list.add(new Book(2.0,"b1",100));
		list.add(new Book(5.0,"b5",200));
		list.add(new Book(3.0,"b3",300));
		
		Collections.sort(list, new Asc());
		
		for(Book book:list){
			System.out.println(book.price+" "+book.name+" "+book.page);
		}
		System.out.println("--------------------------------------------");

		
		Collections.sort(list, new Desc());
		
		for(Book book:list){
			System.out.println(book.price+" "+book.name+" "+book.page);
		}
	}
	
}








package strategy.strategy2;

class Book {
	public double price = 0;
	public String name = "";
	public int page = 0;

	Book(double price, String name, int page) {
		this.price = price;
		this.name = name;
		this.page = page;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

}
package strategy.strategy2;

import java.util.Comparator;

class Asc implements Comparator<Object> {

	@Override
	public int compare(Object o1, Object o2) {
		Book book1 = (Book) o1;
		Book book2 = (Book) o2;
		
		return new Double(book1.price).compareTo(new Double(book2.price));
	}

	
}

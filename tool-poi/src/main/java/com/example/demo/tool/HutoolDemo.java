package com.example.demo.tool;

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.alibaba.fastjson.JSON;

import java.util.List;
import java.util.Map;

public class HutoolDemo {
    public static void main(String[] args) {
        System.out.println(System.getProperty("user.dir"));
//        String path = System.getProperty("user.dir") + "/测试.xlsx";
        String path = System.getProperty("user.dir") + "/测试2.xls";
        ExcelReader reader = ExcelUtil.getReader(path);
        List<Map<String, Object>> readAll = reader.readAll();
        System.out.println(JSON.toJSONString(readAll));
        for (Map<String, Object> m : readAll) {
            System.out.println(m);
            System.out.println(m.get("统一信用代码"));
        }
    }
}

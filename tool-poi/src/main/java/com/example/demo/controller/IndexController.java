package com.example.demo.controller;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSON;
import com.example.demo.model.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class IndexController {

    Log log = LogFactory.getLog(getClass());

    @GetMapping(value = {"", "/index.html"})
    public ModelAndView index(HttpServletRequest request) {

        ModelAndView mv = new ModelAndView();

        User user = (User) request.getSession().getAttribute("user");
        log.info(JSON.toJSONString(user));
        if (user == null) {
            mv.setViewName("redirect:/user/login");
        } else {
            mv.setViewName("page/index");
            mv.addObject(user);
        }

        return mv;
    }
}

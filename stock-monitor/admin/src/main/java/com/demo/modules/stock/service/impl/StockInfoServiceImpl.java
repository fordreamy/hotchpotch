package com.demo.modules.stock.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.common.utils.Query;
import com.demo.modules.stock.dao.StockInfoDao;
import com.demo.modules.stock.entity.StockInfoEntity;
import com.demo.modules.stock.service.StockDayService;
import com.demo.modules.stock.service.StockInfoService;
import com.demo.utils.PageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service("stockInfoService")
public class StockInfoServiceImpl extends ServiceImpl<StockInfoDao, StockInfoEntity> implements StockInfoService {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    StockDayService stockDayService;

    @Resource
    StockInfoDao stockInfoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String stockName = (String) params.get("stockName");
        String industry = (String) params.get("industry");
        String stockCode = (String) params.get("stockCode");
        IPage<StockInfoEntity> page = new Query<StockInfoEntity>().getPage(params);
        stockInfoDao.queryList(page, stockCode, stockName, industry);
        return new PageUtils(page);
    }

    @Override
    public StockInfoEntity getByCode(String code) {
        Wrapper<StockInfoEntity> wrapper = new QueryWrapper<StockInfoEntity>().eq("stock_code", code);
        return this.getOne(wrapper);
    }

    @Override
    public void importStockInfo(List<StockInfoEntity> list) {
        for (StockInfoEntity tmp : list) {
            logger.info("插入股票信息:" + tmp);
            tmp.setCreateTime(DateUtil.date().toTimestamp());
            this.save(tmp);
        }
    }
}

package com.demo.modules.stock.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.modules.stock.entity.StockIndexEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StockIndexDao extends BaseMapper<StockIndexEntity> {


}

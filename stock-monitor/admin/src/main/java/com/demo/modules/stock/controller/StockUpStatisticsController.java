package com.demo.modules.stock.controller;

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.demo.exception.RRException;
import com.demo.modules.stock.entity.StockInfoEntity;
import com.demo.modules.stock.service.StockInfoService;
import com.demo.modules.stock.service.StockUpStatisticsService;
import com.demo.modules.sys.controller.AbstractController;
import com.demo.utils.PageUtils;
import com.demo.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/stockUpStatistics")
public class StockUpStatisticsController extends AbstractController {

    @Autowired
    StockUpStatisticsService stockUpStatisticsService;

    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = stockUpStatisticsService.queryPage(params);
        return R.ok().put("page", page);
    }

    @RequestMapping("/singleKLine")
    public R singleKLine(@RequestParam Map<String, Object> params) {
        logger.debug("参数:" + params);
        PageUtils page = stockUpStatisticsService.singleKLine(params);
        return R.ok().put("page", page);
    }

}

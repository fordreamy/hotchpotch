package com.demo.modules.job.task;

import cn.hutool.core.date.DateUtil;
import com.demo.modules.stock.entity.StockInfoEntity;
import com.demo.modules.stock.service.StockDayService;
import com.demo.modules.stock.service.StockIndexService;
import com.demo.modules.stock.service.StockInfoService;
import com.demo.modules.stock.util.StockUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

/**
 * 从网络获取股票交易价格
 * UpdateStockPriceTaskA为spring bean的名称
 */
@Component("updateStockPriceTaskA")
public class UpdateStockPriceTaskA implements ITask {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    StockInfoService stockInfoService;

    @Autowired
    StockIndexService stockIndexService;

    @Autowired
    StockDayService stockDayService;

    @Override
    public void run(String params) {
        logger.debug("TestTask定时任务正在执行，参数为：{}", params);
        String[] p = params.split(",");
        if ("1".equals(p[0])) {
            getStockClosePriceByBaidu();          //从百度获取股票历史价格
        }
        if ("2".equals(p[0])) {
            insertSSECClosePrice();               //从百度获取近上证指数
        }
        if ("3".equals(p[0])) {
            getStockPriceByBear(p[1], p[2]);      //从小熊网站获取股票历史价格
        }
        if ("4".equals(p[0])) {
            getStockPriceBySina();                //从新浪获取每日股票价格
        }
    }

    /**
     * 从百度获取股票历史价格
     */
    private void getStockClosePriceByBaidu() {
        logger.debug("从百度获取股票历史价格");
        List<StockInfoEntity> stockInfoEntityList = stockInfoService.list();
        List<List<String>> price;
        for (StockInfoEntity stock : stockInfoEntityList) {
            //第三方接口获取股票的历史价格
            price = StockUtils.getStockClosePriceByBaidu(stock.getStockCode());
            if (price == null) {
                continue;
            }
            logger.debug("成功获取股票" + stock.getStockCode() + "历史价格:" + price);
            stockDayService.insertOrUpdateStockHistoryAllPrice(stock, price);
        }
    }

    /**
     * 从新浪获取每日股票价格,http://hq.sinajs.cn/list=sz000002
     */
    private void getStockPriceBySina() {
        logger.debug("从新浪获取每日股票价格");
        List<StockInfoEntity> stockInfoEntityList = stockInfoService.list();
        String[] price;
        for (StockInfoEntity stock : stockInfoEntityList) {
            price = StockUtils.getStockPriceBySina(stock.getStockExchange() + stock.getStockCode());
            if (price == null) continue;
            logger.debug("成功获取股票" + stock.getStockCode() + "股票价格");
            if (Double.parseDouble(price[0]) == 0) continue;
            stockDayService.insertOrUpdateStockRealTimePrice(price[4], stock, price);
        }
    }

    /**
     * 从百度获取近上证指数
     */
    private void insertSSECClosePrice() {
        logger.debug("从百度获取近一年内的上证指数");
        List<List<String>> stockPrice = StockUtils.getStockClosePriceByBaidu("-1");
        stockIndexService.insertSSECClosePrice(stockPrice, "000001");
    }

    /**
     * 从https://www.doctorxiong.club获取股票价格
     *
     * @param year 数据年限
     * @param date 日期
     */
    private void getStockPriceByBear(String year, String date) {
        logger.debug("从https://www.doctorxiong.club获取股票价格,年限" + year);
        List<StockInfoEntity> stockInfoEntityList = stockInfoService.list();
        List<List<String>> price;
        for (StockInfoEntity stock : stockInfoEntityList) {
            //第三方接口获取股票的历史价格
            price = StockUtils.getStockPriceByBear(stock.getStockCode(), Integer.parseInt(year));
            if (price == null) {
                continue;
            }
            logger.debug("成功获取股票" + stock.getStockCode() + "历史价格:" + price);
            stockDayService.insertOrUpdateStockHistoryAllPrice(stock, price);
        }
    }


}

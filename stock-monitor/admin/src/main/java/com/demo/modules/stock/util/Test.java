package com.demo.modules.stock.util;

import cn.hutool.core.lang.Console;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.sax.handler.RowHandler;

import java.util.List;

public class Test {
    private static RowHandler createRowHandler() {
        return new RowHandler() {
            @Override
            public void handle(int sheetIndex, long rowIndex, List<Object> rowlist) {
                Console.log("[{}] [{}] {}", sheetIndex, rowIndex, rowlist);
            }
        };
    }

    public static void main(String[] args) {
//        ExcelUtil.read03BySax("C:/Users/psp/Desktop/主板A股.xls", 1, createRowHandler());

        ExcelReader reader = ExcelUtil.getReader("C:/Users/psp/Desktop/主板A股.xls");
        System.out.println(reader);

//        ExcelReader reader = ExcelUtil.getReader("C:/Users/psp/Desktop/A股列表.xlsx");
//        System.out.println(reader);
    }
}

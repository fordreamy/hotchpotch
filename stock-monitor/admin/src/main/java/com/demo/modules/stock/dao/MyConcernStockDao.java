package com.demo.modules.stock.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.modules.stock.entity.MyConcernStockEntity;
import com.demo.modules.stock.entity.StockDayEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MyConcernStockDao extends BaseMapper<MyConcernStockEntity> {


}

package com.demo.modules.stock.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.demo.modules.stock.entity.StockInfoEntity;
import com.demo.modules.stock.entity.StockUpStatisticsEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface StockUpStatisticsDao extends BaseMapper<StockUpStatisticsEntity> {

    List<StockInfoEntity> queryList(Map<String, Object> params);

    List<StockUpStatisticsEntity> singleKLine(@Param("stockCodes") List<String> stockCodes);

}

package com.demo.modules.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.modules.stock.entity.StockIndexEntity;

import java.util.List;

public interface StockIndexService extends IService<StockIndexEntity> {

    List<StockIndexEntity> getSSECClosePrice(String stockCode);

    void insertSSECClosePrice(List<List<String>> stockPrice, String stockCode);

}

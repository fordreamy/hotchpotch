

package com.demo.modules.sys.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 系统页面视图
 */
@Controller
public class SysPageController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @RequestMapping("modules/{module}/{url}.html")
    public String module(@PathVariable("module") String module, @PathVariable("url") String url) {
        return "modules/" + module + "/" + url;
    }

    @RequestMapping(value = {"/", "index.html"})
    public String index() {
        logger.debug("跳转到index!");
        return "index";
    }

    @RequestMapping("index1.html")
    public String index1() {
        logger.debug("跳转到index1!");
        return "index1";
    }

    @RequestMapping("login.html")
    public String login() {
        logger.debug("跳转登录页面!");
        return "login";
    }

    @RequestMapping("main.html")
    public String main() {
        logger.debug("跳转到main!");
        return "main";
    }

    @RequestMapping("404.html")
    public String notFound() {
        logger.debug("找不到页面啦!");
        return "404";
    }

}

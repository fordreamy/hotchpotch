package com.demo.modules.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.modules.stock.entity.StockDayEntity;
import com.demo.modules.stock.entity.StockInfoEntity;
import com.demo.utils.PageUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public interface StockDayService extends IService<StockDayEntity> {

    void insertOrUpdateStockHistoryClosePrice(StockInfoEntity stockInfoEntity, List<List<String>> price);

    void insertOrUpdateStockRealTimePrice(String stockDate, StockInfoEntity stock, String[] price);

    void insertOrUpdateStockHistoryAllPrice(StockInfoEntity stockInfoEntity, List<List<String>> price);

    String getRecentStockExchangeDate();

    void insertStockHistoryClosePrice();

    List<StockDayEntity> getStockHistoryClosePrice(String stockCode);

    LinkedList<String> riseAndFall(int flag, Integer num);

}

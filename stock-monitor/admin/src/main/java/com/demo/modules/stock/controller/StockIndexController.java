package com.demo.modules.stock.controller;

import com.demo.modules.stock.entity.StockIndexEntity;
import com.demo.modules.stock.service.StockIndexService;
import com.demo.modules.stock.util.StockUtils;
import com.demo.modules.sys.controller.AbstractController;
import com.demo.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/stockIndex")
public class StockIndexController extends AbstractController {

    @Autowired
    StockIndexService stockIndexService;

    @RequestMapping("/getSSECClosePrice")
    @ResponseBody
    public Map<String, Object> getSSECClosePrice(@RequestParam String stockCode) {
        logger.info("stockCode:" + stockCode);

        List<StockIndexEntity> stockInfoEntityList = stockIndexService.getSSECClosePrice(stockCode);

        Map<String, Object> result = new HashMap<>();
        result.put("stockName", "SSEC");
        result.put("title", "上证指数");
        result.put("maxValue", 3000);
        result.put("minValue", 2000);
        result.put("data", StockUtils.covertToEcharts2(stockInfoEntityList));
        return result;
    }

    @RequestMapping("/insertSSECClosePrice")
    public R insertSSECClosePrice() {
        List<List<String>> stockPrice = StockUtils.getStockClosePriceByBaidu("-1");
        stockIndexService.insertSSECClosePrice(stockPrice,"000001");
        return R.ok();
    }

}

package com.demo.modules.stock.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@TableName("STOCK_UP_STATISTICS")
public class StockUpStatisticsEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.UUID)
    private String id;
    private String stockCode;           //股票code
    private String stockName;           //股票名字
    private Integer continueRiseStopDays;           //连续涨停天数
    private Integer continueFallStopDays;           //连续跌停天数
    private Integer statisticsDays;    //涨跌统计天数
    private Integer continueRiseDays;    //
    private Integer continueFallDays;    //
    private Double continueRiseRate;    //
    private Double continueFallRate;    //
    private Integer riseDays;    //
    private Integer riseStopDays;    //
    private Integer fallDays;    //
    private Integer fallStopDays;    //
    private Timestamp createTime;
    private Timestamp updateTime;
    private String kLineName;

}

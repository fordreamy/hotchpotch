package com.demo.modules.stock.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@Data
@TableName("STOCK_INDEX")
public class StockIndexEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.UUID)
    private String id;

    private Date dayDate;

    @TableField(value = "open_price")
    private Double openPrice;
    @TableField(value = "close_price")
    private Double closePrice;
    private Double lowest;
    private Double highest;
    private Double volume;
    private Double netChangeRatio;

    private String stockCode;           //股票code

    private String stockName;           //股票名字

    private Timestamp createTime;

}

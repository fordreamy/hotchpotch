package com.demo.modules.stock.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.modules.stock.entity.StockDayEntity;
import com.demo.modules.stock.entity.StockInfoEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface StockDayDao extends BaseMapper<StockDayEntity> {


}

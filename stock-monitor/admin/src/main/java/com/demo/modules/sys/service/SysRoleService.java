

package com.demo.modules.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.modules.sys.entity.SysRoleEntity;
import com.demo.utils.PageUtils;

import java.util.Map;


/**
 * 角色
 *
 *
 */
public interface SysRoleService extends IService<SysRoleEntity> {

	PageUtils queryPage(Map<String, Object> params);

	void saveRole(SysRoleEntity role);

	void update(SysRoleEntity role);

	void deleteBatch(Long[] roleIds);

}

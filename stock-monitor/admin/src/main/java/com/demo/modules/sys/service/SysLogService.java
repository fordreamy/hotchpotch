

package com.demo.modules.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.modules.sys.entity.SysLogEntity;
import com.demo.utils.PageUtils;

import java.util.Map;


/**
 * 系统日志
 *
 *
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

}

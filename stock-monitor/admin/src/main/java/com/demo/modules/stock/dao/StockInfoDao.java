package com.demo.modules.stock.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.demo.modules.stock.entity.StockInfoEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface StockInfoDao extends BaseMapper<StockInfoEntity> {

    IPage<StockInfoEntity> queryList(IPage<StockInfoEntity> page, String stockCode, String stockName, String industry);

}

package com.demo.modules.stock.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@TableName("STOCK_INFO")
public class StockInfoEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.UUID)
    private String id;

    private String stockCode;           //股票code

    private String stockName;           //股票名字

    private String stockType = "A";     //股票类型

    private String listingDate;         //上市日期

    private Long marketCapital;         //总股本

    private Long negotiableCapital;         //流通股

    private String industry;         //所属行业

    private String stockExchange;       //证券交易所

    private String website;            //公司网址

    private Double currentPrice;       //当前价格

    private Double priceLimit;       //涨跌幅

    private Timestamp createTime;

    private Timestamp updateTime;


}

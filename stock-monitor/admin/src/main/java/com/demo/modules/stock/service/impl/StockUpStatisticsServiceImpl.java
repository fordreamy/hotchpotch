package com.demo.modules.stock.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.common.utils.Query;
import com.demo.modules.stock.dao.StockUpStatisticsDao;
import com.demo.modules.stock.entity.StockDayEntity;
import com.demo.modules.stock.entity.StockInfoEntity;
import com.demo.modules.stock.entity.StockUpStatisticsEntity;
import com.demo.modules.stock.service.StockDayService;
import com.demo.modules.stock.service.StockUpStatisticsService;
import com.demo.utils.PageUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("stockUpStatisticsService")
public class StockUpStatisticsServiceImpl extends ServiceImpl<StockUpStatisticsDao, StockUpStatisticsEntity> implements StockUpStatisticsService {

    @Resource
    StockUpStatisticsDao stockUpStatisticsDao;

    @Resource
    StockDayService stockDayService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = (String) params.get("name");
        String kLineName = (String) params.get("kLineName");
        String continueRiseDays = (String) params.get("continueRiseDays");
        IPage<StockUpStatisticsEntity> page;
        if ("".equals(params.get("sidx").toString())) {
            //没有点击排序时的默认排序
            page = this.page(
                    new Query<StockUpStatisticsEntity>().getPage(params),
                    new QueryWrapper<StockUpStatisticsEntity>()
                            .eq(StringUtils.isNotBlank(kLineName), "k_line_name", kLineName)
                            .eq(StringUtils.isNotBlank(continueRiseDays), "continue_rise_days", continueRiseDays)
                            .like(StringUtils.isNotBlank(name), "stock_name", name).orderByDesc("continue_rise_stop_days")
            );
        } else {
            //页面传递的排序
            page = this.page(
                    new Query<StockUpStatisticsEntity>().getPage(params),
                    new QueryWrapper<StockUpStatisticsEntity>()
                            .like(StringUtils.isNotBlank(name), "stock_name", name)
                            .eq(StringUtils.isNotBlank(continueRiseDays), "continue_rise_days", continueRiseDays)
                            .eq(StringUtils.isNotBlank(kLineName), "k_line_name", kLineName)
            );
        }


        return new PageUtils(page);
    }

    @Override
    public PageUtils singleKLine(Map<String, Object> params) {
        IPage<StockUpStatisticsEntity> page = new Query<StockUpStatisticsEntity>().getPage(params);
        String date;
        if (params.get("date") == null || params.get("date").toString().equals("")) {
            date = stockDayService.getRecentStockExchangeDate();
        } else {
            date = params.get("date").toString();
        }
        String kLineName = params.get("kLineName") == null ? "光头光脚阳线" : params.get("kLineName").toString();
        List<String> stockCodes = new ArrayList<>();

        List<StockDayEntity> list = stockDayService.list(new QueryWrapper<StockDayEntity>()
                .select("stock_code,DAY_DATE,open_price,close_price,highest,lowest")
                .eq("DAY_DATE", date));
        for (StockDayEntity item : list) {
            Double openPrice = item.getOpenPrice();
            Double closePrice = item.getClosePrice();
            Double lowest = item.getLowest();
            Double highest = item.getHighest();
            if ("光头光脚阳线".equals(kLineName)) {
                if (closePrice.equals(highest) && openPrice.equals(lowest)) {
                    stockCodes.add(item.getStockCode());
                }
            } else if ("光头阳线".equals(kLineName)) {
                if (closePrice.equals(highest)) {
                    stockCodes.add(item.getStockCode());
                }
            } else if ("光脚阳线".equals(kLineName)) {
                if (openPrice.equals(lowest)) {
                    stockCodes.add(item.getStockCode());
                }
            }
        }
        if (stockCodes.size() == 0) stockCodes.add("no");
        List<StockUpStatisticsEntity> records = stockUpStatisticsDao.singleKLine(stockCodes);
        page.setRecords(records);
        page.setCurrent(1);
        page.setPages(600);
        page.setSize(20);
        page.setTotal(10000);

        return new PageUtils(page);
    }

    public static void main(String[] args) {
        System.out.println(DateUtil.now().substring(0, 10));
    }
}

package com.demo.modules.stock.util;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;

import java.util.Date;

public class HuToolDateDemo {
    public static void main(String[] args) {
        //当前时间
        Date date = DateUtil.date();
        System.out.println("当前时间:" + date);
        System.out.println("当前时间:" + date.toString());
        System.out.println("当前时间:" + DateUtil.date(System.currentTimeMillis()));
        System.out.println("当前时间:" + DateUtil.now());
        System.out.println("当前时间:" + DateUtil.today());

        //日期时间偏移
        System.out.println("前三个月时间:" + DateUtil.offset(new Date(), DateField.MONTH, 3));
        System.out.println("后三个月时间:" + DateUtil.offsetMonth(new Date(), -3).toString("yyyy-MM-dd"));

        //日期时间差
        Date date1 = new Date();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Date date2 = new Date();

        //相差一个月，31天
        long betweenDay = DateUtil.between(date1, date2, DateUnit.SECOND);
        System.out.println(betweenDay);


    }
}

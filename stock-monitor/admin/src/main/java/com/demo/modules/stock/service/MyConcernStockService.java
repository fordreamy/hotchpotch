package com.demo.modules.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.modules.stock.entity.MyConcernStockEntity;
import com.demo.modules.stock.entity.StockDayEntity;

public interface MyConcernStockService extends IService<MyConcernStockEntity> {

}

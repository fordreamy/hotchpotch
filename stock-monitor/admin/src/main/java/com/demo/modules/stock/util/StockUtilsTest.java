package com.demo.modules.stock.util;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.ParserConfig;
import com.demo.modules.stock.entity.StockDayEntity;
import com.demo.modules.stock.entity.StockIndexEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class StockUtilsTest {


    public static void main(String[] args) throws IOException {

        System.out.println("测试从百度获取股票历史价格");
        System.out.println(StockUtils.getStockClosePriceByBaidu("600036"));
        System.out.println(StockUtils.getStockClosePriceByBaidu("002936"));

        System.out.println("测试从新浪获取股票当天价格");
        System.out.println(Arrays.toString(StockUtils.getStockPriceBySina("sh600036")));
        System.out.println(Arrays.toString(StockUtils.getStockPriceBySina("sz002936")));


    }
}

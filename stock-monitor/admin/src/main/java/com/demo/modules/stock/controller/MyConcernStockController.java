package com.demo.modules.stock.controller;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.demo.modules.stock.entity.MyConcernStockEntity;
import com.demo.modules.stock.entity.StockDayEntity;
import com.demo.modules.stock.entity.StockInfoEntity;
import com.demo.modules.stock.service.MyConcernStockService;
import com.demo.modules.stock.service.StockDayService;
import com.demo.modules.stock.service.StockInfoService;
import com.demo.modules.stock.util.StockUtils;
import com.demo.modules.sys.controller.AbstractController;
import com.demo.utils.R;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/myConcernStock")
public class MyConcernStockController extends AbstractController {

    Log log = LogFactory.getLog(getClass());

    @Autowired
    MyConcernStockService myConcernStockService;

    @Autowired
    StockInfoService stockInfoService;

    @Autowired
    StockDayService stockDayService;

    @RequestMapping("/list")
    public R list(@RequestParam(required = false) String label) {
        /*if (getUserId() == 1) {
            return R.ok().put("list", myConcernStockService.list());
        }*/
        if (StringUtils.isEmpty(label)) {
            return R.ok().put("list", myConcernStockService.list(new QueryWrapper<MyConcernStockEntity>()
                    .eq("user_id", getUserId())
                    .eq("label", "暂无分类")
            ));
        }
        if ("全部".equals(label)) {
            return R.ok().put("list", myConcernStockService.list(new QueryWrapper<MyConcernStockEntity>()
                    .eq("user_id", getUserId())
            ));
        }
        return R.ok().put("list", myConcernStockService.list(new QueryWrapper<MyConcernStockEntity>()
                .eq("user_id", getUserId())
                .eq("label", label)
        ));
    }

    @RequestMapping("/queryRecentStock")
    public R queryRecentFund(@RequestParam String stockCode, @RequestParam int year) {
        StockInfoEntity stockInfoEntity = stockInfoService.getByCode(stockCode);
        String startDate = DateUtil.formatDate(DateUtil.offset(new Date(), DateField.YEAR, -1 * year));
        String endDate = DateUtil.format(new Date(), "yyyy-MM-dd");

        List<StockDayEntity> stockDayEntityList = stockDayService.list(new QueryWrapper<StockDayEntity>()
                .select("id,day_date,open_price,close_price,lowest,highest,volume,net_change_ratio,stock_code,stock_name")
                .between("DAY_DATE", startDate, endDate)
                .eq("STOCK_CODE", stockCode).orderByAsc("DAY_DATE"));

        return R.ok().put("stockName", stockInfoEntity.getStockName())
                .put("title", stockInfoEntity.getStockName() + "(" + stockInfoEntity.getIndustry() + ")-" + stockInfoEntity.getStockCode() + "-" + stockInfoEntity.getListingDate())
                .put("maxValue", stockDayEntityList.stream().max(Comparator.comparing(StockDayEntity::getClosePrice)).get().getClosePrice())
                .put("minValue", stockDayEntityList.stream().min(Comparator.comparing(StockDayEntity::getClosePrice)).get().getClosePrice())
                .put("data", StockUtils.covertToKLine(stockDayEntityList));
    }

    @RequestMapping("/labelOptions")
    public R industry() {
        logger.debug("查询所有关注股票的分类标签");
        List<MyConcernStockEntity> list = myConcernStockService.list(new QueryWrapper<MyConcernStockEntity>()
                .select("DISTINCT(label)").orderByAsc("label"));
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setId(i + "");
        }
        return R.ok().put("list", list);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestParam String stockCode, @RequestParam String label) {
        if (stockInfoService.getByCode(stockCode) == null) return R.error("不存在此股票code:" + stockCode);
        if (StringUtils.isEmpty(label)) {
            label = "暂无分类";
        }

        MyConcernStockEntity myConcernStockEntity = new MyConcernStockEntity();
        myConcernStockEntity.setUserId(getUserId());
        myConcernStockEntity.setLabel(label);
        myConcernStockEntity.setStockCode(stockCode);
        log.info("保存成功!" + JSON.toJSONString(myConcernStockEntity));
        myConcernStockService.save(myConcernStockEntity);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(String stockCode) {
        log.info("移除股票code:" + stockCode);
        myConcernStockService.remove(new QueryWrapper<MyConcernStockEntity>().eq("stock_code", stockCode));
        return R.ok();
    }

    public static void main(String[] args) {
        String startDate = DateUtil.formatDate(DateUtil.offset(new Date(), DateField.YEAR, -12));
        System.out.println(startDate);
    }

}

package com.demo.modules.stock.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.common.utils.Query;
import com.demo.modules.stock.dao.MyConcernStockDao;
import com.demo.modules.stock.dao.StockInfoDao;
import com.demo.modules.stock.entity.MyConcernStockEntity;
import com.demo.modules.stock.entity.StockInfoEntity;
import com.demo.modules.stock.service.MyConcernStockService;
import com.demo.modules.stock.service.StockInfoService;
import com.demo.utils.PageUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("myConcernStockService")
public class MyConcernStockServiceImpl extends ServiceImpl<MyConcernStockDao, MyConcernStockEntity> implements MyConcernStockService {


}

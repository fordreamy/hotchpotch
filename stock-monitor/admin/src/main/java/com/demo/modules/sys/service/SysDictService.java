

package com.demo.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.modules.sys.entity.SysDictEntity;
import com.demo.utils.PageUtils;

import java.util.Map;

/**
 * 数据字典
 *
 *
 */
public interface SysDictService extends IService<SysDictEntity> {

    PageUtils queryPage(Map<String, Object> params);
}


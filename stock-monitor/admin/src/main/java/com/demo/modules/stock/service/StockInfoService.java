package com.demo.modules.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.modules.stock.entity.StockInfoEntity;
import com.demo.utils.PageUtils;

import java.util.List;
import java.util.Map;

public interface StockInfoService extends IService<StockInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    StockInfoEntity getByCode(String code);

    void importStockInfo(List<StockInfoEntity> list);
}

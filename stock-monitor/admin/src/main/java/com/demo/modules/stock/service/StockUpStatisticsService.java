package com.demo.modules.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.modules.stock.entity.StockInfoEntity;
import com.demo.modules.stock.entity.StockUpStatisticsEntity;
import com.demo.utils.PageUtils;

import java.util.List;
import java.util.Map;

public interface StockUpStatisticsService extends IService<StockUpStatisticsEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils singleKLine(Map<String, Object> params);

}

package com.demo.modules.stock.controller;

import com.demo.modules.stock.entity.StockDayEntity;
import com.demo.modules.stock.entity.StockInfoEntity;
import com.demo.modules.stock.service.StockDayService;
import com.demo.modules.stock.service.StockInfoService;
import com.demo.modules.stock.util.StockUtils;
import com.demo.modules.sys.controller.AbstractController;
import com.demo.utils.R;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;

@RestController
@RequestMapping("/stockDay")
public class StockDayController extends AbstractController {

    @Resource
    StockDayService stockDayService;

    @Resource
    StockInfoService stockInfoService;

    /**
     * @param stockCode 股票code
     * @return 股票的K线图
     */
    @RequestMapping("/getStockKLine")
    @ResponseBody
    public Map<String, Object> getStockKLine(@RequestParam String stockCode) {
        logger.info("查询股票{}的K线图" + stockCode);

        StockInfoEntity stockInfo = stockInfoService.getByCode(stockCode);
        List<StockDayEntity> stockInfoEntityList = stockDayService.getStockHistoryClosePrice(stockCode);


        Map<String, Object> result = new HashMap<>();
        result.put("stockName", stockInfo.getStockName());
        result.put("title", stockInfo.getStockName() + "(" + stockInfo.getIndustry() + ")-" + stockInfo.getStockCode() + "-" + stockInfo.getListingDate());
        result.put("data", StockUtils.covertToKLine(stockInfoEntityList));
        return result;
    }

    @RequestMapping("/riseNum")
    @ResponseBody
    public R riseNum(@RequestParam Integer num) {
        return R.ok().put("list", stockDayService.riseAndFall(1, num));
    }

    @RequestMapping("/fallNum")
    @ResponseBody
    public R fallNum(@RequestParam Integer num) {
        return R.ok().put("list", stockDayService.riseAndFall(2, num));
    }

}

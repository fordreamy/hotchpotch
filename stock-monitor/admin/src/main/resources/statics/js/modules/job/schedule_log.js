$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/scheduleLog/list',
        datatype: "json",
        colModel: [			
            { label: '日志ID', name: 'logId', width: 20, key: true },
			{ label: '任务ID', name: 'jobId', width: 20},
			{ label: 'bean名称', name: 'beanName', width: 60 },
			{ label: '参数', name: 'params', width: 30 },
			{ label: '状态', name: 'status', width: 15, formatter: function(value, options, row){
			    var r;
			    if(value === 0) r = '<span class="label label-success">成功</span>';
			    if(value === 1) r = '<span class="label label-danger pointer" onclick="vm.showError('+row.logId+')">失败</span>';
			    if(value === 2) r = '<span class="label label-danger pointer">执行中</span>';
				return r;
			}},
			{ label: '耗时', name: 'timesDisplay', width: 20 },
			{ label: '执行时间', name: 'createTime', width: 80 },
			{ label: '备注', name: 'remark', width: 80 },
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50,100,200],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: false,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			jobId: null
		}
	},
	methods: {
		query: function () {
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{'jobId': vm.q.jobId},
                page:1 
            }).trigger("reloadGrid");
		},
		del: function(logId) {
            $.get(baseURL + "sys/scheduleLog/deleteAll", function(r){
                alert("删除成功!");
            });
        },
		showError: function(logId) {
			$.get(baseURL + "sys/scheduleLog/info/"+logId, function(r){
				parent.layer.open({
				  title:'失败信息',
				  closeBtn:0,
				  content: r.log.error
				});
			});
		},
		back: function (event) {
			history.go(-1);
		}
	}
});


var base = "../../";
var vm = new Vue({
	el:'#vue-div',
	data:{
	    loadA: false,
	    loadATime: 0,
	    loadB: false,
	    loadBTime: 0,
	    loadC: false,
	    loadCTime: 0
	},
	mounted() {
        console.log("come on");
    },
	methods: {
        insertSSECClosePrice: function () {
            vm.loadB = true;
            $.ajax({
                type: "GET",
                url: base + "/stockIndex/insertSSECClosePrice",
                dataType: "json",
                success: function (result) {
                    vm.loadB = false;
                    alert(result.msg);
                }
            });
        },
        updateShanghaiStockExchangeIndustry: function () {
            vm.loadC = true;
            $.ajax({
                type: "GET",
                url: base + "/stockInfo/updateShanghaiStockExchangeIndustry",
                dataType: "json",
                success: function (result) {
                    vm.loadC = false;
                    vm.loadCTime = result.loadTime;
                    alert(result.msg);
                }
            });
        }
	}
}).$mount('#vue-div');

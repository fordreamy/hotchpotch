$(function () {
    $.ajax({
        type: "GET",
        url: baseURL + "/myConcernStock/list",
        dataType: "json",
        success: function (result, status) {
			let tmp = result.list;
			for (let i = 0; i < tmp.length; i++) {
				$.getJSON(baseURL + '/stockDay/getStockKLine?stockCode='+tmp[i].stockCode, function(result) {
					if(result.data.length < 1) return;
					renderKChart(result.stockName, result.title, result.data);
				});
            }
        }
    });
});

var vm = new Vue({
	el:'#vue-div',
	data:{
		showList: true,
		addOrRemoveShow: false,
		addShow: false,
		removeShow: false,
		title: null,
		stockCode:'',
		label:'',
		labelOptions:[]
	},
	methods: {
		query: function () {
		    console.log("methods->query");
		    console.log("baseURL:"+baseURL);
			vm.reload();
		},
		queryRecentStock: function (year) {
			vm.addShow = false;
			$.ajax({
                type: "GET",
                url: baseURL + "/myConcernStock/list",
				data: {
					label: vm.label
				},
                dataType: "json",
                success: function (result, status) {
                    clearTopDiv();
                    for (var i = 0; i < result.list.length; i++) {
                        $.ajax({
                            type: "GET",
                            url: baseURL + "/myConcernStock/queryRecentStock",
                            data: {
                                stockCode: result.list[i].stockCode,
                                year:year
                            },
                            dataType: "json",
                            success: function (data, status) {
								renderKChart(data.stockName, data.title, data.data);
                            }
                        });
                    }
                }
            });
		},
		add: function(){
		    vm.addOrRemoveShow = true;
			vm.addShow = true;
			vm.title = "添加关注";
		},
		del: function(){
		    vm.addOrRemoveShow = true;
            vm.removeShow = true;
            vm.title = "移除关注";
        },
		save: function (event) {
		    console.log("baseURL:"+baseURL);
			$.ajax({
				type: "GET",
			    url: baseURL + "myConcernStock/save",
			    data: {
			        stockCode: vm.stockCode,
					label: vm.label
			    },
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		remove: function (event) {
			confirm('确定要移除该基金？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "myConcernStock/delete",
				    data: {
                        stockCode: vm.stockCode
                    },
				    success: function(r){
						if(r.code === 0){
                            alert('操作成功', function(index){
                                vm.reload();
                            });
                        }else{
                            alert(r.msg);
                        }
					}
				});
			});
		},
		reload: function (event) {
			vm.addOrRemoveShow = false;
			vm.addShow = false;
			vm.removeShow = false;
			$.ajax({
                type: "GET",
                url: baseURL + "/myConcernStock/list",
				data: {
					label: vm.label
				},
                dataType: "json",
                success: function (result, status) {
                    clearTopDiv();
                    var tmp = result.list;
                    for (var i = 0; i < tmp.length; i++) {
						$.getJSON(baseURL + '/stockDay/getStockKLine?stockCode='+tmp[i].stockCode, function(result) {
							if(result.data.length < 1) return;
							renderKChart(result.stockName, result.title, result.data);
						});
                    }
                }
                });
		}
	}
});

$.ajax({
	type: "GET",
	url: baseURL + "myConcernStock/labelOptions",
	contentType: "application/json",
	success: function(result){
		vm.labelOptions = result.list;
	}
});

$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'stockUpStatistics/singleKLine?kLineName=光头光脚阳线',
        datatype: "json",
        colModel: [
            {label: '股票代码', name: 'stockCode', index: 'stock_code', width: 30, classes: 'code-style'},
            {label: '简称', name: 'stockName', index: 'stock_name', width: 30},
            {label: '统计天数', name: 'statisticsDays', index: 'statistics_days', width: 22},
            {
                label: '上涨次数', name: 'riseDays', index: 'rise_days', width: 22,
                formatter: function (value, options, row) {
                    return '<span style="color:red;font-size:15px;">' + row.riseDays + '<i style="margin-left:3px" class="fa fa-long-arrow-up" aria-hidden="true"></i></span>';
                }
            },
            {
                label: '下跌次数', name: 'fallDays', index: 'fall_days', width: 22,
                formatter: function (value, options, row) {
                    return '<span style="color:#0ebe98;font-size:15px;">' + row.fallDays + '<i style="margin-left:3px" class="fa fa-long-arrow-down" aria-hidden="true"></i></span>';
                }
            },

            {label: '连涨次数', name: 'continueRiseDays', index: 'continue_rise_days', width: 22},
            {label: '连跌次数', name: 'continueFallDays', index: 'continue_fall_days', width: 22},

            {label: '涨停数', name: 'riseStopDays', index: 'rise_stop_days', width: 20},
            {label: '跌停数', name: 'fallStopDays', index: 'fall_stop_days', width: 20},

            {label: '连续涨停', name: 'continueRiseStopDays', index: 'continue_rise_stop_days', width: 20},
            {label: '连续跌停', name: 'continueFallStopDays', index: 'continue_fall_stop_days', width: 20}
        ],
        viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList: [10, 30, 50, 100],
        rownumbers: true,
        rownumWidth: 60,
        autowidth: true,
        multiselect: false,
        pager: "#jqGridPager",
        jsonReader: {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames: {
            page: "page",
            rows: "limit",
            order: "order"
        },
        gridComplete: function () {
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({"overflow-x": "hidden"});
        },
        onCellSelect: function (rowid, iCol, cellcontent, e) {
            if (iCol == 1) {
                vm.showList = false;
                $.getJSON(baseURL + '/stockDay/getStockKLine?stockCode=' + cellcontent, function (result) {
                    if (result.data.length < 1) return;
                    renderKChart(result.stockName, result.title, result.data);
                });
            }
        }
    });

});

var base = "../../";
var vm = new Vue({
    el: '#vue-div',
    data: {
        kLineOptions: [{"id": 1, "kLineName": "光头光脚阳线"},
            {"id": 2, "kLineName": "光脚阳线"},
            {"id": 3, "kLineName": "光头阳线"}],
        q: {
            kLineName: '',
            kDate: ''
        },
        showList: true,
        getHistoryPriceLoad: false,
        riseNum: 2,
        riseSum: 0,
        riseLoad: false,
        fallNum: 2,
        fallSum: 0,
        fallLoad: false
    },
    mounted() {
        console.log("come on");
    },
    created() {
        this.q.kLineName = this.kLineOptions[0].kLineName
    },
    methods: {
        getHistoryPrice: function (year) {
            vm.showList = false;
            clearTopDiv();
            vm.getHistoryPriceLoad = true;
            let ids = $('#jqGrid').jqGrid('getDataIDs');
            for (let n in ids) {
                let url = baseURL + '/myConcernStock/queryRecentStock?year=' + year + '&stockCode=' + $('#jqGrid').jqGrid('getRowData', ids[n]).stockCode;
                $.getJSON(url, function (result) {
                    if (result.data.length < 1) return;
                    renderKChart(result.stockName, result.title, result.data);
                });
            }
            vm.getHistoryPriceLoad = false;
        },
        reload: function (event) {
            console.log("开始查询");
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam', 'page');
            console.log("当前页:" + page);
            console.log("查询参数:" + JSON.stringify(vm.q));
            $("#jqGrid").jqGrid('setGridParam', {
                postData: {
                    'kLineName': vm.q.kLineName,
                    'date': vm.q.kDate
                },
                page: page
            }).trigger("reloadGrid");
        },
        kLineQuery: function () {
            vm.reload();
        },
        chooseDate: function () {
            console.log("选择日期:" + this.q.date);
            vm.reload();
        },
        back: function (event) {
            vm.showList = true;
            clearTopDiv();
        }
    }
}).$mount('#vue-div');

var base = "../../";
var vm = new Vue({
	el:'#vue-div',
	data:{
        q:{
            rank: 1,
            startDate: '',
            endDate: ''
        },
        options: [],
        stockRandomCount: 10,
        stockCode: '',
        stockName: '',
        hotSearch: [{ "value": "银行"},{ "value": "证券"},{ "value": "钢"},{ "value": "电力"},{ "value": "机电"},{ "value": "工"},{ "value": "农"},{ "value": "药"},
                    { "value": "能源"},{ "value": "地产"},{ "value": "航空"}],
        searchByNameCount: 0,
        rules: [{id:1,name:'默认'},{id:2,name:'今日最低'},{id:3,name:'今日最高'}],
        rule: 1
	},
	mounted() {

    },
	methods: {
		querySingleStock: function () {
		    clearTopDiv();
            $.getJSON(baseURL + '/stockDay/getStockKLine?stockCode=' + vm.stockCode, function(result) {
                if(result.data.length < 1) return;
                renderKChart(result.stockName, result.title, result.data);
            });

		},
		queryRandomStock: function (event) {
		    console.log("reload");
		    console.log(vm.q.startDate);
		    console.log(vm.q.endDate);
		    console.log(vm.q.rank);
		    console.log(vm.rule);
		    var length = vm.options.length;
		    var n = vm.stockRandomCount;
		    var tmp = [];
            var i = 0;
            while (i < n) {
                r = Math.floor(Math.random() * length);
                if (tmp.includes(r)) {
                } else {
                    tmp[i++] = r;
                }
            }
            clearTopDiv();
            for (var i = 0; i < tmp.length; i++) {
                $.getJSON(baseURL + '/stockDay/getStockKLine?stockCode=' + vm.options[tmp[i]].stockCode, function(result) {
                    if(result.data.length < 1) return;
                    renderKChart(result.stockName, result.title, result.data);
                });
            }
		},
		querySearch(queryString, cb) {
            var s = vm.hotSearch;
            var results = queryString ? s.filter((item) => {
               return (item.value.indexOf(queryString) > -1);
             }) : s;
            // 调用 callback 返回建议列表的数据
            cb(results);
        },
        searchByName: function () {
            var queryString = vm.stockName;
            var results = vm.options.filter((item) => {
                           return (item.stockName.indexOf(queryString) > -1);
                         });
            vm.searchByNameCount = results.length;
            let length = results.length;
            if(length > 200) length = 200;
            clearTopDiv();
            for (var i = 0; i < length; i++) {
                $.getJSON(baseURL + '/stockDay/getStockKLine?stockCode=' + results[i].stockCode, function(result) {
                    if(result.data.length < 1) return;
                    renderKChart(result.stockName, result.title, result.data);
                });
            }
        }
	}
}).$mount('#vue-div');

$(function () {
    $.ajax({
        type: "GET",
        url: base + "/stockInfo/allStockCode",
        dataType: "json",
        success: function (result, status) {
            console.log(result);
            vm.options = result.list;
            vm.stockCode = result.list[0].stockCode;
        }
    });
});

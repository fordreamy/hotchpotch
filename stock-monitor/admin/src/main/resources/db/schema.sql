-- 菜单
CREATE TABLE `sys_menu` (
  `menu_id` bigint NOT NULL AUTO_INCREMENT,
  `parent_id` bigint COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) COMMENT '菜单名称',
  `url` varchar(200) COMMENT '菜单URL',
  `perms` varchar(500) COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) COMMENT '菜单图标',
  `order_num` int COMMENT '排序',
  PRIMARY KEY (`menu_id`)
) ;

-- 部门
CREATE TABLE `sys_dept` (
  `dept_id` bigint NOT NULL AUTO_INCREMENT,
  `parent_id` bigint COMMENT '上级部门ID，一级部门为0',
  `name` varchar(50) COMMENT '部门名称',
  `order_num` int COMMENT '排序',
  `del_flag` tinyint DEFAULT 0 COMMENT '是否删除  -1：已删除  0：正常',
  PRIMARY KEY (`dept_id`)
) ;

-- 系统用户
CREATE TABLE `sys_user` (
  `user_id` bigint NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) COMMENT '密码',
  `salt` varchar(20) COMMENT '盐',
  `email` varchar(100) COMMENT '邮箱',
  `mobile` varchar(100) COMMENT '手机号',
  `status` tinyint COMMENT '状态  0：禁用   1：正常',
  `dept_id` bigint(20) COMMENT '部门ID',
  `create_time` datetime COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX (`username`)
) ;

-- 角色
CREATE TABLE `sys_role` (
  `role_id` bigint NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) COMMENT '角色名称',
  `remark` varchar(100) COMMENT '备注',
  `dept_id` bigint(20) COMMENT '部门ID',
  `create_time` datetime COMMENT '创建时间',
  PRIMARY KEY (`role_id`)
) ;

-- 用户与角色对应关系
CREATE TABLE `sys_user_role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint COMMENT '用户ID',
  `role_id` bigint COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ;

-- 角色与菜单对应关系
CREATE TABLE `sys_role_menu` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `role_id` bigint COMMENT '角色ID',
  `menu_id` bigint COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ;

-- 角色与部门对应关系
CREATE TABLE `sys_role_dept` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `role_id` bigint COMMENT '角色ID',
  `dept_id` bigint COMMENT '部门ID',
  PRIMARY KEY (`id`)
) ;


-- 系统配置信息
CREATE TABLE `sys_config` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) COMMENT 'key',
  `param_value` varchar(2000) COMMENT 'value',
  `status` tinyint DEFAULT 1 COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE INDEX (`param_key`)
) ;

-- 数据字典
CREATE TABLE `sys_dict` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '字典名称',
  `type` varchar(100) NOT NULL COMMENT '字典类型',
  `code` varchar(100) NOT NULL COMMENT '字典码',
  `value` varchar(1000) NOT NULL COMMENT '字典值',
  `order_num` int DEFAULT 0 COMMENT '排序',
  `remark` varchar(255) COMMENT '备注',
  `del_flag` tinyint DEFAULT 0 COMMENT '删除标记  -1：已删除  0：正常',
  PRIMARY KEY (`id`),
  UNIQUE KEY(`type`,`code`)
) ;

-- 系统日志
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COMMENT '用户名',
  `operation` varchar(50) COMMENT '用户操作',
  `method` varchar(200) COMMENT '请求方法',
  `params` varchar(5000) COMMENT '请求参数',
  `time` bigint NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) COMMENT 'IP地址',
  `create_date` datetime COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ;

----------------------------------------------- 业务表
drop table day_history if exists;
CREATE TABLE  DAY_HISTORY(
  ID CHAR(64) NOT NULL,
  STOCK_CODE CHAR(6) DEFAULT NULL,
  STOCK_NAME VARCHAR(10) DEFAULT NULL,
  DAY_DATE CHAR(10) DEFAULT NULL,
  OPENING DOUBLE DEFAULT NULL,
  CLOSING DOUBLE DEFAULT NULL,
  HIGHEST DOUBLE DEFAULT NULL,
  LOWEST DOUBLE DEFAULT NULL,
  RATE DOUBLE DEFAULT NULL,
  PRIMARY KEY (ID),
  KEY stockCode (STOCK_CODE)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;


CREATE TABLE `STOCK_MONTH` (
  `ID` CHAR(64) CHARACTER SET utf8 NOT NULL,
  `STOCK_CODE` CHAR(6) CHARACTER SET utf8 DEFAULT NULL,
  `STOCK_NAME` VARCHAR(10) CHARACTER SET utf8 DEFAULT NULL,
  `DAY_DATE` CHAR(10) CHARACTER SET utf8 DEFAULT NULL,
  `OPEN` DOUBLE DEFAULT NULL,
  `CLOSE` DOUBLE DEFAULT NULL,
  `HIGHEST` DOUBLE DEFAULT NULL,
  `LOWEST` DOUBLE DEFAULT NULL,
  `VOLUME` DOUBLE DEFAULT NULL,
  `NETChangeRATIO` DOUBLE DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `stockCode` (`STOCK_CODE`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `STOCK_WEEK` (
  `ID` CHAR(64) CHARACTER SET utf8 NOT NULL,
  `STOCK_CODE` CHAR(6) CHARACTER SET utf8 DEFAULT NULL,
  `STOCK_NAME` VARCHAR(10) CHARACTER SET utf8 DEFAULT NULL,
  `DAY_DATE` CHAR(10) CHARACTER SET utf8 DEFAULT NULL,
  `OPEN` DOUBLE DEFAULT NULL,
  `CLOSE` DOUBLE DEFAULT NULL,
  `HIGHEST` DOUBLE DEFAULT NULL,
  `LOWEST` DOUBLE DEFAULT NULL,
  `VOLUME` DOUBLE DEFAULT NULL,
  `NETChangeRATIO` DOUBLE DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `stockCode` (`STOCK_CODE`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `STOCK_INFO` (
  `id` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `stock_code` char(6) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '股票code',
  `stock_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '股票名称',
  `listing_date` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '上市日期',
  `stock_type` char(3) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '股票类型',
  `market_capital` bigint(20) unsigned DEFAULT NULL COMMENT '总股本',
  `negotiable_capital` bigint(20) unsigned DEFAULT NULL COMMENT '流通股',
  `industry` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属行业',
  `stock_exchange` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '所属交易所',
  `website` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '公司网址',
  PRIMARY KEY (`id`),
  KEY `stock_code` (`stock_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

package com.demo;

import cn.hutool.core.util.NumberUtil;

public class Test {
    public static void main(String[] args) {
        Double a = 1001D;
        Double b = 1060.0D;
        System.out.println(a == b);
        System.out.println(a > b);
        System.out.println(a < b);
        System.out.println(a/b);
        System.out.println(NumberUtil.round(a/b,4));
    }
}

// 全局的常量
// 股票分组ID
export const GROUP_ID = '1'
export default {
  install(Vue) {
    Vue.prototype.$constants = {
      GROUP_ID
    }
  }
}

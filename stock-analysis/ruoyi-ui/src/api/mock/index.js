import Mock from 'mockjs'

console.log('mock.........................')

// Mock.mock("/dev-api/system/notice/list?pageNum=1&pageSize=10", (e) => {
Mock.mock(/.*\/notice\/list.*/, (e) => {
  console.log('Mock正则拦截')
  console.log(e)
  return {
    "total": 20, "rows": [{
      "createBy": "admin",
      "createTime": "2022-09-01 21:43:27",
      "updateBy": "",
      "updateTime": null,
      "remark": "管理员",
      "noticeId": 1,
      "noticeTitle": "Mock数据",
      "noticeType": "2",
      "noticeContent": "新版本内容",
      "status": "0"
    }], "code": 200, "msg": "查询成功"
  }
})

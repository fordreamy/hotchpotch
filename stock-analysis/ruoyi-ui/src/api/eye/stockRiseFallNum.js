import request from '@/utils/request'

// 查询股票涨跌数列表
export function listStockRiseFallNum(query) {
  return request({
    url: '/eye/stockRiseFallNum/list',
    method: 'get',
    params: query
  })
}

// 查询股票涨跌日历
export function listStockRiseCalendar(query) {
  return request({
    url: '/eye/stockRiseFallNum/listStockRiseCalendar',
    method: 'get',
    params: query
  })
}

// 查询股票涨跌数详细
export function getStockRiseFallNum(id) {
  return request({
    url: '/eye/stockRiseFallNum/' + id,
    method: 'get'
  })
}

// 新增股票涨跌数
export function addStockRiseFallNum(data) {
  return request({
    url: '/eye/stockRiseFallNum',
    method: 'post',
    data: data
  })
}

// 修改股票涨跌数
export function updateStockRiseFallNum(data) {
  return request({
    url: '/eye/stockRiseFallNum',
    method: 'put',
    data: data
  })
}

// 删除股票涨跌数
export function delStockRiseFallNum(id) {
  return request({
    url: '/eye/stockRiseFallNum/' + id,
    method: 'delete'
  })
}

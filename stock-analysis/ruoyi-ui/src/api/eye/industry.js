import request from '@/utils/request'

// 查询股票行业列表
export function listIndustry(query) {
  return request({
    url: '/eye/industry/list',
    method: 'get',
    params: query
  })
}

// 查询股票行业下拉框
export function industryOptions() {
  return request({
    url: '/eye/industry/industryOptions',
    method: 'get',
    params: {}
  })
}

// 查询股票行业详细
export function getIndustry(id) {
  return request({
    url: '/eye/industry/' + id,
    method: 'get'
  })
}

// 新增股票行业
export function addIndustry(data) {
  return request({
    url: '/eye/industry',
    method: 'post',
    data: data
  })
}

// 修改股票行业
export function updateIndustry(data) {
  return request({
    url: '/eye/industry',
    method: 'put',
    data: data
  })
}

// 批量修改股票行业
export function batchUpdateStockIndustry(data) {
  return request({
    url: '/eye/industry/batchUpdateStockIndustry',
    method: 'put',
    data: data
  })
}

// 删除股票行业
export function delIndustry(id) {
  return request({
    url: '/eye/industry/' + id,
    method: 'delete'
  })
}

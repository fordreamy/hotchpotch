import * as echarts from 'echarts'

/**
 * 根据基金code生成一条折线图,多条则循环调用
 */
export function renderLineChart(lineData) {

  let chartDom = document.getElementById(lineData.id);
  //初始化echarts实例
  let myChart = echarts.init(chartDom)

  // 指定图表的配置项和数据
  let option = {
    color: lineData.color,
    title: {
      text: lineData.title
    },
    tooltip: {
      trigger: 'axis'
    },
    xAxis: {
      type: 'category',
      data: lineData.dateList
    },
    yAxis: {
      type: 'value'
    },
    dataZoom: [
      {
        type: 'inside',
        start: 50,
        end: 100
      },
      {
        start: 0,
        end: 10
      }
    ],
    series: [
      {
        data: lineData.valueList,
        type: 'line'
      }
    ]
  };

  // 使用指定的配置项和数据显示图表。
  myChart.setOption(option)
}

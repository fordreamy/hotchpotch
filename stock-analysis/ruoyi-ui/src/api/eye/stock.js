import request from '@/utils/request'

// 查询股票基础信息列表
export function listStock(query) {
  return request({
    url: '/eye/stock/list',
    method: 'get',
    params: query
  })
}

// 查询股票基础信息详细
export function getStock(id) {
  return request({
    url: '/eye/stock/' + id,
    method: 'get'
  })
}

// 新增股票基础信息
export function addStock(data) {
  return request({
    url: '/eye/stock',
    method: 'post',
    data: data
  })
}

// 修改股票基础信息
export function updateStock(data) {
  return request({
    url: '/eye/stock',
    method: 'put',
    data: data
  })
}

// 删除股票基础信息
export function delStock(id) {
  return request({
    url: '/eye/stock/' + id,
    method: 'delete'
  })
}

// 清空股票基础信息
export function clearStockData() {
  return request({
    url: '/eye/stock/clearStockData',
    method: 'delete'
  })
}

/**
 * 从第三方接口更新所有的股票信息
 * @returns {*}
 */
export function updateAllStockInfoFromAPI() {
  return request({
    url: '/eye/stock/updateAllStockInfoFromAPI',
    method: 'get',
    timeout: 10 * 60 * 1000
  })
}

/**
 * 测试第三方接口是否可用
 * @returns {*}
 */
export function testUpdateAllStockAPI() {
  return request({
    url: '/eye/stock/testUpdateAllStockAPI',
    method: 'get',
    timeout: 10 * 60 * 1000
  })
}

import request from '@/utils/request'

// 查询app菜单列表
export function listAppMenu(query) {
  return request({
    url: '/eye/app/menu/list',
    method: 'get',
    params: query
  })
}

// 查询app菜单详细
export function getAppMenu(id) {
  return request({
    url: '/eye/app/menu/' + id,
    method: 'get'
  })
}

// 新增app菜单
export function addAppMenu(data) {
  return request({
    url: '/eye/app/menu',
    method: 'post',
    data: data
  })
}

// 修改app菜单
export function updateAppMenu(data) {
  return request({
    url: '/eye/app/menu',
    method: 'put',
    data: data
  })
}

// 删除app菜单
export function delAppMenu(id) {
  return request({
    url: '/eye/app/menu/' + id,
    method: 'delete'
  })
}

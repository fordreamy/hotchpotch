import request from '@/utils/request'

// 查询app更新列表
export function listAppUpdate(query) {
  return request({
    url: '/eye/app/update/list',
    method: 'get',
    params: query
  })
}

// 根据appid查询最新的版本
export function getLatestVersion(query) {
  return request({
    url: '/eye/app/update/getLatestVersion',
    method: 'get',
    params: query
  })
}

// 查询app更新详细
export function getAppUpdate(id) {
  return request({
    url: '/eye/app/update/' + id,
    method: 'get'
  })
}

// 新增app更新
export function addAppUpdate(data) {
  return request({
    url: '/eye/app/update',
    method: 'post',
    data: data
  })
}

// 修改app更新
export function updateAppUpdate(data) {
  return request({
    url: '/eye/app/update',
    method: 'put',
    data: data
  })
}

// 删除app更新
export function delAppUpdate(id) {
  return request({
    url: '/eye/app/update/' + id,
    method: 'delete'
  })
}

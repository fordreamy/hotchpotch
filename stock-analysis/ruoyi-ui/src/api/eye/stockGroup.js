import request from '@/utils/request'

// 查询股票分组列表
export function listStockGroup(query) {
  return request({
    url: '/eye/stockGroup/list',
    method: 'get',
    params: query
  })
}

// 查询股票分组下拉框
export function listStockGroupOption() {
  return request({
    url: '/eye/stockGroup/listOption',
    method: 'get'
  })
}

// 查询股票分组详细
export function getStockGroup(id) {
  return request({
    url: '/eye/stockGroup/' + id,
    method: 'get'
  })
}

// 新增股票分组
export function addStockGroup(data) {
  return request({
    url: '/eye/stockGroup',
    method: 'post',
    data: data
  })
}

// 修改股票分组
export function updateStockGroup(data) {
  return request({
    url: '/eye/stockGroup',
    method: 'put',
    data: data
  })
}

// 批量修改股票分组
export function batchUpdateStockMyGroup(data) {
  return request({
    url: '/eye/stockGroup/batchUpdateStockMyGroup',
    method: 'put',
    data: data
  })
}

// 删除股票分组
export function delStockGroup(id) {
  return request({
    url: '/eye/stockGroup/' + id,
    method: 'delete'
  })
}

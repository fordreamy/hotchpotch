import request from '@/utils/request'

// 查询股票指标统计信息列表
export function listStockUpStatistics(query) {
  return request({
    url: '/eye/stockUpStatistics/list',
    method: 'get',
    params: query
  })
}

// 根据均线类型查询
export function averageLineType(query) {
  return request({
    url: '/eye/stockUpStatistics/averageLineType',
    method: 'get',
    params: query
  })
}

// 查询股票指标统计信息详细
export function getStockUpStatistics(id) {
  return request({
    url: '/eye/stockUpStatistics/' + id,
    method: 'get'
  })
}

// 新增股票指标统计信息
export function addStockUpStatistics(data) {
  return request({
    url: '/eye/stockUpStatistics',
    method: 'post',
    data: data
  })
}

// 修改股票指标统计信息
export function updateStockUpStatistics(data) {
  return request({
    url: '/eye/stockUpStatistics',
    method: 'put',
    data: data
  })
}

// 删除股票指标统计信息
export function delStockUpStatistics(id) {
  return request({
    url: '/eye/stockUpStatistics/' + id,
    method: 'delete'
  })
}

// 查询大盘趋势数据
export function getMarketTrendData() {
  return request({
    url: '/eye/stockRiseFallNum/getMarketTrendData',
    method: 'get'
  })
}

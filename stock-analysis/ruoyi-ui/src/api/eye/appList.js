import request from '@/utils/request'

// 查询APP应用列表
export function appList(query) {
  return request({
    url: '/eye/app/curd/list',
    method: 'get',
    params: query
  })
}

// 查询APP下拉框
export function getAppListOption(query) {
  return request({
    url: '/eye/app/curd/getAppListOption',
    method: 'get',
    params: query
  })
}

// 查询APP应用详细
export function getApp(id) {
  return request({
    url: '/eye/app/curd/' + id,
    method: 'get'
  })
}

// 新增APP应用
export function addApp(data) {
  return request({
    url: '/eye/app/curd',
    method: 'post',
    data: data
  })
}

// 修改APP应用
export function updateApp(data) {
  return request({
    url: '/eye/app/curd',
    method: 'put',
    data: data
  })
}

// 删除APP应用
export function delApp(id) {
  return request({
    url: '/eye/app/curd/' + id,
    method: 'delete'
  })
}

import request from '@/utils/request'
import { Message } from 'element-ui'
import Vue from 'vue'

// 查询我的关注列表
export function listMyAttention(query) {
  return request({
    url: '/eye/myAttention/list',
    method: 'get',
    params: query
  })
}

// 查询我的关注详细
export function getMyAttention(id) {
  return request({
    url: '/eye/myAttention/' + id,
    method: 'get'
  })
}

// 新增我的关注
export function addMyAttention(data) {
  return request({
    url: '/eye/myAttention',
    method: 'post',
    data: data
  })
}

/**
 * 将股票添加到默认分组
 * @param stockCode 股票编码
 * @returns {*}
 */
export function addToDefaultGroupByStockCode(stockCode) {
  Message({
    message: '成功添加到分组!',
    type: 'success',
    duration: 1000,
    center: true
  })
  let groupId = Vue.prototype.$constants.GROUP_ID
  console.log('成功添加到分组:' + groupId)
  return request({
    url: '/eye/myAttention',
    method: 'post',
    data: { stockCode: stockCode, groupId: groupId }
  })
}

/**
 * 将股票添从默认分组中移除
 * @param stockCode 股票编码
 * @returns {*}
 */
export function deleteFromDefaultGroupByStockCode(stockCode) {
  Message({
    message: '成功从分组中移除!',
    type: 'success',
    duration: 1000,
    center: true
  })
  let groupId = Vue.prototype.$constants.GROUP_ID
  console.log('成功从分组中移除:' + groupId)
  return request({
    url: '/eye/myAttention/deleteFromDefaultGroupByStockCode?stockCode=' + stockCode + '&groupId=' + groupId,
    method: 'get'
  })
}

// 修改我的关注
export function updateMyAttention(data) {
  return request({
    url: '/eye/myAttention',
    method: 'put',
    data: data
  })
}

// 删除我的关注
export function delMyAttention(id) {
  return request({
    url: '/eye/myAttention/' + id,
    method: 'delete'
  })
}

// 批量修改我的关注
export function batchUpdateMyAttention(data) {
  return request({
    url: '/eye/myAttention/batchUpdateMyAttention',
    method: 'put',
    data: data
  })
}

export function batchInsertMyAttention(data) {
  return request({
    url: '/eye/myAttention/batchInsertMyAttention',
    method: 'post',
    data: data
  })
}

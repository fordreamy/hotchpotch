## 开发

```bash
# 克隆项目
git clone https://gitee.com/y_project/RuoYi-Vue

# 进入项目目录
cd ruoyi-ui

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npmmirror.com

# 启动服务
npm run dev
```

浏览器访问 http://localhost:80

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## Mock使用
### 安装
`npm install mockjs`
### main.js引入
`import '@/api/mock/index.js' 或者 require('@/api/mock/index.js')`
### 去掉Mock
main.js中注释掉引用即可
### 说明
正常进行业务开发，这时候页面发送的请求会被mock拦截，如果url一致，或者正则匹配到了，则返回mock数据，没有进行网络请求发起

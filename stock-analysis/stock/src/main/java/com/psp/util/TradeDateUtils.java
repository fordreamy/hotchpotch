package com.psp.util;

import com.psp.stockprice.domain.StockPrice;
import com.psp.stockprice.service.IStockPriceService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 根据不退市不停牌股票(中国银行等)获取股票是否交易等信息
 *
 * @author tiger
 */
@Component
public class TradeDateUtils {
    /**
     * 参考的股票-中国银行,用于获取股票的最后非法定交易日等信息
     */
    public static String REFERENCE_STOCK_CODE = "601988";

    @Resource
    private IStockPriceService stockPriceService;

    /**
     * 查询n天交易日期的开始日期和结束日期
     *
     * @return dates[0]是始日期, dates[1]是最新的交易日期即结束日期
     */
    public Date[] selectStockTradeStartAndEndDate(int days) {
        List<Map<String, Object>> mapList = stockPriceService.selectStockTradeStartAndEndDate(days);
        Date[] dates = new Date[2];
        dates[0] = (Date) mapList.get(mapList.size() - 1).get("trade_date");
        dates[1] = (Date) mapList.get(0).get("trade_date");
        return dates;
    }

}

package com.psp.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

public class GetResponse {

    protected static final Logger logger = LoggerFactory.getLogger(GetResponse.class);

    public static int connectTimeout = 2000;
    public static int readTimeout = 2000;

    /**
     * 根据url获取返回结果
     *
     * @param urlName 接口url
     * @param charsetName 字符类型,GBK、UTF-8等
     * @return 接口返回内容
     */
    public static String getResponse(String urlName,String charsetName) {
        logger.debug("开始获取返回页面");
        URL url;
        URLConnection uc;
        InputStream in;
        try {
            // 返回一个 URLConnection 对象，它表示到 URL 所引用的远程对象的连接。
            url = new URL(urlName);
            // 打开的连接读取的输入流。
            uc = url.openConnection();
            uc.addRequestProperty("Referer", "https://finance.sina.com.cn/");
            uc.setConnectTimeout(connectTimeout);
            uc.setReadTimeout(readTimeout);
            in = uc.getInputStream();
        } catch (Exception e) {
            logger.error("获取页面失败,网络资源不存在或连接超时!");
            e.printStackTrace();
            return "";
        }
        logger.debug("已获取连接:" + urlName);
        StringBuilder sb = new StringBuilder();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(in, Charset.forName(charsetName)));
            // 读取返回结果
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        logger.debug("成功获取返回结果!");
        return sb.toString();
    }

}

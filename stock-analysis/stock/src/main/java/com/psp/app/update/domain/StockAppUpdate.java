package com.psp.app.update.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * app更新对象 stock_app_update
 *
 * @author peishuaipeng
 * @date 2023-03-14
 */
public class StockAppUpdate extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * AppID
     */
    @Excel(name = "AppID")
    private String appId;

    /**
     * 应用名称
     */
    @Excel(name = "应用名称")
    private String name;

    /**
     * 更新标题
     */
    @Excel(name = "更新标题")
    private String title;

    /**
     * 更新内容
     */
    @Excel(name = "更新内容")
    private String content;

    /**
     * 平台
     */
    @Excel(name = "平台")
    private String platform;

    /**
     * 版本号
     */
    @Excel(name = "版本号")
    private String versionNumber;

    /**
     * 自增版本号
     */
    @Excel(name = "自增版本号")
    private Integer autoIncrementVersionNumber;

    /**
     * 文件原始名称
     */
    @Excel(name = "文件原始名称")
    private String originalName;

    /**
     * 磁盘存储路径
     */
    @Excel(name = "磁盘存储路径")
    private String storagePath;

    /**
     * 下载链接
     */
    @Excel(name = "下载链接")
    private String downloadUrl;

    /**
     * 上传时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上传时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date uploadTime;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppId() {
        return appId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getPlatform() {
        return platform;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setAutoIncrementVersionNumber(Integer autoIncrementVersionNumber) {
        this.autoIncrementVersionNumber = autoIncrementVersionNumber;
    }

    public Integer getAutoIncrementVersionNumber() {
        return autoIncrementVersionNumber;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }

    public String getStoragePath() {
        return storagePath;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    public Date getUploadTime() {
        return uploadTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("appId", getAppId())
                .append("name", getName())
                .append("title", getTitle())
                .append("content", getContent())
                .append("platform", getPlatform())
                .append("versionNumber", getVersionNumber())
                .append("autoIncrementVersionNumber", getAutoIncrementVersionNumber())
                .append("originalName", getOriginalName())
                .append("storagePath", getStoragePath())
                .append("downloadUrl", getDownloadUrl())
                .append("uploadTime", getUploadTime())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}

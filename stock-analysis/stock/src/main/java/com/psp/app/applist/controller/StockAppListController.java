package com.psp.app.applist.controller;

import java.util.List;

import com.psp.app.applist.entity.StockAppListEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.psp.app.applist.domain.StockAppList;
import com.psp.app.applist.service.IStockAppListService;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.annotation.Resource;

/**
 * APP应用Controller
 *
 * @author Tiger
 * @date 2023-03-21
 */
@RestController
@RequestMapping("/eye/app/curd")
public class StockAppListController extends BaseController {
    @Resource
    private IStockAppListService stockAppListService;

    /**
     * 查询APP应用列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StockAppList stockAppList) {
        startPage();
        List<StockAppList> list = stockAppListService.selectStockAppListList(stockAppList);
        return getDataTable(list);
    }

    @GetMapping("/getAppListOption")
    public AjaxResult getAppListOption(StockAppList stockAppList) {
        List<StockAppList> list = stockAppListService.selectStockAppListList(stockAppList);
        return AjaxResult.success(list);
    }

    /**
     * 获取APP应用详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return success(stockAppListService.getById(id));
    }

    /**
     * 新增APP应用
     */
    @Log(title = "APP应用", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockAppListEntity entity) {
        stockAppListService.save(entity);
        return success("新增成功!");
    }

    /**
     * 修改APP应用
     */
    @Log(title = "APP应用", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockAppListEntity entity) {
        stockAppListService.updateById(entity);
        return success("修改成功!");
    }

    /**
     * 删除APP应用
     */
    @Log(title = "APP应用", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(stockAppListService.deleteStockAppListByIds(ids));
    }
}

package com.psp.app.update.mapper;

import java.util.List;

import com.psp.app.update.domain.StockAppUpdate;

/**
 * app更新Mapper接口
 *
 * @author peishuaipeng
 */
public interface StockAppUpdateMapper {
    /**
     * 查询app更新
     *
     * @param id app更新主键
     * @return app更新
     */
    StockAppUpdate selectStockAppUpdateById(String id);

    /**
     * 查询app更新列表
     *
     * @param stockAppUpdate app更新
     * @return app更新集合
     */
    List<StockAppUpdate> selectStockAppUpdateList(StockAppUpdate stockAppUpdate);

    /**
     * 新增app更新
     *
     * @param stockAppUpdate app更新
     * @return 结果
     */
    int insertStockAppUpdate(StockAppUpdate stockAppUpdate);

    Integer selectMaxVersionNum();

    StockAppUpdate selectLatestVersionRecord(StockAppUpdate stockAppUpdate);

    /**
     * 修改app更新
     *
     * @param stockAppUpdate app更新
     * @return 结果
     */
    int updateStockAppUpdate(StockAppUpdate stockAppUpdate);

    /**
     * 删除app更新
     *
     * @param id app更新主键
     * @return 结果
     */
    int deleteStockAppUpdateById(String id);

    /**
     * 批量删除app更新
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deleteStockAppUpdateByIds(String[] ids);
}

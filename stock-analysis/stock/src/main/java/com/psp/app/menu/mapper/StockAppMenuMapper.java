package com.psp.app.menu.mapper;

import java.util.List;

import com.psp.app.menu.domain.StockAppMenu;

/**
 * app菜单Mapper接口
 *
 * @author peishuaipeng
 */
public interface StockAppMenuMapper {
    /**
     * 查询app菜单
     *
     * @param id app菜单主键
     * @return app菜单
     */
    StockAppMenu selectStockAppMenuById(String id);

    /**
     * 查询app菜单列表
     *
     * @param stockAppMenu app菜单
     * @return app菜单集合
     */
    List<StockAppMenu> selectStockAppMenuList(StockAppMenu stockAppMenu);

    /**
     * 新增app菜单
     *
     * @param stockAppMenu app菜单
     * @return 结果
     */
    int insertStockAppMenu(StockAppMenu stockAppMenu);

    /**
     * 修改app菜单
     *
     * @param stockAppMenu app菜单
     * @return 结果
     */
    int updateStockAppMenu(StockAppMenu stockAppMenu);

    /**
     * 删除app菜单
     *
     * @param id app菜单主键
     * @return 结果
     */
    int deleteStockAppMenuById(String id);

    /**
     * 批量删除app菜单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deleteStockAppMenuByIds(String[] ids);
}

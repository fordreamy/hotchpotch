package com.psp.app.applist.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.psp.app.applist.domain.StockAppList;
import com.psp.app.applist.entity.StockAppListEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * APP应用Mapper接口
 *
 * @author Tiger
 * @date 2023-03-21
 */
@Mapper
public interface StockAppListMapper extends BaseMapper<StockAppListEntity> {
    /**
     * 查询APP应用
     *
     * @param id APP应用主键
     * @return APP应用
     */
    public StockAppList selectStockAppListById(String id);

    /**
     * 查询APP应用列表
     *
     * @param stockAppList APP应用
     * @return APP应用集合
     */
    public List<StockAppList> selectStockAppListList(StockAppList stockAppList);

    /**
     * 新增APP应用
     *
     * @param stockAppList APP应用
     * @return 结果
     */
    public int insertStockAppList(StockAppList stockAppList);

    /**
     * 修改APP应用
     *
     * @param stockAppList APP应用
     * @return 结果
     */
    public int updateStockAppList(StockAppList stockAppList);

    /**
     * 删除APP应用
     *
     * @param id APP应用主键
     * @return 结果
     */
    public int deleteStockAppListById(String id);

    /**
     * 批量删除APP应用
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockAppListByIds(String[] ids);
}

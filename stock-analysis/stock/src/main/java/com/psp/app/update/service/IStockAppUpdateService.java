package com.psp.app.update.service;

import java.util.List;

import com.psp.app.update.domain.StockAppUpdate;

/**
 * app更新Service接口
 *
 * @author peishuaipeng
 * @date 2023-03-11
 */
public interface IStockAppUpdateService {
    /**
     * 查询app更新
     *
     * @param id app更新主键
     * @return app更新
     */
    public StockAppUpdate selectStockAppUpdateById(String id);

    /**
     * 查询app更新列表
     *
     * @param stockAppUpdate app更新
     * @return app更新集合
     */
    public List<StockAppUpdate> selectStockAppUpdateList(StockAppUpdate stockAppUpdate);

    /**
     * 新增app更新
     *
     * @param stockAppUpdate app更新
     * @return 结果
     */
    public int insertStockAppUpdate(StockAppUpdate stockAppUpdate);

    /**
     * 修改app更新
     *
     * @param stockAppUpdate app更新
     * @return 结果
     */
    public int updateStockAppUpdate(StockAppUpdate stockAppUpdate);

    /**
     * 批量删除app更新
     *
     * @param ids 需要删除的app更新主键集合
     * @return 结果
     */
    public int deleteStockAppUpdateByIds(String[] ids);

    /**
     * 删除app更新信息
     *
     * @param id app更新主键
     * @return 结果
     */
    public int deleteStockAppUpdateById(String id);
}

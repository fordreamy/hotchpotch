package com.psp.app.menu.service;

import java.util.List;

import com.psp.app.menu.domain.StockAppMenu;

/**
 * app菜单Service接口
 *
 * @author peishuaipeng
 * @date 2023-03-11
 */
public interface IStockAppMenuService {
    /**
     * 查询app菜单
     *
     * @param id app菜单主键
     * @return app菜单
     */
    public StockAppMenu selectStockAppMenuById(String id);

    /**
     * 查询app菜单列表
     *
     * @param stockAppMenu app菜单
     * @return app菜单集合
     */
    public List<StockAppMenu> selectStockAppMenuList(StockAppMenu stockAppMenu);

    /**
     * 新增app菜单
     *
     * @param stockAppMenu app菜单
     * @return 结果
     */
    public int insertStockAppMenu(StockAppMenu stockAppMenu);

    /**
     * 修改app菜单
     *
     * @param stockAppMenu app菜单
     * @return 结果
     */
    public int updateStockAppMenu(StockAppMenu stockAppMenu);

    /**
     * 批量删除app菜单
     *
     * @param ids 需要删除的app菜单主键集合
     * @return 结果
     */
    public int deleteStockAppMenuByIds(String[] ids);

    /**
     * 删除app菜单信息
     *
     * @param id app菜单主键
     * @return 结果
     */
    public int deleteStockAppMenuById(String id);
}

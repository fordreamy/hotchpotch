package com.psp.app.update.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.stereotype.Service;
import com.psp.app.update.mapper.StockAppUpdateMapper;
import com.psp.app.update.domain.StockAppUpdate;
import com.psp.app.update.service.IStockAppUpdateService;

import javax.annotation.Resource;

/**
 * app更新Service业务层处理
 *
 * @author peishuaipeng
 */
@Service
public class StockAppUpdateServiceImpl implements IStockAppUpdateService {
    @Resource
    private StockAppUpdateMapper stockAppUpdateMapper;

    /**
     * 查询app更新
     *
     * @param id app更新主键
     * @return app更新
     */
    @Override
    public StockAppUpdate selectStockAppUpdateById(String id) {
        return stockAppUpdateMapper.selectStockAppUpdateById(id);
    }

    /**
     * 查询app更新列表
     *
     * @param stockAppUpdate app更新
     * @return app更新
     */
    @Override
    public List<StockAppUpdate> selectStockAppUpdateList(StockAppUpdate stockAppUpdate) {
        return stockAppUpdateMapper.selectStockAppUpdateList(stockAppUpdate);
    }

    /**
     * 新增app更新
     *
     * @param stockAppUpdate app更新
     * @return 结果
     */
    @Override
    public int insertStockAppUpdate(StockAppUpdate stockAppUpdate) {
        stockAppUpdate.setCreateTime(DateUtils.getNowDate());
        stockAppUpdate.setId(IdUtils.fastSimpleUUID());
        stockAppUpdate.setUploadTime(DateUtils.getNowDate());
        if (stockAppUpdate.getAutoIncrementVersionNumber() == null) {
            Integer max = stockAppUpdateMapper.selectMaxVersionNum();
            if (max != null) {
                stockAppUpdate.setAutoIncrementVersionNumber(max + 1);
            } else {
                stockAppUpdate.setAutoIncrementVersionNumber(1);
            }
        }
        return stockAppUpdateMapper.insertStockAppUpdate(stockAppUpdate);
    }

    /**
     * 修改app更新
     *
     * @param stockAppUpdate app更新
     * @return 结果
     */
    @Override
    public int updateStockAppUpdate(StockAppUpdate stockAppUpdate) {
        stockAppUpdate.setUpdateTime(DateUtils.getNowDate());
        return stockAppUpdateMapper.updateStockAppUpdate(stockAppUpdate);
    }

    /**
     * 批量删除app更新
     *
     * @param ids 需要删除的app更新主键
     * @return 结果
     */
    @Override
    public int deleteStockAppUpdateByIds(String[] ids) {
        return stockAppUpdateMapper.deleteStockAppUpdateByIds(ids);
    }

    /**
     * 删除app更新信息
     *
     * @param id app更新主键
     * @return 结果
     */
    @Override
    public int deleteStockAppUpdateById(String id) {
        return stockAppUpdateMapper.deleteStockAppUpdateById(id);
    }
}

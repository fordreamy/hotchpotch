package com.psp.app.menu.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * app菜单对象 stock_app_menu
 *
 * @author peishuaipeng
 * @date 2023-03-11
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class StockAppMenu extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 分组名称
     */
    private String groupName;

    /**
     * 显示顺序
     */
    private Integer groupOrderNum;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 路由参数
     */
    private String query;

    /**
     * 是否自定义图标（0是 1否）
     */
    private Integer isCustom;

    /**
     * 是否为外链（0是 1否）
     */
    private Integer isFrame;

    /**
     * 是否缓存（0缓存 1不缓存）
     */
    private Integer isCache;

    /**
     * 图标类型
     */
    private String iconType;

    /**
     * 图标类样式
     */
    private String iconClass;

    /**
     * 菜单状态（0正常 1停用）
     */
    private Integer status;

}

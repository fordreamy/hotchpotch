package com.psp.app.update.controller;

import java.util.List;
import javax.annotation.Resource;

import com.psp.app.update.mapper.StockAppUpdateMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.psp.app.update.domain.StockAppUpdate;
import com.psp.app.update.service.IStockAppUpdateService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * app更新Controller
 *
 * @author peishuaipeng
 */
@RestController
@RequestMapping("/eye/app/update")
public class StockAppUpdateController extends BaseController {
    @Resource
    private IStockAppUpdateService stockAppUpdateService;

    @Resource
    private StockAppUpdateMapper stockAppUpdateMapper;

    /**
     * 查询app更新列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StockAppUpdate stockAppUpdate) {
        startPage();
        List<StockAppUpdate> list = stockAppUpdateService.selectStockAppUpdateList(stockAppUpdate);
        return getDataTable(list);
    }

    @GetMapping("/getLatestVersion")
    public AjaxResult getVersion(StockAppUpdate stockAppUpdate) {
        return success(stockAppUpdateMapper.selectLatestVersionRecord(stockAppUpdate));
    }

    /**
     * 获取app更新详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return success(stockAppUpdateService.selectStockAppUpdateById(id));
    }

    /**
     * 新增app更新
     */
    @Log(title = "app更新", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockAppUpdate stockAppUpdate) {
        return toAjax(stockAppUpdateService.insertStockAppUpdate(stockAppUpdate));
    }

    /**
     * 修改app更新
     */
    @Log(title = "app更新", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockAppUpdate stockAppUpdate) {
        return toAjax(stockAppUpdateService.updateStockAppUpdate(stockAppUpdate));
    }

    /**
     * 删除app更新
     */
    @Log(title = "app更新", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(stockAppUpdateService.deleteStockAppUpdateByIds(ids));
    }
}

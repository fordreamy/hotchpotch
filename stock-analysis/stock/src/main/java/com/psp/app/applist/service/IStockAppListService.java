package com.psp.app.applist.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.psp.app.applist.domain.StockAppList;
import com.psp.app.applist.entity.StockAppListEntity;

/**
 * APP应用Service接口
 *
 * @author Tiger
 * @date 2023-03-21
 */
public interface IStockAppListService extends IService<StockAppListEntity> {
    /**
     * 查询APP应用
     *
     * @param id APP应用主键
     * @return APP应用
     */
    StockAppList selectStockAppListById(String id);

    /**
     * 查询APP应用列表
     *
     * @param stockAppList APP应用
     * @return APP应用集合
     */
    List<StockAppList> selectStockAppListList(StockAppList stockAppList);

    /**
     * 新增APP应用
     *
     * @param stockAppList APP应用
     * @return 结果
     */
    int insertStockAppList(StockAppList stockAppList);

    /**
     * 修改APP应用
     *
     * @param stockAppList APP应用
     * @return 结果
     */
    int updateStockAppList(StockAppList stockAppList);

    /**
     * 批量删除APP应用
     *
     * @param ids 需要删除的APP应用主键集合
     * @return 结果
     */
    int deleteStockAppListByIds(String[] ids);

    /**
     * 删除APP应用信息
     *
     * @param id APP应用主键
     * @return 结果
     */
    int deleteStockAppListById(String id);
}

package com.psp.app.applist.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.psp.app.applist.entity.StockAppListEntity;
import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.stereotype.Service;
import com.psp.app.applist.mapper.StockAppListMapper;
import com.psp.app.applist.domain.StockAppList;
import com.psp.app.applist.service.IStockAppListService;

import javax.annotation.Resource;

/**
 * APP应用Service业务层处理
 *
 * @author Tiger
 * @date 2023-03-21
 */
@Service
public class StockAppListServiceImpl extends ServiceImpl<StockAppListMapper, StockAppListEntity> implements IStockAppListService {
    @Resource
    private StockAppListMapper stockAppListMapper;

    /**
     * 查询APP应用
     *
     * @param id APP应用主键
     * @return APP应用
     */
    @Override
    public StockAppList selectStockAppListById(String id) {
        return stockAppListMapper.selectStockAppListById(id);
    }

    /**
     * 查询APP应用列表
     *
     * @param stockAppList APP应用
     * @return APP应用
     */
    @Override
    public List<StockAppList> selectStockAppListList(StockAppList stockAppList) {
        return stockAppListMapper.selectStockAppListList(stockAppList);
    }

    /**
     * 新增APP应用
     *
     * @param stockAppList APP应用
     * @return 结果
     */
    @Override
    public int insertStockAppList(StockAppList stockAppList) {
//        stockAppList.setCreateTime(DateUtils.getNowDate());
        stockAppList.setId(IdUtils.fastSimpleUUID());
        return stockAppListMapper.insertStockAppList(stockAppList);
    }

    /**
     * 修改APP应用
     *
     * @param stockAppList APP应用
     * @return 结果
     */
    @Override
    public int updateStockAppList(StockAppList stockAppList) {
//        stockAppList.setUpdateTime(DateUtils.getNowDate());
        return stockAppListMapper.updateStockAppList(stockAppList);
    }

    /**
     * 批量删除APP应用
     *
     * @param ids 需要删除的APP应用主键
     * @return 结果
     */
    @Override
    public int deleteStockAppListByIds(String[] ids) {
        return stockAppListMapper.deleteStockAppListByIds(ids);
    }

    /**
     * 删除APP应用信息
     *
     * @param id APP应用主键
     * @return 结果
     */
    @Override
    public int deleteStockAppListById(String id) {
        return stockAppListMapper.deleteStockAppListById(id);
    }
}

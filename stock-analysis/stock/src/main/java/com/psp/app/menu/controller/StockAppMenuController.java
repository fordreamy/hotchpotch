package com.psp.app.menu.controller;

import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.psp.app.menu.domain.StockAppMenu;
import com.psp.app.menu.service.IStockAppMenuService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import static java.util.stream.Collectors.groupingBy;

/**
 * app菜单Controller
 *
 * @author peishuaipeng
 */
@RestController
@RequestMapping("/eye/app/menu")
public class StockAppMenuController extends BaseController {
    @Autowired
    private IStockAppMenuService stockAppMenuService;

    /**
     * 查询app菜单列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StockAppMenu stockAppMenu) {
        startPage();
        List<StockAppMenu> list = stockAppMenuService.selectStockAppMenuList(stockAppMenu);
        return getDataTable(list);
    }

    /**
     * 查询app动态菜单
     */
    @GetMapping("/appMenuList")
    public AjaxResult appMenuList(StockAppMenu stockAppMenu) {
        List<StockAppMenu> list = stockAppMenuService.selectStockAppMenuList(stockAppMenu);
        Map<String, List<StockAppMenu>> map = list.stream().collect(groupingBy(StockAppMenu::getGroupName, LinkedHashMap::new, Collectors.toList()));
        List<String> group = new ArrayList<>();
        List<List<StockAppMenu>> menu = new ArrayList<>();
        map.forEach((k, v) -> {
            group.add(k);
            menu.add(v);
        });
        Map<String, Object> result = new HashMap<>();
        result.put("group", group);
        result.put("menu", menu);
        return AjaxResult.success(result);
    }

    /**
     * 查询app菜单列表无分页
     */
    @GetMapping("/noPagingList")
    public TableDataInfo noPagingList(StockAppMenu stockAppMenu) {
        List<StockAppMenu> list = stockAppMenuService.selectStockAppMenuList(stockAppMenu);
        return getDataTable(list);
    }

    /**
     * 获取app菜单详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return success(stockAppMenuService.selectStockAppMenuById(id));
    }

    /**
     * 新增app菜单
     */
    @Log(title = "app菜单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockAppMenu stockAppMenu) {
        return toAjax(stockAppMenuService.insertStockAppMenu(stockAppMenu));
    }

    /**
     * 修改app菜单
     */
    @Log(title = "app菜单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockAppMenu stockAppMenu) {
        return toAjax(stockAppMenuService.updateStockAppMenu(stockAppMenu));
    }

    /**
     * 删除app菜单
     */
    @Log(title = "app菜单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(stockAppMenuService.deleteStockAppMenuByIds(ids));
    }
}

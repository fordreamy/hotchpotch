package com.psp.app.applist.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * APP应用对象 stock_app_list
 *
 * @author Tiger
 * @date 2023-03-21
 */
public class StockAppList extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * uniapp的应用标识appid
     */
    @Excel(name = "uniapp的应用标识appid")
    private String appId;

    /**
     * 应用名称
     */
    @Excel(name = "应用名称")
    private String name;

    /**
     * 应用描述
     */
    @Excel(name = "应用描述")
    private String description;

    /**
     * 创建人id
     */
    @Excel(name = "创建人id")
    private String createId;

    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名")
    private String createName;

    /**
     * 更新人id
     */
    @Excel(name = "更新人id")
    private String updateId;

    /**
     * 更新人姓名
     */
    @Excel(name = "更新人姓名")
    private String updateName;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppId() {
        return appId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setCreateId(String createId) {
        this.createId = createId;
    }

    public String getCreateId() {
        return createId;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getCreateName() {
        return createName;
    }

    public void setUpdateId(String updateId) {
        this.updateId = updateId;
    }

    public String getUpdateId() {
        return updateId;
    }

    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

    public String getUpdateName() {
        return updateName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("appid", getAppId())
                .append("name", getName())
                .append("description", getDescription())
                .append("createId", getCreateId())
                .append("createName", getCreateName())
                .append("createTime", getCreateTime())
                .append("updateId", getUpdateId())
                .append("updateName", getUpdateName())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}

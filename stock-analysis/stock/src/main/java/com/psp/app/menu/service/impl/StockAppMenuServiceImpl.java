package com.psp.app.menu.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.psp.app.menu.mapper.StockAppMenuMapper;
import com.psp.app.menu.domain.StockAppMenu;
import com.psp.app.menu.service.IStockAppMenuService;

import javax.annotation.Resource;

/**
 * app菜单Service业务层处理
 *
 * @author peishuaipeng
 * @date 2023-03-11
 */
@Service
public class StockAppMenuServiceImpl implements IStockAppMenuService {
    @Resource
    private StockAppMenuMapper stockAppMenuMapper;

    /**
     * 查询app菜单
     *
     * @param id app菜单主键
     * @return app菜单
     */
    @Override
    public StockAppMenu selectStockAppMenuById(String id) {
        return stockAppMenuMapper.selectStockAppMenuById(id);
    }

    /**
     * 查询app菜单列表
     *
     * @param stockAppMenu app菜单
     * @return app菜单
     */
    @Override
    public List<StockAppMenu> selectStockAppMenuList(StockAppMenu stockAppMenu) {
        return stockAppMenuMapper.selectStockAppMenuList(stockAppMenu);
    }

    /**
     * 新增app菜单
     *
     * @param stockAppMenu app菜单
     * @return 结果
     */
    @Override
    public int insertStockAppMenu(StockAppMenu stockAppMenu) {
        stockAppMenu.setId(IdUtils.fastSimpleUUID());
        stockAppMenu.setCreateTime(DateUtils.getNowDate());
        return stockAppMenuMapper.insertStockAppMenu(stockAppMenu);
    }

    /**
     * 修改app菜单
     *
     * @param stockAppMenu app菜单
     * @return 结果
     */
    @Override
    public int updateStockAppMenu(StockAppMenu stockAppMenu) {
        stockAppMenu.setUpdateTime(DateUtils.getNowDate());
        return stockAppMenuMapper.updateStockAppMenu(stockAppMenu);
    }

    /**
     * 批量删除app菜单
     *
     * @param ids 需要删除的app菜单主键
     * @return 结果
     */
    @Override
    public int deleteStockAppMenuByIds(String[] ids) {
        return stockAppMenuMapper.deleteStockAppMenuByIds(ids);
    }

    /**
     * 删除app菜单信息
     *
     * @param id app菜单主键
     * @return 结果
     */
    @Override
    public int deleteStockAppMenuById(String id) {
        return stockAppMenuMapper.deleteStockAppMenuById(id);
    }
}

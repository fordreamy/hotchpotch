package com.psp.industry.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 股票行业对象 stock_industry
 *
 * @author tiger
 */
public class StockIndustry extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 排序
     */
    @Excel(name = "排序")
    private Integer sortNum;

    /**
     * 股票编码
     */
    @Excel(name = "股票编码")
    private String stockCode;

    /**
     * 行业名称
     */
    @Excel(name = "行业名称")
    private String name;

    /**
     * 分组名称
     */
    @Excel(name = "样式")
    private String classStyle;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setSortNum(Integer sortNum) {
        this.sortNum = sortNum;
    }

    public Integer getSortNum() {
        return sortNum;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setClassStyle(String classStyle) {
        this.classStyle = classStyle;
    }

    public String getClassStyle() {
        return classStyle;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("sortNum", getSortNum())
                .append("stockCode", getStockCode())
                .append("name", getName())
                .append("classStyle", getClassStyle())
                .toString();
    }
}

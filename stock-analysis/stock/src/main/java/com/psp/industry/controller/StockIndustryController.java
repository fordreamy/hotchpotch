package com.psp.industry.controller;

import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

import cn.hutool.core.date.DateUtil;
import com.psp.stockprice.domain.StockPrice;
import com.psp.stockprice.service.IStockPriceService;
import com.ruoyi.common.annotation.Anonymous;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.psp.industry.domain.StockIndustry;
import com.psp.industry.service.IStockIndustryService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 股票行业Controller
 *
 * @author tiger
 */
@RestController
@RequestMapping("/eye/industry")
public class StockIndustryController extends BaseController {
    @Resource
    private IStockIndustryService stockIndustryService;

    @Resource
    private IStockPriceService stockPriceService;

    @Anonymous
    @GetMapping("/test")
    @ResponseBody
    public AjaxResult test() {
        try {
            logger.info("开始获取所有股票一段天数内的历史股价");
            Date startDate = DateUtil.parseDate("2022-01-01");
            Date endDate = DateUtil.parseDate("2022-12-01");
            List<StockPrice> stockPriceList = stockPriceService.selectStockPriceByStartAndEndDate(startDate, endDate);
            System.out.println(stockPriceList.size());
        } catch (Exception e) {
            logger.error("查询数据库失败!");
            e.printStackTrace();
        }

        return AjaxResult.success();
    }

    /**
     * 查询股票行业列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StockIndustry stockIndustry) {
        startPage();
        List<StockIndustry> list = stockIndustryService.selectStockIndustryList(stockIndustry);
        return getDataTable(list);
    }

    @GetMapping("/industryOptions")
    public TableDataInfo industryOptions() {
        List<StockIndustry> list = stockIndustryService.selectStockIndustryList(new StockIndustry());
        return getDataTable(list);
    }

    /**
     * 获取股票行业详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(stockIndustryService.selectStockIndustryById(id));
    }

    /**
     * 新增股票行业
     */
    @Log(title = "股票行业", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockIndustry stockIndustry) {
        return toAjax(stockIndustryService.insertStockIndustry(stockIndustry));
    }

    /**
     * 修改股票行业
     */
    @Log(title = "股票行业", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockIndustry stockIndustry) {
        return toAjax(stockIndustryService.updateStockIndustry(stockIndustry));
    }

    /**
     * 删除股票行业
     */
    @Log(title = "股票行业", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(stockIndustryService.deleteStockIndustryByIds(ids));
    }

    /**
     * 批量修改股票分组
     */
    @PutMapping("/batchUpdateStockIndustry")
    public AjaxResult batchUpdateStockMyGroup(@RequestBody List<StockIndustry> stockIndustryList) {
        stockIndustryService.batchUpdateStockIndustry(stockIndustryList);
        return AjaxResult.success("拖动成功!");
    }

}

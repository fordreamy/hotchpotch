package com.psp.industry.service;

import java.util.List;

import com.psp.industry.domain.StockIndustry;

/**
 * 股票行业Service接口
 *
 * @author tiger
 */
public interface IStockIndustryService {
    /**
     * 查询股票行业
     *
     * @param id 股票行业主键
     * @return 股票行业
     */
    StockIndustry selectStockIndustryById(String id);

    /**
     * 查询股票行业列表
     *
     * @param stockIndustry 股票行业
     * @return 股票行业集合
     */
    List<StockIndustry> selectStockIndustryList(StockIndustry stockIndustry);

    /**
     * 新增股票行业
     *
     * @param stockIndustry 股票行业
     * @return 结果
     */
    int insertStockIndustry(StockIndustry stockIndustry);

    /**
     * 修改股票行业
     *
     * @param stockIndustry 股票行业
     * @return 结果
     */
    int updateStockIndustry(StockIndustry stockIndustry);

    /**
     * 批量删除股票行业
     *
     * @param ids 需要删除的股票行业主键集合
     * @return 结果
     */
    int deleteStockIndustryByIds(String[] ids);

    /**
     * 删除股票行业信息
     *
     * @param id 股票行业主键
     * @return 结果
     */
    int deleteStockIndustryById(String id);

    /**
     * 批量新增股票行业
     *
     * @param stockIndustryList 股票行业
     */
    void batchInsertStockIndustry(List<StockIndustry> stockIndustryList);

    /**
     * 批量更新股票行业
     *
     * @param stockIndustryList 股票行业
     */
    void batchUpdateStockIndustry(List<StockIndustry> stockIndustryList);
}

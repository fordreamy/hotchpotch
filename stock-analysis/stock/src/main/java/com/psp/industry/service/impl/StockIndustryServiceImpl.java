package com.psp.industry.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import com.psp.industry.mapper.StockIndustryMapper;
import com.psp.industry.domain.StockIndustry;
import com.psp.industry.service.IStockIndustryService;

import javax.annotation.Resource;

/**
 * 股票行业Service业务层处理
 *
 * @author tiger
 */
@Service
public class StockIndustryServiceImpl implements IStockIndustryService {
    @Resource
    private StockIndustryMapper stockIndustryMapper;

    /**
     * 查询股票行业
     *
     * @param id 股票行业主键
     * @return 股票行业
     */
    @Override
    public StockIndustry selectStockIndustryById(String id) {
        return stockIndustryMapper.selectStockIndustryById(id);
    }

    /**
     * 查询股票行业列表
     *
     * @param stockIndustry 股票行业
     * @return 股票行业
     */
    @Override
    public List<StockIndustry> selectStockIndustryList(StockIndustry stockIndustry) {
        return stockIndustryMapper.selectStockIndustryList(stockIndustry);
    }

    /**
     * 新增股票行业
     *
     * @param stockIndustry 股票行业
     * @return 结果
     */
    @Override
    public int insertStockIndustry(StockIndustry stockIndustry) {
        stockIndustry.setId(IdUtils.fastSimpleUUID());
        Integer max = stockIndustryMapper.selectMaxSortNum();
        if (max != null) {
            stockIndustry.setSortNum(max + 1);
        } else {
            stockIndustry.setSortNum(1);
        }
        return stockIndustryMapper.insertStockIndustry(stockIndustry);
    }

    /**
     * 修改股票行业
     *
     * @param stockIndustry 股票行业
     * @return 结果
     */
    @Override
    public int updateStockIndustry(StockIndustry stockIndustry) {
        return stockIndustryMapper.updateStockIndustry(stockIndustry);
    }

    /**
     * 批量删除股票行业
     *
     * @param ids 需要删除的股票行业主键
     * @return 结果
     */
    @Override
    public int deleteStockIndustryByIds(String[] ids) {
        return stockIndustryMapper.deleteStockIndustryByIds(ids);
    }

    /**
     * 删除股票行业信息
     *
     * @param id 股票行业主键
     * @return 结果
     */
    @Override
    public int deleteStockIndustryById(String id) {
        return stockIndustryMapper.deleteStockIndustryById(id);
    }

    /**
     * 批量新增股票行业
     *
     * @param stockIndustryList 股票行业
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchInsertStockIndustry(List<StockIndustry> stockIndustryList) {
        stockIndustryList.forEach(n -> {
            n.setId(IdUtils.fastSimpleUUID());
            n.setCreateTime(DateUtils.getNowDate());
            n.setCreateBy(SecurityUtils.getUsername());
        });
        stockIndustryMapper.batchInsertStockIndustry(stockIndustryList);
    }

    /**
     * 批量更新股票行业
     *
     * @param stockIndustryList 股票行业
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchUpdateStockIndustry(List<StockIndustry> stockIndustryList) {
        stockIndustryList.forEach(n -> {
            n.setUpdateTime(DateUtils.getNowDate());
            n.setUpdateBy(SecurityUtils.getUsername());
        });
        stockIndustryMapper.batchUpdateStockIndustry(stockIndustryList);
    }
}

package com.psp.stockinterface.mapper;

import java.util.List;

import com.psp.stockinterface.domain.StockInterface;

/**
 * 接口Mapper接口
 *
 * @author tiger
 * @date 2022-09-18
 */
public interface StockInterfaceMapper {
    /**
     * 查询接口
     *
     * @param id 接口主键
     * @return 接口
     */
    public StockInterface selectStockInterfaceById(String id);

    /**
     * 查询接口列表
     *
     * @param stockInterface 接口
     * @return 接口集合
     */
    List<StockInterface> selectStockInterfaceList(StockInterface stockInterface);

    /**
     * 新增接口
     *
     * @param stockInterface 接口
     * @return 结果
     */
    int insertStockInterface(StockInterface stockInterface);

    /**
     * 修改接口
     *
     * @param stockInterface 接口
     * @return 结果
     */
    int updateStockInterface(StockInterface stockInterface);

    /**
     * 删除接口
     *
     * @param id 接口主键
     * @return 结果
     */
    int deleteStockInterfaceById(String id);

    /**
     * 批量删除接口
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deleteStockInterfaceByIds(String[] ids);

    /**
     * 批量新增接口
     *
     * @param stockInterfaceList 接口
     */
    void batchInsertStockInterface(List<StockInterface> stockInterfaceList);

    /**
     * 批量更新接口
     *
     * @param stockInterfaceList 接口
     */
    void batchUpdateStockInterface(List<StockInterface> stockInterfaceList);

}

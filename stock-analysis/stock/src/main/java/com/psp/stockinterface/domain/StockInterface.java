package com.psp.stockinterface.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 接口对象 stock_interface
 *
 * @author tiger
 * @date 2022-09-18
 */
public class StockInterface extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 接口名称
     */
    @Excel(name = "接口名称")
    private String name;

    /**
     * 接口URL
     */
    @Excel(name = "接口URL")
    private String url;

    /**
     * 接口类型
     */
    @Excel(name = "接口类型")
    private Integer type;

    /**
     * 请求参数
     */
    @Excel(name = "请求参数")
    private String parameter;

    /**
     * 请求缓存
     */
    @Excel(name = "请求缓存")
    private String cookie;

    /**
     * 请求body
     */
    @Excel(name = "请求body")
    private String body;

    /**
     * 请求header
     */
    @Excel(name = "请求header")
    private String header;

    /**
     * 返回内容
     */
    @Excel(name = "返回内容")
    private String responseContent;

    /**
     * 接口访问时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "接口访问时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date accessTime;

    /**
     * 接口状态,1失败,0成功
     */
    @Excel(name = "接口状态,1失败,0成功")
    private Integer status;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getType() {
        return type;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getParameter() {
        return parameter;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String getCookie() {
        return cookie;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHeader() {
        return header;
    }

    public void setResponseContent(String responseContent) {
        this.responseContent = responseContent;
    }

    public String getResponseContent() {
        return responseContent;
    }

    public void setAccessTime(Date accessTime) {
        this.accessTime = accessTime;
    }

    public Date getAccessTime() {
        return accessTime;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("url", getUrl())
                .append("type", getType())
                .append("parameter", getParameter())
                .append("cookie", getCookie())
                .append("body", getBody())
                .append("header", getHeader())
                .append("responseContent", getResponseContent())
                .append("accessTime", getAccessTime())
                .append("status", getStatus())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}

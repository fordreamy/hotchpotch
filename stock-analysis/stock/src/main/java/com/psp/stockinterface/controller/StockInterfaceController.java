package com.psp.stockinterface.controller;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.psp.stockinterface.domain.StockInterface;
import com.psp.stockinterface.service.IStockInterfaceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 接口Controller
 *
 * @author tiger
 */
@RestController
@RequestMapping("/eye/interface")
public class StockInterfaceController extends BaseController {
    @Resource
    private IStockInterfaceService stockInterfaceService;

    /**
     * 查询接口列表
     */
    @PreAuthorize("@ss.hasPermi('eye:interface:list')")
    @GetMapping("/list")
    public TableDataInfo list(StockInterface stockInterface) {
        startPage();
        List<StockInterface> list = stockInterfaceService.selectStockInterfaceList(stockInterface);
        return getDataTable(list);
    }

    /**
     * 获取接口详细信息
     */
    @PreAuthorize("@ss.hasPermi('eye:interface:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(stockInterfaceService.selectStockInterfaceById(id));
    }

    /**
     * 新增接口
     */
    @PreAuthorize("@ss.hasPermi('eye:interface:add')")
    @Log(title = "接口", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockInterface stockInterface) {
        return toAjax(stockInterfaceService.insertStockInterface(stockInterface));
    }

    /**
     * 修改接口
     */
    @PreAuthorize("@ss.hasPermi('eye:interface:edit')")
    @Log(title = "接口", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockInterface stockInterface) {
        return toAjax(stockInterfaceService.updateStockInterface(stockInterface));
    }

    /**
     * 删除接口
     */
    @PreAuthorize("@ss.hasPermi('eye:interface:remove')")
    @Log(title = "接口", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(stockInterfaceService.deleteStockInterfaceByIds(ids));
    }

    /**
     * 导出接口列表
     */
    @Log(title = "接口", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockInterface stockInterface) {
        List<StockInterface> list = stockInterfaceService.selectStockInterfaceList(stockInterface);
        ExcelUtil<StockInterface> util = new ExcelUtil<>(StockInterface.class);
        util.exportExcel(response, list, "接口数据");
    }

    /**
     * 下载导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        ExcelUtil<StockInterface> util = new ExcelUtil<>(StockInterface.class);
        util.importTemplateExcel(response, "股票模板");
    }

    /**
     * 导入接口
     */
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception {
        ExcelUtil<StockInterface> util = new ExcelUtil<>(StockInterface.class);
        List<StockInterface> stockInterfaceList = util.importExcel(file.getInputStream());
        stockInterfaceService.batchInsertStockInterface(stockInterfaceList);
        return AjaxResult.success();
    }

    /**
     * 测试接口是否可用
     */
    @PostMapping("/test")
    public AjaxResult testInterface(@RequestBody StockInterface stockInterface) {
        String result = stockInterfaceService.testInterface(stockInterface);
        if (result == null) {
            return AjaxResult.success("ERROR", "出错啦!");
        }
        return AjaxResult.success("OK", result);
    }
}

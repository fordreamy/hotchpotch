package com.psp.stockprice.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 股票的历史价格
 *
 * @author tiger
 */
public class StockHistoryPriceDTO {

    public StockHistoryPriceDTO() {
        priceList = new ArrayList<>();
        openPriceList = new ArrayList<>();
        closePriceList = new ArrayList<>();
        highestList = new ArrayList<>();
        lowestList = new ArrayList<>();
        volumeList = new ArrayList<>();
    }

    /**
     * 股票编码
     */
    private String stockCode;

    /**
     * 交易日期
     */
    private List<Date> tradeDateList;

    /**
     * 交易日对应的开盘价、收盘价、最高价、最低价
     */
    private List<Double> volumeList;

    /**
     * 交易日对应的开盘价、收盘价、最高价、最低价
     */
    private List<List<Double>> priceList;

    /**
     * 开盘价
     */
    private List<Double> openPriceList;

    /**
     * 收盘价
     */
    private List<Double> closePriceList;

    /**
     * 最高价
     */
    private List<Double> highestList;

    /**
     * 最低价
     */
    private List<Double> lowestList;

    public List<Double> getVolumeList() {
        return volumeList;
    }

    public void setVolumeList(List<Double> volumeList) {
        this.volumeList = volumeList;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public List<Date> getTradeDateList() {
        return tradeDateList;
    }

    public void setTradeDateList(List<Date> tradeDateList) {
        this.tradeDateList = tradeDateList;
    }

    public List<List<Double>> getPriceList() {
        return priceList;
    }

    public void setPriceList(List<List<Double>> priceList) {
        this.priceList = priceList;
    }

    public List<Double> getClosePriceList() {
        return closePriceList;
    }

    public void setClosePriceList(List<Double> closePriceList) {
        this.closePriceList = closePriceList;
    }

    public List<Double> getOpenPriceList() {
        return openPriceList;
    }

    public void setOpenPriceList(List<Double> openPriceList) {
        this.openPriceList = openPriceList;
    }

    public List<Double> getHighestList() {
        return highestList;
    }

    public void setHighestList(List<Double> highestList) {
        this.highestList = highestList;
    }

    public List<Double> getLowestList() {
        return lowestList;
    }

    public void setLowestList(List<Double> lowestList) {
        this.lowestList = lowestList;
    }

    @Override
    public String toString() {
        return "StockHistoryPriceDTO{" +
                "stockCode='" + stockCode + '\'' +
                ", tradeDateList=" + tradeDateList +
                ", priceList=" + priceList +
                ", closePriceList=" + closePriceList +
                '}';
    }
}

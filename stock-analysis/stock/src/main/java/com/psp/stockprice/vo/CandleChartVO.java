package com.psp.stockprice.vo;

import lombok.Data;

import java.util.List;

/**
 * K线图的返回对象
 */
@Data
public class CandleChartVO {
    /**
     * 日期
     */
    List<String> date;

    /**
     * 价格
     */
    List<List<Float>> price;

}

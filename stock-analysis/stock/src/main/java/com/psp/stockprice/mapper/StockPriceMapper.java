package com.psp.stockprice.mapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.psp.stockprice.domain.StockPrice;
import org.apache.ibatis.annotations.Param;

/**
 * 股票每日价格Mapper接口
 *
 * @author ruoyi
 */
public interface StockPriceMapper {
    /**
     * 查询股票每日价格
     *
     * @param id 股票每日价格主键
     * @return 股票每日价格
     */
    StockPrice selectStockPriceById(String id);

    /**
     * 查询股票每日价格列表
     *
     * @param stockPrice 股票每日价格
     * @return 股票每日价格集合
     */
    List<StockPrice> selectStockPriceList(StockPrice stockPrice);

    /**
     * 获取最近的两条记录
     */
    List<StockPrice> selectRecentTwoRecord(String stockCode);

    /**
     * 查询n天的交易日期
     *
     * @param days 查询天数
     * @return 交易日期
     */
    List<Map<String, Object>> selectStockTradeStartAndEndDate(int days);

    /**
     * 查询一段时间内的股票价格
     */
    List<StockPrice> selectStockPriceByStartAndEndDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    /**
     * 查询某股票的一段时间内的股票价格
     */
    List<StockPrice> selectStockPriceByStartAndEndDateAndStockCode(@Param("stockCode") String stockCode, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    /**
     * @param stockCode 股票code
     * @param tradeDate 交易日期
     * @return 查询股票的价格记录数
     */
    StockPrice selectByStockCodeAndTradeDate(@Param("stockCode") String stockCode, @Param("tradeDate") Date tradeDate);

    /**
     * 获取某股票的历史价格
     *
     * @param stockCode 股票编码
     * @return 股票的历史价格
     */
    List<StockPrice> getHistoryPriceByStockCode(String stockCode);

    /**
     * 新增股票每日价格
     *
     * @param stockPrice 股票每日价格
     * @return 结果
     */
    int insertStockPrice(StockPrice stockPrice);

    int batchInsertStockPrice(List<StockPrice> stockPriceList);

    int backupStockHistoryPrice(List<StockPrice> stockPriceList);

    /**
     * 修改股票每日价格
     *
     * @param stockPrice 股票每日价格
     * @return 结果
     */
    int updateStockPrice(StockPrice stockPrice);

    /**
     * 删除股票每日价格
     *
     * @param id 股票每日价格主键
     * @return 结果
     */
    int deleteStockPriceById(String id);

    /**
     * 删除股票时间范围内的历史价格
     *
     * @param stockCode 股票code
     * @param fromDate  开始日期
     * @param toDate    结束日期
     * @return 删除条数
     */
    int deleteStockPriceByDate(@Param("stockCode") String stockCode, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

    /**
     * 根据股票交易日期删除股票价格
     *
     * @param tradeDate 股票交易日期
     */
    void deleteStockPriceByTradeDate(@Param("tradeDate") Date tradeDate);

    /**
     * 根据股票code删除股票价格
     *
     * @param stockCode 股票code
     */
    void deleteStockPriceByCode(@Param("stockCode") String stockCode);

    /**
     * 批量删除股票每日价格
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deleteStockPriceByIds(String[] ids);
}

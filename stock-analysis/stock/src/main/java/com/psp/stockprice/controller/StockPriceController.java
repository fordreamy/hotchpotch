package com.psp.stockprice.controller;

import java.util.List;
import java.util.Map;

import com.psp.stockprice.vo.CandleChartVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.psp.stockprice.service.IStockPriceService;

import javax.annotation.Resource;

/**
 * 股票每日价格Controller
 */
@RestController
@RequestMapping("/eye/stockPrice")
public class StockPriceController extends BaseController {
    @Resource
    private IStockPriceService stockPriceService;

    /**
     * 获取某股票的历史价格
     */
    @GetMapping("/getHistoryPriceByStockCode")
    public AjaxResult list(String stockCode) {
        CandleChartVO price = stockPriceService.getHistoryPriceByStockCode(stockCode);
        return AjaxResult.success(price);
    }

}

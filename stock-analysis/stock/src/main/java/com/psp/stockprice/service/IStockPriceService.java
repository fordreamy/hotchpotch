package com.psp.stockprice.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.psp.stock.domain.Stock;
import com.psp.stockprice.domain.StockPrice;
import com.psp.stockprice.vo.CandleChartVO;

/**
 * 股票每日价格Service接口
 */
public interface IStockPriceService {
    /**
     * 查询股票每日价格
     *
     * @param id 股票每日价格主键
     * @return 股票每日价格
     */
    StockPrice selectStockPriceById(String id);

    /**
     * 查询股票每日价格列表
     *
     * @param stockPrice 股票每日价格
     * @return 股票每日价格集合
     */
    List<StockPrice> selectStockPriceList(StockPrice stockPrice);

    /**
     * 获取最近的两条记录
     */
    List<StockPrice> selectRecentTwoRecord(String stockCode);

    /**
     * 查询n天的交易日期
     *
     * @param days 查询天数
     * @return 交易日期
     */
    List<Map<String, Object>> selectStockTradeStartAndEndDate(int days);

    /**
     * 查询一段时间内的股票价格
     */
    List<StockPrice> selectStockPriceByStartAndEndDate(Date startTime, Date endTime);

    /**
     * 查询某股票的一段时间内的股票价格
     */
    List<StockPrice> selectStockPriceByStartAndEndDate(String stockCode, Date startTime, Date endTime);

    /**
     * 查询是否存在某天的股票价格
     *
     * @param stockCode 股票编码
     * @param tradeDate 交易日期
     */
    Long selectByStockCodeAndTradeDate(String stockCode, Date tradeDate);

    /**
     * 获取某股票的历史价格
     *
     * @param stockCode 股票编码
     * @return 股票的历史价格
     */
    CandleChartVO getHistoryPriceByStockCode(String stockCode);

    /**
     * 新增股票每日价格
     *
     * @param stockPrice 股票每日价格
     * @return 结果
     */
    int insertStockPrice(StockPrice stockPrice);

    /**
     * 修改股票每日价格
     *
     * @param stockPrice 股票每日价格
     * @return 结果
     */
    int updateStockPrice(StockPrice stockPrice);

    /**
     * 批量删除股票每日价格
     *
     * @param ids 需要删除的股票每日价格主键集合
     * @return 结果
     */
    int deleteStockPriceByIds(String[] ids);

    /**
     * 删除股票每日价格信息
     *
     * @param id 股票每日价格主键
     * @return 结果
     */
    int deleteStockPriceById(String id);

    /**
     * 根据股票交易日期删除股票价格
     *
     * @param tradeDate 股票交易日期
     */
    void deleteStockPriceByTradeDate(Date tradeDate);

    /**
     * 更新股票的历史股价,股价必须有日期且从小到大连续
     */
    void insertOrUpdateStockHistoryPrice(Stock stock, List<List<String>> price);

}

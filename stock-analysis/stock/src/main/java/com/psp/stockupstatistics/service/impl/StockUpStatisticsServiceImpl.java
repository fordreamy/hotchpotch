package com.psp.stockupstatistics.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import com.psp.stockupstatistics.mapper.StockUpStatisticsMapper;
import com.psp.stockupstatistics.domain.StockUpStatistics;
import com.psp.stockupstatistics.service.IStockUpStatisticsService;

import javax.annotation.Resource;

/**
 * 股票指标统计信息Service业务层处理
 *
 * @author tiger
 */
@Service
public class StockUpStatisticsServiceImpl implements IStockUpStatisticsService {

    protected static final Logger logger = LoggerFactory.getLogger(StockUpStatisticsServiceImpl.class);

    @Resource
    private StockUpStatisticsMapper stockUpStatisticsMapper;

    /**
     * 查询股票指标统计信息
     *
     * @param id 股票指标统计信息主键
     * @return 股票指标统计信息
     */
    @Override
    public StockUpStatistics selectStockUpStatisticsById(String id) {
        return stockUpStatisticsMapper.selectStockUpStatisticsById(id);
    }

    /**
     * 查询股票指标统计信息列表
     *
     * @param stockUpStatistics 股票指标统计信息
     * @return 股票指标统计信息
     */
    @Override
    public List<StockUpStatistics> selectStockUpStatisticsList(StockUpStatistics stockUpStatistics) {
        return stockUpStatisticsMapper.selectStockUpStatisticsList(stockUpStatistics);
    }

    /**
     * 根据均线查询
     */
    @Override
    public List<StockUpStatistics> averageLineType(StockUpStatistics stockUpStatistics) {
        int averageLineType = stockUpStatistics.getAverageLineType();
        List<StockUpStatistics> stockUpStatisticsList = stockUpStatisticsMapper.selectStockUpStatisticsList(new StockUpStatistics());
        if (averageLineType == 1) {
            stockUpStatisticsList = stockUpStatisticsList.stream().filter(n -> n.getClosePrice() >= n.getMa5() && n.getMa5() >= n.getMa10() && n.getMa10() >= n.getMa20() && n.getMa20() >= n.getMa30()
                    && n.getMa30() >= n.getMa60() && n.getMa60() >= n.getMa125() && n.getMa125() >= n.getMa250()).collect(Collectors.toList());
        } else if (averageLineType == 2) {
            stockUpStatisticsList = stockUpStatisticsList.stream().filter(n -> n.getClosePrice() >= n.getMa5() && n.getMa5() >= n.getMa10()).collect(Collectors.toList());
        } else if (averageLineType == 3) {
            stockUpStatisticsList = stockUpStatisticsList.stream().filter(n -> n.getClosePrice() >= n.getMa5() && n.getMa5() >= n.getMa10() && n.getMa10() >= n.getMa20()).collect(Collectors.toList());
        } else if (averageLineType == 4) {
            stockUpStatisticsList = stockUpStatisticsList.stream().filter(n -> n.getClosePrice() >= n.getMa5() && n.getMa5() >= n.getMa10() && n.getMa10() >= n.getMa20()
                    && n.getMa20() >= n.getMa30()).collect(Collectors.toList());
        } else if (averageLineType == 5) {
            stockUpStatisticsList = stockUpStatisticsList.stream().filter(n -> n.getClosePrice() >= n.getMa5() && n.getMa5() >= n.getMa10() && n.getMa10() >= n.getMa20() && n.getMa20() >= n.getMa30()
                    && n.getMa30() >= n.getMa60()).collect(Collectors.toList());
        } else if (averageLineType == 6) {
            stockUpStatisticsList = stockUpStatisticsList.stream().filter(n -> n.getClosePrice() >= n.getMa5() && n.getMa5() >= n.getMa10() && n.getMa10() >= n.getMa20() && n.getMa20() >= n.getMa30()
                    && n.getMa30() >= n.getMa60() && n.getMa60() >= n.getMa125() && n.getMa125() > 0).collect(Collectors.toList());
        } else if (averageLineType == 7) {
            stockUpStatisticsList = stockUpStatisticsList.stream().filter(n -> n.getClosePrice() >= n.getMa5() && n.getMa5() >= n.getMa10() && n.getMa10() >= n.getMa20() && n.getMa20() >= n.getMa30()
                    && n.getMa30() >= n.getMa60() && n.getMa60() >= n.getMa125() && n.getMa125() >= n.getMa250() && n.getMa250() > 0).collect(Collectors.toList());
        }
        return stockUpStatisticsList;
    }

    /**
     * 新增股票指标统计信息
     *
     * @param stockUpStatistics 股票指标统计信息
     * @return 结果
     */
    @Override
    public int insertStockUpStatistics(StockUpStatistics stockUpStatistics) {
        stockUpStatistics.setId(IdUtils.fastSimpleUUID());
        stockUpStatistics.setCreateTime(DateUtils.getNowDate());
        return stockUpStatisticsMapper.insertStockUpStatistics(stockUpStatistics);
    }

    /**
     * 修改股票指标统计信息
     *
     * @param stockUpStatistics 股票指标统计信息
     * @return 结果
     */
    @Override
    public int updateStockUpStatistics(StockUpStatistics stockUpStatistics) {
        stockUpStatistics.setUpdateTime(DateUtils.getNowDate());
        return stockUpStatisticsMapper.updateStockUpStatistics(stockUpStatistics);
    }

    /**
     * 批量删除股票指标统计信息
     *
     * @param ids 需要删除的股票指标统计信息主键
     * @return 结果
     */
    @Override
    public int deleteStockUpStatisticsByIds(String[] ids) {
        return stockUpStatisticsMapper.deleteStockUpStatisticsByIds(ids);
    }

    /**
     * 删除股票指标统计信息信息
     *
     * @param id 股票指标统计信息主键
     * @return 结果
     */
    @Override
    public int deleteStockUpStatisticsById(String id) {
        return stockUpStatisticsMapper.deleteStockUpStatisticsById(id);
    }

    /**
     * 批量新增股票指标统计信息
     *
     * @param stockUpStatisticsList 股票指标统计信息
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchInsertStockUpStatistics(List<StockUpStatistics> stockUpStatisticsList) {
        for (StockUpStatistics n : stockUpStatisticsList) {
            n.setId(IdUtils.fastSimpleUUID());
            n.setCreateTime(DateUtils.getNowDate());
        }
        stockUpStatisticsMapper.batchInsertStockUpStatistics(stockUpStatisticsList);
    }

    /**
     * 批量更新股票指标统计信息
     *
     * @param stockUpStatisticsList 股票指标统计信息
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchUpdateStockUpStatistics(List<StockUpStatistics> stockUpStatisticsList) {
        stockUpStatisticsList.forEach(n -> {
            n.setUpdateTime(DateUtils.getNowDate());
            n.setUpdateBy(SecurityUtils.getUsername());
        });
        stockUpStatisticsMapper.batchUpdateStockUpStatistics(stockUpStatisticsList);
    }

    /**
     * 清空数据
     */
    @Override
    public void clearData() {
        stockUpStatisticsMapper.clearData();
    }
}

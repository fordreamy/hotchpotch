package com.psp.stockupstatistics.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 股票指标统计信息对象 stock_up_statistics
 *
 * @author psp
 */
public class StockUpStatistics extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 股票code
     */
    @Excel(name = "股票code")
    private String stockCode;

    /**
     * 股票名称
     */
    @Excel(name = "股票名称")
    private String stockName;

    /**
     * 1涨停2跌停3光头4光头光脚
     */
    @Excel(name = "1涨停2跌停3光头4光头光脚")
    private Integer type;

    /**
     * 均线类型
     */
    private Integer averageLineType;

    /**
     * 收盘价和均线价格关系
     */
    private Integer closeAverageLineType;

    /**
     * 涨跌统计天数
     */
    @Excel(name = "涨跌统计天数")
    private Integer statisticsDays;

    /**
     * 收盘价
     */
    @Excel(name = "收盘价")
    private Double closePrice;

    /**
     * 收盘价查询范围开始
     */
    private Double closePriceStart;

    /**
     * 收盘价查询范围结束
     */
    private Double closePriceEnd;

    /**
     * 成交量的倍数
     */
    @Excel(name = "成交量的倍数")
    private Double volumeTimes;

    /**
     * 涨的天数
     */
    @Excel(name = "涨的天数")
    private Integer riseDays;

    /**
     * 跌的天数
     */
    @Excel(name = "跌的天数")
    private Integer fallDays;

    /**
     * 连涨天数
     */
    @Excel(name = "连涨天数")
    private Integer continueRiseDays;

    /**
     * 连涨幅度
     */
    @Excel(name = "连涨幅度")
    private Double continueRiseRate;

    /**
     * 连跌天数
     */
    @Excel(name = "连跌天数")
    private Integer continueFallDays;

    /**
     * 连跌幅度
     */
    @Excel(name = "连跌幅度")
    private Double continueFallRate;

    /**
     * 涨停天数
     */
    @Excel(name = "涨停天数")
    private Integer riseStopDays;

    /**
     * 跌停天数
     */
    @Excel(name = "跌停天数")
    private Integer fallStopDays;

    /**
     * 连续涨停天数
     */
    @Excel(name = "连续涨停天数")
    private Integer continueRiseStopDays;

    /**
     * 连续跌停天数
     */
    @Excel(name = "连续跌停天数")
    private Integer continueFallStopDays;

    /**
     * K线名称
     */
    @Excel(name = "K线名称")
    private String kLineName;

    /**
     * 5日均价
     */
    @Excel(name = "5日均价")
    private Double ma5;

    /**
     * 10日均价
     */
    @Excel(name = "10日均价")
    private Double ma10;

    /**
     * 20日均价
     */
    @Excel(name = "20日均价")
    private Double ma20;

    /**
     * 30日均价
     */
    @Excel(name = "30日均价")
    private Double ma30;

    /**
     * 60日均价
     */
    @Excel(name = "60日均价")
    private Double ma60;

    /**
     * 125日均价
     */
    @Excel(name = "125日均价")
    private Double ma125;

    /**
     * 250日均价
     */
    @Excel(name = "250日均价")
    private Double ma250;

    /**
     * kdj金叉次数
     */
    @Excel(name = "kdj金叉次数")
    private Integer kdjGoldForkTimes;

    /**
     * 正处于kdj金叉状态
     */
    @Excel(name = "正处于kdj金叉状态")
    private Integer inKdjGoldFork;

    /**
     * kdj是否超跌,1是0否
     */
    @Excel(name = "kdj是否超跌,1是0否")
    private Integer kdjOverfall;

    /**
     * macd金叉次数
     */
    @Excel(name = "macd金叉次数")
    private Integer macdGoldForkTimes;

    /**
     * 正处于macd金叉状态
     */
    @Excel(name = "正处于macd金叉状态")
    private Integer inMacdGoldFork;

    /**
     * macd值是否大于0,1是0否
     */
    @Excel(name = "macd值是否大于0,1是0否")
    private Integer macdGreaterThan0;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockName() {
        return stockName;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getType() {
        return type;
    }

    public void setStatisticsDays(Integer statisticsDays) {
        this.statisticsDays = statisticsDays;
    }

    public Integer getStatisticsDays() {
        return statisticsDays;
    }

    public void setRiseDays(Integer riseDays) {
        this.riseDays = riseDays;
    }

    public Integer getRiseDays() {
        return riseDays;
    }

    public void setFallDays(Integer fallDays) {
        this.fallDays = fallDays;
    }

    public Integer getFallDays() {
        return fallDays;
    }

    public void setContinueRiseDays(Integer continueRiseDays) {
        this.continueRiseDays = continueRiseDays;
    }

    public Integer getContinueRiseDays() {
        return continueRiseDays;
    }

    public void setContinueRiseRate(Double continueRiseRate) {
        this.continueRiseRate = continueRiseRate;
    }

    public Double getContinueRiseRate() {
        return continueRiseRate;
    }

    public void setContinueFallDays(Integer continueFallDays) {
        this.continueFallDays = continueFallDays;
    }

    public Integer getContinueFallDays() {
        return continueFallDays;
    }

    public void setContinueFallRate(Double continueFallRate) {
        this.continueFallRate = continueFallRate;
    }

    public Double getContinueFallRate() {
        return continueFallRate;
    }

    public void setRiseStopDays(Integer riseStopDays) {
        this.riseStopDays = riseStopDays;
    }

    public Integer getRiseStopDays() {
        return riseStopDays;
    }

    public void setFallStopDays(Integer fallStopDays) {
        this.fallStopDays = fallStopDays;
    }

    public Integer getFallStopDays() {
        return fallStopDays;
    }

    public void setContinueRiseStopDays(Integer continueRiseStopDays) {
        this.continueRiseStopDays = continueRiseStopDays;
    }

    public Integer getContinueRiseStopDays() {
        return continueRiseStopDays;
    }

    public void setContinueFallStopDays(Integer continueFallStopDays) {
        this.continueFallStopDays = continueFallStopDays;
    }

    public Integer getContinueFallStopDays() {
        return continueFallStopDays;
    }

    public void setkLineName(String kLineName) {
        this.kLineName = kLineName;
    }

    public String getkLineName() {
        return kLineName;
    }

    public void setMa5(Double ma5) {
        this.ma5 = ma5;
    }

    public Double getMa5() {
        return ma5;
    }

    public void setMa10(Double ma10) {
        this.ma10 = ma10;
    }

    public Double getMa10() {
        return ma10;
    }

    public void setMa20(Double ma20) {
        this.ma20 = ma20;
    }

    public Double getMa20() {
        return ma20;
    }

    public void setMa30(Double ma30) {
        this.ma30 = ma30;
    }

    public Double getMa30() {
        return ma30;
    }

    public void setMa60(Double ma60) {
        this.ma60 = ma60;
    }

    public Double getMa60() {
        return ma60;
    }

    public void setMa125(Double ma125) {
        this.ma125 = ma125;
    }

    public Double getMa125() {
        return ma125;
    }

    public void setMa250(Double ma250) {
        this.ma250 = ma250;
    }

    public Double getMa250() {
        return ma250;
    }

    public Double getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(Double closePrice) {
        this.closePrice = closePrice;
    }

    public Integer getAverageLineType() {
        return averageLineType;
    }

    public void setAverageLineType(Integer averageLineType) {
        this.averageLineType = averageLineType;
    }

    public Integer getKdjGoldForkTimes() {
        return kdjGoldForkTimes;
    }

    public void setKdjGoldForkTimes(Integer kdjGoldForkTimes) {
        this.kdjGoldForkTimes = kdjGoldForkTimes;
    }

    public Integer getInKdjGoldFork() {
        return inKdjGoldFork;
    }

    public void setInKdjGoldFork(Integer inKdjGoldFork) {
        this.inKdjGoldFork = inKdjGoldFork;
    }

    public Integer getMacdGoldForkTimes() {
        return macdGoldForkTimes;
    }

    public void setMacdGoldForkTimes(Integer macdGoldForkTimes) {
        this.macdGoldForkTimes = macdGoldForkTimes;
    }

    public Integer getInMacdGoldFork() {
        return inMacdGoldFork;
    }

    public void setInMacdGoldFork(Integer inMacdGoldFork) {
        this.inMacdGoldFork = inMacdGoldFork;
    }

    public Integer getKdjOverfall() {
        return kdjOverfall;
    }

    public void setKdjOverfall(Integer kdjOverfall) {
        this.kdjOverfall = kdjOverfall;
    }

    public Integer getMacdGreaterThan0() {
        return macdGreaterThan0;
    }

    public void setMacdGreaterThan0(Integer macdGreaterThan0) {
        this.macdGreaterThan0 = macdGreaterThan0;
    }

    public Double getVolumeTimes() {
        return volumeTimes;
    }

    public void setVolumeTimes(Double volumeTimes) {
        this.volumeTimes = volumeTimes;
    }

    public Integer getCloseAverageLineType() {
        return closeAverageLineType;
    }

    public void setCloseAverageLineType(Integer closeAverageLineType) {
        this.closeAverageLineType = closeAverageLineType;
    }

    public Double getClosePriceStart() {
        return closePriceStart;
    }

    public void setClosePriceStart(Double closePriceStart) {
        this.closePriceStart = closePriceStart;
    }

    public Double getClosePriceEnd() {
        return closePriceEnd;
    }

    public void setClosePriceEnd(Double closePriceEnd) {
        this.closePriceEnd = closePriceEnd;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("stockCode", getStockCode())
                .append("stockName", getStockName())
                .append("type", getType())
                .append("statisticsDays", getStatisticsDays())
                .append("riseDays", getRiseDays())
                .append("fallDays", getFallDays())
                .append("continueRiseDays", getContinueRiseDays())
                .append("continueRiseRate", getContinueRiseRate())
                .append("continueFallDays", getContinueFallDays())
                .append("continueFallRate", getContinueFallRate())
                .append("riseStopDays", getRiseStopDays())
                .append("fallStopDays", getFallStopDays())
                .append("continueRiseStopDays", getContinueRiseStopDays())
                .append("continueFallStopDays", getContinueFallStopDays())
                .append("kLineName", getkLineName())
                .append("ma5", getMa5())
                .append("ma10", getMa10())
                .append("ma20", getMa20())
                .append("ma30", getMa30())
                .append("ma60", getMa60())
                .append("ma125", getMa125())
                .append("ma250", getMa250())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}

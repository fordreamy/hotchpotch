package com.psp.stockupstatistics.controller;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.psp.stockupstatistics.domain.StockUpStatistics;
import com.psp.stockupstatistics.service.IStockUpStatisticsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 股票指标统计信息Controller
 *
 * @author tiger
 * @date 2022-09-18
 */
@RestController
@RequestMapping("/eye/stockUpStatistics")
public class StockUpStatisticsController extends BaseController {
    @Autowired
    private IStockUpStatisticsService stockUpStatisticsService;

    /**
     * 查询股票指标统计信息列表
     */
    @PreAuthorize("@ss.hasPermi('eye:stockUpStatistics:list')")
    @GetMapping("/list")
    public TableDataInfo list(StockUpStatistics stockUpStatistics) {
        startPage();
        List<StockUpStatistics> list = stockUpStatisticsService.selectStockUpStatisticsList(stockUpStatistics);
        return getDataTable(list);
    }

    /**
     * 均线查询
     */
    @GetMapping("/averageLineType")
    public TableDataInfo averageLineType(StockUpStatistics stockUpStatistics) {
        List<StockUpStatistics> list = stockUpStatisticsService.averageLineType(stockUpStatistics);
        return getDataTable(list);
    }

    /**
     * 获取股票指标统计信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('eye:stockUpStatistics:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(stockUpStatisticsService.selectStockUpStatisticsById(id));
    }

    /**
     * 新增股票指标统计信息
     */
    @PreAuthorize("@ss.hasPermi('eye:stockUpStatistics:add')")
    @Log(title = "股票指标统计信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockUpStatistics stockUpStatistics) {
        return toAjax(stockUpStatisticsService.insertStockUpStatistics(stockUpStatistics));
    }

    /**
     * 修改股票指标统计信息
     */
    @PreAuthorize("@ss.hasPermi('eye:stockUpStatistics:edit')")
    @Log(title = "股票指标统计信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockUpStatistics stockUpStatistics) {
        return toAjax(stockUpStatisticsService.updateStockUpStatistics(stockUpStatistics));
    }

    /**
     * 删除股票指标统计信息
     */
    @PreAuthorize("@ss.hasPermi('eye:stockUpStatistics:remove')")
    @Log(title = "股票指标统计信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(stockUpStatisticsService.deleteStockUpStatisticsByIds(ids));
    }

    /**
     * 导出股票指标统计信息列表
     */
    @Log(title = "股票指标统计信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockUpStatistics stockUpStatistics) {
        List<StockUpStatistics> list = stockUpStatisticsService.selectStockUpStatisticsList(stockUpStatistics);
        ExcelUtil<StockUpStatistics> util = new ExcelUtil<StockUpStatistics>(StockUpStatistics.class);
        util.exportExcel(response, list, "股票指标统计信息数据");
    }

    /**
     * 下载导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        ExcelUtil<StockUpStatistics> util = new ExcelUtil<>(StockUpStatistics.class);
        util.importTemplateExcel(response, "股票模板");
    }

    /**
     * 导入股票指标统计信息
     */
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<StockUpStatistics> util = new ExcelUtil<>(StockUpStatistics.class);
        List<StockUpStatistics> exportList = util.importExcel(file.getInputStream());
        List<StockUpStatistics> updateList = new ArrayList<>();
        List<StockUpStatistics> addList = new ArrayList<>();
        List<StockUpStatistics> deleteList = new ArrayList<>();

        List<StockUpStatistics> dbList = stockUpStatisticsService.selectStockUpStatisticsList(new StockUpStatistics());

        // 导入的数据在数据库中存在股票编码则更新,不存在则新增
        exportList.forEach(n -> {
            // 确定唯一值的判定
            Optional<StockUpStatistics> optional = dbList.stream().filter(b -> n.getStockCode().equals(b.getStockCode())).findFirst();
            if (optional.isPresent()) {
                // 更新项
                optional.get().setStockName(n.getStockName());
                updateList.add(optional.get());
            } else {
                addList.add(n);
            }
        });
        // 查找待删除记录
        dbList.forEach(n -> {
            if (exportList.stream().noneMatch(b -> n.getStockCode().equals(b.getStockCode()))) {
                deleteList.add(n);
            }
        });
        if (addList.size() > 0) {
            stockUpStatisticsService.batchInsertStockUpStatistics(addList);
        }
        if (updateList.size() > 0) {
            stockUpStatisticsService.batchUpdateStockUpStatistics(updateList);
        }
        if (deleteList.size() > 0) {
            stockUpStatisticsService.deleteStockUpStatisticsByIds(deleteList.stream().map(StockUpStatistics::getId).toArray(String[]::new));
        }
        return AjaxResult.success();

    }

}

package com.psp.stockupstatistics.service;

import java.util.List;

import com.psp.stockupstatistics.domain.StockUpStatistics;

/**
 * 股票指标统计信息Service接口
 *
 * @author tiger
 * @date 2022-09-18
 */
public interface IStockUpStatisticsService {
    /**
     * 查询股票指标统计信息
     *
     * @param id 股票指标统计信息主键
     * @return 股票指标统计信息
     */
    StockUpStatistics selectStockUpStatisticsById(String id);

    /**
     * 查询股票指标统计信息列表
     *
     * @param stockUpStatistics 股票指标统计信息
     * @return 股票指标统计信息集合
     */
    List<StockUpStatistics> selectStockUpStatisticsList(StockUpStatistics stockUpStatistics);

    /**
     * 根据均线查询
     */
    List<StockUpStatistics> averageLineType(StockUpStatistics stockUpStatistics);

    /**
     * 新增股票指标统计信息
     *
     * @param stockUpStatistics 股票指标统计信息
     * @return 结果
     */
    int insertStockUpStatistics(StockUpStatistics stockUpStatistics);

    /**
     * 修改股票指标统计信息
     *
     * @param stockUpStatistics 股票指标统计信息
     * @return 结果
     */
    int updateStockUpStatistics(StockUpStatistics stockUpStatistics);

    /**
     * 批量删除股票指标统计信息
     *
     * @param ids 需要删除的股票指标统计信息主键集合
     * @return 结果
     */
    int deleteStockUpStatisticsByIds(String[] ids);

    /**
     * 删除股票指标统计信息信息
     *
     * @param id 股票指标统计信息主键
     * @return 结果
     */
    int deleteStockUpStatisticsById(String id);

    /**
     * 批量新增股票指标统计信息
     *
     * @param stockUpStatisticsList 股票指标统计信息
     */
    void batchInsertStockUpStatistics(List<StockUpStatistics> stockUpStatisticsList);

    /**
     * 批量更新股票指标统计信息
     *
     * @param stockUpStatisticsList 股票指标统计信息
     */
    void batchUpdateStockUpStatistics(List<StockUpStatistics> stockUpStatisticsList);

    /**
     * 清空数据
     */
    void clearData();
}

package com.psp.myattention.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.psp.myattention.domain.MyAttention;
import com.psp.myattention.service.IMyAttentionService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 我的关注Controller
 *
 * @author psp
 */
@RestController
@RequestMapping("/eye/myAttention")
public class MyAttentionController extends BaseController {
    @Autowired
    private IMyAttentionService myAttentionService;

    /**
     * 查询我的关注列表
     */
    @GetMapping("/list")
    public TableDataInfo list(MyAttention myAttention) {
        startPage();
        List<MyAttention> list = myAttentionService.selectMyAttentionList(myAttention);
        return getDataTable(list);
    }

    /**
     * 导出我的关注列表
     */
    @Log(title = "我的关注", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MyAttention myAttention) {
        List<MyAttention> list = myAttentionService.selectMyAttentionList(myAttention);
        ExcelUtil<MyAttention> util = new ExcelUtil<>(MyAttention.class);
        util.exportExcel(response, list, "我的关注数据");
    }

    /**
     * 获取我的关注详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(myAttentionService.selectMyAttentionById(id));
    }

    /**
     * 新增我的关注
     */
    @Log(title = "我的关注", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MyAttention myAttention) {
        return toAjax(myAttentionService.insertMyAttention(myAttention));
    }

    /**
     * 批量新增我的关注
     */
    @PostMapping("/batchInsertMyAttention")
    public AjaxResult batchInsertMyAttention(@RequestBody MyAttention myAttention) {
        myAttentionService.batchInsertMyAttention(myAttention);
        return AjaxResult.success("添加成功!");
    }

    /**
     * 修改我的关注
     */
    @Log(title = "我的关注", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MyAttention myAttention) {
        return toAjax(myAttentionService.updateMyAttention(myAttention));
    }

    /**
     * 删除我的关注
     */
    @Log(title = "我的关注", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(myAttentionService.deleteMyAttentionByIds(ids));
    }

    /**
     * 将股票添从默认分组中移除
     */
    @GetMapping("/deleteFromDefaultGroupByStockCode")
    public AjaxResult deleteFromDefaultGroupByStockCode(String stockCode, String groupId) {
        myAttentionService.deleteFromDefaultGroupByStockCode(stockCode, groupId);
        return AjaxResult.success("移除成功!");
    }

    /**
     * 批量修改股票分组
     */
    @PutMapping("/batchUpdateMyAttention")
    public AjaxResult batchUpdateMyAttention(@RequestBody List<MyAttention> myAttentionList) {
        myAttentionService.batchUpdateMyAttention(myAttentionList);
        return AjaxResult.success("拖动成功!");
    }

}

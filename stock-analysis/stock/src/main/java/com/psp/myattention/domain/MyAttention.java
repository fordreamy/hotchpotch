package com.psp.myattention.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 我的关注对象 stock_my_attention
 *
 * @author psp
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MyAttention extends BaseEntity {
    private static final Long serialVersionUID = 1L;

    /**
     *
     */
    private String id;

    /**
     * 排序
     */
    @Excel(name = "排序")
    private Integer sortNum;

    /**
     * 股票编码
     */
    @Excel(name = "股票编码")
    private String stockCode;

    private List<String> stockCodeList;

    /**
     * 股票名称
     */
    private String stockName;

    /**
     * 分组ID
     */
    @Excel(name = "分组ID")
    private String groupId;

    /**
     * 分组名称
     */
    @Excel(name = "分组名称")
    private String groupName;

    /**
     * 用户ID
     */
    @Excel(name = "用户ID")
    private String userId;

}

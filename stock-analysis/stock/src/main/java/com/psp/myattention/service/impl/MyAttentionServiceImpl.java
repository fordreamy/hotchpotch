package com.psp.myattention.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.stereotype.Service;
import com.psp.myattention.mapper.MyAttentionMapper;
import com.psp.myattention.domain.MyAttention;
import com.psp.myattention.service.IMyAttentionService;

import javax.annotation.Resource;

/**
 * 我的关注Service业务层处理
 *
 * @author psp
 */
@Service
public class MyAttentionServiceImpl implements IMyAttentionService {
    @Resource
    private MyAttentionMapper myAttentionMapper;

    /**
     * 查询我的关注
     *
     * @param id 我的关注主键
     * @return 我的关注
     */
    @Override
    public MyAttention selectMyAttentionById(String id) {
        return myAttentionMapper.selectMyAttentionById(id);
    }

    /**
     * 查询我的关注列表
     *
     * @param myAttention 我的关注
     * @return 我的关注
     */
    @Override
    public List<MyAttention> selectMyAttentionList(MyAttention myAttention) {
        return myAttentionMapper.selectMyAttentionList(myAttention);
    }

    /**
     * 新增我的关注
     *
     * @param myAttention 我的关注
     * @return 结果
     */
    @Override
    public int insertMyAttention(MyAttention myAttention) {
        myAttention.setId(IdUtils.fastSimpleUUID());
        Integer max = myAttentionMapper.selectMaxSortNum();
        if (max != null) {
            myAttention.setSortNum(max + 1);
        } else {
            myAttention.setSortNum(1);
        }
        return myAttentionMapper.insertMyAttention(myAttention);
    }

    @Override
    public void batchInsertMyAttention(MyAttention myAttention) {
        List<MyAttention> addList = new ArrayList<>();
        int max = myAttentionMapper.selectMaxSortNum() + 1;
        for (String s : myAttention.getStockCodeList()) {
            MyAttention add = new MyAttention();
            add.setId(IdUtils.fastSimpleUUID());
            add.setSortNum(max++);
            add.setGroupId(myAttention.getGroupId());
            add.setStockCode(s);
            addList.add(add);
        }
        myAttentionMapper.batchInsertMyAttention(addList);
    }

    /**
     * 修改我的关注
     *
     * @param myAttention 我的关注
     * @return 结果
     */
    @Override
    public int updateMyAttention(MyAttention myAttention) {
        return myAttentionMapper.updateMyAttention(myAttention);
    }

    /**
     * 批量删除我的关注
     *
     * @param ids 需要删除的我的关注主键
     * @return 结果
     */
    @Override
    public int deleteMyAttentionByIds(String[] ids) {
        return myAttentionMapper.deleteMyAttentionByIds(ids);
    }

    /**
     * 根据股票编码和分组ID删除
     *
     * @param stockCode 股票编码
     * @param groupId   分组ID
     */
    @Override
    public void deleteFromDefaultGroupByStockCode(String stockCode, String groupId) {
        myAttentionMapper.deleteFromDefaultGroupByStockCode(stockCode,groupId);
    }

    /**
     * 删除我的关注信息
     *
     * @param id 我的关注主键
     * @return 结果
     */
    @Override
    public int deleteMyAttentionById(String id) {
        return myAttentionMapper.deleteMyAttentionById(id);
    }

    @Override
    public void batchUpdateMyAttention(List<MyAttention> myAttentionList) {
        myAttentionMapper.batchUpdateMyAttention(myAttentionList);
    }
}

package com.psp.myattention.mapper;

import java.util.List;

import com.psp.myattention.domain.MyAttention;
import org.apache.ibatis.annotations.Param;

/**
 * 我的关注Mapper接口
 *
 * @author psp
 * @date 2022-09-05
 */
public interface MyAttentionMapper {
    /**
     * 查询我的关注
     *
     * @param id 我的关注主键
     * @return 我的关注
     */
    MyAttention selectMyAttentionById(String id);

    /**
     * 查询我的关注列表
     *
     * @param myAttention 我的关注
     * @return 我的关注集合
     */
    List<MyAttention> selectMyAttentionList(MyAttention myAttention);

    Integer selectMaxSortNum();

    /**
     * 新增我的关注
     *
     * @param myAttention 我的关注
     * @return 结果
     */
    int insertMyAttention(MyAttention myAttention);

    /**
     * 修改我的关注
     *
     * @param myAttention 我的关注
     * @return 结果
     */
    int updateMyAttention(MyAttention myAttention);

    /**
     * 删除我的关注
     *
     * @param id 我的关注主键
     * @return 结果
     */
    int deleteMyAttentionById(String id);

    /**
     * 批量删除我的关注
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deleteMyAttentionByIds(String[] ids);

    /**
     * 根据股票编码和分组ID删除
     *
     * @param stockCode 股票编码
     * @param groupId   分组ID
     */
    void deleteFromDefaultGroupByStockCode(@Param("stockCode") String stockCode, @Param("groupId") String groupId);

    /**
     * 批量更新我的关注信息
     */
    void batchUpdateMyAttention(List<MyAttention> myAttentionList);

    void batchInsertMyAttention(List<MyAttention> myAttentionList);
}

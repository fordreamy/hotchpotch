package com.psp.myattention.service;

import java.util.List;

import com.psp.myattention.domain.MyAttention;

/**
 * 我的关注Service接口
 *
 * @author psp
 */
public interface IMyAttentionService {
    /**
     * 查询我的关注
     *
     * @param id 我的关注主键
     * @return 我的关注
     */
    MyAttention selectMyAttentionById(String id);

    /**
     * 查询我的关注列表
     *
     * @param myAttention 我的关注
     * @return 我的关注集合
     */
    List<MyAttention> selectMyAttentionList(MyAttention myAttention);

    /**
     * 新增我的关注
     *
     * @param myAttention 我的关注
     * @return 结果
     */
    int insertMyAttention(MyAttention myAttention);

    void batchInsertMyAttention(MyAttention myAttention);

    /**
     * 修改我的关注
     *
     * @param myAttention 我的关注
     * @return 结果
     */
    int updateMyAttention(MyAttention myAttention);

    /**
     * 批量删除我的关注
     *
     * @param ids 需要删除的我的关注主键集合
     * @return 结果
     */
    int deleteMyAttentionByIds(String[] ids);

    /**
     * 根据股票编码和分组ID删除
     *
     * @param stockCode 股票编码
     * @param groupId   分组ID
     */
    void deleteFromDefaultGroupByStockCode(String stockCode, String groupId);

    /**
     * 删除我的关注信息
     *
     * @param id 我的关注主键
     * @return 结果
     */
    int deleteMyAttentionById(String id);

    /**
     * 批量更新我的关注信息
     */
    void batchUpdateMyAttention(List<MyAttention> myAttentionList);
}

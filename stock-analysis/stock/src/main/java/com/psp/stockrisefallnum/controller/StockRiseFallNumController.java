package com.psp.stockrisefallnum.controller;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.psp.stockrisefallnum.domain.StockRiseFallNum;
import com.psp.stockrisefallnum.service.IStockRiseFallNumService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 股票涨跌数Controller
 *
 * @author ruoyi
 * @date 2022-11-01
 */
@RestController
@RequestMapping("/eye/stockRiseFallNum")
public class StockRiseFallNumController extends BaseController {
    @Resource
    private IStockRiseFallNumService stockRiseFallNumService;

    /**
     * 查询股票涨跌数列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StockRiseFallNum stockRiseFallNum) {
        startPage();
        List<StockRiseFallNum> list;
        if (stockRiseFallNum.getTradeDate() != null) {
            stockRiseFallNum.setStartDate(stockRiseFallNum.getTradeDate());
            stockRiseFallNum.setEndDate(DateUtil.endOfMonth(stockRiseFallNum.getTradeDate()));
        }
        list = stockRiseFallNumService.selectStockRiseFallNumList(stockRiseFallNum);
        return getDataTable(list);
    }

    /**
     * 查询股票涨跌日历
     */
    @GetMapping("/listStockRiseCalendar")
    public AjaxResult listStockRiseCalendar(String startDate) {
        Date start = DateUtil.parseDate(startDate);
        Date end = DateUtil.endOfMonth(start);
        List<StockRiseFallNum> list = stockRiseFallNumService.listStockRiseCalendar(start, end);
        return AjaxResult.success(list);
    }

    /**
     * 获取股票涨跌数详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(stockRiseFallNumService.selectStockRiseFallNumById(id));
    }

    /**
     * 新增股票涨跌数
     */
    @Log(title = "股票涨跌数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockRiseFallNum stockRiseFallNum) {
        return toAjax(stockRiseFallNumService.insertStockRiseFallNum(stockRiseFallNum));
    }

    /**
     * 修改股票涨跌数
     */
    @Log(title = "股票涨跌数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockRiseFallNum stockRiseFallNum) {
        return toAjax(stockRiseFallNumService.updateStockRiseFallNum(stockRiseFallNum));
    }

    /**
     * 删除股票涨跌数
     */
    @Log(title = "股票涨跌数", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(stockRiseFallNumService.deleteStockRiseFallNumByIds(ids));
    }

    /**
     * 获取股票涨跌数详细信息
     */
    @GetMapping(value = "/getMarketTrendData")
    public AjaxResult getMarketTrendData() {
        return AjaxResult.success(stockRiseFallNumService.getMarketTrendData());
    }

}

package com.psp.stockrisefallnum.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 股票涨跌数对象 stock_rise_fall_num
 */
public class StockRiseFallNum extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 股票code
     */
    @Excel(name = "股票code")
    private String stockCode;

    /**
     * 交易日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "交易日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date tradeDate;

    private Date startDate;

    private Date endDate;

    /**
     * 成交额
     */
    @Excel(name = "成交额")
    private Integer turnover;

    /**
     * 上证深证净流入(单位亿)
     */
    @Excel(name = "上证深证净流入(单位亿)")
    private Double shSzNetInflow;

    /**
     * 沪深股通净流入(单位亿)
     */
    @Excel(name = "沪深股通净流入(单位亿)")
    private Double hkNetInflow;

    /**
     * 上涨数
     */
    @Excel(name = "上涨数")
    private Integer riseNum;

    /**
     * 下跌数
     */
    @Excel(name = "下跌数")
    private Integer fallNum;

    /**
     * 涨停数
     */
    @Excel(name = "涨停数")
    private Integer riseStopNum;

    /**
     * 跌停数
     */
    @Excel(name = "跌停数")
    private Integer fallStopNum;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setTradeDate(Date tradeDate) {
        this.tradeDate = tradeDate;
    }

    public Date getTradeDate() {
        return tradeDate;
    }

    public void setRiseNum(Integer riseNum) {
        this.riseNum = riseNum;
    }

    public Integer getRiseNum() {
        return riseNum;
    }

    public void setFallNum(Integer fallNum) {
        this.fallNum = fallNum;
    }

    public Integer getFallNum() {
        return fallNum;
    }

    public void setRiseStopNum(Integer riseStopNum) {
        this.riseStopNum = riseStopNum;
    }

    public Integer getRiseStopNum() {
        return riseStopNum;
    }

    public void setFallStopNum(Integer fallStopNum) {
        this.fallStopNum = fallStopNum;
    }

    public Integer getFallStopNum() {
        return fallStopNum;
    }

    public Double getShSzNetInflow() {
        return shSzNetInflow;
    }

    public void setShSzNetInflow(Double shSzNetInflow) {
        this.shSzNetInflow = shSzNetInflow;
    }

    public Double getHkNetInflow() {
        return hkNetInflow;
    }

    public void setHkNetInflow(Double hkNetInflow) {
        this.hkNetInflow = hkNetInflow;
    }

    public Integer getTurnover() {
        return turnover;
    }

    public void setTurnover(Integer turnover) {
        this.turnover = turnover;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("stockCode", getStockCode())
                .append("tradeDate", getTradeDate())
                .append("riseNum", getRiseNum())
                .append("fallNum", getFallNum())
                .append("riseStopNum", getRiseStopNum())
                .append("fallStopNum", getFallStopNum())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}

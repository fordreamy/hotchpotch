package com.psp.stockrisefallnum.mapper;

import java.util.Date;
import java.util.List;

import com.psp.stockrisefallnum.domain.StockRiseFallNum;
import org.apache.ibatis.annotations.Param;

/**
 * 股票涨跌数Mapper接口
 *
 * @author ruoyi
 */
public interface StockRiseFallNumMapper {
    /**
     * 查询股票涨跌数
     *
     * @param id 股票涨跌数主键
     * @return 股票涨跌数
     */
    public StockRiseFallNum selectStockRiseFallNumById(String id);

    /**
     * 查询股票涨跌数列表
     *
     * @param stockRiseFallNum 股票涨跌数
     * @return 股票涨跌数集合
     */
    List<StockRiseFallNum> selectStockRiseFallNumList(StockRiseFallNum stockRiseFallNum);

    /**
     * 查询股票涨跌日历
     *
     * @param startDate 开始日期
     * @param endDate   结束日期
     * @return 股票涨跌日历
     */
    List<StockRiseFallNum> listStockRiseCalendar(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    /**
     * 新增股票涨跌数
     *
     * @param stockRiseFallNum 股票涨跌数
     * @return 结果
     */
    int insertStockRiseFallNum(StockRiseFallNum stockRiseFallNum);

    /**
     * 修改股票涨跌数
     *
     * @param stockRiseFallNum 股票涨跌数
     * @return 结果
     */
    int updateStockRiseFallNum(StockRiseFallNum stockRiseFallNum);

    /**
     * 删除股票涨跌数
     *
     * @param id 股票涨跌数主键
     * @return 结果
     */
    int deleteStockRiseFallNumById(String id);

    /**
     * 根据交易日期删除股票涨跌数信息
     *
     * @param tradeDate 交易日期
     */
    void deleteStockRiseFallNumByTradeDate(Date tradeDate);

    /**
     * 批量删除股票涨跌数
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deleteStockRiseFallNumByIds(String[] ids);

    /**
     * 批量新增股票涨跌数
     *
     * @param stockRiseFallNumList 股票涨跌数
     */
    void batchInsertStockRiseFallNum(List<StockRiseFallNum> stockRiseFallNumList);

    /**
     * 批量更新股票涨跌数
     *
     * @param stockRiseFallNumList 股票涨跌数
     */
    void batchUpdateStockRiseFallNum(List<StockRiseFallNum> stockRiseFallNumList);

}

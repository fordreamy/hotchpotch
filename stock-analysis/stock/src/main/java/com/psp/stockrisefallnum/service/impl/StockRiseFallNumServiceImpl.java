package com.psp.stockrisefallnum.service.impl;

import java.util.*;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.stereotype.Service;
import com.psp.stockrisefallnum.mapper.StockRiseFallNumMapper;
import com.psp.stockrisefallnum.domain.StockRiseFallNum;
import com.psp.stockrisefallnum.service.IStockRiseFallNumService;

import javax.annotation.Resource;

/**
 * 股票涨跌数Service业务层处理
 *
 * @author ruoyi
 */
@Service
public class StockRiseFallNumServiceImpl implements IStockRiseFallNumService {
    @Resource
    private StockRiseFallNumMapper stockRiseFallNumMapper;

    /**
     * 查询股票涨跌数
     *
     * @param id 股票涨跌数主键
     * @return 股票涨跌数
     */
    @Override
    public StockRiseFallNum selectStockRiseFallNumById(String id) {
        return stockRiseFallNumMapper.selectStockRiseFallNumById(id);
    }

    /**
     * 查询股票涨跌数列表
     *
     * @param stockRiseFallNum 股票涨跌数
     * @return 股票涨跌数
     */
    @Override
    public List<StockRiseFallNum> selectStockRiseFallNumList(StockRiseFallNum stockRiseFallNum) {
        return stockRiseFallNumMapper.selectStockRiseFallNumList(stockRiseFallNum);
    }

    @Override
    public List<StockRiseFallNum> listStockRiseCalendar(Date startDate, Date endDate) {
        return stockRiseFallNumMapper.listStockRiseCalendar(startDate, endDate);
    }

    /**
     * 新增股票涨跌数
     *
     * @param stockRiseFallNum 股票涨跌数
     * @return 结果
     */
    @Override
    public int insertStockRiseFallNum(StockRiseFallNum stockRiseFallNum) {
        stockRiseFallNum.setId(IdUtils.fastSimpleUUID());
        stockRiseFallNum.setCreateTime(DateUtils.getNowDate());
        return stockRiseFallNumMapper.insertStockRiseFallNum(stockRiseFallNum);
    }

    /**
     * 修改股票涨跌数
     *
     * @param stockRiseFallNum 股票涨跌数
     * @return 结果
     */
    @Override
    public int updateStockRiseFallNum(StockRiseFallNum stockRiseFallNum) {
        stockRiseFallNum.setUpdateTime(DateUtils.getNowDate());
        return stockRiseFallNumMapper.updateStockRiseFallNum(stockRiseFallNum);
    }

    /**
     * 批量删除股票涨跌数
     *
     * @param ids 需要删除的股票涨跌数主键
     * @return 结果
     */
    @Override
    public int deleteStockRiseFallNumByIds(String[] ids) {
        return stockRiseFallNumMapper.deleteStockRiseFallNumByIds(ids);
    }

    /**
     * 删除股票涨跌数信息
     *
     * @param id 股票涨跌数主键
     * @return 结果
     */
    @Override
    public int deleteStockRiseFallNumById(String id) {
        return stockRiseFallNumMapper.deleteStockRiseFallNumById(id);
    }

    /**
     * 根据交易日期删除股票涨跌数信息
     *
     * @param tradeDate 交易日期
     */
    @Override
    public void deleteStockRiseFallNumByTradeDate(Date tradeDate) {
        stockRiseFallNumMapper.deleteStockRiseFallNumByTradeDate(tradeDate);
    }

    @Override
    public List<Map<String, Object>> getMarketTrendData() {
        List<StockRiseFallNum> list = stockRiseFallNumMapper.selectStockRiseFallNumList(new StockRiseFallNum());
        Collections.reverse(list);
        List<Map<String, Object>> result = new ArrayList<>();
        String[] dateList = list.stream().map(obj -> DateUtil.format(obj.getTradeDate(), "yyyy-MM-dd")).toArray(String[]::new);

        Map<String, Object> riseMap = new HashMap<>();
        riseMap.put("id", "A" + RandomUtil.randomString(5));
        riseMap.put("title", "上涨个数");
        riseMap.put("color", "#409eff");
        riseMap.put("dateList", dateList);
        riseMap.put("valueList", list.stream().map(StockRiseFallNum::getRiseNum).toArray(Integer[]::new));
        result.add(riseMap);

        riseMap = new HashMap<>();
        riseMap.put("id", "A" + RandomUtil.randomString(5));
        riseMap.put("title", "成交额");
        riseMap.put("color", "#1b6f0b");
        riseMap.put("dateList", dateList);
        riseMap.put("valueList", list.stream().map(StockRiseFallNum::getTurnover).toArray(Integer[]::new));
        result.add(riseMap);

        riseMap = new HashMap<>();
        riseMap.put("id", "A" + RandomUtil.randomString(5));
        riseMap.put("title", "上证深证净流入");
        riseMap.put("color", "#409eff");
        riseMap.put("dateList", dateList);
        riseMap.put("valueList", list.stream().map(StockRiseFallNum::getShSzNetInflow).toArray(Double[]::new));
        result.add(riseMap);

        riseMap = new HashMap<>();
        riseMap.put("id", "A" + RandomUtil.randomString(5));
        riseMap.put("title", "沪深股通净流入");
        riseMap.put("color", "#409eff");
        riseMap.put("dateList", dateList);
        riseMap.put("valueList", list.stream().map(StockRiseFallNum::getHkNetInflow).toArray(Double[]::new));
        result.add(riseMap);

        riseMap = new HashMap<>();
        riseMap.put("id", "A" + RandomUtil.randomString(5));
        riseMap.put("title", "涨停个数");
        riseMap.put("color", "#ff0000");
        riseMap.put("dateList", dateList);
        riseMap.put("valueList", list.stream().map(StockRiseFallNum::getRiseStopNum).toArray(Integer[]::new));
        result.add(riseMap);

        riseMap = new HashMap<>();
        riseMap.put("id", "A" + RandomUtil.randomString(5));
        riseMap.put("title", "跌停个数");
        riseMap.put("color", "#1b6f0b");
        riseMap.put("dateList", dateList);
        riseMap.put("valueList", list.stream().map(StockRiseFallNum::getFallStopNum).toArray(Integer[]::new));
        result.add(riseMap);

        return result;
    }

}

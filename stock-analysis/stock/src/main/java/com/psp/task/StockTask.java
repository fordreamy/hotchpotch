package com.psp.task;

import com.psp.task.service.IStockTaskService;
import com.ruoyi.common.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 定时任务调度测试
 *
 * @author ruoyi
 */
@Component("stockTask")
public class StockTask {

    protected static final Logger logger = LoggerFactory.getLogger(StockTask.class);

    @Resource
    private IStockTaskService stockTaskService;

    /**
     * API----通过百度获取历史股价
     */
    public void getStockHistoryPriceByBaiDu() {
        try {
            stockTaskService.getStockHistoryPriceByBaiDu();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("任务执行失败!");
        }
    }

    /**
     * API----通过百度获取ETF历史股价
     */
    public void getETFHistoryPriceByBaiDu() {
        try {
            stockTaskService.getETFHistoryPriceByBaiDu();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("任务执行失败!");
        }
    }

    /**
     * API----通过Sina获取实时股价
     */
    public void getStockTodayPriceBySina() {
        stockTaskService.getStockTodayPriceBySina();
    }

    /**
     * 更新stock表的最新收盘价和涨跌幅
     */
    public void updateStockTodayPriceAndRice() {
        try {
            stockTaskService.updateStockTodayPriceAndRice();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("任务执行失败!");
        }
    }

    /**
     * 更新stock_rise_fall_num表当天内的股票涨跌数量
     */
    public void statisticsStockRiseNum() {
        try {
            stockTaskService.statisticsStockRiseNum();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("任务执行失败!");
        }
    }

    /**
     * 更新stock_up_statistics一段时间内的股票波动,涨跌天数、涨跌幅度、5日均线等
     */
    public void statisticsStockPriceOverAPeriodOfTime(Integer statisticDays) {
        try {
            stockTaskService.statisticsStockPriceOverAPeriodOfTime(statisticDays);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("任务执行失败!");
        }
    }

    /**
     * 检查接口是否可用
     */
    public void checkApiIsOk() {
        try {
            stockTaskService.checkApiIsOk();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("任务执行失败!");
        }
    }

    /**
     * 备份历史价格表stock_price到表stock_price_backup
     */
    public void backupStockHistoryPrice() {
        stockTaskService.backupStockHistoryPrice();
    }

}

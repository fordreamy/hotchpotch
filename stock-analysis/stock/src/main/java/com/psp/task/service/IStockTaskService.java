package com.psp.task.service;

/**
 * @author tiger
 */
public interface IStockTaskService {

    /**
     * 通过百度获取历史股价
     */
    void getStockHistoryPriceByBaiDu();

    /**
     * 通过百度获取ETF历史股价
     */
    void getETFHistoryPriceByBaiDu();

    /**
     * 通过Sina获取实时股价
     */
    void getStockTodayPriceBySina();

    /**
     * 更新股票的最新价格和涨跌幅
     */
    void updateStockTodayPriceAndRice();

    /**
     * 统计一段时间内的股票波动
     */
    void statisticsStockPriceOverAPeriodOfTime(Integer statisticDays);

    /**
     * 统计当天内的股票涨跌数量
     */
    void statisticsStockRiseNum();

    /**
     * 检查接口是否可用
     */
    void checkApiIsOk();

    /**
     * 备份历史价格数据
     */
    void backupStockHistoryPrice();
}

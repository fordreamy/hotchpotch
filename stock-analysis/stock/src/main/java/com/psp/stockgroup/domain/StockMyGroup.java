package com.psp.stockgroup.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 股票分组对象 stock_my_group
 *
 * @author tiger
 */
public class StockMyGroup extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    @Excel(name = "id")
    private String id;

    /**
     * 股票分组名称
     */
    @Excel(name = "股票分组名称")
    private String name;

    /**
     * 样式
     */
    @Excel(name = "样式")
    private String classStyle;

    /**
     * 分组序号
     */
    @Excel(name = "分组序号")
    private Integer sortNum;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSortNum(Integer sortNum) {
        this.sortNum = sortNum;
    }

    public Integer getSortNum() {
        return sortNum;
    }

    public String getClassStyle() {
        return classStyle;
    }

    public void setClassStyle(String classStyle) {
        this.classStyle = classStyle;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("sortNum", getSortNum())
                .toString();
    }
}

package com.psp.stockgroup.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import com.psp.stockgroup.mapper.StockMyGroupMapper;
import com.psp.stockgroup.domain.StockMyGroup;
import com.psp.stockgroup.service.IStockMyGroupService;

import javax.annotation.Resource;

/**
 * 股票分组Service业务层处理
 *
 * @author tiger
 * @date 2022-09-24
 */
@Service
public class StockMyGroupServiceImpl implements IStockMyGroupService {
    @Resource
    private StockMyGroupMapper stockMyGroupMapper;

    /**
     * 查询股票分组
     *
     * @param id 股票分组主键
     * @return 股票分组
     */
    @Override
    public StockMyGroup selectStockMyGroupById(String id) {
        return stockMyGroupMapper.selectStockMyGroupById(id);
    }

    /**
     * 查询股票分组列表
     *
     * @param stockMyGroup 股票分组
     * @return 股票分组
     */
    @Override
    public List<StockMyGroup> selectStockMyGroupList(StockMyGroup stockMyGroup) {
        return stockMyGroupMapper.selectStockMyGroupList(stockMyGroup);
    }

    /**
     * 查询股票分组下拉框
     */
    @Override
    public List<Map<String, Object>> selectStockMyGroupOption() {
        return stockMyGroupMapper.selectStockMyGroupOption();
    }

    /**
     * 新增股票分组
     *
     * @param stockMyGroup 股票分组
     * @return 结果
     */
    @Override
    public int insertStockMyGroup(StockMyGroup stockMyGroup) {
        stockMyGroup.setId(IdUtils.fastSimpleUUID());
        Integer max = stockMyGroupMapper.selectMaxSortNum();
        if (max != null) {
            stockMyGroup.setSortNum(max + 1);
        } else {
            stockMyGroup.setSortNum(1);
        }
        return stockMyGroupMapper.insertStockMyGroup(stockMyGroup);
    }

    /**
     * 修改股票分组
     *
     * @param stockMyGroup 股票分组
     * @return 结果
     */
    @Override
    public int updateStockMyGroup(StockMyGroup stockMyGroup) {
        return stockMyGroupMapper.updateStockMyGroup(stockMyGroup);
    }

    /**
     * 批量删除股票分组
     *
     * @param ids 需要删除的股票分组主键
     * @return 结果
     */
    @Override
    public int deleteStockMyGroupByIds(String[] ids) {
        return stockMyGroupMapper.deleteStockMyGroupByIds(ids);
    }

    /**
     * 删除股票分组信息
     *
     * @param id 股票分组主键
     * @return 结果
     */
    @Override
    public int deleteStockMyGroupById(String id) {
        return stockMyGroupMapper.deleteStockMyGroupById(id);
    }

    /**
     * 批量新增股票分组
     *
     * @param stockMyGroupList 股票分组
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchInsertStockMyGroup(List<StockMyGroup> stockMyGroupList) {
        stockMyGroupList.forEach(n -> {
            n.setId(IdUtils.fastSimpleUUID());
            n.setCreateTime(DateUtils.getNowDate());
            n.setCreateBy(SecurityUtils.getUsername());
        });
        stockMyGroupMapper.batchInsertStockMyGroup(stockMyGroupList);
    }

    /**
     * 批量更新股票分组
     *
     * @param stockMyGroupList 股票分组
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchUpdateStockMyGroup(List<StockMyGroup> stockMyGroupList) {
        stockMyGroupList.forEach(n -> {
            n.setUpdateTime(DateUtils.getNowDate());
            n.setUpdateBy(SecurityUtils.getUsername());
        });
        stockMyGroupMapper.batchUpdateStockMyGroup(stockMyGroupList);
    }
}

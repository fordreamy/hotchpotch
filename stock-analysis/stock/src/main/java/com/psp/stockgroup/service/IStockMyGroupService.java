package com.psp.stockgroup.service;

import java.util.List;
import java.util.Map;

import com.psp.stockgroup.domain.StockMyGroup;

/**
 * 股票分组Service接口
 *
 * @author tiger
 * @date 2022-09-24
 */
public interface IStockMyGroupService {
    /**
     * 查询股票分组
     *
     * @param id 股票分组主键
     * @return 股票分组
     */
    StockMyGroup selectStockMyGroupById(String id);

    /**
     * 查询股票分组列表
     *
     * @param stockMyGroup 股票分组
     * @return 股票分组集合
     */
    List<StockMyGroup> selectStockMyGroupList(StockMyGroup stockMyGroup);

    /**
     * 查询股票分组下拉框
     */
    List<Map<String, Object>> selectStockMyGroupOption();

    /**
     * 新增股票分组
     *
     * @param stockMyGroup 股票分组
     * @return 结果
     */
    int insertStockMyGroup(StockMyGroup stockMyGroup);

    /**
     * 修改股票分组
     *
     * @param stockMyGroup 股票分组
     * @return 结果
     */
    int updateStockMyGroup(StockMyGroup stockMyGroup);

    /**
     * 批量删除股票分组
     *
     * @param ids 需要删除的股票分组主键集合
     * @return 结果
     */
    int deleteStockMyGroupByIds(String[] ids);

    /**
     * 删除股票分组信息
     *
     * @param id 股票分组主键
     * @return 结果
     */
    int deleteStockMyGroupById(String id);

    /**
     * 批量新增股票分组
     *
     * @param stockMyGroupList 股票分组
     */
    void batchInsertStockMyGroup(List<StockMyGroup> stockMyGroupList);

    /**
     * 批量更新股票分组
     *
     * @param stockMyGroupList 股票分组
     */
    void batchUpdateStockMyGroup(List<StockMyGroup> stockMyGroupList);
}

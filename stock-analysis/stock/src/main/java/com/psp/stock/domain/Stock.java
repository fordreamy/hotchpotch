package com.psp.stock.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 股票基础信息对象 stock
 *
 * @author psp
 * @date 2022-09-02
 */
public class Stock extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private String id;

    /**
     * 股票code
     */
    @Excel(name = "股票code")
    private String stockCode;

    /**
     * 股票名称
     */
    @Excel(name = "股票名称")
    private String stockName;

    /**
     * 上市日期
     */
    @Excel(name = "上市日期")
    private String listingDate;

    /**
     * 股票类型
     */
    @Excel(name = "股票类型")
    private String stockType;

    /**
     * 总股本
     */
    @Excel(name = "总股本")
    private String marketCapital;

    /**
     * 流通股
     */
    @Excel(name = "流通股")
    private String negotiableCapital;

    /**
     * 所属行业
     */
    @Excel(name = "所属行业")
    private String industry;

    /**
     * 所属交易所
     */
    @Excel(name = "所属交易所")
    private String stockExchange;

    /**
     * 公司网址
     */
    @Excel(name = "公司网址")
    private String website;

    /**
     * 当前价
     */
    private Double currentPrice;

    /**
     * 涨跌幅
     */
    private Double priceRiseRange;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockName() {
        return stockName;
    }

    public void setListingDate(String listingDate) {
        this.listingDate = listingDate;
    }

    public String getListingDate() {
        return listingDate;
    }

    public void setStockType(String stockType) {
        this.stockType = stockType;
    }

    public String getStockType() {
        return stockType;
    }

    public void setMarketCapital(String marketCapital) {
        this.marketCapital = marketCapital;
    }

    public String getMarketCapital() {
        return marketCapital;
    }

    public void setNegotiableCapital(String negotiableCapital) {
        this.negotiableCapital = negotiableCapital;
    }

    public String getNegotiableCapital() {
        return negotiableCapital;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getIndustry() {
        return industry;
    }

    public void setStockExchange(String stockExchange) {
        this.stockExchange = stockExchange;
    }

    public String getStockExchange() {
        return stockExchange;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getWebsite() {
        return website;
    }

    public void setCurrentPrice(Double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Double getCurrentPrice() {
        return currentPrice;
    }

    public void setPriceRiseRange(Double priceRiseRange) {
        this.priceRiseRange = priceRiseRange;
    }

    public Double getPriceRiseRange() {
        return priceRiseRange;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("stockCode", getStockCode())
                .append("stockName", getStockName())
                .append("listingDate", getListingDate())
                .append("stockType", getStockType())
                .append("marketCapital", getMarketCapital())
                .append("negotiableCapital", getNegotiableCapital())
                .append("industry", getIndustry())
                .append("stockExchange", getStockExchange())
                .append("website", getWebsite())
                .append("currentPrice", getCurrentPrice())
                .append("priceRiseRange", getPriceRiseRange())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}

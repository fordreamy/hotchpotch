package com.psp.stock.mapper;

import java.util.List;

import com.psp.stock.domain.Stock;

/**
 * 股票基础信息Mapper接口
 *
 * @author psp
 */
public interface StockMapper {
    /**
     * 查询股票基础信息
     *
     * @param id 股票基础信息主键
     * @return 股票基础信息
     */
    Stock selectStockById(String id);

    /**
     * 查询股票个数
     *
     * @return 股票个数
     */
    int selectStockCount();

    /**
     * 查询股票基础信息列表
     *
     * @param stock 股票基础信息
     * @return 股票基础信息集合
     */
    List<Stock> selectStockList(Stock stock);

    /**
     * 查询股票基础信息列表,不包含股票型基金
     */
    List<Stock> selectStockListExcludeFund();

    /**
     * 新增股票基础信息
     *
     * @param stock 股票基础信息
     * @return 结果
     */
    int insertStock(Stock stock);

    /**
     * 批量导入股票基础信息
     */
    int batchInsertStock(List<Stock> stockList);

    /**
     * 修改股票基础信息
     *
     * @param stock 股票基础信息
     * @return 结果
     */
    int updateStock(Stock stock);

    /**
     * 批量更新股票信息
     */
    void batchUpdateStock(List<Stock> updateList);

    /**
     * 删除股票基础信息
     *
     * @param id 股票基础信息主键
     * @return 结果
     */
    int deleteStockById(String id);

    /**
     * 批量删除股票基础信息
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deleteStockByIds(String[] ids);

    /**
     * 清空股票数据,不包含基金
     */
    void clearStockData();
}

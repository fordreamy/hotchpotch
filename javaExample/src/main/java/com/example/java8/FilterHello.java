package com.example.java8;

/**
 * 函数式接口定义且只定义了一个抽象方法。
 *
 * @param <T>
 */
public interface FilterHello<T> {
    boolean test(T apple);
}

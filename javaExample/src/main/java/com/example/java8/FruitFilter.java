package com.example.java8;

import com.alibaba.fastjson2.JSON;
import com.example.vo.StudentVO;

import java.util.ArrayList;
import java.util.List;

public class FruitFilter {
    public static boolean ageGreat(StudentVO apple) {
        return apple.getAge() > 20;
    }

    public static boolean ageLess(StudentVO apple) {
        return apple.getAge() < 20;
    }

    public static List<StudentVO> filterApples(List<StudentVO> inventory,
                                               FilterHello<StudentVO> p) {
        List<StudentVO> result = new ArrayList<>();
        for (StudentVO student : inventory) {
            if (p.test(student)) {
                result.add(student);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        String init = "[" +
                "{\"id\":\"1\",\"age\":25,\"birthDay\":'2021-01-05 12:34:23',\"name\":\"李白\",\"score\":88.5}," +
                "{\"id\":\"2\",\"age\":21,\"birthDay\":'2020-01-01 12:34:23',\"name\":\"苏轼\",\"score\":90}," +
                "{\"id\":\"3\",\"age\":22,\"birthDay\":'2022-12-12 12:34:23',\"name\":\"杜甫\",\"score\":90.5}," +
                "]";

        List<StudentVO> list = JSON.parseArray(init, StudentVO.class);
        System.out.println(JSON.toJSONString(FruitFilter.filterApples(list, FruitFilter::ageGreat)));
        System.out.println(JSON.toJSONString(FruitFilter.filterApples(list, FruitFilter::ageLess)));

        System.out.println(FruitFilter.filterApples(list, (StudentVO s) -> s.getAge() > 20));
    }
}

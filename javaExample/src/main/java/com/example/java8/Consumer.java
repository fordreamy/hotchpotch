package com.example.java8;

import java.util.List;
import java.util.function.IntToDoubleFunction;

@FunctionalInterface
public interface Consumer<T> {
    void accept(T t);

    public static <T> void forEach(List<T> list, Consumer<T> c) {
        IntToDoubleFunction s;
        for (T i : list) {
            c.accept(i);
        }
    }
}

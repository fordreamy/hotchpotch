package com.example;

import sun.misc.DoubleConsts;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

class Car {
    private String model;
    private int year;

    public Car(String model, int year) {
        this.model = model;
        this.year = year;
    }

}

public class Test {
    public static void main(String[] args) {
       /* Car car1 = new Car("a", 2023);
        System.out.println(car1);
        System.out.println(car1.hashCode());
        Car car2 = new Car("a", 2023);
        System.out.println(car2);
        System.out.println(car2.hashCode());
        System.out.println(car1.equals(car2));*/
        Integer n = 100;
        System.out.println(n.hashCode());
        String s1 = "1";
        System.out.println(s1.hashCode());
        System.out.println((int)('1'));
        System.out.println("A".hashCode());
        System.out.println((int)('A'));
        // 创建一个哈希表
        HashMap<Integer, String> phoneBook = new HashMap<>();

        // 添加联系人及电话号码
        phoneBook.put(1, "123456789");
        phoneBook.put(2, "987654321");
        phoneBook.put(3, "456123789");

        // 根据姓名查找电话号码
        String phoneNumber = phoneBook.get(1);
        System.out.println("Bob的电话号码是：" + phoneNumber);
    }

}

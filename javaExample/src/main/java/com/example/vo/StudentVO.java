package com.example.vo;

import com.alibaba.fastjson2.JSON;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class StudentVO {
    String id;
    String name;
    int age;
    Double score;

    String sex;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Timestamp birthDay;

    public StudentVO(String id, String name, int age, Double score, Timestamp birthDay) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.score = score;
        this.birthDay = birthDay;
    }

    public StudentVO() {
    }

    public static List<StudentVO> init() {
        String init = "[" +
                "{\"id\":\"1\",\"age\":25,\"birthDay\":'2021-01-05 12:34:23',\"name\":\"李白\",\"score\":88.5}," +
                "{\"id\":\"2\",\"age\":21,\"birthDay\":'2020-01-01 12:34:23',\"name\":\"苏轼\",\"score\":90}," +
                "{\"id\":\"3\",\"age\":22,\"birthDay\":'2022-12-12 12:34:23',\"name\":\"杜甫\",\"score\":90.5}," +
                "]";

        return JSON.parseArray(init, StudentVO.class);
    }

    public static List<StudentVO> init2() {
        String init = "[" +
                "{\"id\":\"1\",\"age\":20,\"birthDay\":'2017-01-05 12:34:23',\"name\":\"李白\",\"score\":88.5}," +
                "{\"id\":\"2\",\"age\":21,\"birthDay\":'2017-01-05 12:34:23',\"name\":\"苏轼\",\"score\":90}," +
                "]";

        return JSON.parseArray(init, StudentVO.class);
    }

    public static List<StudentVO> initRepeatData() {
        String init = "[" +
                "{\"id\":\"1\",\"age\":20,\"birthDay\":'2017-01-05 12:34:23',\"name\":\"李白\",\"score\":88.5}," +
                "{\"id\":\"1\",\"age\":20,\"birthDay\":'2021-01-05 12:34:23',\"name\":\"苏轼\",\"score\":90}," +
                "{\"id\":\"1\",\"age\":20,\"birthDay\":'2021-01-05 12:34:23',\"name\":\"王维\",\"score\":95}," +
                "{\"id\":\"1\",\"age\":30,\"birthDay\":'2017-01-05 12:34:23',\"name\":\"杜甫\",\"score\":90.5}," +
                "{\"id\":\"1\",\"age\":30,\"birthDay\":'2017-01-05 12:34:23',\"name\":\"张三\",\"score\":100}," +
                "]";

        return JSON.parseArray(init, StudentVO.class);
    }
}

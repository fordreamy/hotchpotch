package com.example.utils.excel;

import java.io.*;

import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.*;

public class PoiUtils {

    public static void main(String[] args) {
        XSSFWorkbook excel = new XSSFWorkbook();
//创建第一个sheet
        XSSFSheet sheet = excel.createSheet("我的POI之旅");
//创建第一行
        XSSFRow row = sheet.createRow((short) 0);
//创建第一个单元格
        for (int i = 0; i < 500; i++) {
            XSSFCell cell = row.createCell(i);
//设置单元格的值
            cell.setCellValue("Ay");
//生成单元格样式
            XSSFCellStyle cellStyle = excel.createCellStyle();

            DefaultIndexedColorMap defaultIndexedColorMap = new DefaultIndexedColorMap();
            XSSFColor color = new XSSFColor(defaultIndexedColorMap);
            color.setRGB(new byte[]{(byte) 0, (byte) 117, (byte) 194});
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            cellStyle.setFillForegroundColor(color);

            cell.setCellStyle(cellStyle);
        }


//通过流写到硬盘

        FileOutputStream out = null;
        try {
            out = new FileOutputStream("./old_color.xlsx");
            excel.write(out);

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

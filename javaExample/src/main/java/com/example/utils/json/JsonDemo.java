package com.example.utils.json;

public class JsonDemo {
    public static void main(String[] args) {
        System.out.println("---------------JSON字符串转Bean----------------");
        JsonFunction.jsonToBean();
        System.out.println("---------------JSON字符串转List<Bean>----------------");
        JsonFunction.jsonToListBean();
        System.out.println("---------------JSON字符串转Map----------------");
        JsonFunction.jsonToMap();
        System.out.println("---------------JSON字符串转List<Map>----------------");
        JsonFunction.jsonToListMap();
    }
}

package com.example.utils.db;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 数据库工具类
 *
 */
public class DBConnection {
    public static Connection conn = null;
    public static DataSource ds = null;

    /**
     * 获取数据库连接
     *
     */
    public static Connection getConnection(String driverClassName, String url, String username, String password) {
        if (conn != null) {
            return conn;
        }
        try {
            Class.forName(driverClassName);
            conn = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * 关闭数据库连接
     *
     */
    public static void closeCon(Connection con)  {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}

package com.example.utils.date;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;

import java.util.Date;

/**
 * @author tiger
 */
public class HuToolDateDemo {
    public static void main(String[] args) {
        Date date = new Date();
//        Date date = DateUtil.parse("2022-10-10 12:12:12");
        System.out.println("时间:" + date);
        String dateStr = "2022-10-10";
        String dateTimeStr = "2022-10-10 12:12:12";

        System.out.println("---------------日期类转字符串----------------");
        System.out.println(DateUtil.format(date, "yyyy-MM-dd HH:mm:ss:SSS"));

        System.out.println("---------------字符串转日期类----------------");
        //DateUtil.parse方法会自动识别一些常用格式，包括：yyyy-MM-dd yyyy-MM-dd HH:mm:ss
        System.out.println(DateUtil.parse(dateStr));
        System.out.println(DateUtil.parse(dateTimeStr, "yyyy-MM-dd HH:mm:ss"));

        System.out.println("---------------当天零点到午夜----------------");
        System.out.println(DateUtil.beginOfDay(date) + "--" + DateUtil.endOfDay(date));

        System.out.println("---------------获取Date对象的某个部分----------------");
        System.out.println("年:" + DateUtil.year(date) + ",月:" + DateUtil.month(date) + ",日:" + DateUtil.dayOfMonth(date));
        System.out.println("月的最后一天:" + DateUtil.endOfMonth(date));

        System.out.println("---------------日期时间偏移----------------");
        System.out.println("偏移两天:" + DateUtil.offset(date, DateField.DAY_OF_MONTH, 2));

        System.out.println("---------------是否同一天----------------");
        System.out.println(DateUtil.isSameDay(new Date(), new Date()));

    }
}

package com.example.utils.date;

import cn.hutool.core.date.LocalDateTimeUtil;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tiger
 */
public class LocalDateDemo {
    public static void main(String[] args) {
        LocalDate now = LocalDate.now();
        LocalDate date = LocalDate.parse("2022-12-31");
        LocalDateTime time = LocalDateTime.now();
        System.out.println("现在日期:" + now);
        System.out.println("现在时间:" + LocalDateTime.now());

        System.out.println("---------------Date转LocalDate----------------");
        System.out.println(new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

        System.out.println("---------------字符串转日期----------------");
        System.out.println(LocalDate.parse("2022-12-31"));

        System.out.println("---------------日期格式化----------------");
        System.out.println(LocalDateTimeUtil.format(now, "yyyy年MM月dd日"));

        System.out.println("---------------日期加n天----------------");
        date = LocalDate.parse("2022-12-31");
        System.out.println(date.plusDays(1));

        System.out.println("---------------是否同一天----------------");
        date = LocalDate.parse("2022-12-14");
        int sameDay = now.compareTo(date);
        if (sameDay == 0) {
            System.out.println("同一天:" + now + " " + date);
        } else {
            System.out.println("非同一天:" + now + " " + date);
        }

        System.out.println("---------------时间比较(左-右)----------------");
        System.out.println("相等为0:" + LocalTime.parse("00:01:01").compareTo(LocalTime.parse("00:01:01")));
        System.out.println("大于为1:" + LocalTime.parse("00:01:02").compareTo(LocalTime.parse("00:01:01")));
        System.out.println("小于为-1:" + LocalTime.parse("00:01:01").compareTo(LocalTime.parse("00:01:59")));

        System.out.println("---------------相差分钟(右-左)----------------");
        System.out.println("相差分钟:" + Duration.between(LocalTime.parse("00:01:01"), LocalTime.parse("00:01:01")).toMinutes());
        System.out.println("相差分钟:" + Duration.between(LocalTime.parse("00:01:01"), LocalTime.parse("00:02:01")).toMinutes());
        System.out.println("相差分钟:" + Duration.between(LocalTime.parse("00:01:01"), LocalTime.parse("00:02:35")).toMinutes());
        System.out.println("相差分钟:" + Duration.between(LocalTime.parse("23:58:00"), LocalTime.parse("23:59:59")).toMinutes());
        System.out.println("相差天数:" + Duration.between(LocalDateTime.parse("2022-12-15T12:00:00"), LocalDateTime.parse("2022-12-16T08:00:00")).toDays());

        System.out.println("---------------相差秒----------------");
        System.out.println(ChronoUnit.SECONDS.between(LocalTime.parse("00:01:01"), LocalTime.parse("00:01:31")));
        System.out.println(Duration.between(LocalTime.parse("00:01:01"), LocalTime.parse("00:01:21")).toMillis() / 1000);

        System.out.println("---------------相差天数----------------");
        System.out.println("相差天数:" + Period.between(LocalDate.parse("2022-12-30"), LocalDate.parse("2022-12-31")).getDays());

        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-'AB'");

        String formattedDateTime = dateTime.format(formatter);
        System.out.println("Formatted DateTime: " + formattedDateTime);

        System.out.println(LocalDateTimeUtil.format(now, "YYYY-M-dd-'AB'"));
        System.out.println(LocalDateTimeUtil.format(now, "YY-M-dd-'AB'"));
        System.out.println(LocalDateTimeUtil.format(now, "yy-M-d-'AB'"));

    }
}

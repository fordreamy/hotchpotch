package com.example.utils.date;

import java.util.Date;

/**
 * @author tiger
 */
public class DateDemo {
    public static void main(String[] args) {
        System.out.println("---------------日期转字符串----------------");
        System.out.println(DateFunction.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss:SSS"));
        System.out.println("---------------字符串转日期----------------");
        System.out.println(DateFunction.dateToString(DateFunction.stringToDate("2022-03-20"), "yyyy-MM-dd HH:mm:ss:SSS"));
        System.out.println("---------------自定义日期----------------");
        System.out.println(DateFunction.dateToString(DateFunction.setDate(2022, 2, 22, 2, 2, 1, 1), "yyyy-MM-dd HH:mm:ss:SSS"));
        System.out.println("---------------是否同一天----------------");
        System.out.println(DateFunction.isSameDay(new Date(), new Date()));
        System.out.println("---------------N天后日期除去周末----------------");
        System.out.println(DateFunction.dateToString(DateFunction.addDaysExceptWeekend(new Date(), 5), "yyyy-MM-dd"));

    }
}

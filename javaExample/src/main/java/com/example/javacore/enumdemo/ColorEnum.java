package com.example.javacore.enumdemo;

/**
 * @author tiger
 */
public enum ColorEnum {
    red,
    orange,
    yellow,
    green,
    blue,
    purple;

    private int value = 0;

    private ColorEnum(int value) {
        this.value = value;
    }

    ColorEnum() {

    }

    public int value() {
        return this.value;
    }
}



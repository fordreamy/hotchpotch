package com.example.javacore.enumdemo;

/**
 * @author tiger
 */
public class EnumDemo {

    public static void main(String[] args) {
        System.out.println(ColorEnum.valueOf("red").name());
        System.out.println(ColorEnum.blue.ordinal());
    }

}

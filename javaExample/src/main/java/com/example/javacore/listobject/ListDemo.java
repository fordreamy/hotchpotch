package com.example.javacore.listobject;

/**
 * @author tiger
 */
public class ListDemo {

    public static void main(String[] args) {

        System.out.println("---------------------去重--List元素去重----------------------------------------");
        ListUtil.removeRepeat();

        System.out.println("---------------------去重--List某个字段转为数组并去重-----------------------------");
        ListUtil.getFieldAndRemoveDuplicate();

        System.out.println("---------------------聚合(max/min/count)-------------------------------------");
        ListUtil.aggregate();

        System.out.println("---------------------遍历/匹配(foreach/find/match)-----------------------------");
        System.out.println("---------------------List元素过滤----------------------------------------------");
        ListUtil.filterByField();

        System.out.println("---------------------查找--list集合中是否存在某个值--------------------------------");
        ListUtil.existValue();

        System.out.println("---------------------查找--查询匹配的第一条记录------------------------------------------");
        ListUtil.findFirstSameRecord();

        System.out.println("---------------------查找--查询匹配的所有记录----------------------------------------------");
        ListUtil.findAllSameRecord();

        System.out.println("---------------------查找--匹配后获取某元素并转换为List数组----------------------------------------------");
        ListUtil.findAndConvert();

        System.out.println("---------------------查找--所有年龄小于90--------------------------------------------");
        ListUtil.allMatch();

        System.out.println("---------------------不存在年龄大于100的记录-------------------------------------");
        ListUtil.noneMatch();

        System.out.println("---------------------List某个字段转为数组---------------------------------------");
        ListUtil.toArray();

        System.out.println("---------------------两个集合是否存在相同元素-------------------------------------");
        ListUtil.existEquals();

        System.out.println("---------------------集合A中某属性值和集合B中属性值相等的记录------------------------");
        ListUtil.listEqualsRecord();

        System.out.println("---------------------集合A中属性不在集合B中记录------------------------");
        ListUtil.listOneNotInListTwoRecord();

        System.out.println("---------------------两个集合存在相同的元素的任意一条记录----------------------------");
        ListUtil.equalsRecord();

        System.out.println("---------------------排序--单字段排序--------------------------------------------");
        ListUtil.sortByOneFiled();

        System.out.println("---------------------排序--多字段排序--------------------------------------------");
        ListUtil.sortByManyFiled();

        System.out.println("---------------------分组--多字段排序--------------------------------------------");
        ListUtil.groupBySex();

        System.out.println("---------------------求和--条件过滤--------------------------------------------");
        ListUtil.sumByFilter();

    }
}

package com.example.javacore.convert;

import java.time.LocalDate;
import java.util.*;

/**
 * 初始化数据
 *
 * @author tiger
 */
public class InitConvertData {
    public static List<Map<String, Object>> list = new ArrayList<>();

    public static Map<String, Object> initMap() {
        Map<String, Object> item = new HashMap<>();
        item.put("id", "mapID");
        item.put("name", "语文");
        item.put("age", 90L);
        item.put("score", 90L);
        item.put("birthday", "2000-01-01");
        item.put("enrollmentDate", "2000-01-01");
        return item;
    }

    public static List<Map<String, Object>> initListMap() {
        Map<String, Object> item = new HashMap<>();
        item.put("id", "mapID1");
        item.put("name", "李白");
        item.put("age", "20");
        item.put("score", "20.2");
        item.put("birthday", "2000-01-01");
        item.put("enrollmentDate", "2000-01-01");
        list.add(item);
        item = new HashMap<>();
        item.put("id", "mapID2");
        item.put("name", "王五");
        item.put("age", 20);
        item.put("score", "21.22");
        item.put("birthday", "2000-01-01");
        item.put("enrollmentDate", "2000-01-01");
        list.add(item);
        return list;
    }

    public static List<ConvertUser> initUserList() {
        List<ConvertUser> userList = new ArrayList<>();
        ConvertUser user = new ConvertUser();
        user.setId("1");
        user.setName("李白");
        user.setAge(20);
        user.setBirthday(new Date());
        user.setEnrollmentDate(LocalDate.now());
        userList.add(user);
        user = new ConvertUser();
        user.setId("2");
        user.setName("张三");
        user.setAge(20);
        user.setBirthday(new Date());
        user.setEnrollmentDate(LocalDate.now());
        userList.add(user);
        return userList;
    }

    public static ConvertUser initUser() {
        ConvertUser user = new ConvertUser();
        user.setId("1");
        user.setName("李白");
        user.setAge(20);
        user.setBirthday(new Date());
        user.setEnrollmentDate(LocalDate.now());
        return user;
    }
}

package com.example.javacore.convert;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

/**
 * @author tiger
 */
@Data
public class ConvertUser {
    private String id;
    private String name;
    private Integer age;
    private BigDecimal score;
    private Date birthday;
    // 入学时间
    private LocalDate enrollmentDate;

}

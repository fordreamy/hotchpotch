package com.example.javacore.convert;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 对象转换
 *
 * @author tiger
 */
public class ConvertDemo {
    public static void main(String[] args) {
        List<ConvertUser> userList;
        List<Map<String, Object>> listMap;
        Map<String, Object> map;

        System.out.println("---------------------Map转对象(通过FastJSON)-------------------------------------");
        map = InitConvertData.initMap();
        JSONObject jsonObject = new JSONObject();
        jsonObject.putAll(map);
        ConvertUser user = jsonObject.toJavaObject(ConvertUser.class);
        System.out.println(user);
        user = JSON.parseObject(JSON.toJSONString(map), ConvertUser.class);
        System.out.println(user);

        System.out.println("---------------------Map转对象(通过HuTool)-------------------------------------");
        map = InitConvertData.initMap();
        ConvertUser person = BeanUtil.toBean(map, ConvertUser.class);
        System.out.println(person);

        // 设置别名，用于对应bean的字段名,将map的字段name转为bean的nick字段
        HashMap<String, String> mapping = new HashMap<>();
        mapping.put("name", "nick");
        ConvertUser person2 = BeanUtil.toBean(map, ConvertUser.class, CopyOptions.create().setFieldMapping(mapping));
        System.out.println(person2);

        System.out.println("---------------------listMap转List<对象>(通过FastJSON)-------------------------------------");
        listMap = InitConvertData.initListMap();
        List<ConvertUser> toList = JSON.parseArray(JSON.toJSONString(listMap), ConvertUser.class);
        System.out.println(toList);

        System.out.println("---------------------listMap转List<对象>(通过HuTool)-------------------------------------");
        userList = BeanUtil.copyToList(listMap, ConvertUser.class);
        System.out.println(userList);

        System.out.println("---------------------List转为Map主键-对象-------------------------------------");
        userList = InitConvertData.initUserList();
        Map<String, ConvertUser> userMap = userList.stream().collect(Collectors.toMap(ConvertUser::getId, (p) -> p));
        System.out.println(userMap);
        Map<String, ConvertUser> userIdMap = Optional.of(userList).orElse(new ArrayList<>())
                .stream().collect(Collectors.toMap(ConvertUser::getId, Function.identity()));
        System.out.println(userIdMap);

        System.out.println("---------------------List转为ListMap(通过FastJSON)-------------------------------------");
        userList = InitConvertData.initUserList();
        List<Map> listMap1 = JSON.parseArray(JSON.toJSONString(userList), Map.class);
        System.out.println(listMap1);

        System.out.println("---------------------List转为ListMap(通过HuTool)-------------------------------------");
        userList = InitConvertData.initUserList();
        List<Map> listMap2 = BeanUtil.copyToList(userList, Map.class);
        System.out.println(listMap2);

        System.out.println("---------------------Bean转Map(通过HuTool)-------------------------------------");
        user = InitConvertData.initUser();
        map = BeanUtil.beanToMap(person);
        System.out.println(map);

        System.out.println("---------------------Bean转Bean(通过HuTool)-------------------------------------");
        user = InitConvertData.initUser();
        ConvertUser user2 = new ConvertUser();
        //两个Bean互转
        BeanUtil.copyProperties(user, user2);
        System.out.println(user2);
        //Bean转Map
        map = new HashMap<>();
        BeanUtil.copyProperties(user, map);
        System.out.println(map);
        //Map转Bean
        map = InitConvertData.initMap();
        user = new ConvertUser();
        BeanUtil.copyProperties(map, user);
        System.out.println(user);
        System.out.println(BeanUtil.toBean(map, ConvertUser.class));

    }
}

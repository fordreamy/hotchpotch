package com.example.javacore.listtree;

import cn.hutool.core.date.DateUtil;

import java.util.Date;
import java.util.List;

/**
 * @author tiger
 */
public class AreaDemo {
    public static void main(String[] args) {

        Date start;
        List<Area> allList = Area.initRandomTree(5, 30000);

        System.out.println("----------------------默认树----------------------");
        System.out.println(Area.initArea());

        System.out.println("----------------------递归生成树结构----------------------");
        start = new Date();
        List<Area> result = Area.generateTreeStructure(allList);
        System.out.println("递归生成树结构用时毫秒:" + DateUtil.betweenMs(start, new Date()));
        System.out.println("------------输出结果集------------");
//        Area.output(result);

        System.out.println("----------------------循环生成树结构----------------------");
        /*start = new Date();
        List<Area> result2 = Area.loopTraversal(allList);
        System.out.println("循环遍历用时毫秒:" + DateUtil.betweenMs(start, new Date()));
        System.out.println("------------输出结果集------------");
        Area.output(result2);*/
    }
}

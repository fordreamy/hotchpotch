# 1 安装nginx

## 1.1  docker中安装

启动:  `docker run --name my-nginx -d -p 8080:80 nginx:1.17.0`

根据配置文件启动

```sh
# 普通方式启动后进入容器获取配置文件,docker cp拷贝出来
docker run --name my-nginx -p 8080:80 
           -v /etc/nginx/nginx.conf:/etc/nginx/nginx.conf:ro -v /etc/nginx/conf.d:/etc/nginx/conf.d 
           -d nginx:1.17.0
```

访问:  `http://localhost:8080` or `http://host-ip:8080`

## 1.2 ubuntu中安装

安装必要包

```shell
sudo apt install curl gnupg2 ca-certificates lsb-release
```

为稳定的nginx包设置apt存储库

```shell
echo "deb http://nginx.org/packages/ubuntu lsb_release -cs nginx" | sudo tee /etc/apt/sources.list.d/nginx.list
```

导入官方nginx签名密钥以便apt验证包的真实性

```shell
curl -fsSL https://nginx.org/keys/nginx_signing.key | sudo apt-key add -
sudo apt-key fingerprint ABF5BD827BD9BF62
# 输出包含573B FD6B 3D8F BC64 1079 A6AB ABF5 BD82 7BD9 BF62
```

安装

```shell
sudo apt update
sudo apt install nginx
```

源码安装

ubuntu16.04

```shell
# 1.安装gcc g++的依赖库
apt-get install build-essential
apt-get install libtool
# 2.安装pcre依赖库
sudo apt-get update
sudo apt-get install libpcre3 libpcre3-dev
# 3.安装zlib依赖库
apt-get install zlib1g-dev
# 4.安装ssl依赖库
apt-get install openssl
# 5.下载
wget http://nginx.org/download/nginx-1.16.0.tar.gz
# 6. 安装
cd nginx-1.16.0
./configure --prefix=/usr/local/nginx 
make && make install
```

ubuntu16源码安装

下载文件并tar解压

```shell
wget http://nginx.org/download/nginx-1.13.2.tar.gz
wget https://www.openssl.org/source/openssl-1.0.2h.tar.gz
wget http://zlib.net/zlib-1.2.11.tar.gz
wget https://ftp.pcre.org/pub/pcre/pcre-8.43.tar.gz
```

编译

```shell
cd nginx-1.13.2
./configure  --sbin-path=/usr/local/nginx/nginx --conf-path=/usr/local/nginx/nginx.conf --pid-path=/usr/local/nginx/nginx.pid --with-http_ssl_module --with-pcre=/opt/nginx/pcre-8.43 --with-zlib=/opt/nginx/zlib-1.2.11 --with-openssl=/opt/nginx/openssl-1.0.2h
make && make install
```

FAQ

```shell
# 安装编译环境
apt-get install build-essential
apt-get install make
apt-get install gcc
```

## 1.3 centos7安装

文档：[nginx: Linux packages](http://nginx.org/en/linux_packages.html#RHEL)

在线安装:

Install the prerequisites:

```shell
sudo yum install yum-utils
```

To set up the yum repository, create the file named `/etc/yum.repos.d/nginx.repo` with the following contents:

```ini
[nginx-stable]
name=nginx stable repo
baseurl=http://nginx.org/packages/amzn2/$releasever/$basearch/
gpgcheck=1
enabled=1
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true

[nginx-mainline]
name=nginx mainline repo
baseurl=http://nginx.org/packages/mainline/amzn2/$releasever/$basearch/
gpgcheck=1
enabled=0
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true
```

To install nginx, run the following command:

```shell
sudo yum install nginx
```

源码安装:

```shell
yum install -y  gcc gcc-c++ make pcre pcre-devel zlib zlib-devel openssl openssl-devel
下载并解压 wget http://nginx.org/download/nginx-1.16.0.tar.gz 
./configure --prefix=/usr/local/nginx-1.16.0 --with-http_stub_status_module --with-http_ssl_module --with-pcre
make
make install
```

# 2 常用命令

## 2.1 查看版本

```shell
# 查看nginx的版本号
nginx -v
nginx -V 加配置参数
```

## 2.2 查看帮助

```shell
# 查看nginx的帮助
nginx -h
```

## 2.3 启动停止

**<mark>Windows</mark>**

```batch
# 进入nginx目录
# 启动无窗口
C:\server\nginx-1.0.2>start nginx

# 启动有窗口,关闭窗口即关闭nginx
C:\server\nginx-1.0.2>nginx.exe

如果需要特殊设置nginx的配置文件路径，可以nginx -c conf/nginx.conf


# 停止
taskkill /f /t /im nginx.exe

# 停止
C:\server\nginx-1.0.2>nginx.exe -s stop

# 停止

C:\server\nginx-1.0.2>nginx.exe -s quit


# 重新载入Nginx

C:\server\nginx-1.0.2>nginx.exe -s reload

# 检查80端口是否被占用的命令： 
netstat -ano | findstr 0.0.0.0:80 或 netstat -ano | findstr "80"
```

<mark>**Linux**</mark>

```shell
# 进入nginx目录
# 启动无窗口
nginx

如果需要特殊设置nginx的配置文件路径，可以nginx -c conf/nginx.conf

# 停止
nginx -s stop
nginx -s quit
taskkill /f /t /im nginx.exe

# 重新载入Nginx配置
nginx -s reload
```

## 2.4 测试配置文件

```shell
# 验证默认配置文件
nginx -t 
# 验证自定义配置文件
nginx -t -c conf/nginx.conf
```

# 3 配置

## 3.1 权重配置

```nginx
#user  nobody;

worker_processes  1;

#error_log  logs/error.log; 设置日志输出位置和文件名
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid; 设置pid存储位置

events {
    #单个worker对应的并发连接数，但不等于用户连接数，客户端有n个请求，则worker_connections要有2n+2个
    worker_connections  1024; 
}

http {
    include       mime.types; # 导入文件
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    #gzip  on;

    server {
        listen       80;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
            root   html;
            index  index.html index.htm;
        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }

    # another virtual host using mix of IP-, name-, and port-based configuration
    #
    #server {
    #    listen       8000;
    #    listen       somename:8080;
    #    server_name  somename  alias  another.alias;

    #    location / {
    #        root   html;
    #        index  index.html index.htm;
    #    }
    #}


    # HTTPS server
    #

    upstream myapphttps{
        server 10.10.10.131:443;
    }

    server {
        listen       443 ssl;
        server_name  localhost;

        ssl_certificate      /opt/tmp/example_com.crt; #服务器证书路径
        ssl_certificate_key  /opt/tmp/example_com.key; #私钥路径

        ssl_session_cache    shared:SSL:1m;
        ssl_session_timeout  5m;
        #    ssl_ciphers  HIGH:!aNULL:!MD5;
        #    ssl_prefer_server_ciphers  on;

        location / {
            proxy_pass https://myapphttps;
        }
    }

     upstream myapp1 {
        server 172.17.0.2 weight=1;
        server 172.17.0.3 weitht=3;
    }

    server {
        listen 8081;
        location / {
            proxy_pass http://myapp1;
        }
    }
}

stream{
    upstream amqp{
        server 10.10.10.130:5672 weight=2;
        server 10.10.10.131:5672 weight=1;
    }
    server{
        listen 5670;
        proxy_pass amqp;
    }

    upstream mqtt{
        server 10.10.10.130:1883 weight=2;
        server 10.10.10.131:1883 weight=1;
    }
    server{
        listen 1800;
        proxy_pass mqtt;
    }
}
```

## 3.2 Vue多前端一后端配置

前端有多个文件夹,访问后端接口有统一的前缀prod-api

#### 3.2.1 Windows配置

```nginx
server {
    listen       80;
    server_name  localhost;

    #charset koi8-r;

    #access_log  logs/host.access.log  main;

    location / {
        root   D:/InstalledSoftware/Development/nginx/html/sso;
        index  index.html index.htm;
        try_files $uri $uri/ /index.html;
    }

   location /sys/ {
      root   D:/InstalledSoftware/Development/nginx/html;
      index  index.html index.htm;
      try_files $uri $uri/ /sys/index.html;
   }

    location /prod-api/ {
         proxy_pass  http://192.168.110.224:8001/;
         proxy_set_header Host $host:$server_port;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header REMOTE-HOST $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   html;
    }
}
```

#### 3.2.2 Linux配置

```nginx
server {
    listen       80;
    server_name  localhost;

    #access_log  /var/log/nginx/host.access.log  main;

    client_max_body_size  200M;

    gzip on;
    gzip_disable "msie6";
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain application/css text/css application/xml text/javascript application/javascript application/x-javascript;

    location / {
        root   /home/nginx/mom_all_html/pc;
        index  index.html index.htm;
        try_files $uri $uri/ /index.html;
    }

    location /momApp/ {
        root   /home/nginx/mom_all_html;
        index  index.html index.htm;
        try_files $uri $uri/ /momApp/index.html;
    }
    
    location /prod-api/ {
        proxy_pass http://192.168.110.224:9001/;
        proxy_set_header Host $host:$server_port;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header REMOTE-HOST $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location /onlinePreviewServer/ {
        proxy_pass http://192.168.110.224:7001;
        proxy_set_header Host $host:$server_port;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}


server {
    listen       90;
    server_name  localhost;

    #access_log  /var/log/nginx/host.access.log  main;

    client_max_body_size  200M;

    gzip on;
    gzip_disable "msie6";
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain application/css text/css application/xml text/javascript application/javascript application/x-javascript;

    location / {
        root   /home/nginx/casic/pc;
        index  index.html index.htm;
        try_files $uri $uri/ /index.html;
    }

    location /momApp/ {
        root   /home/nginx/casic;
        index  index.html index.htm;
        try_files $uri $uri/ /momApp/index.html;
    }
    
    location /prod-api/ {
        proxy_pass http://192.168.110.224:9901/;
        proxy_set_header Host $host:$server_port;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header REMOTE-HOST $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location /onlinePreviewServer/ {
        proxy_pass http://192.168.110.224:7001;
        proxy_set_header Host $host:$server_port;
    }


    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}


server {
    listen       81;
    server_name  localhost;

    client_max_body_size  200M;

    #access_log  /var/log/nginx/host.access.log  main;

    location / {
        root   /home/nginx/html/sso;
        index  index.html index.htm;
        try_files $uri $uri/ /index.html;
    }
    
    location /sys/ {
        root   /home/nginx/html/;
        index  index.html index.htm;
        try_files $uri $uri/ /sys/index.html;
    }

    location /mes/ {
        root   /home/nginx/html;
        index  index.html index.htm;
        try_files $uri $uri/ /mes/index.html;
    }

    location /mesReport/ {
        root   /home/nginx/html;
        index  index.html index.htm;
        try_files $uri $uri/ /mesReport/index.html;
    }

    location /mesReportApp/ {
        root   /home/nginx/html;
        index  index.html index.htm;
        try_files $uri $uri/ /mesReportApp/index.html;
    }

    location /eam/ {
        root   /home/nginx/html;
        index  index.html index.htm;
        try_files $uri $uri/ /eam/index.html;
    }    

    location /qms/ {
        root   /home/nginx/html;
        index  index.html index.htm;
        try_files $uri $uri/ /qms/index.html;
    }

    location /aps/ {
        root   /home/nginx/html;
        index  index.html index.htm;
        try_files $uri $uri/ /aps/index.html;
    }

    location /uip/ {
        root   /home/nginx/html;
        index  index.html index.htm;
        try_files $uri $uri/ /uip/index.html;
    }

    location /bigScreen/ {
        root   /home/nginx/html;
        index  index.html index.htm;
        try_files $uri $uri/ /bigScreen/index.html;
    }

    location /mesProcess/ {
        root   /home/nginx/html;
        index  index.html index.htm;
        try_files $uri $uri/ /mesProcess/index.html;
    }

    location /prod-api/ {
        proxy_pass http://192.168.110.222:8201/;
        proxy_set_header Host $host:$server_port;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header REMOTE-HOST $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    # proxy the PHP scripts to Apache listening on 127.0.0.1:80
    #
    #location ~ \.php$ {
    #    proxy_pass   http://127.0.0.1;
    #}

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    #location ~ \.php$ {
    #    root           html;
    #    fastcgi_pass   127.0.0.1:9000;
    #    fastcgi_index  index.php;
    #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
    #    include        fastcgi_params;
    #}

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    #location ~ /\.ht {
    #    deny  all;
    #}
}
```

# 4 Nginx高可用

使用keepalived实现nginx高可用,安装如下:

```shell
wget https://www.keepalived.org/software/keepalived-2.0.16.tar.gz
tar -zxvf keepalived-2.0.16
cd keepalived-2.0.16
./configure --prefix=/usr/local/server/keepalived
make && make install
```

# 5 配置GZIP

将js和css压缩成gzip进行传输，以加快页面访问

```nginx
server {
    listen       80;
    server_name  localhost;

    gzip on;
    gzip_disable "msie6";
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain application/css text/css application/xml text/javascript application/javascript application/x-javascript;

    location / {
        root   /home/nginx/mom_all_html;
        index  index.html index.htm;
        try_files $uri $uri/ /index.html;
    }

    location /prod-api/ {
        proxy_pass http://192.168.110.224:9001/;
        proxy_set_header Host $host:$server_port;
    }
}
```

# @ FAQ

## @1

## @2

## @3

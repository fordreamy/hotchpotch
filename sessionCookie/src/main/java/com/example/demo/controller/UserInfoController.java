package com.example.demo.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.stat.DruidStatManagerFacade;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;


/**
 * @author tiger
 */
@Controller
@RequestMapping("/user")
public class UserInfoController {

    Log log = LogFactory.getLog(getClass());

    @Resource
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/index")
    public String index(@RequestParam("userName") String userName, @RequestParam("password") String password) {
        System.out.println(userName + password);

        return "index";
    }

    @GetMapping("/list")
    @ResponseBody
    public List<Map<String, Object>> list(HttpServletRequest request) {
        System.out.println("----获取请求cookies----");
        Cookie[] cookies = request.getCookies();
        for (Cookie c : cookies) {
            System.out.println("name[" + c.getName() + "] value[" + c.getValue() + "]");
            System.out.println(c.getPath());
        }
        String sql = "select * from user";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        log.info(list);
        log.info(jdbcTemplate.queryForMap("select count(*) from user"));
        return list;
    }

    @GetMapping("/add")
    @ResponseBody
    public String add() {
        String sql = "insert into user (name, addr) values ('张三', '山东')";
        jdbcTemplate.update(sql);
        return "ok";
    }

    @GetMapping("/druid/stat")
    @ResponseBody
    public Object druidStat() {
        // DruidStatManagerFacade#getDataSourceStatDataList 该方法可以获取所有数据源的监控数据，除此之外 DruidStatManagerFacade 还提供了一些其他方法，你可以按需选择使用。
        return DruidStatManagerFacade.getInstance().getDataSourceStatDataList();
    }

    public static void main(String[] args) {
    }

}

package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tiger
 */
@Controller
@RequestMapping("/s")
public class SessionController {

    @GetMapping("/getSession")
    @ResponseBody
    public Map<String, Object> getSession(HttpServletRequest request, HttpServletResponse response) {

        //使用request对象的getSession()获取session，如果session不存在则创建一个
        HttpSession session = request.getSession();
        //将数据存储到session中
        session.setAttribute("data", "孤傲苍狼");
        //获取session的Id
        String sessionId = session.getId();
        //判断session是不是新创建的
        if (session.isNew()) {
            //如果客户端还不知道,或者如果客户端选择不加入会话。例如如果服务器仅使用基于cookie的会话，
            // 而客户端已禁用如果使用cookie，那么每次请求都会有新的会话。
            System.out.println("session创建成功，session的id是：" + sessionId);
        } else {
            System.out.println("服务器已经存在该session了，session的id是：" + sessionId);
        }

        Map<String, Object> map = new HashMap<>(20);
        map.put("sessionId", sessionId);

        System.out.println("----默认返回参数----");
        System.out.println("CharacterEncoding->" + response.getCharacterEncoding());
        System.out.println("ContentType->" + response.getContentType());
        System.out.println("----设置返回参数----");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        System.out.println("CharacterEncoding->" + response.getCharacterEncoding());
        System.out.println("ContentType->" + response.getContentType());

        return map;
    }

    @GetMapping("/destroySession")
    @ResponseBody
    public Map<String, Object> destroySession(HttpServletRequest request, HttpServletResponse response) {

        //使用request对象的getSession()获取session，如果session不存在则创建一个
        HttpSession session = request.getSession();

        session.invalidate();

        Map<String, Object> map = new HashMap<>(20);
        map.put("sessionId", session.getId());
        return map;
    }

    @GetMapping("/getCookie")
    @ResponseBody
    public Map<String, Object> getCookie(HttpServletRequest request, HttpServletResponse response) {

        System.out.println("----获取请求cookies----");
        Map<String, Object> map = new HashMap<>(20);
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                System.out.println("name[" + c.getName() + "] value[" + c.getValue() + "]");
                map.put(c.getName(), c.getValue());
            }
        }
        return map;
    }

    @GetMapping("/setCookie")
    @ResponseBody
    public Map<String, Object> setCookie(HttpServletRequest request, HttpServletResponse response) {

        System.out.println("----设置cookies----");
        Map<String, Object> map = new HashMap<>(20);
        Cookie c = new Cookie("random", "10秒后消失：" + Math.random());
        //指定cookie存活时间,单位秒
        c.setMaxAge(10);
        map.put(c.getName(), c.getValue());
        response.addCookie(c);
        response.addCookie(new Cookie("name", "花非花"));
        map.put("name", "花非花");

        return map;
    }

}

package com.example.demo.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 需要添加ServletComponentScan注解才能访问,标记为servlet，以便启动器扫描。
 *
 * @author tiger
 */
@WebServlet(name = "firstServlet", urlPatterns = "/firstServlet")
public class ServletController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.getWriter().append("firstServlet");
    }

}




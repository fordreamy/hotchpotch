package com.example.demo.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * DruidConfig
 *
 * @author tiger
 */

@Configuration
public class DruidConfig {

    /**
     * Configuring DruidDataSource
     *
     * @return new DruidDataSource()
     */
    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource dataSource() {
        return new DruidDataSource();
    }

    /**
     * Configuring Druid Monitoring -- StatViewServlet
     *
     * @return new ServletRegistrationBean<T extends Servlet>(T servlet, String... urlMappings)
     */
    @Bean
    public ServletRegistrationBean<StatViewServlet> servletRegistrationBean() {
        ServletRegistrationBean<StatViewServlet> statViewServletServletRegistrationBean = new ServletRegistrationBean<>(new StatViewServlet(), "/druid/*");
        Map<String, String> initParameters = new HashMap<>(16);
        initParameters.put("loginUsername", "admin");
        initParameters.put("loginPassword", "1");
        initParameters.put("allow", "");
        initParameters.put("deny", "");
        statViewServletServletRegistrationBean.setInitParameters(initParameters);
        return statViewServletServletRegistrationBean;
    }

    /**
     * Configuring Druid Monitoring -- StatViewFilter
     *
     * @return filterFilterRegistrationBean
     */
    @Bean
    public FilterRegistrationBean<WebStatFilter> filterRegistrationBean() {
        FilterRegistrationBean<WebStatFilter> filterFilterRegistrationBean = new FilterRegistrationBean<>();
        filterFilterRegistrationBean.setFilter(new WebStatFilter());
        Map<String, String> initParameters = new HashMap<>(16);
        initParameters.put("exclusions", "*.js,*.gif,*.jpg, *.css, /druid/*");
        filterFilterRegistrationBean.setInitParameters(initParameters);
        filterFilterRegistrationBean.setUrlPatterns(Arrays.asList("/*", "/*"));
        return filterFilterRegistrationBean;
    }

}

import {
	getLatestVersion
} from "@/api/eye/appUpdate"
import config from '@/config'
const baseUrl = config.baseUrl
export default function() {
	// #ifdef APP-PLUS
	return new Promise((resolve, reject) => {
		plus.runtime.getProperty(plus.runtime.appid, function(widgetInfo) {
			const data = {
				action: 'checkVersion',
				appid: plus.runtime.appid,
				appVersion: plus.runtime.version,
				wgtVersion: widgetInfo.version,
				wgtVersionCode: widgetInfo.versionCode
			}
			console.log("AppId:", data.appid);
			console.log("appVersion:", data.appVersion);
			console.log("wgtVersion:", data.wgtVersion);
			console.log("wgtVersionCode:", data.wgtVersionCode);

			if(data.appid == 'HBuilder'){
				data.appid = '__UNI__AF0C14D'
			}
			getLatestVersion(data.appid).then((res) => {
				if(!res.data) {
					uni.showToast({
						title: '查不到版本!',
						duration: 2000
					});
					return;
				}
				console.log('后台获取最新的版本信息')
				console.log('版本号:', res.data.versionNumber)
				console.log('版本code', res.data.autoIncrementVersionNumber)
				let r = {}
				if (data.wgtVersionCode < res.data.autoIncrementVersionNumber) {
					console.log('当前版本小于最新版')
					r.result = {
						"code": res.code,
						"message": "整包更新",
						"_id": res.data.id,
						//是否静默更新
						"is_silently": false,
						//是否强制更新
						"is_mandatory": false,
						"appid": res.data.appid,
						"name": res.data.name,
						"title": res.data.title,
						"contents": res.data.content,
						"platform": ["Android"],
						"version": res.data.versionNumber,
						"url": baseUrl + res.data.downloadUrl,
						"stable_publish": true,
						"type": "native_app",
						"uni_platform": "android"
					}
				} else {
					console.log('已经是最新版无需更新!')
					uni.showToast({
						title: '已经是最新版!',
						duration: 2000
					});
				}
				resolve(r)
			}).catch(err => {
				console.log('获取版本出错!')
				console.log(err)
				reject(err)
			})

			// uniCloud.callFunction({
			// 	name: 'uni-upgrade-center',
			// 	data,
			// 	success: (e) => {
			// 		console.log("e: ", e);
			// 		resolve(e)
			// 	},
			// 	fail: (error) => {
			// 		reject(error)
			// 	}
			// })
		})
	})
	// #endif
	// #ifndef APP-PLUS
	return new Promise((resolve, reject) => {
		reject({
			message: '请在App中使用'
		})
	})
	// #endif
}

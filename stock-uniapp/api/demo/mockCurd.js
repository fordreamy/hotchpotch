import request from '@/utils/request'

// 查询
export function dataList() {
  return request({
    url: '/eye/app/mockCurd/list',
    method: 'get'
  })
}

// 新增菜单
export function addData(data) {
  return request({
    url: '/eye/app/mockCurd/add',
    method: 'post',
	data: data
  })
}

// 修改菜单
export function updateData(data) {
  return request({
    url: '/eye/app/mockCurd/update',
    method: 'put',
	data: data
  })
}

// 修改菜单
export function getDetail(id) {
  return request({
    url: '/eye/app/mockCurd/detail/'+id,
    method: 'get'
  })
}

// 删除菜单
export function removeById(id) {
  return request({
    url: '/eye/app/mockCurd/delete/'+id,
    method: 'delete'
  })
}

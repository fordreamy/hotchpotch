import request from '@/utils/request'

// 查询最新版本相关信息
export function getLatestVersion(appId) {
	return request({
		url: '/eye/app/update/getLatestVersion?appId=' + appId,
		method: 'get'
	})
}

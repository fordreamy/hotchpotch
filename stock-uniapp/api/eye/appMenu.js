import request from '@/utils/request'

// 查询app动态菜单
export function appMenuList() {
  return request({
    url: '/eye/app/menu/appMenuList',
    method: 'get'
  })
}

// 查询无分页菜单
export function noPagingList() {
  return request({
    url: '/eye/app/menu/noPagingList',
    method: 'get'
  })
}

// 新增菜单
export function addAppMenuList(data) {
  return request({
    url: '/eye/app/menu',
    method: 'post',
	data: data
  })
}

// 修改菜单
export function updateAppMenuList(data) {
  return request({
    url: '/eye/app/menu',
    method: 'put',
	data: data
  })
}

// 修改菜单
export function getDetail(id) {
  return request({
    url: '/eye/app/menu/'+id,
    method: 'get'
  })
}

// 删除菜单
export function removeById(id) {
  return request({
    url: '/eye/app/menu/'+id,
    method: 'delete'
  })
}

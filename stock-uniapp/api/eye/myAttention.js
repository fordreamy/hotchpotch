import request from '@/utils/request'

// 查询股票分组
export function getStockGroup() {
  return request({
    url: '/eye/stockGroup/listOption',
    method: 'get'
  })
}

// 查询我的持仓列表
export function listMyHolding(query) {
  return request({
    url: '/eye/myAttention/list',
    method: 'get',
    params: query
  })
}

// 查询我的关注详细
export function getMyAttention(id) {
  return request({
    url: '/eye/myAttention/' + id,
    method: 'get'
  })
}

// 新增我的关注
export function addMyAttention(data) {
  return request({
    url: '/eye/myAttention',
    method: 'post',
    data: data
  })
}

// 修改我的关注
export function updateMyAttention(data) {
  return request({
    url: '/eye/myAttention',
    method: 'put',
    data: data
  })
}

// 删除我的关注
export function delMyAttention(id) {
  return request({
    url: '/eye/myAttention/' + id,
    method: 'delete'
  })
}

// 批量修改我的关注
export function batchUpdateMyAttention(data) {
  return request({
    url: '/eye/myAttention/batchUpdateMyAttention',
    method: 'put',
    data: data
  })
}

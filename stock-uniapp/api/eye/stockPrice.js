import request from '@/utils/request'

// 获取某股票的历史价格
export function getHistoryPriceByStockCode(stockCode) {
  return request({
    url: '/eye/stockPrice/getHistoryPriceByStockCode?stockCode=' + stockCode,
    method: 'get'
  })
}

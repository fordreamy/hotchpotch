import Mock from 'mockjs'

console.log('mock.........................')

Mock.mock(/.*\/mockCurd\/list.*/, (e) => {
	console.log('Mock正则拦截')
	console.log(e)
	return {
		"success": true,
		"data": {
			"code": 0,
			"errCode": 0,
			"message": "",
			"errMsg": "",
			"systemInfo": [],
			"affectedDocs": 1,
			"data": [{
				"_id": "63e9f93ff43e606dd8bf83cf",
				"permission_id": "OP001",
				"permission_name": "权限001",
				"comment": "",
				"create_date": 1676278079122
			},{
				"_id": "63e9f93ff43e606dd8bf83cd",
				"permission_id": "OP002",
				"permission_name": "权限002",
				"comment": "",
				"create_date": 1676278079122
			}],
			"count": 1
		}
	}
})

Mock.mock(/.*\/mockCurd\/detail.*/, (e) => {
	console.log('Mock正则拦截')
	console.log(e)
	return {
		"success": true,
		"data": {
			"code": 0,
			"errCode": 0,
			"message": "",
			"errMsg": "",
			"systemInfo": [],
			"affectedDocs": 1,
			"data": [{
				"_id": "63e9f93ff43e606dd8bf83cf",
				"permission_id": "OP001",
				"permission_name": "权限001",
				"comment": ""
			}]
		}
	}
})

Mock.mock(/.*\/mockCurd\/delete.*/, (e) => {
	console.log('Mock正则拦截')
	console.log(e)
	return {
		"success": true,
		"data": {}
	}
})

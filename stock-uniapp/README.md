# UniAPP平台打包发布安卓apk应用

Android平台打包发布apk应用，需要使用数字证书（.keystore文件）进行签名，用于表明开发者身份。可以使用JRE环境中的keytool命令生成。以下是windows平台生成证书的方法。

# 生成签名证书
## 使用keytool -genkey命令生成证书：
```
	keytool -genkey -alias testalias -keyalg RSA -keysize 2048 -validity 36500 -keystore test.keystore
```

+ testalias是证书别名，可修改为自己想设置的字符，建议使用英文字母和数字
+ test.keystore是证书文件名称，可修改为自己想设置的文件名称，也可以指定完整文件路径
+ 36500是证书的有效期，表示100年有效期，单位天，建议时间设置长一点，避免证书过期

回车后会提示：
```
Enter keystore password:  //输入证书文件密码，输入完成回车  
Re-enter new password:   //再次输入证书文件密码，输入完成回车  
What is your first and last name?  
  [Unknown]:  //输入名字和姓氏，输入完成回车  
What is the name of your organizational unit?  
  [Unknown]:  //输入组织单位名称，输入完成回车  
What is the name of your organization?  
  [Unknown]:  //输入组织名称，输入完成回车  
What is the name of your City or Locality?  
  [Unknown]:  //输入城市或区域名称，输入完成回车  
What is the name of your State or Province?  
  [Unknown]:  //输入省/市/自治区名称，输入完成回车  
What is the two-letter country code for this unit?  
  [Unknown]:  //输入国家/地区代号（两个字母），中国为CN，输入完成回车  
Is CN=XX, OU=XX, O=XX, L=XX, ST=XX, C=XX correct?  
  [no]:  //确认上面输入的内容是否正确，输入y，回车  

Enter key password for <testalias>  
        (RETURN if same as keystore password):  //确认证书密码与证书文件密码一样（HBuilder|HBuilderX要求这两个密码一致），直接回车就可以
```

以上命令运行完成后就会生成证书，路径为"执行命令行路径\test.keystore"。

# 查看证书
可以使用以下命令查看：
```
keytool -list -v -keystore test.keystore  
Enter keystore password: //输入密码，回车
```

# 云打包：keytool -list -v -keystore test.keystore  
Enter keystore password: //输入密码，回车
1. 通过HBuilderX->发行->原生App-云打包
2. Android包名用默认即可，选"使用自有证书"，填证书别名"testalias"，证书私钥密码"admin123"，证书文件导入"test.keystore"

## Mock使用
### 安装
`npm install mockjs`
### main.js引入
`import '@/api/mock/index.js' 或者 require('@/api/mock/index.js')`
### 去掉Mock
main.js中注释掉引用即可

#KLineChart使用
1. 默认值:KDJ(9,3,3) MACD(12,26,9) BOLL(20,2) SAR(2,2,20) BBI(3,6,12,24) DMI(14,6) RSI(6,12,24)






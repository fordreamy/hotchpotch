import Vue from 'vue'
import App from './App'

import store from './store' // store
import plugins from './plugins' // plugins
import './permission' // permission
Vue.use(plugins)

// 引入mock
import '@/api/mock/index.js'

// 引入国际化
const lang = uni.getLocale()
import messages from './i18n/index.js'
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
// 通过选项创建 VueI18n 实例
const i18n = new VueI18n({
  locale: lang, // 设置地区
  messages, // 设置地区信息
})

Vue.config.productionTip = false
Vue.prototype.$store = store

App.mpType = 'app'

const app = new Vue({
	i18n,
  ...App
})

app.$mount()

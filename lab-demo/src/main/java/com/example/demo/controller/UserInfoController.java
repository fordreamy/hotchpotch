package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.stat.DruidStatManagerFacade;

@Controller
@RequestMapping("/user")
public class UserInfoController {

    Log log = LogFactory.getLog(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/list")
    @ResponseBody
    public List<Map<String, Object>> list() {
        log.info(Thread.currentThread().getId());
        log.info(Thread.currentThread().getName());
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String sql = "select * from user";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        log.info(list);
        log.info(jdbcTemplate.queryForMap("select count(*) from user"));
        return list;
    }

    @GetMapping("/druid/stat")
    @ResponseBody
    public Object druidStat() {
        // DruidStatManagerFacade#getDataSourceStatDataList 该方法可以获取所有数据源的监控数据，除此之外 DruidStatManagerFacade 还提供了一些其他方法，你可以按需选择使用。
        return DruidStatManagerFacade.getInstance().getDataSourceStatDataList();
    }

    public static void main(String[] args) {
        String s = "1. 在【统计周期:{统计周期}】内收到的人员着装违规事件数大于等于【违规次数:{预警线}】，则统计一次违规，并记录一次【扣分:{事件扣分}】。<br/>\n" +
                "2. 违规每天0点重新开始统计，每【统计周期:{统计周期}】为一个时段，每个时段内最多只统计一次违规。<br/>\n" +
                "3. 违规只在每个时段内统计，不做跨时段的统计。一天内不同时段内多次违规，将统计多次违规。";
        System.out.println(s);
        System.out.println(s.replaceAll("\\{统计周期\\}", "2"));
        System.out.println(s.replaceAll("\\{预警线\\}", "2"));
        System.out.println(s.replaceAll("\\{事件扣分\\}", "2"));
    }

}
